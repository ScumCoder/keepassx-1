<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Current Translation: Russian&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Автор:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="unfinished">Дмитрий Функ</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="unfinished">dmitry.funk@gmail.com</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation type="unfinished">Комманда разработчиков</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="46"/>
        <source>Developer, Project Admin</source>
        <translation type="unfinished">Разработчик, руководитель проекта</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Web Designer</source>
        <translation type="unfinished">Web дизайнер</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Thanks To</source>
        <translation type="unfinished">Благодарность</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Patches for better MacOS X support</source>
        <translation type="unfinished">Исправления для улучшения поддержки MacOS X</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">www.outofhanwell.com</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Main Application Icon</source>
        <translation type="unfinished">Значок программы</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Various fixes and improvements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="65"/>
        <source>Error</source>
        <translation type="unfinished">Ошибка</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="66"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="unfinished">Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="unfinished">Убедитесь что программа установлена корректно.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно открыть файл &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="60"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="199"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="182"/>
        <source>Translation</source>
        <translation>Перевод</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Кроссплатформенный менеджер паролей&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="165"/>
        <source>Credits</source>
        <translation>Благодарности</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="137"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="144"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="84"/>
        <source>AppName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="102"/>
        <source>AppFunc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="151"/>
        <source>Copyright (C) 2005 - 2008 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation type="unfinished">Название:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation type="unfinished">Обзор...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="68"/>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="95"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="133"/>
        <source>Auto-Type string contains invalid characters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation type="unfinished">Название</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="89"/>
        <source>Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CAboutDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="obsolete">Убедитесь что программа установлена корректно.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно открыть файл &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Current Translation: Russian&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Автор:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">dmitry.funk@gmail.com</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Дмитрий Функ</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Team</source>
        <translation type="obsolete">Комманда разработчиков</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Developer, Project Admin</source>
        <translation type="obsolete">Разработчик, руководитель проекта</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Web Designer</source>
        <translation type="obsolete">Web дизайнер</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Thanks To</source>
        <translation type="obsolete">Благодарность</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Patches for better MacOS X support</source>
        <translation type="obsolete">Исправления для улучшения поддержки MacOS X</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">www.outofhanwell.com</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Information on how to translate KeePassX can be found under:
http://keepassx.sourceforge.net/</source>
        <translation type="obsolete">Информацию по переводу KeePassX можно найти на:
http://keepassx.sourceforge.net/</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Main Application Icon</source>
        <translation type="obsolete">Значок программы</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 бит   (по умолчанию)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 бит</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="71"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; не корректное целое значение.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="176"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="176"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Пароль и повтор пароля не эквивалентны.
Проверьте введённые данные.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="330"/>
        <source>Save Attachment...</source>
        <translation>Сохранить вложение...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Overwrite?</source>
        <translation type="obsolete">Перезаписать?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not remove old file.</source>
        <translation type="obsolete">Невозможно удалить старый файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not create new file.</source>
        <translation type="obsolete">Невозможно создать новый файл.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>Error while writing the file.</source>
        <translation>Ошибка записи в файл.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>Delete Attachment?</source>
        <translation>Удалить вложение?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>No, Cancel</source>
        <translation>Нет, Отмена</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="136"/>
        <source>Edit Entry</source>
        <translation type="unfinished">Изменить запись</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="337"/>
        <source>Could not open file.</source>
        <translation>Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="100"/>
        <source>%1 Bit</source>
        <translation>%1 бит</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="300"/>
        <source>Add Attachment...</source>
        <translation>Добавить вложение...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="327"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="60"/>
        <source>Today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="62"/>
        <source>1 Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>2 Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="64"/>
        <source>3 Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>1 Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="68"/>
        <source>6 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>1 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="72"/>
        <source>Calendar...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="255"/>
        <source>[Untitled Entry]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="136"/>
        <source>New Entry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation type="unfinished">Предупреждение</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation>Необходимо ввести более одного символа</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open &apos;/dev/random&apos; or &apos;/dev/urandom&apos;.</source>
        <translation type="obsolete">Невозможно открыть &apos;/dev/random&apos; или &apos;/dev/urandom&apos;.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="76"/>
        <source>Password Generator</source>
        <translation>Генератор паролей</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1 Bit</source>
        <translation type="obsolete">%1 бит</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="252"/>
        <source>%1 Bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a Password.</source>
        <translation type="obsolete">Введите пароль.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please choose a key file.</source>
        <translation type="obsolete">Выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">Введите пароль или выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Database Key</source>
        <translation type="obsolete">Ключ базы паролей</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select a Key File</source>
        <translation type="obsolete">Выбор файл-ключа</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>*.key</source>
        <translation type="obsolete">*.key</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Warning</source>
        <translation type="obsolete">Внимание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">Пароль и повтор пароля не эквивалентны.
Проверьте введённые данные.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">Введите пароль или выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with the name &apos;pwsafe.key&apos; already exisits in the given directory.
Do you want to replace it?</source>
        <translation type="obsolete">Файл с именем &apos;pwsafe.key&apos; уже существует в данной директории.
Заменить его?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The exisiting file is not writable.</source>
        <translation type="obsolete">Сужествующий файл незаписываем.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with the this name already exisits.
Do you want to replace it?</source>
        <translation type="obsolete">Файл с таким именем уже существует.
Заменить его?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Выбранный файл-ключ или директория нечитаемы.
Проверьте права доступа.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>CSearchDlg</name>
    <message>
        <location filename="" line="0"/>
        <source>Notice</source>
        <translation type="obsolete">Предупреждение</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a search string.</source>
        <translation type="obsolete">Введите строку для поиска.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search</source>
        <translation type="obsolete">Поиск</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="92"/>
        <source>Add Icons...</source>
        <translation>Добавить значок...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Images (%1)</source>
        <translation>Изображение (%1)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: Файл не может быть загружен.
</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="97"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Заменить...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">Ошибка при загрузке значков:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="97"/>
        <source>An error occured while loading the icon.</source>
        <translation>Ошибка при загрузке значка.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation type="unfinished">Выбрать</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="67"/>
        <source>An error occured while loading the icon(s):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="293"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="298"/>
        <source>Select a directory...</source>
        <translation>Выберите директорию...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="305"/>
        <source>Select an executable...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation type="unfinished">Название</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation type="unfinished">Пароль</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="26"/>
        <source>Rich Text Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="40"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="127"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="59"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="62"/>
        <source>I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="72"/>
        <source>Underlined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="75"/>
        <source>U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="85"/>
        <source>Left-Aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="88"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="98"/>
        <source>Centered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="140"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="111"/>
        <source>Right-Aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="114"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="124"/>
        <source>Justified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="137"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="147"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="154"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="159"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="164"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="169"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="174"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="179"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="184"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="189"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="194"/>
        <source>16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="199"/>
        <source>18</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="204"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="209"/>
        <source>22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="214"/>
        <source>24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="219"/>
        <source>26</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="224"/>
        <source>28</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="229"/>
        <source>36</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="234"/>
        <source>42</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="239"/>
        <source>78</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>Templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="250"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="282"/>
        <source>HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation type="unfinished">Никогда</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation type="unfinished">Н&amp;астройки базы паролей...</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation type="unfinished">Шифрование</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation type="unfinished">Алгоритм:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="250"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="251"/>
        <source>Title</source>
        <translation type="unfinished">Название</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="252"/>
        <source>Username</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="253"/>
        <source>Password</source>
        <translation type="unfinished">Пароль</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="254"/>
        <source>URL</source>
        <translation type="unfinished">Ссылка</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="255"/>
        <source>Creation</source>
        <translation type="unfinished">Создание</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="256"/>
        <source>Last Access</source>
        <translation type="unfinished">Последний доступ</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="257"/>
        <source>Last Modification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Comment</source>
        <translation type="unfinished">Комментарий</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Изменить запись</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="146"/>
        <source>Username:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="211"/>
        <source>Password Repet.:</source>
        <translation>Повтор пароля:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="133"/>
        <source>Title:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="179"/>
        <source>URL:</source>
        <translation>Ссылка:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="224"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="107"/>
        <source>Quality:</source>
        <translation>Качество:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="159"/>
        <source>Comment:</source>
        <translation>Комментарий:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="237"/>
        <source>Expires:</source>
        <translation>Окончание:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="192"/>
        <source>Group:</source>
        <translation>Группа:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="166"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="438"/>
        <source>Icon:</source>
        <translation>Значок:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="58"/>
        <source>Ge&amp;n.</source>
        <translation>&amp;Генерация.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="356"/>
        <source>Never</source>
        <translation>Никогда</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="120"/>
        <source>Attachment:</source>
        <translation>Вложение:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="395"/>
        <source>%1 Bit</source>
        <translation>%1 бит</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Параметры группы</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="44"/>
        <source>Title:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="37"/>
        <source>Icon:</source>
        <translation>Значок:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="70"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation type="unfinished">Название</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation type="unfinished">Имя</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Невозможно открыть файл  (FileError=%1)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="51"/>
        <source>No error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="52"/>
        <source>An error occurred while reading from the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="53"/>
        <source>An error occurred while writing to the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="54"/>
        <source>A fatal error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="55"/>
        <source>An resource error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="56"/>
        <source>The file could not be opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="57"/>
        <source>The operation was aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="58"/>
        <source>A timeout occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="59"/>
        <source>An unspecified error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="60"/>
        <source>The file could not be removed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="61"/>
        <source>The file could not be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>The position in the file could not be changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>The file could not be resized.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>The file could not be accessed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>The file could not be copied.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="135"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="112"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="306"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="89"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Генератор паролей</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Accep&amp;t</source>
        <translation type="obsolete">&amp;Принять</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="372"/>
        <source>Generate</source>
        <translation>Генерировать</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="362"/>
        <source>New Password:</source>
        <translation>Новый пароль:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="276"/>
        <source>Quality:</source>
        <translation>Качество:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="38"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="132"/>
        <source>&amp;Upper Letters</source>
        <translation>&amp;Прописные буквы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="86"/>
        <source>&amp;Lower Letters</source>
        <translation>&amp;Строчные буквы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="109"/>
        <source>&amp;Numbers</source>
        <translation>&amp;Цифры</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="152"/>
        <source>&amp;Special Characters</source>
        <translation>&amp;Специальные символы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation>Знак минуса</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation>По&amp;дчёркивание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>h&amp;igher ANSI-Characters</source>
        <translation type="obsolete">ANSI-символы &amp;второй половины таблицы</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="179"/>
        <source>Use &amp;only following characters:</source>
        <translation>&amp;Только следующие символы:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="182"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="240"/>
        <source>Length:</source>
        <translation>Длинна:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Use &quot;/dev/rando&amp;m&quot;</source>
        <translation type="obsolete">Использовать  &quot;/dev/rando&amp;m&quot;</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="47"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>Только следующие &amp;группы символов:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation>&amp;Пробелы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="125"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="303"/>
        <source>Enable entropy collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="332"/>
        <source>Collect only once per session</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation type="unfinished">Неверный файл XML.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation type="unfinished">Документ не содержит данных.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="27"/>
        <source>PwManager Files (*.pwm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="112"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="38"/>
        <source>File is empty.</source>
        <translation type="unfinished">Файл пуст.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="46"/>
        <source>File is no valid PwManager file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="49"/>
        <source>Unsupported file version.</source>
        <translation type="unfinished">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="55"/>
        <source>Unsupported hash algorithm.</source>
        <translation type="unfinished">Неподдерживаемы хэш алгоритм.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="58"/>
        <source>Unsupported encryption algorithm.</source>
        <translation type="unfinished">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="65"/>
        <source>Compressed files are not supported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="90"/>
        <source>Wrong password.</source>
        <translation type="unfinished">Неверный пароль.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="105"/>
        <source>File is damaged (hash test failed).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="112"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="434"/>
        <source>Could not open file.</source>
        <translation type="unfinished">Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="445"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="462"/>
        <source>Wrong Signature</source>
        <translation type="unfinished">Неверная сигнатура</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="467"/>
        <source>Unsupported File Version.</source>
        <translation type="unfinished">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="476"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="unfinished">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="505"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="unfinished">Расшифровка прервана.
Ключ неверен или файл повреждён.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="524"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="609"/>
        <source>Invalid group tree.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="794"/>
        <source>Key file is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1133"/>
        <source>The database must contain at least one group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1147"/>
        <source>Could not open file for writing.</source>
        <translation type="unfinished">Невозможно открыть файл для записи.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="603"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="992"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1000"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1006"/>
        <source>MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1011"/>
        <source>GiB</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="409"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="411"/>
        <source>Username</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="413"/>
        <source>URL</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="415"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="417"/>
        <source>Comments</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="419"/>
        <source>Expires</source>
        <translation>Окончание</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="421"/>
        <source>Creation</source>
        <translation>Создание</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="423"/>
        <source>Last Change</source>
        <translation>Последнее изменение</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="425"/>
        <source>Last Access</source>
        <translation>Последний доступ</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="427"/>
        <source>Attachment</source>
        <translation>Вложение</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1 items</source>
        <translation type="obsolete">%1 пунктов</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="429"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>Error</source>
        <translation type="unfinished">Ошибка</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="144"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="146"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="54"/>
        <source>Search Results</source>
        <translation>Результаты поиска</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Groups</source>
        <translation type="obsolete">Группы</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="85"/>
        <source>Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="86"/>
        <source>Are you sure you want to delete this group, all it&apos;s child groups and all their entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="345"/>
        <source>Ctrl+N</source>
        <translation type="unfinished">Ctrl+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="346"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="351"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="353"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="347"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="453"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="470"/>
        <source>Save modified file?</source>
        <translation>Сохранить изменения файла?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation>Текущий файл был изменён. Хотите
сохранить изменения?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX - %1</source>
        <translation type="obsolete">KeePassX - %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;B&gt;Group: &lt;/B&gt;%1  &lt;B&gt;Title: &lt;/B&gt;%2  &lt;B&gt;Username: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Password: &lt;/B&gt;%5  &lt;B&gt;Creation: &lt;/B&gt;%6  &lt;B&gt;Last Change: &lt;/B&gt;%7  &lt;B&gt;LastAccess: &lt;/B&gt;%8  &lt;B&gt;Expires: &lt;/B&gt;%9</source>
        <translation type="obsolete">&lt;B&gt;Группа: &lt;/B&gt;%1  &lt;B&gt;Название: &lt;/B&gt;%2  &lt;B&gt;Имя: &lt;/B&gt;%3  &lt;B&gt;Ссылка: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Пароль: &lt;/B&gt;%5  &lt;B&gt;Создано: &lt;/B&gt;%6  &lt;B&gt;Изменено: &lt;/B&gt;%7  &lt;B&gt;Доступ: &lt;/B&gt;%8  &lt;B&gt;Окончание: &lt;/B&gt;%9</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="788"/>
        <source>Clone Entry</source>
        <translation>Дублировать запись</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="790"/>
        <source>Delete Entry</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="802"/>
        <source>Clone Entries</source>
        <translation>Дублировать записи</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="804"/>
        <source>Delete Entries</source>
        <translation>Удалить записи</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">Невозможно сохранить файл.
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Database As...</source>
        <translation type="obsolete">Сохранить базу паролей как...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="445"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>[new]</source>
        <translation type="obsolete">[новый]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="544"/>
        <source>Open Database...</source>
        <translation>Открыть базу паролей...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="416"/>
        <source>Loading Database...</source>
        <translation>Загрузка базы паролей...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="430"/>
        <source>Loading Failed</source>
        <translation>Ошибка загрузки</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Export To...</source>
        <translation type="obsolete">Экспорт в...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in Import_PwManager::importFile()()</source>
        <translation type="obsolete">Неизвестная ошибка в Import_PwManager::importFile()()</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in Import_KWalletXml::importFile()</source>
        <translation type="obsolete">Неизвестная ошибка в Import_KWalletXml::importFile()</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in PwDatabase::openDatabase()</source>
        <translation type="obsolete">Неизвестная ошибка в PwDatabase::openDatabase()</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">Отобразить панель инструментов</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="432"/>
        <source>Unknown error while loading database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>Save Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="683"/>
        <source>1 Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="685"/>
        <source>%1 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="691"/>
        <source>1 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="693"/>
        <source>%1 Years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="699"/>
        <source>1 Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="701"/>
        <source>%1 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="705"/>
        <source>less than 1 day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1222"/>
        <source>Locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1237"/>
        <source>Unlocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="349"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="350"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="453"/>
        <source>The database file does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="883"/>
        <source>new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="669"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1223"/>
        <source>Un&amp;lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1238"/>
        <source>&amp;Lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="434"/>
        <source>The following error occured while opening the database:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="833"/>
        <source>File could not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="295"/>
        <source>Show &amp;Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="136"/>
        <source>Error</source>
        <translation type="unfinished">Ошибка</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="138"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="unfinished">Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="138"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Columns</source>
        <translation type="obsolete">Столбцы</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>PwManager File (*.pwm)</source>
        <translation type="obsolete">Файл PwManager (*.pwm)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KWallet XML-File (*.xml)</source>
        <translation type="obsolete">XML-файл KWallet (*.xml)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add New Group...</source>
        <translation type="obsolete">Добавить новую группу...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Edit Group...</source>
        <translation type="obsolete">Изменить группу...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete Group</source>
        <translation type="obsolete">Удалить группу</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">Скопировать &amp;пароль в буфер обмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">Скопировать имя в буфер обмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Open URL</source>
        <translation type="obsolete">Открыть &amp;ссылку</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Attachment As...</source>
        <translation type="obsolete">Сохранить вложение как...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add New Entry...</source>
        <translation type="obsolete">Добавить новую запись...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">Просмотр/правка записи...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete Entry</source>
        <translation type="obsolete">Удалить запись</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clone Entry</source>
        <translation type="obsolete">Дублировать запись</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search In Database...</source>
        <translation type="obsolete">Поиск в базе паролей...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search in this group...</source>
        <translation type="obsolete">Поиск в текущей группе...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">Отобразить панель инструментов</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Entry Details</source>
        <translation type="obsolete">Отобразить данные записи</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hide Usernames</source>
        <translation type="obsolete">Скрыть имена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hide Passwords</source>
        <translation type="obsolete">Скрыть пароли</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Title</source>
        <translation type="obsolete">Название</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Username</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>URL</source>
        <translation type="obsolete">Ссылка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password</source>
        <translation type="obsolete">Пароль</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Comment</source>
        <translation type="obsolete">Комментарий</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Expires</source>
        <translation type="obsolete">Окончание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Creation</source>
        <translation type="obsolete">Создание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last Change</source>
        <translation type="obsolete">Последнее изменение</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last Access</source>
        <translation type="obsolete">Последний доступ</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Attachment</source>
        <translation type="obsolete">Вложение</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Statusbar</source>
        <translation type="obsolete">Отобразить панель статуса</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Plain Text (*.txt)</source>
        <translation type="obsolete">Plain Text (*.txt)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="481"/>
        <source>Hide</source>
        <translation>Скрыть</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Perform AutoType</source>
        <translation type="obsolete">Применить автоввод</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Type Here</source>
        <translation type="obsolete">Ввести сюда</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">Размер значков панели инструментов</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>16x16</source>
        <translation type="obsolete">16x16</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>22x22</source>
        <translation type="obsolete">22x22</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>28x28</source>
        <translation type="obsolete">28x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="186"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="137"/>
        <source>&amp;Import from...</source>
        <translation>&amp;Импортировать из...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="132"/>
        <source>&amp;Export to...</source>
        <translation>&amp;Экспортировать в...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="164"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="225"/>
        <source>E&amp;xtras</source>
        <translation>&amp;Дополнительно</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="120"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="525"/>
        <source>&amp;New Database...</source>
        <translation type="unfinished">Со&amp;здать базу паролей...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="246"/>
        <source>&amp;Open Database...</source>
        <translation>&amp;Открыть базу паролей...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="251"/>
        <source>&amp;Close Database</source>
        <translation>&amp;Закрыть базу паролей</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="256"/>
        <source>&amp;Save Database</source>
        <translation>&amp;Сохранить базу паролей</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="261"/>
        <source>Save Database &amp;As...</source>
        <translation>Сохранить базу паролей &amp;как...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="266"/>
        <source>&amp;Database Settings...</source>
        <translation>Н&amp;астройки базы паролей...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="271"/>
        <source>Change &amp;Master Key...</source>
        <translation>&amp;Изменить основной пароль...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>E&amp;xit</source>
        <translation type="obsolete">В&amp;ыход</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="455"/>
        <source>&amp;Settings...</source>
        <translation>&amp;Настройка...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="460"/>
        <source>&amp;About...</source>
        <translation>&amp;О программе...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="473"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>&amp;Руководство &quot;KeePassX&quot;...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="515"/>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="520"/>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="551"/>
        <source>Recycle Bin...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="47"/>
        <source>Groups</source>
        <translation type="unfinished">Группы</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="276"/>
        <source>&amp;Lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="142"/>
        <source>&amp;Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="190"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="198"/>
        <source>&amp;Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="241"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="281"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Add New Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="291"/>
        <source>&amp;Edit Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="296"/>
        <source>&amp;Delete Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="301"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="306"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="311"/>
        <source>&amp;Open URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="316"/>
        <source>&amp;Save Attachment As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="321"/>
        <source>Add &amp;New Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="326"/>
        <source>&amp;View/Edit Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="331"/>
        <source>De&amp;lete Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="336"/>
        <source>&amp;Clone Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="341"/>
        <source>Search &amp;in Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="346"/>
        <source>Search in this &amp;Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="354"/>
        <source>Show &amp;Entry Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="362"/>
        <source>Hide &amp;Usernames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="370"/>
        <source>Hide &amp;Passwords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="378"/>
        <source>&amp;Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="386"/>
        <source>User&amp;name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="394"/>
        <source>&amp;URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="402"/>
        <source>&amp;Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="410"/>
        <source>&amp;Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="418"/>
        <source>E&amp;xpires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="426"/>
        <source>C&amp;reation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="434"/>
        <source>&amp;Last Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="442"/>
        <source>Last &amp;Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="450"/>
        <source>A&amp;ttachment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="468"/>
        <source>Show &amp;Statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="486"/>
        <source>&amp;Perform AutoType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="494"/>
        <source>&amp;16x16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="502"/>
        <source>&amp;22x22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="510"/>
        <source>2&amp;8x28</source>
        <translation type="unfinished">28x28 {2&amp;8x?}</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="530"/>
        <source>&amp;Password Generator...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="541"/>
        <source>&amp;Group (search results only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="546"/>
        <source>Show &amp;Expired Entries...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="559"/>
        <source>&amp;Add Bookmark...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="564"/>
        <source>Bookmark &amp;this Database...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="31"/>
        <source>Enter Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="34"/>
        <source>Set Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="37"/>
        <source>Change Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="41"/>
        <source>Database Key</source>
        <translation type="unfinished">Ключ базы паролей</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="125"/>
        <source>Last File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="171"/>
        <source>Select a Key File</source>
        <translation type="unfinished">Выбор файл-ключа</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="335"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="335"/>
        <source>Key Files (*.key)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="199"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="unfinished">Введите пароль или выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="204"/>
        <source>Please enter a Password.</source>
        <translation type="unfinished">Введите пароль.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="209"/>
        <source>Please provide a key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="255"/>
        <source>%1:
No such file or directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="221"/>
        <source>The selected key file or directory is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="240"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="246"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="259"/>
        <source>%1:
File is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="333"/>
        <source>Create Key File...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="" line="0"/>
        <source>TextLabel</source>
        <translation type="obsolete">ТекстЛабел</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Введите пароль и/или выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="138"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file or directory:</source>
        <translation type="obsolete">Папка с файл-ключом:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="186"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Обзор...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="189"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">Использовать пароль И файл-ключ</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Exit</source>
        <translation type="obsolete">Выход</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password Repet.:</source>
        <translation type="obsolete">Повтор пароля:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="158"/>
        <source>Key File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="198"/>
        <source>Generate Key File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="222"/>
        <source>Please repeat your password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="234"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="261"/>
        <source>Passwords are not equal.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PwDatabase</name>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Error</source>
        <translation type="obsolete">Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong Signature</source>
        <translation type="obsolete">Неверная сигнатура</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open key file.</source>
        <translation type="obsolete">Невозможно открыть файл-ключ.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file could not be written.</source>
        <translation type="obsolete">Файл-ключ не записываем.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Невозможно открыть файл для записи.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Расшифровка прервана.
Ключ неверен или файл повреждён.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="0"/>
        <source>Warning</source>
        <translation type="obsolete">Внимание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File not found.</source>
        <translation type="obsolete">Файл не найден.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported file version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported hash algorithm.</source>
        <translation type="obsolete">Неподдерживаемы хэш алгоритм.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported encryption algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong password.</source>
        <translation type="obsolete">Неверный пароль.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File is empty.</source>
        <translation type="obsolete">Файл пуст.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid XML file.</source>
        <translation type="obsolete">Неверный файл XML.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Document does not contain data.</source>
        <translation type="obsolete">Документ не содержит данных.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Warning:</source>
        <translation type="obsolete">Внимание:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid RGB color value.
</source>
        <translation type="obsolete">Неверное значение цвета RGB.
</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Never</source>
        <translation type="obsolete">Никогда</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="120"/>
        <source>Could not locate library file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation type="unfinished">Поиск</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="90"/>
        <source>A&amp;nhang</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Поиск...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="180"/>
        <source>Search For:</source>
        <translation>Поиск:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="158"/>
        <source>Regular E&amp;xpression</source>
        <translation>Регулярное &amp;выражение</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="148"/>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;С учётом регистра</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="68"/>
        <source>Include:</source>
        <translation>Включая:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="130"/>
        <source>&amp;Titles</source>
        <translation>&amp;Название</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="120"/>
        <source>&amp;Usernames</source>
        <translation>&amp;Имя</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="110"/>
        <source>C&amp;omments</source>
        <translation>Ко&amp;мментарий</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="100"/>
        <source>U&amp;RLs</source>
        <translation>&amp;Ссылки</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="80"/>
        <source>Pass&amp;words</source>
        <translation>&amp;Пароли</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search</source>
        <translation type="obsolete">Поиск</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clo&amp;se</source>
        <translation type="obsolete">&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="168"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Включая подгруппы (рекурсивно)</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Выбор значка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">Добавить свой значок...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Pick</source>
        <translation type="obsolete">Выбрать</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>Alt+&#xd6;</source>
        <translation type="obsolete">Alt+Ö</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="728"/>
        <source>Clear clipboard after:</source>
        <translation>Буфер будет очищен через:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Seconds</source>
        <translation type="obsolete">секунд</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Sh&amp;ow passwords in plain text by default</source>
        <translation type="obsolete">&amp;Отображать пароли в текстовом виде по умолчанию</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="700"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation>&amp;Внешний вид</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="318"/>
        <source>Banner Color</source>
        <translation>Цвет банера</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="335"/>
        <source>Text Color:</source>
        <translation>Цвет текста:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="470"/>
        <source>Change...</source>
        <translation>Изменить...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="392"/>
        <source>Color 2:</source>
        <translation>Цвет 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="526"/>
        <source>C&amp;hange...</source>
        <translation>&amp;Изменить</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="529"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="542"/>
        <source>Color 1:</source>
        <translation>Цвет 1:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Expand group tree when opening a database</source>
        <translation type="obsolete">Раскрывать дерево групп при открытии базы паролей</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Other</source>
        <translation type="obsolete">&amp;Другой</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Browser Command:</source>
        <translation type="obsolete">Комманда браузера:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Securi&amp;ty</source>
        <translation type="obsolete">Безопасность</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="570"/>
        <source>Alternating Row Colors</source>
        <translation>Изменённые цвета столбцов</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1026"/>
        <source>Browse...</source>
        <translation>Обзор...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="160"/>
        <source>Remember last key type and location</source>
        <translation>Запоминать последний тип и положение ключа</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Mounting Root:</source>
        <translation type="obsolete">Корень монтирования:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="129"/>
        <source>Remember last opened file</source>
        <translation>Запоминать последний открытый файл</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="51"/>
        <source>The integration plugins provide features like usage of the native file dialogs and message boxes of the particular desktop environments.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="54"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="60"/>
        <source>Show system tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="118"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation type="unfinished"></translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="132"/>
        <source>Alt+Ö</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="236"/>
        <source>Save recent directories of file dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="614"/>
        <source>Group tree at start-up:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="621"/>
        <source>Restore last state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="628"/>
        <source>Expand all items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="635"/>
        <source>Do not expand any item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="673"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="697"/>
        <source>Edit Entry Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="864"/>
        <source>Desktop Integration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="895"/>
        <source>Plug-Ins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="904"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="911"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="918"/>
        <source>KDE 4 Desktop Integration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="933"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="953"/>
        <source>Configure...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="976"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="259"/>
        <source>Clear History Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="288"/>
        <source>Always ask before deleting entries or groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="582"/>
        <source>Customize Entry Detail View...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="827"/>
        <source>Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="833"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="843"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1045"/>
        <source>Auto-Type Fine Tuning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1057"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1106"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1070"/>
        <source>Pre-Gap:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1093"/>
        <source>Key Stroke Delay:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1103"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1019"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1009"/>
        <source>Media Root:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1035"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1038"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="88"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="190"/>
        <source>Start minimized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="220"/>
        <source>Start locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="766"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1121"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="985"/>
        <source>Custom Browser Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="997"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="281"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="688"/>
        <source>Show plain text passwords in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="707"/>
        <source>Database Key Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="804"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="775"/>
        <source>Lock database after inactivity of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1150"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="66"/>
        <source>Ctrl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="68"/>
        <source>Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="70"/>
        <source>Alt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="72"/>
        <source>AltGr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="74"/>
        <source>Win</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Введите пароль:</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
</context>
<context>
    <name>StandardDatabase</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong Signature</source>
        <translation type="obsolete">Неверная сигнатура</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Расшифровка прервана.
Ключ неверен или файл повреждён.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Невозможно открыть файл для записи.</translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>Title</source>
        <translation type="obsolete">Название</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Username</source>
        <translation type="obsolete">Имя</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <location filename="" line="0"/>
        <source>Database Settings</source>
        <translation type="obsolete">Н&amp;астройки базы паролей...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Encryption</source>
        <translation type="obsolete">Шифрование</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Algorithm:</source>
        <translation type="obsolete">Алгоритм:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ctrl+K</source>
        <translation type="obsolete">Ctrl+K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ctrl+C</source>
        <translation type="obsolete">Ctrl+C</translation>
    </message>
</context>
</TS>
