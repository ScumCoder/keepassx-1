<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>@default</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden. (FileError=%1)</translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Aktuelle Übersetzung: Deutsch&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Autor:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="unfinished">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="unfinished">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation type="unfinished">Team</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="46"/>
        <source>Developer, Project Admin</source>
        <translation type="unfinished">Entwickler und Projektadministrator</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Web Designer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Thanks To</source>
        <translation type="unfinished">Dank An</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Patches for better MacOS X support</source>
        <translation type="unfinished">Patches für bessere MacOS X Unterstützung</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Main Application Icon</source>
        <translation type="unfinished">Anwendungssymbol</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Various fixes and improvements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="65"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="66"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="unfinished">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="unfinished">Stellen Sie sicher, dass das Programm korrekt installiert wurde.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">Folgender Fehler ist aufgetreten:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="60"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Thanks To</source>
        <translation type="obsolete">Dank An</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="199"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="182"/>
        <source>Translation</source>
        <translation>Übersetzung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX steht unter der
General Public License (GPL) Version 2.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>tarek.saidi@arcor.de</source>
        <translation type="obsolete">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://keepass.berlios.de/</source>
        <translation type="obsolete">http://keepass.berlios.de/</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="165"/>
        <source>Credits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="137"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="144"/>
        <source>keepassx@gmail.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="84"/>
        <source>AppName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="102"/>
        <source>AppFunc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="151"/>
        <source>Copyright (C) 2005 - 2008 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation type="unfinished">Titel:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation type="unfinished">Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="68"/>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation>Es wurde mehr als eine &apos;Auto-Type:&apos;-Zeichenkette gefunden.
Erlaubt ist nur eine pro Eintrag.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="obsolete">Syntaxfehler in Auto-Type-Zeichenkette bei Zeichen %1
Öffnende Klammer &apos;{&apos; ohne Gegenstück gefunden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="95"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="133"/>
        <source>Auto-Type string contains invalid characters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation type="unfinished">Titel</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation type="unfinished">Benutzername</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="89"/>
        <source>Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CAboutDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="obsolete">Stellen Sie sicher, dass das Programm korrekt installiert wurde.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">Folgender Fehler ist aufgetreten:
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://keepass.berlios.de/index.php</source>
        <translation type="obsolete">http://keepass.berlios.de/index.php</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Aktuelle Übersetzung: Deutsch&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Autor:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSALTION_AUTHOR</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/translation-howto.html</source>
        <translation type="obsolete">Informationen wie Sie eine Übersetztung für KeePassX  erstellen können finden Sie unter:
http://keepass.berlios.de/translation-howto.html</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://www.outofhanwell.com/&lt;br&gt;Mac OS X Support</source>
        <translation type="obsolete">http://www.outofhanwell.com/&lt;br&gt;Mac OS X Unterstützung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/</source>
        <translation type="obsolete">Informationen über das Erstellen eine Übersetztung für KeePassX sind hier zu finden:
http://keepass.berlios.de</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Team</source>
        <translation type="obsolete">Team</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Developer, Project Admin</source>
        <translation type="obsolete">Entwickler und Projektadministrator</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Thanks To</source>
        <translation type="obsolete">Dank An</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Patches for better MacOS X support</source>
        <translation type="obsolete">Patches für bessere MacOS X Unterstützung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Main Application Icon</source>
        <translation type="obsolete">Anwendungssymbol</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 Bit   (Standard)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Bitte geben Sie die Zahl der Verschlüsselungsrunden an.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="71"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; ist kein gültiger Ganzzahlwert.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>Die Anzahl an Verschlüsselungsrunden muss mindestens Eins betragen.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="176"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="176"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Passwort und Passwortwiederholung stimmen nicht überein.
Bitte prüfen Sie Ihre Eingabe.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="330"/>
        <source>Save Attachment...</source>
        <translation>Anhang Speichern...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Overwrite?</source>
        <translation type="obsolete">Überschreiben?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation type="obsolete">Eine Datei mit diesem Namen existiert bereits.
Möchten Sie diese ersetzen.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not remove old file.</source>
        <translation type="obsolete">Alte Datei konnte nicht entfernt werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not create new file.</source>
        <translation type="obsolete">Neue Datei konnte nicht angelegt werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>Error while writing the file.</source>
        <translation>Beim Schreiben der Datei ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>Delete Attachment?</source>
        <translation>Anhang löschen?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Sie sind dabei den Dateianhang dieses Eintrages zu löschen.
Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>No, Cancel</source>
        <translation>Nein, Abbrechen</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="136"/>
        <source>Edit Entry</source>
        <translation type="unfinished">Eintrag bearbeiten</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="337"/>
        <source>Could not open file.</source>
        <translation>Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="100"/>
        <source>%1 Bit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="300"/>
        <source>Add Attachment...</source>
        <translation>Anhang hinzufügen...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="327"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="60"/>
        <source>Today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="62"/>
        <source>1 Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>2 Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="64"/>
        <source>3 Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>1 Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="68"/>
        <source>6 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>1 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="72"/>
        <source>Calendar...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="255"/>
        <source>[Untitled Entry]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="136"/>
        <source>New Entry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation>Hinweis</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation>Sie müssen mindestens ein Zeichen angeben.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open &apos;/dev/random&apos; or &apos;/dev/urandom&apos;.</source>
        <translation type="obsolete">&apos;/dev/random&apos; oder &apos;/dev/urandom&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="76"/>
        <source>Password Generator</source>
        <translation>Passwortgenerator</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="252"/>
        <source>%1 Bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a Password.</source>
        <translation type="obsolete">Bitte geben Sie ein Passwort ein.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please choose a key file.</source>
        <translation type="obsolete">Bitte wählen Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">Bitte geben Sie ein Passwort ein oder wählen
Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Database Key</source>
        <translation type="obsolete">Datenbankschlüssel</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select a Key File</source>
        <translation type="obsolete">Schlüsseldatei wählen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>*.key</source>
        <translation type="obsolete">*.key</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unexpected Error: File does not exist.</source>
        <translation type="obsolete">Unerwarteter Fehler: Datei existiert nicht.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The selected key file or directory does not exist.</source>
        <translation type="obsolete">Die gewählte Schlüsseldatei bzw. das gewählte Verzeichnis existiert nicht.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="obsolete">Das angegebene Verzeichnis enthält keine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The given directory contains more then one key file.
Please specify the key file directly.</source>
        <translation type="obsolete">Das angegebene Verzeichnis enthält mehrere Schlüsseldateien.
Bitte geben Sie die gewünschte Schlüsseldatei direkt an.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The key file found in the given directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Die im angegebenen Verzeichnis gefundene Schlüsseldatei ist nicht lesbar.
Bitter prüfen Sie Ihre Zugriffsrechte.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file could not be found.</source>
        <translation type="obsolete">Schlüsseldatei konnte nicht gefunden werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Die angegebene Schlüsseldatei ist nicht lesbar.
Bitter prüfen Sie Ihre Zugriffsrechte.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">Passwort und Passwortwiederholung stimmen nicht überein.
Bitte prüfen Sie Ihre Eingabe.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">Bitte geben Sie ein Passwort ein oder wählen
Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with the name &apos;pwsafe.key&apos; already exisits in the given directory.
Do you want to replace it?</source>
        <translation type="obsolete">Im angegebenen Verzeichnis existiert bereits eine Datei mit dem Namen &apos;pwsafe.key&apos;.
Möchten Sie diese ersetzen? </translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The exisiting file is not writable.</source>
        <translation type="obsolete">Die exisitierende Datei ist nicht überschreibbar.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with the this name already exisits.
Do you want to replace it?</source>
        <translation type="obsolete">Eine Datei mit diesem Namen existiert bereits.
Möchten Sie diese ersetzen.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Das angegebene Verzeichnis oder die angegebene Schlüsseldatei ist nicht lesbar.
Bitter prüfen Sie Ihre Zugriffsrechte.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>CSearchDlg</name>
    <message>
        <location filename="" line="0"/>
        <source>Notice</source>
        <translation type="obsolete">Hinweis</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a search string.</source>
        <translation type="obsolete">Bitte geben Sie einen Suchbegriff ein.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search</source>
        <translation type="obsolete">Suchen</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="92"/>
        <source>Add Icons...</source>
        <translation>Symbol hinzufügen...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Images (%1)</source>
        <translation>Symbole (%1)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: Datei konnte nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="97"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Ersetzen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">Beim Laden der Symbole traten Fehler auf:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="97"/>
        <source>An error occured while loading the icon.</source>
        <translation>Beim Laden des Symbols ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation type="unfinished">Wählen</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="67"/>
        <source>An error occured while loading the icon(s):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="293"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="298"/>
        <source>Select a directory...</source>
        <translation>Verzeichnis wählen...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="305"/>
        <source>Select an executable...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation type="unfinished">Titel</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation type="unfinished">Benutzername</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation type="unfinished">Passwort</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation type="unfinished">Kommentar</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="26"/>
        <source>Rich Text Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="40"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="127"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="59"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="62"/>
        <source>I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="72"/>
        <source>Underlined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="75"/>
        <source>U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="85"/>
        <source>Left-Aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="88"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="98"/>
        <source>Centered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="140"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="111"/>
        <source>Right-Aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="114"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="124"/>
        <source>Justified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="137"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="147"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="154"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="159"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="164"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="169"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="174"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="179"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="184"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="189"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="194"/>
        <source>16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="199"/>
        <source>18</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="204"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="209"/>
        <source>22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="214"/>
        <source>24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="219"/>
        <source>26</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="224"/>
        <source>28</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="229"/>
        <source>36</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="234"/>
        <source>42</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="239"/>
        <source>78</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>Templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="250"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="282"/>
        <source>HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation type="unfinished">Nie</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation type="unfinished">Datenbankeinstellungen</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation type="unfinished">Verschlüsselung</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation type="unfinished">Algorithmus:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation type="unfinished">Verschlüsselungsrunden:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="250"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="251"/>
        <source>Title</source>
        <translation type="unfinished">Titel</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="252"/>
        <source>Username</source>
        <translation type="unfinished">Benutzername</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="253"/>
        <source>Password</source>
        <translation type="unfinished">Passwort</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="254"/>
        <source>URL</source>
        <translation type="unfinished">URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="255"/>
        <source>Creation</source>
        <translation type="unfinished">Erstellung</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="256"/>
        <source>Last Access</source>
        <translation type="unfinished">Letzter Zugriff</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="257"/>
        <source>Last Modification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Comment</source>
        <translation type="unfinished">Kommentar</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Eintrag bearbeiten</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="146"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="211"/>
        <source>Password Repet.:</source>
        <translation>Passwort Wdhlg.:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="133"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="179"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="224"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="107"/>
        <source>Quality:</source>
        <translation>Qualität:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="159"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="237"/>
        <source>Expires:</source>
        <translation>Läuft ab:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="192"/>
        <source>Group:</source>
        <translation>Gruppe:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="166"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="438"/>
        <source>Icon:</source>
        <translation>Symbol:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>% Bit</source>
        <translation type="obsolete">% Bit</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="58"/>
        <source>Ge&amp;n.</source>
        <translation>Ge&amp;n.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="356"/>
        <source>Never</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="120"/>
        <source>Attachment:</source>
        <translation>Anhang:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="395"/>
        <source>%1 Bit</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Gruppen-Eigenschaften</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="44"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="37"/>
        <source>Icon:</source>
        <translation>Symbol:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="70"/>
        <source>&gt;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation type="unfinished">Titel</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation type="unfinished">Benutzername</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden. (FileError=%1)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="51"/>
        <source>No error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="52"/>
        <source>An error occurred while reading from the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="53"/>
        <source>An error occurred while writing to the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="54"/>
        <source>A fatal error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="55"/>
        <source>An resource error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="56"/>
        <source>The file could not be opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="57"/>
        <source>The operation was aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="58"/>
        <source>A timeout occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="59"/>
        <source>An unspecified error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="60"/>
        <source>The file could not be removed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="61"/>
        <source>The file could not be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>The position in the file could not be changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>The file could not be resized.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>The file could not be accessed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>The file could not be copied.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="135"/>
        <source>Alt+U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="112"/>
        <source>Alt+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="306"/>
        <source>Alt+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="89"/>
        <source>Alt+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Passwortgenerator</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Accep&amp;t</source>
        <translation type="obsolete">Annehmen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="372"/>
        <source>Generate</source>
        <translation>Generieren</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="362"/>
        <source>New Password:</source>
        <translation>Neues Passwort:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="276"/>
        <source>Quality:</source>
        <translation>Qualität:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="38"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="132"/>
        <source>&amp;Upper Letters</source>
        <translation>Großbuchstaben:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="86"/>
        <source>&amp;Lower Letters</source>
        <translation>Kleinbuchstaben:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="109"/>
        <source>&amp;Numbers</source>
        <translation>Zahlen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="152"/>
        <source>&amp;Special Characters</source>
        <translation>Sonderzeichen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation>Minus</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation>Unterstrich</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>h&amp;igher ANSI-Characters</source>
        <translation type="obsolete">höhere ANSI-Zeichen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="179"/>
        <source>Use &amp;only following characters:</source>
        <translation>Nur folgende Zeichen benutzen:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="182"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="240"/>
        <source>Length:</source>
        <translation>Länge:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Use &quot;/dev/rando&amp;m&quot;</source>
        <translation type="obsolete">&apos;/dev/random&apos; benutzen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="47"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>Folgende Zeichengruppen nutzen:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation>Leerzeichen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="125"/>
        <source>Alt+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="303"/>
        <source>Enable entropy collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="332"/>
        <source>Collect only once per session</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="unfinished">Ungültige XML-Daten (siehe stdout für Fehlerbeschreibung).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation type="unfinished">Ungültige XML-Datei.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation type="unfinished">Dokument enthält keine Daten.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="27"/>
        <source>PwManager Files (*.pwm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="112"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="38"/>
        <source>File is empty.</source>
        <translation type="unfinished">Datei ist leer.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="46"/>
        <source>File is no valid PwManager file.</source>
        <translation type="unfinished">Datei ist keine gültige PwManager-Datei.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="49"/>
        <source>Unsupported file version.</source>
        <translation type="unfinished">Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="55"/>
        <source>Unsupported hash algorithm.</source>
        <translation type="unfinished">Nicht unterstützter Hash-Algorithmus.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="58"/>
        <source>Unsupported encryption algorithm.</source>
        <translation type="unfinished">Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="65"/>
        <source>Compressed files are not supported yet.</source>
        <translation type="unfinished">Komprimierte Dateien werden noch nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="90"/>
        <source>Wrong password.</source>
        <translation type="unfinished">Falsches Passwort.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="105"/>
        <source>File is damaged (hash test failed).</source>
        <translation type="unfinished">Datei ist beschädigt (Hash-Test fehlgeschlagen).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="112"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="unfinished">Ungültige XML-Daten (siehe stdout für Fehlerbeschreibung).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="434"/>
        <source>Could not open file.</source>
        <translation type="unfinished">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="445"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="unfinished">Unerwartete Dateigrößen (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="462"/>
        <source>Wrong Signature</source>
        <translation type="unfinished">Falsche Signatur</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="467"/>
        <source>Unsupported File Version.</source>
        <translation type="unfinished">Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="476"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="unfinished">Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="505"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="unfinished">Entschlüsselung fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="524"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="unfinished">Hash-Test fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="609"/>
        <source>Invalid group tree.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="794"/>
        <source>Key file is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1133"/>
        <source>The database must contain at least one group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1147"/>
        <source>Could not open file for writing.</source>
        <translation type="unfinished">Datei konnte nicht zum Schreiben geöffnent werden.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="603"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="992"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1000"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1006"/>
        <source>MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1011"/>
        <source>GiB</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="409"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="411"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="413"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="415"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="417"/>
        <source>Comments</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="419"/>
        <source>Expires</source>
        <translation>Läuft ab</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="421"/>
        <source>Creation</source>
        <translation>Erstellung</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="423"/>
        <source>Last Change</source>
        <translation>Letzte Änderung</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="425"/>
        <source>Last Access</source>
        <translation>Letzter Zugriff</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="427"/>
        <source>Attachment</source>
        <translation>Anhang</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1 items</source>
        <translation type="obsolete">%1 Elemente</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="429"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="144"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="146"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="54"/>
        <source>Search Results</source>
        <translation>Suchergebnisse</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Groups</source>
        <translation type="obsolete">Gruppen</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="85"/>
        <source>Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="86"/>
        <source>Are you sure you want to delete this group, all it&apos;s child groups and all their entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="346"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="351"/>
        <source>Ctrl+G</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="353"/>
        <source>Ctrl+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>Ctrl+U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+K</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="347"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Shift+Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Shift+Ctrl+F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="453"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The following error occured while opening the database:
%1</source>
        <translation type="obsolete">Beim Öffnen der Datenbank ist folgender Fehler aufgetreten:
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="470"/>
        <source>Save modified file?</source>
        <translation>Geändete Datei speichern?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation>Die aktuelle Datei wurde verändert. Möchten Sie
die Änderungen speichern?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;B&gt;Group: &lt;/B&gt;%1  &lt;B&gt;Title: &lt;/B&gt;%2  &lt;B&gt;Username: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Password: &lt;/B&gt;%5  &lt;B&gt;Creation: &lt;/B&gt;%6  &lt;B&gt;Last Change: &lt;/B&gt;%7  &lt;B&gt;LastAccess: &lt;/B&gt;%8  &lt;B&gt;Expires: &lt;/B&gt;%9</source>
        <translation type="obsolete">&lt;B&gt;Gruppe: &lt;/B&gt;%1  &lt;B&gt;Titel: &lt;/B&gt;%2  &lt;B&gt;Benutzername: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Passwort: &lt;/B&gt;%5  &lt;B&gt;Erstellung: &lt;/B&gt;%6  &lt;B&gt;Letzte Änderung: &lt;/B&gt;%7  &lt;B&gt;Letzter Zugriff: &lt;/B&gt;%8  &lt;B&gt;Läuft ab: &lt;/B&gt;%9</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="788"/>
        <source>Clone Entry</source>
        <translation>Eintrag duplizieren</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="790"/>
        <source>Delete Entry</source>
        <translation>Eintrag löschen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="802"/>
        <source>Clone Entries</source>
        <translation>Einträge duplizieren</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="804"/>
        <source>Delete Entries</source>
        <translation>Einträge löschen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">Datei konnte nicht gespeichert werden.
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Database As...</source>
        <translation type="obsolete">Datenbank speichern unter...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="445"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>[new]</source>
        <translation type="obsolete">[neu]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="544"/>
        <source>Open Database...</source>
        <translation>Datenbank öffnen...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="416"/>
        <source>Loading Database...</source>
        <translation>Lade Datenbank...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="430"/>
        <source>Loading Failed</source>
        <translation>Laden fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not create key file. The following error occured:
%1</source>
        <translation type="obsolete">Datei konnte nicht angelegt werden. Der folgende Fehler trat auf:
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Export To...</source>
        <translation type="obsolete">Exportieren nach...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX [new]</source>
        <translation type="obsolete">KeePassX [neu]</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in Import_PwManager::importFile()()</source>
        <translation type="obsolete">Unbekannter Fehler in Import_PwManager::importFile()()</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in Import_KWalletXml::importFile()</source>
        <translation type="obsolete">Unbekannter Fehler in Import_KWalletXml::importFile()</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in PwDatabase::openDatabase()</source>
        <translation type="obsolete">Unbekannter Fehler in PwDatabase::openDatabase()</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="432"/>
        <source>Unknown error while loading database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>Save Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="683"/>
        <source>1 Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="685"/>
        <source>%1 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="691"/>
        <source>1 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="693"/>
        <source>%1 Years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="699"/>
        <source>1 Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="701"/>
        <source>%1 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="705"/>
        <source>less than 1 day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1222"/>
        <source>Locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1237"/>
        <source>Unlocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="349"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="350"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="453"/>
        <source>The database file does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="883"/>
        <source>new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="669"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1223"/>
        <source>Un&amp;lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1238"/>
        <source>&amp;Lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="434"/>
        <source>The following error occured while opening the database:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="833"/>
        <source>File could not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="295"/>
        <source>Show &amp;Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="345"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="136"/>
        <source>Error</source>
        <translation type="unfinished">Fehler</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="138"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="unfinished">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="138"/>
        <source>OK</source>
        <translation type="unfinished">OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File</source>
        <translation type="obsolete">Datei</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Import from...</source>
        <translation type="obsolete">Importieren aus...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>View</source>
        <translation type="obsolete">Ansicht</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Columns</source>
        <translation type="obsolete">Spalten</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Extras</source>
        <translation type="obsolete">Extras</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Help</source>
        <translation type="obsolete">Hilfe</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>New Database...</source>
        <translation type="obsolete">Neue Datenbank...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Open Database...</source>
        <translation type="obsolete">Datenbank öffnen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Close Database</source>
        <translation type="obsolete">Datenbank schließen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Database</source>
        <translation type="obsolete">Datenbank speichern</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Database As...</source>
        <translation type="obsolete">Datenbank speichern unter...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Database Settings...</source>
        <translation type="obsolete">Datenbankeinstellungen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Change Master Key...</source>
        <translation type="obsolete">Hauptschlüssel ändern...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Exit</source>
        <translation type="obsolete">Beenden</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>PwManager File (*.pwm)</source>
        <translation type="obsolete">PwManager-Datei (*.pwm)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KWallet XML-File (*.xml)</source>
        <translation type="obsolete">KWallet XML-Datei (*.xml)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add New Group...</source>
        <translation type="obsolete">Neu Gruppe hinzufügen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Edit Group...</source>
        <translation type="obsolete">Gruppe bearbeiten...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete Group</source>
        <translation type="obsolete">Gruppe löschen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">Passwort in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">Benutzername in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Open URL</source>
        <translation type="obsolete">URL öffnen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Attachment As...</source>
        <translation type="obsolete">Anhang speichern unter...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add New Entry...</source>
        <translation type="obsolete">Neuen Eintrag hinzufügen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">Eintrag anzeigen/bearbeiten...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete Entry</source>
        <translation type="obsolete">Eintrag löschen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clone Entry</source>
        <translation type="obsolete">Eintrag duplizieren</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search In Database...</source>
        <translation type="obsolete">In Datenbank suchen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search in this group...</source>
        <translation type="obsolete">In dieser Gruppe suchen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Entry Details</source>
        <translation type="obsolete">Eintragsdetails anzeigen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hide Usernames</source>
        <translation type="obsolete">Benutzernamen verbergen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hide Passwords</source>
        <translation type="obsolete">Passwörter verbergen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Title</source>
        <translation type="obsolete">Titel</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Username</source>
        <translation type="obsolete">Benutzername</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>URL</source>
        <translation type="obsolete">URL</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password</source>
        <translation type="obsolete">Passwort</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Comment</source>
        <translation type="obsolete">Kommentar</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Expires</source>
        <translation type="obsolete">Läuft ab</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Creation</source>
        <translation type="obsolete">Erstellung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last Change</source>
        <translation type="obsolete">Letzte Änderung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last Access</source>
        <translation type="obsolete">Letzter Zugriff</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Attachment</source>
        <translation type="obsolete">Anhang</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Settings...</source>
        <translation type="obsolete">Einstellungen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>About...</source>
        <translation type="obsolete">Über...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Edit</source>
        <translation type="obsolete">Bearbeiten</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Statusbar</source>
        <translation type="obsolete">Statusleiste anzeigen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Export to...</source>
        <translation type="obsolete">Exportieren nach...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX Handbook...</source>
        <translation type="obsolete">KeePassX Handbuch...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Plain Text (*.txt)</source>
        <translation type="obsolete">Klartext (*.txt)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="481"/>
        <source>Hide</source>
        <translation>Verbergen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Perform AutoType</source>
        <translation type="obsolete">AutoType ausführen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Type Here</source>
        <translation type="obsolete">Type Here</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">Symbolleistengröße</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="186"/>
        <source>&amp;View</source>
        <translation>Ansicht</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="137"/>
        <source>&amp;Import from...</source>
        <translation>&amp;Importieren aus...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="132"/>
        <source>&amp;Export to...</source>
        <translation>&amp;Exportieren nach...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="164"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="225"/>
        <source>E&amp;xtras</source>
        <translation>E&amp;xtras</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="120"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="525"/>
        <source>&amp;New Database...</source>
        <translation type="unfinished">&amp;Neue Datenbank...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="246"/>
        <source>&amp;Open Database...</source>
        <translation>Datenbank &amp;öffnen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="251"/>
        <source>&amp;Close Database</source>
        <translation>Datenbank s&amp;chließen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="256"/>
        <source>&amp;Save Database</source>
        <translation>Datenbank &amp;speichern</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="261"/>
        <source>Save Database &amp;As...</source>
        <translation>D&amp;atenbank speichern unter...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="266"/>
        <source>&amp;Database Settings...</source>
        <translation>&amp;Datenbankeinstellungen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="271"/>
        <source>Change &amp;Master Key...</source>
        <translation>Hauptschlüssel &amp;ändern...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>E&amp;xit</source>
        <translation type="obsolete">Beend&amp;en</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="455"/>
        <source>&amp;Settings...</source>
        <translation>Ein&amp;stellungen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="460"/>
        <source>&amp;About...</source>
        <translation>&amp;Über...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="473"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>&amp;KeePassX Handbuch...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="515"/>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="520"/>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="551"/>
        <source>Recycle Bin...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="47"/>
        <source>Groups</source>
        <translation type="unfinished">Gruppen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="276"/>
        <source>&amp;Lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="142"/>
        <source>&amp;Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="190"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="198"/>
        <source>&amp;Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="241"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="281"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Add New Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="291"/>
        <source>&amp;Edit Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="296"/>
        <source>&amp;Delete Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="301"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="306"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="311"/>
        <source>&amp;Open URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="316"/>
        <source>&amp;Save Attachment As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="321"/>
        <source>Add &amp;New Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="326"/>
        <source>&amp;View/Edit Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="331"/>
        <source>De&amp;lete Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="336"/>
        <source>&amp;Clone Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="341"/>
        <source>Search &amp;in Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="346"/>
        <source>Search in this &amp;Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="354"/>
        <source>Show &amp;Entry Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="362"/>
        <source>Hide &amp;Usernames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="370"/>
        <source>Hide &amp;Passwords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="378"/>
        <source>&amp;Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="386"/>
        <source>User&amp;name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="394"/>
        <source>&amp;URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="402"/>
        <source>&amp;Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="410"/>
        <source>&amp;Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="418"/>
        <source>E&amp;xpires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="426"/>
        <source>C&amp;reation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="434"/>
        <source>&amp;Last Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="442"/>
        <source>Last &amp;Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="450"/>
        <source>A&amp;ttachment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="468"/>
        <source>Show &amp;Statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="486"/>
        <source>&amp;Perform AutoType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="494"/>
        <source>&amp;16x16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="502"/>
        <source>&amp;22x22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="510"/>
        <source>2&amp;8x28</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="530"/>
        <source>&amp;Password Generator...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="541"/>
        <source>&amp;Group (search results only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="546"/>
        <source>Show &amp;Expired Entries...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="559"/>
        <source>&amp;Add Bookmark...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="564"/>
        <source>Bookmark &amp;this Database...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="31"/>
        <source>Enter Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="34"/>
        <source>Set Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="37"/>
        <source>Change Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="41"/>
        <source>Database Key</source>
        <translation type="unfinished">Datenbankschlüssel</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="125"/>
        <source>Last File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="171"/>
        <source>Select a Key File</source>
        <translation type="unfinished">Schlüsseldatei wählen</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="335"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="335"/>
        <source>Key Files (*.key)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="199"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="unfinished">Bitte geben Sie ein Passwort ein oder wählen
Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="204"/>
        <source>Please enter a Password.</source>
        <translation type="unfinished">Bitte geben Sie ein Passwort ein.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="209"/>
        <source>Please provide a key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="255"/>
        <source>%1:
No such file or directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="221"/>
        <source>The selected key file or directory is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="240"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="unfinished">Das angegebene Verzeichnis enthält keine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="246"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="259"/>
        <source>%1:
File is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="333"/>
        <source>Create Key File...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Geben Sie ein Passwort ein oder wählen Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="138"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file or directory:</source>
        <translation type="obsolete">Schlüsseldatei oder Datenträger:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="186"/>
        <source>&amp;Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="189"/>
        <source>Alt+B</source>
        <translation></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">Passwort UND Schlüsseldatei verwenden</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Exit</source>
        <translation type="obsolete">Beenden</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password Repet.:</source>
        <translation type="obsolete">Passwort Wdhlg.:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="158"/>
        <source>Key File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="198"/>
        <source>Generate Key File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="222"/>
        <source>Please repeat your password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="234"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="261"/>
        <source>Passwords are not equal.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PwDatabase</name>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Error</source>
        <translation type="obsolete">Unbekannter Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Unerwartete Dateigrößen (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong Signature</source>
        <translation type="obsolete">Falsche Signatur</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>AES-Init Failed</source>
        <translation type="obsolete">AES Initialisierung fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Hash-Test fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open key file.</source>
        <translation type="obsolete">Schlüsseldatei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file could not be written.</source>
        <translation type="obsolete">Schlüsseldatei konnte nicht geschrieben werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Datei konnte nicht zum Schreiben geöffnent werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Entschlüsselung fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="0"/>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not save configuration file.
Make sure you have write access to &apos;~/.keepass&apos;.</source>
        <translation type="obsolete">Konfigurationsdatei konnte nicht gespeichert werden.
Stellen Sie sicher, dass Sie Schreibzugriff auf &apos;~/.keepass&apos; haben.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File not found.</source>
        <translation type="obsolete">Datei nicht gefunden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File is no valid PwManager file.</source>
        <translation type="obsolete">Datei ist keine gültige PwManager-Datei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported file version.</source>
        <translation type="obsolete">Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported hash algorithm.</source>
        <translation type="obsolete">Nicht unterstützter Hash-Algorithmus.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported encryption algorithm.</source>
        <translation type="obsolete">Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Compressed files are not supported yet.</source>
        <translation type="obsolete">Komprimierte Dateien werden noch nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong password.</source>
        <translation type="obsolete">Falsches Passwort.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File is damaged (hash test failed).</source>
        <translation type="obsolete">Datei ist beschädigt (Hash-Test fehlgeschlagen).</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="obsolete">Ungültige XML-Daten (siehe stdout für Fehlerbeschreibung).</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File is empty.</source>
        <translation type="obsolete">Datei ist leer.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid XML file (see stdout for details).</source>
        <translation type="obsolete">Ungültige XML-Daten (siehe stdout für Fehlerbeschreibung).</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid XML file.</source>
        <translation type="obsolete">Ungültige XML-Datei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Document does not contain data.</source>
        <translation type="obsolete">Dokument enthält keine Daten.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Warning:</source>
        <translation type="obsolete">Warnung:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid RGB color value.
</source>
        <translation type="obsolete">Ungültiger RGB-Farbwert.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Never</source>
        <translation type="obsolete">Nie</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="120"/>
        <source>Could not locate library file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation type="unfinished">Suchen</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="90"/>
        <source>A&amp;nhang</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Suche...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="180"/>
        <source>Search For:</source>
        <translation>Suche nach:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="158"/>
        <source>Regular E&amp;xpression</source>
        <translation>regulärer Ausdruck</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="148"/>
        <source>&amp;Case Sensitive</source>
        <translation>Groß- und Kleinschreibung beachten</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="68"/>
        <source>Include:</source>
        <translation>Einbeziehen:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="130"/>
        <source>&amp;Titles</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="120"/>
        <source>&amp;Usernames</source>
        <translation>Benutzernamen</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="110"/>
        <source>C&amp;omments</source>
        <translation>Kommentare</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="100"/>
        <source>U&amp;RLs</source>
        <translation>URLs</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="80"/>
        <source>Pass&amp;words</source>
        <translation>Passwörter</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search</source>
        <translation type="obsolete">Suchen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clo&amp;se</source>
        <translation type="obsolete">Schließen</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="168"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Untergruppen einbeziehen (rekursiv)</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Symbolauswahl</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">Eigenes Symbol hinzufügen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Pick</source>
        <translation type="obsolete">Wählen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="132"/>
        <source>Alt+Ö</source>
        <translation></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="728"/>
        <source>Clear clipboard after:</source>
        <translation>Zwischenablage löschen nach:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Seconds</source>
        <translation type="obsolete">Sekunden</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Sh&amp;ow passwords in plain text by default</source>
        <translation type="obsolete">Passwort standardmäßig im Klartext anzeigen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="700"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation>Erscheinungsbild</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="318"/>
        <source>Banner Color</source>
        <translation>Bannerfarbe</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="335"/>
        <source>Text Color:</source>
        <translation>Textfarbe</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="470"/>
        <source>Change...</source>
        <translation>Ändern...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="392"/>
        <source>Color 2:</source>
        <translation>Farbe 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="526"/>
        <source>C&amp;hange...</source>
        <translation>Ändern...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="529"/>
        <source>Alt+H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="542"/>
        <source>Color 1:</source>
        <translation>Farbe 1:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Expand group tree when opening a database</source>
        <translation type="obsolete">Gruppenbaum beim Öffnen aufklappen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Other</source>
        <translation type="obsolete">Sonstiges</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Remember last opend file</source>
        <translation type="obsolete">Zuletzt geöffnete Datei beim Starten öffnen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Browser Command:</source>
        <translation type="obsolete">Browser Befehl:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Securi&amp;ty</source>
        <translation type="obsolete">Sicherheit</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="570"/>
        <source>Alternating Row Colors</source>
        <translation>Abwechselnde Zeilenfarben</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1026"/>
        <source>Browse...</source>
        <translation>Durchsuchen...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="160"/>
        <source>Remember last key type and location</source>
        <translation>Art und Ort des Schlüssels der letzten Datenbank merken</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Mounting Root:</source>
        <translation type="obsolete">Datenträgerwurzelverzeichnis:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="129"/>
        <source>Remember last opened file</source>
        <translation>Zuletzt geöffnete Datei merken</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="51"/>
        <source>The integration plugins provide features like usage of the native file dialogs and message boxes of the particular desktop environments.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="54"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="60"/>
        <source>Show system tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="118"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="236"/>
        <source>Save recent directories of file dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="614"/>
        <source>Group tree at start-up:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="621"/>
        <source>Restore last state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="628"/>
        <source>Expand all items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="635"/>
        <source>Do not expand any item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="673"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="697"/>
        <source>Edit Entry Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="864"/>
        <source>Desktop Integration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="895"/>
        <source>Plug-Ins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="904"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="911"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="918"/>
        <source>KDE 4 Desktop Integration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="933"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="953"/>
        <source>Configure...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="976"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="259"/>
        <source>Clear History Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="288"/>
        <source>Always ask before deleting entries or groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="582"/>
        <source>Customize Entry Detail View...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="827"/>
        <source>Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="833"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="843"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1045"/>
        <source>Auto-Type Fine Tuning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1057"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1106"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1070"/>
        <source>Pre-Gap:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1093"/>
        <source>Key Stroke Delay:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1103"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1019"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1009"/>
        <source>Media Root:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1035"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1038"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="88"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="190"/>
        <source>Start minimized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="220"/>
        <source>Start locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="766"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1121"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="985"/>
        <source>Custom Browser Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="997"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="281"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="688"/>
        <source>Show plain text passwords in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="707"/>
        <source>Database Key Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="804"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="775"/>
        <source>Lock database after inactivity of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1150"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="66"/>
        <source>Ctrl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="68"/>
        <source>Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="70"/>
        <source>Alt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="72"/>
        <source>AltGr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="74"/>
        <source>Win</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>StandardDatabase</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Unerwartete Dateigrößen (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong Signature</source>
        <translation type="obsolete">Falsche Signatur</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Entschlüsselung fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Hash-Test fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Datei konnte nicht zum Schreiben geöffnent werden.</translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>Title</source>
        <translation type="obsolete">Titel</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Username</source>
        <translation type="obsolete">Benutzername</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation type="unfinished">Datenbank schließen</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <location filename="" line="0"/>
        <source>Database Settings</source>
        <translation type="obsolete">Datenbankeinstellungen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Encryption</source>
        <translation type="obsolete">Verschlüsselung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Algorithm:</source>
        <translation type="obsolete">Algorithmus:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Encryption Rounds:</source>
        <translation type="obsolete">Verschlüsselungsrunden:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
</TS>
