<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de_DE">
<defaultcodec></defaultcodec>
<context>
    <name>@default</name>
    <message>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Ne peut ouvrir le fichier (FileError=%1)</translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;La présente traduction française&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt; À pour auteur:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">&lt;br&gt;Djellel DIDA</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete"> &lt;b&gt;Courriel:&lt;/b&gt;  &lt;br&gt; djellel@free.fr
</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation type="unfinished">Équipe</translation>
    </message>
    <message>
        <source>Tarek Saidi</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation type="unfinished">Développeur et Administrateur du Projet</translation>
    </message>
    <message>
        <source>tariq@users.berlios.de</source>
        <translation type="obsolete">tariq@users.berlios.de</translation>
    </message>
    <message>
        <source>Eugen Gorschenin</source>
        <translation type="obsolete">Eugen Gorschenin
</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation type="unfinished">Concepteur du site Internet</translation>
    </message>
    <message>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation type="unfinished">Remerciement à</translation>
    </message>
    <message>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation type="unfinished">Pour les rustines ayant permis un meilleur support de MacOS X</translation>
    </message>
    <message>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">&lt;ADDRESS&gt;www.outofhanwell.com&lt;ADDRESS&gt;</translation>
    </message>
    <message>
        <source>James Nicholls</source>
        <translation type="obsolete">James Nicholls</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation type="unfinished">Pour le logo de KeepassX</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation type="unfinished">Erreur</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="unfinished">S&apos;assurer que l&apos;application est correctement installée.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">N&apos;a pas pu ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">L&apos;erreur suivante est survenue:
%1</translation>
    </message>
    <message>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="50"/>
        <source>About</source>
        <translation>À propos </translation>
    </message>
    <message>
        <source>Thanks To</source>
        <translation type="obsolete">Remerciement à</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="213"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="188"/>
        <source>Translation</source>
        <translation>Traduction</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Gest. de mot de passe multiplateforme&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">&lt;span style=&quot; font-size:9pt; font-weight:400;&quot;&gt;Copyright (c)  2005 - 2006     Tarek Saidi &lt;br&gt;
KeePassX  est  distribué  sous  les  termes  de  la&lt;br&gt; Licence Publique Générale GNU v2  (GPL v2).&lt;/span&gt;</translation>
    </message>
    <message>
        <source>tarek.saidi@arcor.de</source>
        <translation type="obsolete">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <source>http://keepass.berlios.de/</source>
        <translation type="obsolete">http://keepass.berlios.de/</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="163"/>
        <source>Credits</source>
        <translation>Crédits </translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>http://keepassx.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="74"/>
        <source>AppName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="92"/>
        <source>AppFunc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX est distribué sous les termes de la
Licence Publique Générale (GPL) v2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation type="unfinished">Títre:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation type="unfinished">Parcourir...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="68"/>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation type="obsolete">Plus d&apos;une auto-saisie: Séquence clé trouvée.
Seulement une par entrée est autorisée. </translation>
    </message>
    <message>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="obsolete">Erreur de syntaxe à l&apos;intérieur de la séquence d&apos;auto-saisie près du caractère %1
     Trouvé &apos;{&apos;  sans accolade fermante &apos;}&apos;</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation type="unfinished">Títre</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation type="unfinished">Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="89"/>
        <source>Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CAboutDialog</name>
    <message>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Le fichier &apos;%1&apos; n&apos;a pu être  trouvé.</translation>
    </message>
    <message>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="obsolete">S&apos;assurer que l&apos;application est correctement installée.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Ok</translation>
    </message>
    <message>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">N&apos;a pas pu ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">L&apos;erreur suivante est survenue:
%1</translation>
    </message>
    <message>
        <source>http://keepass.berlios.de/index.php</source>
        <translation type="obsolete">http://keepass.berlios.de/index.php </translation>
    </message>
    <message>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;La présente traduction française&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt; À pour auteur:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <source>$TRANSALTION_AUTHOR</source>
        <translation type="obsolete">&lt;br&gt;Djellel DIDA</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete"> &lt;b&gt;Courriel:&lt;/b&gt;  &lt;br&gt; djellel@free.fr
</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/translation-howto.html</source>
        <translation type="obsolete">Les informations concernant la méthode de traduction de KeePassX peut être trouvé à l&apos;adresse suivante:&lt;br&gt;
 &lt;ADDRESS&gt;http://keepass.berlios.de/translation-howto.html&lt;/ADDRESS&gt;</translation>
    </message>
    <message>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <source>http://www.outofhanwell.com/&lt;br&gt;Mac OS X Support</source>
        <translation type="obsolete">&lt;ADDRESS&gt;http://www.outofhanwell.com &lt;/ADDRESS&gt;</translation>
    </message>
    <message>
        <source>Eugen Gorschenin</source>
        <translation type="obsolete">Eugen Gorschenin
</translation>
    </message>
    <message>
        <source>geugen@users.berlios.de&lt;br&gt;New Website</source>
        <translation type="obsolete">&lt;ADDRESS&gt;geugen@users.berlios.de&lt;/ADDRESS&gt;</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">&lt;br&gt;Djellel DIDA</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/</source>
        <translation type="obsolete">Toutes les informations concernant la méthode pour traduire KeePassX peuvent être obtenues à l&apos;adresse suivante:&lt;br&gt;
&lt;ADDRESS&gt;http://keepass.berlios.de/&lt;ADDRESS&gt;</translation>
    </message>
    <message>
        <source>Team</source>
        <translation type="obsolete">Équipe</translation>
    </message>
    <message>
        <source>Tarek Saidi</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <source>Developer, Project Admin</source>
        <translation type="obsolete">Développeur et Administrateur du Projet</translation>
    </message>
    <message>
        <source>tariq@users.berlios.de</source>
        <translation type="obsolete">tariq@users.berlios.de</translation>
    </message>
    <message>
        <source>Web Designer</source>
        <translation type="obsolete">Concepteur du site Internet</translation>
    </message>
    <message>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <source>Thanks To</source>
        <translation type="obsolete">Remerciement à</translation>
    </message>
    <message>
        <source>Patches for better MacOS X support</source>
        <translation type="obsolete">Pour les rustines ayant permis un meilleur support de MacOS X</translation>
    </message>
    <message>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">&lt;ADDRESS&gt;www.outofhanwell.com&lt;ADDRESS&gt;</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:
http://keepassx.sourceforge.net/</source>
        <translation type="obsolete">Les informations concernant la méthode de traduction de KeePassX peuvent être trouvées à l&apos;adresse suivante:&lt;br&gt;
 &lt;ADDRESS&gt;http://keepass.berlios.de/translation-howto.html&lt;/ADDRESS&gt;</translation>
    </message>
    <message>
        <source>James Nicholls</source>
        <translation type="obsolete">James Nicholls</translation>
    </message>
    <message>
        <source>Main Application Icon</source>
        <translation type="obsolete">Pour le logo de KeepassX</translation>
    </message>
    <message>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 Bits   (défaut)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish: 256 Bits</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Définiser le nombre de passes.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>OK</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="71"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; n&apos;est pas un nombre entier valide.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>Le nombre de passes doit être supérieur à 0.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Le mot de passe et sa confirmation ne sont pas identiques !
S&apos;il vous plait, vérifier votre saisie.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>OK</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="334"/>
        <source>Save Attachment...</source>
        <translation>Enregistrer la pièce jointe...</translation>
    </message>
    <message>
        <source>Overwrite?</source>
        <translation type="obsolete">Écraser ?</translation>
    </message>
    <message>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation type="obsolete">Un fichier ayant le même nom existe déjà.
Voulez-vous le remplacer ?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Could not remove old file.</source>
        <translation type="obsolete">N&apos;a pas pu enlever l&apos;ancien fichier.</translation>
    </message>
    <message>
        <source>Could not create new file.</source>
        <translation type="obsolete">N&apos;a pas pu créer un nouveau fichier.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Error while writing the file.</source>
        <translation>Erreur lors de l&apos;écriture du fichier.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>Delete Attachment?</source>
        <translation>Supprimer la pièce jointe ?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Vous êtes sur le point de supprimer la pièce jointe de cette entrée.
En êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>No, Cancel</source>
        <translation>Non, annuler</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation type="unfinished">Édition de l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="341"/>
        <source>Could not open file.</source>
        <translation>N&apos;a pas pu ouvrir le fichier.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 Bits</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="304"/>
        <source>Add Attachment...</source>
        <translation>Ajouter une pièce jointe...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="259"/>
        <source>[Untitled Entry]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation type="obsolete">Notification</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation type="obsolete">Vous devez au moins entrer un caractère</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>Could not open &apos;/dev/random&apos; or &apos;/dev/urandom&apos;.</source>
        <translation type="obsolete">N&apos;a pas pu ouvrir &apos;/dev/random&apos; ou &apos;/dev/urandom&apos;.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="121"/>
        <source>Password Generator</source>
        <translation>Générateur de mots de passe</translation>
    </message>
    <message>
        <source>%1 Bit</source>
        <translation type="obsolete">%1 Bits</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="270"/>
        <source>%1 Bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>Please enter a Password.</source>
        <translation type="obsolete">Entrer un mot de passe.</translation>
    </message>
    <message>
        <source>Please choose a key file.</source>
        <translation type="obsolete">Sélectionner un trousseau de clés.</translation>
    </message>
    <message>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">Entrer un mot de passe ou sélectionner un trousseau de clés.</translation>
    </message>
    <message>
        <source>Database Key</source>
        <translation type="obsolete">Base de données des clés</translation>
    </message>
    <message>
        <source>Select a Key File</source>
        <translation type="obsolete">Selectionner  un trousseau de clés</translation>
    </message>
    <message>
        <source>*.key</source>
        <translation type="obsolete">*.key</translation>
    </message>
    <message>
        <source>Unexpected Error: File does not exist.</source>
        <translation type="obsolete">Erreur inattendue: Le fichier n&apos;existe pas.</translation>
    </message>
    <message>
        <source>The selected key file or directory does not exist.</source>
        <translation type="obsolete">Le trousseau de clés ou le répertoire n&apos;existe pas.</translation>
    </message>
    <message>
        <source>The given directory does not contain any key files.</source>
        <translation type="obsolete">Le répertoire désigné ne contient aucun trousseau de clés.</translation>
    </message>
    <message>
        <source>The given directory contains more then one key file.
Please specify the key file directly.</source>
        <translation type="obsolete">Le répertoire désigné contient plus d&apos;un trousseau de clés.
Pourriez-vous sélectionner le trousseau de clés désiré.</translation>
    </message>
    <message>
        <source>The key file found in the given directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Le trousseau de clés choisi dans le répertoire n&apos;est pas lisible.
S&apos;il vous plait, vérifier vos permissions.</translation>
    </message>
    <message>
        <source>Key file could not be found.</source>
        <translation type="obsolete">Le trousseau de clés n&apos;a pu être trouvé.</translation>
    </message>
    <message>
        <source>Key file is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Le trousseau de clés n&apos;est pas lisible.
S&apos;il vous plait, vérifier vos permissions.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Avertissement</translation>
    </message>
    <message>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">Le mot de passe et sa confirmation ne sont pas identiques !
S&apos;il vous plait, vérifier votre saisie.</translation>
    </message>
    <message>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">Entrer un mot de passe ou sélectionner un trousseau de clés.</translation>
    </message>
    <message>
        <source>A file with the name &apos;pwsafe.key&apos; already exisits in the given directory.
Do you want to replace it?</source>
        <translation type="obsolete">Un fichier avec le nom &apos;pwsafe.key&apos; existe déjà dans le répertoire selectionné.
Voulez-vous le remplacer ?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <source>The exisiting file is not writable.</source>
        <translation type="obsolete">L&apos;existant fichier est protégé en écriture .</translation>
    </message>
    <message>
        <source>A file with the this name already exisits.
Do you want to replace it?</source>
        <translation type="obsolete">Un fichier avec un même nom existe déjà.
Désirez-vous le remplacer ?</translation>
    </message>
    <message>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Le trousseau de clés choisi n&apos;est pas lisible.
S&apos;il vous plait, vérifier vos permissions.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
</context>
<context>
    <name>CSearchDlg</name>
    <message>
        <source>Notice</source>
        <translation type="obsolete">Notification</translation>
    </message>
    <message>
        <source>Please enter a search string.</source>
        <translation type="obsolete">Saisissez une recherche.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Recherche</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Add Icons...</source>
        <translation>Ajouter une icône...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="94"/>
        <source>Images (%1)</source>
        <translation>Images (%1)</translation>
    </message>
    <message>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: Le fichier n&apos;a pu être chargé.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>An error occured while loading the icon(s):
</source>
        <translation type="obsolete">Une erreur est survenue lors du chargement (des) de l&apos;icône(s):</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Remplacez... </translation>
    </message>
    <message>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">Une erreur est survenue lors du chargement (des) de l&apos;icône(s): %1</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>An error occured while loading the icon.</source>
        <translation>Une erreur est survenue lors du chargement de l&apos;icône.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation type="unfinished">Sélectionner</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="68"/>
        <source>An error occured while loading the icon(s):</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="349"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="354"/>
        <source>Select a directory...</source>
        <translation>Sélectionner un répertoire....</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="361"/>
        <source>Select an executable...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation type="unfinished">Títre</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation type="unfinished">Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation type="unfinished">Mot de passe</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation type="unfinished">Jamais</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation type="unfinished">Préférences de la Base de Données</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation type="unfinished">Encryptage</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation type="unfinished">Algorithme:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation type="unfinished">Nombre de passes:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation type="unfinished">Títre</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation type="unfinished">Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation type="unfinished">Mot de passe</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation type="unfinished">URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation type="unfinished">Dernier accès</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation type="unfinished">Commentaire</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Édition de l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="140"/>
        <source>Username:</source>
        <translation>Nom d&apos;utilisateur:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="198"/>
        <source>Password Repet.:</source>
        <translation>Confirmation:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="127"/>
        <source>Title:</source>
        <translation>Títre:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="166"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="211"/>
        <source>Password:</source>
        <translation>Mot de passe:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="101"/>
        <source>Quality:</source>
        <translation>Qualité</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="153"/>
        <source>Comment:</source>
        <translation>Commentaire:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="224"/>
        <source>Expires:</source>
        <translation>Expire le:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="179"/>
        <source>Group:</source>
        <translation>Groupe:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="338"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="450"/>
        <source>Icon:</source>
        <translation>Icône:</translation>
    </message>
    <message>
        <source>% Bit</source>
        <translation type="obsolete">% Bits</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="52"/>
        <source>Ge&amp;n.</source>
        <translation>&amp;Gen.</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="371"/>
        <source>Never</source>
        <translation>Jamais</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="114"/>
        <source>Attachment:</source>
        <translation>Pièce jointe:</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="407"/>
        <source>%1 Bit</source>
        <translation>%1 Bits</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Propriétés du groupe</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>Títre:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>Icône:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation type="unfinished">Títre</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation type="unfinished">Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">N&apos;a pas pu ouvrir le fichier (FileError=%1)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="59"/>
        <source>No error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="60"/>
        <source>An error occurred while reading from the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="61"/>
        <source>An error occurred while writing to the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>A fatal error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An resource error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>The file could not be opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>The operation was aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>A timeout occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>An unspecified error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The file could not be removed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>The file could not be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>The position in the file could not be changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be resized.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be accessed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The file could not be copied.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="141"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="118"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="399"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="95"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Générateur de mots de passe</translation>
    </message>
    <message>
        <source>Accep&amp;t</source>
        <translation type="obsolete">Accep&amp;ter</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="472"/>
        <source>Generate</source>
        <translation>Générer</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="455"/>
        <source>New Password:</source>
        <translation>Nouveau mot de passe:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="369"/>
        <source>Quality:</source>
        <translation>Qualité:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="319"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="138"/>
        <source>&amp;Upper Letters</source>
        <translation>Lettres majusc&amp;ules</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="92"/>
        <source>&amp;Lower Letters</source>
        <translation>Lettres minuscu&amp;les</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="115"/>
        <source>&amp;Numbers</source>
        <translation>&amp;Nombres</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="158"/>
        <source>&amp;Special Characters</source>
        <translation>Caractères &amp;Spéciaux</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation type="obsolete">Moins</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation type="obsolete">Soulig&amp;né</translation>
    </message>
    <message>
        <source>h&amp;igher ANSI-Characters</source>
        <translation type="obsolete">Caractères ANS&amp;I étendus</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="169"/>
        <source>Use &amp;only following characters:</source>
        <translation>Utiliser s&amp;eulement les caractères suivant:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="172"/>
        <source>Alt+O</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="333"/>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Use &quot;/dev/rando&amp;m&quot;</source>
        <translation type="obsolete">Utiliser &quot;/dev/rando&amp;m&quot;</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="56"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>Utiliser le &amp;groupe de caractères suivant:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="59"/>
        <source>Alt+W</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation type="obsolete">E&amp;space</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="131"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="396"/>
        <source>Enable entropy collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="425"/>
        <source>Collect only once per session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="105"/>
        <source>&amp;Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="128"/>
        <source>&amp;White Spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>&amp;Minus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="209"/>
        <source>Exclude look-alike characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="216"/>
        <source>Ensure that password contains characters from every group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="232"/>
        <source>Pronounceable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="256"/>
        <source>Lower Letters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="263"/>
        <source>Upper Letters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="270"/>
        <source>Numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="277"/>
        <source>Special Characters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="unfinished">Donnée XML invalide (voir &apos;stdout pour plus de détails).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation type="unfinished">Fichier XML invalide.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation type="unfinished">Le document  ne contient pas de donnée.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation type="unfinished">Le fichier est vide.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation type="unfinished">Le fichier n&apos;est pas un fichier PwManager valide.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation type="unfinished">Version de fichier non supportée.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation type="unfinished">L&apos;algorithme de hachage non supporté. </translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation type="unfinished">Algorithme d&apos;encryptage non supporté.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation type="unfinished">Fichiers de compression non supportés encore.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation type="unfinished">Mauvais mot de passe.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation type="unfinished">Le fichier est endommagé (Le test de hachage a échoué).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="unfinished">Donnée XML invalide (voir &apos;stdout pour plus de détails).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="511"/>
        <source>Could not open file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="522"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="unfinished">Taille de fichier inattendue (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="539"/>
        <source>Wrong Signature</source>
        <translation type="unfinished">Mauvaise signature</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="544"/>
        <source>Unsupported File Version.</source>
        <translation type="unfinished">Version de fichier non supportée.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="553"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="unfinished">Algorithme d&apos;encryptage inconnu.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="588"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="unfinished">Le décryptage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="608"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="unfinished">Le test de hachage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E3]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E3]</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="693"/>
        <source>Invalid group tree.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="890"/>
        <source>Key file is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1271"/>
        <source>The database must contain at least one group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1295"/>
        <source>Could not open file for writing.</source>
        <translation type="unfinished">N&apos;a pu ouvrir le fichier pour écriture.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="687"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="580"/>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1135"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1143"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1149"/>
        <source>MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1154"/>
        <source>GiB</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="481"/>
        <source>Title</source>
        <translation>Títre</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="483"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="485"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="487"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="489"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="491"/>
        <source>Expires</source>
        <translation>Expire le</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="493"/>
        <source>Creation</source>
        <translation>Créé le</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="495"/>
        <source>Last Change</source>
        <translation>Dernier changement</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="497"/>
        <source>Last Access</source>
        <translation>Dernier accès</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="499"/>
        <source>Attachment</source>
        <translation>Pièce jointe</translation>
    </message>
    <message>
        <source>%1 items</source>
        <translation type="obsolete">%1 élements</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="148"/>
        <source>Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="501"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>Error</source>
        <translation type="unfinished">Erreur</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="145"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="57"/>
        <source>Search Results</source>
        <translation>Résultats de la recherche</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation type="obsolete">Groupes</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="88"/>
        <source>Delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="89"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>Ctrl+N</source>
        <translation type="unfinished">Ctrl+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="353"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="374"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="375"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="515"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>The following error occured while opening the database:
%1</source>
        <translation type="obsolete">l&apos;erreur suivante est survenue à l&apos;ouverture de la base de données:
%1</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="501"/>
        <source>Save modified file?</source>
        <translation>Enregistrer le fichier modifié ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation type="obsolete">Le courant fichier a été modifié.
Désirez-vous enregistrer le changement ?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <source>KeePassX - %1</source>
        <translation type="obsolete">KeePassX - %1</translation>
    </message>
    <message>
        <source>&lt;B&gt;Group: &lt;/B&gt;%1  &lt;B&gt;Title: &lt;/B&gt;%2  &lt;B&gt;Username: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Password: &lt;/B&gt;%5  &lt;B&gt;Creation: &lt;/B&gt;%6  &lt;B&gt;Last Change: &lt;/B&gt;%7  &lt;B&gt;LastAccess: &lt;/B&gt;%8  &lt;B&gt;Expires: &lt;/B&gt;%9</source>
        <translation type="obsolete">&lt;B&gt;Groupe: &lt;/B&gt;%1  &lt;B&gt;Titre:  &lt;/B&gt;%2   &lt;B&gt;Nom d&apos;utilisateur:  &lt;/B&gt;%3   &lt;B&gt;URL:   &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;   &lt;B&gt;Mot de passe:  &lt;/B&gt;%5   &lt;B&gt;Date de création:   &lt;/B&gt;%6  &lt;B&gt;Dernier changement:   &lt;/B&gt;%7  &lt;B&gt;Dernier accès:   &lt;/B&gt;%8  &lt;B&gt;Date d&apos;expiration:   &lt;/B&gt;%9</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="834"/>
        <source>Clone Entry</source>
        <translation>Dupliquer l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="836"/>
        <source>Delete Entry</source>
        <translation>Supprimer l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="849"/>
        <source>Clone Entries</source>
        <translation>Dupliquer les entrées</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="851"/>
        <source>Delete Entries</source>
        <translation>Supprimer les entrées</translation>
    </message>
    <message>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">Le fichier n&apos;a pu être enregistré.
%1</translation>
    </message>
    <message>
        <source>Save Database As...</source>
        <translation type="obsolete">Enregistrer la BD sous...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>[new]</source>
        <translation type="obsolete">[nouveau]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="580"/>
        <source>Open Database...</source>
        <translation>Ouvrir la BD...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="439"/>
        <source>Loading Database...</source>
        <translation>Chargement de la BD...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="461"/>
        <source>Loading Failed</source>
        <translation>Le chargement a échoué</translation>
    </message>
    <message>
        <source>Could not create key file. The following error occured:
%1</source>
        <translation type="obsolete">N&apos;a pu créer le trousseau de clés. L&apos;erreur suivante est survenue:
%1</translation>
    </message>
    <message>
        <source>Export To...</source>
        <translation type="obsolete">Exporter vers...</translation>
    </message>
    <message>
        <source>KeePassX [new]</source>
        <translation type="obsolete">KeePassX [nouveau]</translation>
    </message>
    <message>
        <source>Unknown error in Import_PwManager::importFile()()</source>
        <translation type="obsolete">Erreur inconnue dans Import_PwManager::importFile()()</translation>
    </message>
    <message>
        <source>Unknown error in Import_KWalletXml::importFile()</source>
        <translation type="obsolete">Erreur inconnue dans Import_KWalletXml::importFile()</translation>
    </message>
    <message>
        <source>Unknown error in PwDatabase::openDatabase()</source>
        <translation type="obsolete">Erreur inconnue dans PwDatabase::openDatabase()</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <source>Show Toolbar</source>
        <translation type="obsolete">Afficher la barre outils</translation>
    </message>
    <message>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="463"/>
        <source>Unknown error while loading database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>Save Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="722"/>
        <source>1 Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="724"/>
        <source>%1 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>1 Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="732"/>
        <source>%1 Years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="738"/>
        <source>1 Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="740"/>
        <source>%1 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="744"/>
        <source>less than 1 day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1301"/>
        <source>Locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1316"/>
        <source>Unlocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="484"/>
        <source>The database file does not exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1090"/>
        <source>new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="708"/>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1302"/>
        <source>Un&amp;lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>&amp;Lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="465"/>
        <source>The following error occured while opening the database:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="890"/>
        <source>File could not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="302"/>
        <source>Show &amp;Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="401"/>
        <source>Database locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="405"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="445"/>
        <source>Couldn&apos;t create database lock file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="502"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="515"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="144"/>
        <source>Error</source>
        <translation type="unfinished">Erreur</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Fichier</translation>
    </message>
    <message>
        <source>Import from...</source>
        <translation type="obsolete">Importer d&apos;un...</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="obsolete">Affichage</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="obsolete">Colonnes</translation>
    </message>
    <message>
        <source>Extras</source>
        <translation type="obsolete">Extras</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <source>New Database...</source>
        <translation type="obsolete">Nouvelle BD...</translation>
    </message>
    <message>
        <source>Open Database...</source>
        <translation type="obsolete">Ouvrir une BD...</translation>
    </message>
    <message>
        <source>Close Database</source>
        <translation type="obsolete">Fermer la BD</translation>
    </message>
    <message>
        <source>Save Database</source>
        <translation type="obsolete">Enregistrer la BD</translation>
    </message>
    <message>
        <source>Save Database As...</source>
        <translation type="obsolete">Enregistrer la BD sous...</translation>
    </message>
    <message>
        <source>Database Settings...</source>
        <translation type="obsolete">Paramétrage de la BD...</translation>
    </message>
    <message>
        <source>Change Master Key...</source>
        <translation type="obsolete">Changer la clé maitresse...</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
    <message>
        <source>PwManager File (*.pwm)</source>
        <translation type="obsolete">Fichier PwManager (*.pwm)</translation>
    </message>
    <message>
        <source>KWallet XML-File (*.xml)</source>
        <translation type="obsolete">Fichier XML, KWallet (*.xml)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="593"/>
        <source>Add New Group...</source>
        <translation type="unfinished">Ajouter un nouveau groupe...</translation>
    </message>
    <message>
        <source>Edit Group...</source>
        <translation type="obsolete">Modifier le groupe...</translation>
    </message>
    <message>
        <source>Delete Group</source>
        <translation type="obsolete">Supprimer le groupe</translation>
    </message>
    <message>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">Copier le mot de passe dans le presse-papier</translation>
    </message>
    <message>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">Copier l&apos;utilisateur dans le presse-papier</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation type="obsolete">Ouvrir l&apos;URL</translation>
    </message>
    <message>
        <source>Save Attachment As...</source>
        <translation type="obsolete">Enregistrer la pièce jointe sous...</translation>
    </message>
    <message>
        <source>Add New Entry...</source>
        <translation type="obsolete">Ajouter une nouvelle entrée...</translation>
    </message>
    <message>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">Modifier/afficher l&apos;entrée...</translation>
    </message>
    <message>
        <source>Delete Entry</source>
        <translation type="obsolete">Supprimer l&apos;entrée</translation>
    </message>
    <message>
        <source>Clone Entry</source>
        <translation type="obsolete">Dupliquer l&apos;entrée</translation>
    </message>
    <message>
        <source>Search In Database...</source>
        <translation type="obsolete">Rechercher dans la BD...</translation>
    </message>
    <message>
        <source>Search in this group...</source>
        <translation type="obsolete">Rechercher dans ce groupe...</translation>
    </message>
    <message>
        <source>Show Toolbar</source>
        <translation type="obsolete">Afficher la barre outils</translation>
    </message>
    <message>
        <source>Show Entry Details</source>
        <translation type="obsolete">Afficher les détails de l&apos;entrée</translation>
    </message>
    <message>
        <source>Hide Usernames</source>
        <translation type="obsolete">Cacher l&apos;utilisateur</translation>
    </message>
    <message>
        <source>Hide Passwords</source>
        <translation type="obsolete">Cacher les mots de passe</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="obsolete">Títre</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="obsolete">URL</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Mot de passe</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="obsolete">Commentaire</translation>
    </message>
    <message>
        <source>Expires</source>
        <translation type="obsolete">Date d&apos;expiration</translation>
    </message>
    <message>
        <source>Creation</source>
        <translation type="obsolete">Date de création</translation>
    </message>
    <message>
        <source>Last Change</source>
        <translation type="obsolete">Dernier changement</translation>
    </message>
    <message>
        <source>Last Access</source>
        <translation type="obsolete">Dernier accès</translation>
    </message>
    <message>
        <source>Attachment</source>
        <translation type="obsolete">Pièce jointe</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation type="obsolete">Préférences...</translation>
    </message>
    <message>
        <source>About...</source>
        <translation type="obsolete">À propos...</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Édition</translation>
    </message>
    <message>
        <source>Show Statusbar</source>
        <translation type="obsolete">Afficher la barre  de statuts</translation>
    </message>
    <message>
        <source>Export to...</source>
        <translation type="obsolete">Exporter vers...</translation>
    </message>
    <message>
        <source>KeePassX Handbook...</source>
        <translation type="obsolete">Le manuel de KeePassX...</translation>
    </message>
    <message>
        <source>Plain Text (*.txt)</source>
        <translation type="obsolete">Un fichier plein texte (*.txt)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="500"/>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
    <message>
        <source>Perform AutoType</source>
        <translation type="obsolete">Exécuter l&apos;auto-saisie</translation>
    </message>
    <message>
        <source>Type Here</source>
        <translation type="obsolete">Saisir ici</translation>
    </message>
    <message>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">Taille des icônes de la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="196"/>
        <source>&amp;View</source>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="136"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="145"/>
        <source>&amp;Import from...</source>
        <translation>&amp;Importer d&apos;un...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="140"/>
        <source>&amp;Export to...</source>
        <translation>&amp;Exporter vers...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Edit</source>
        <translation>Ed&amp;iter</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="235"/>
        <source>E&amp;xtras</source>
        <translation>E&amp;xtras</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;Help</source>
        <translation>Ai&amp;de</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="544"/>
        <source>&amp;New Database...</source>
        <translation type="unfinished">&amp;Nouvelle BD...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="256"/>
        <source>&amp;Open Database...</source>
        <translation>&amp;Ouvrir une BD...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="261"/>
        <source>&amp;Close Database</source>
        <translation>Fer&amp;mer la BD</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="266"/>
        <source>&amp;Save Database</source>
        <translation>&amp;Enregistrer la BD</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="271"/>
        <source>Save Database &amp;As...</source>
        <translation>Enre&amp;gistrer la BD sous...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="276"/>
        <source>&amp;Database Settings...</source>
        <translation>Paramé&amp;trage de la BD...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="281"/>
        <source>Change &amp;Master Key...</source>
        <translation>&amp;Changer la clé maitresse...</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="468"/>
        <source>&amp;Settings...</source>
        <translation>&amp;Préférences...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="476"/>
        <source>&amp;About...</source>
        <translation>À pr&amp;opos...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="492"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>Le manuel de &amp;KeePassX...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="534"/>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="539"/>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="570"/>
        <source>Recycle Bin...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="55"/>
        <source>Groups</source>
        <translation type="unfinished">Groupes</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Lock Workspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="150"/>
        <source>&amp;Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="200"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="208"/>
        <source>&amp;Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="251"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="291"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="304"/>
        <source>&amp;Edit Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="309"/>
        <source>&amp;Delete Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="314"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="319"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="324"/>
        <source>&amp;Open URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="329"/>
        <source>&amp;Save Attachment As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="334"/>
        <source>Add &amp;New Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="339"/>
        <source>&amp;View/Edit Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="344"/>
        <source>De&amp;lete Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="349"/>
        <source>&amp;Clone Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="354"/>
        <source>Search &amp;in Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="359"/>
        <source>Search in this &amp;Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="367"/>
        <source>Show &amp;Entry Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="375"/>
        <source>Hide &amp;Usernames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="383"/>
        <source>Hide &amp;Passwords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="391"/>
        <source>&amp;Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="399"/>
        <source>User&amp;name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="407"/>
        <source>&amp;URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="415"/>
        <source>&amp;Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="423"/>
        <source>&amp;Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="431"/>
        <source>E&amp;xpires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="439"/>
        <source>C&amp;reation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="447"/>
        <source>&amp;Last Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="455"/>
        <source>Last &amp;Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="463"/>
        <source>A&amp;ttachment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="487"/>
        <source>Show &amp;Statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="505"/>
        <source>&amp;Perform AutoType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="513"/>
        <source>&amp;16x16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="521"/>
        <source>&amp;22x22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="529"/>
        <source>2&amp;8x28</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="549"/>
        <source>&amp;Password Generator...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="560"/>
        <source>&amp;Group (search results only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="565"/>
        <source>Show &amp;Expired Entries...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="578"/>
        <source>&amp;Add Bookmark...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="583"/>
        <source>Bookmark &amp;this Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="299"/>
        <source>&amp;Add New Subgroup...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="588"/>
        <source>Copy URL to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="31"/>
        <source>Enter Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="34"/>
        <source>Set Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="37"/>
        <source>Change Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="41"/>
        <source>Database Key</source>
        <translation type="unfinished">Base de données des clés</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="131"/>
        <source>Last File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="177"/>
        <source>Select a Key File</source>
        <translation type="unfinished">Selectionner  un trousseau de clés</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="341"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="341"/>
        <source>Key Files (*.key)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="205"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="unfinished">Entrer un mot de passe ou sélectionner un trousseau de clés.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="210"/>
        <source>Please enter a Password.</source>
        <translation type="unfinished">Entrer un mot de passe.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="215"/>
        <source>Please provide a key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="261"/>
        <source>%1:
No such file or directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="227"/>
        <source>The selected key file or directory is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="246"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="unfinished">Le répertoire désigné ne contient aucun trousseau de clés.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="252"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="265"/>
        <source>%1:
File is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="339"/>
        <source>Create Key File...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Entrer un mot de passe et/ou sélectionner un trousseau de clés.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Clé maitresse</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>Mot de passe:</translation>
    </message>
    <message>
        <source>Key file or directory:</source>
        <translation type="obsolete">Trousseau de clés ou répertoire:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="185"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Parcourir...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="188"/>
        <source>Alt+B</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">Utiliser  un mot de passe ET un trousseau de clés</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
    <message>
        <source>Password Repet.:</source>
        <translation type="obsolete">Confirmation:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="157"/>
        <source>Key File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="197"/>
        <source>Generate Key File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="229"/>
        <source>Please repeat your password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="241"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="268"/>
        <source>Passwords are not equal.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PwDatabase</name>
    <message>
        <source>Unknown Error</source>
        <translation type="obsolete">Erreur inconnue</translation>
    </message>
    <message>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Taille de fichier inattendue (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation type="obsolete">Mauvaise signature</translation>
    </message>
    <message>
        <source>AES-Init Failed</source>
        <translation type="obsolete">L&apos;initialisation de AES a échoué</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E3]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E3]</translation>
    </message>
    <message>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Le test de hachage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Could not open key file.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le trousseau de clés.</translation>
    </message>
    <message>
        <source>Key file could not be written.</source>
        <translation type="obsolete">Le trousseau de clés n&apos;a pas pu être écrit.</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier.</translation>
    </message>
    <message>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Version de fichier non supportée.</translation>
    </message>
    <message>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Algorithme d&apos;encryptage inconnu.</translation>
    </message>
    <message>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Le décryptage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier pour écriture.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Avertissement</translation>
    </message>
    <message>
        <source>Could not save configuration file.
Make sure you have write access to &apos;~/.keepass&apos;.</source>
        <translation type="obsolete">N&apos;a pu enregistrer le fichier de configuration.
Étes-vous sûr de posséder le droit en écriture sur &apos;~/.keepass&apos;.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Le fichier &apos;%1&apos; n&apos;a pu être trouvé.</translation>
    </message>
    <message>
        <source>File not found.</source>
        <translation type="obsolete">Fichier non trouvé.</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier.</translation>
    </message>
    <message>
        <source>File is no valid PwManager file.</source>
        <translation type="obsolete">Le fichier n&apos;est pas un fichier PwManager valide.</translation>
    </message>
    <message>
        <source>Unsupported file version.</source>
        <translation type="obsolete">Version de fichier non supportée.</translation>
    </message>
    <message>
        <source>Unsupported hash algorithm.</source>
        <translation type="obsolete">L&apos;algorithme de hachage non supporté. </translation>
    </message>
    <message>
        <source>Unsupported encryption algorithm.</source>
        <translation type="obsolete">Algorithme d&apos;encryptage non supporté.</translation>
    </message>
    <message>
        <source>Compressed files are not supported yet.</source>
        <translation type="obsolete">Fichiers de compression non supportés encore.</translation>
    </message>
    <message>
        <source>Wrong password.</source>
        <translation type="obsolete">Mauvais mot de passe.</translation>
    </message>
    <message>
        <source>File is damaged (hash test failed).</source>
        <translation type="obsolete">Le fichier est endommagé (Le test de hachage a échoué).</translation>
    </message>
    <message>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="obsolete">Donnée XML invalide (voir &apos;stdout pour plus de détails).</translation>
    </message>
    <message>
        <source>File is empty.</source>
        <translation type="obsolete">Le fichier est vide.</translation>
    </message>
    <message>
        <source>Invalid XML file (see stdout for details).</source>
        <translation type="obsolete">Fichier XML invalide (voir &apos;stdout&apos; pour plus de détails).</translation>
    </message>
    <message>
        <source>Invalid XML file.</source>
        <translation type="obsolete">Fichier XML invalide.</translation>
    </message>
    <message>
        <source>Document does not contain data.</source>
        <translation type="obsolete">Le document  ne contient pas de donnée.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation type="obsolete">Avertissement:</translation>
    </message>
    <message>
        <source>Invalid RGB color value.
</source>
        <translation type="obsolete">Valeur de la couleur RGB  invalide.</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="obsolete">Jamais</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="90"/>
        <source>A&amp;nhang</source>
        <translation>A&amp;nnexe</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Rechercher...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="180"/>
        <source>Search For:</source>
        <translation>Recherche de:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="158"/>
        <source>Regular E&amp;xpression</source>
        <translation>E&amp;xpression Régulière</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="148"/>
        <source>&amp;Case Sensitive</source>
        <translation>Respecter la &amp;casse</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="68"/>
        <source>Include:</source>
        <translation>Inclure:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="130"/>
        <source>&amp;Titles</source>
        <translation>&amp;Títres</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="120"/>
        <source>&amp;Usernames</source>
        <translation>Nom d&apos;&amp;utilisateurs</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="110"/>
        <source>C&amp;omments</source>
        <translation>C&amp;ommentaires</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="100"/>
        <source>U&amp;RLs</source>
        <translation>U&amp;RLs</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="80"/>
        <source>Pass&amp;words</source>
        <translation>Mot de &amp;passe</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Chercher</translation>
    </message>
    <message>
        <source>Clo&amp;se</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+Q</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="168"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Inclure les sous-groupes (récursive)</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Choix d&apos;icônes</translation>
    </message>
    <message>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">Ajouter une icône... </translation>
    </message>
    <message>
        <source>Pick</source>
        <translation type="obsolete">Sélectionner</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="194"/>
        <source>Alt+Ö</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="981"/>
        <source>Clear clipboard after:</source>
        <translation>Effacer le presse-papier après:</translation>
    </message>
    <message>
        <source>Seconds</source>
        <translation type="obsolete">Secondes</translation>
    </message>
    <message>
        <source>Sh&amp;ow passwords in plain text by default</source>
        <translation type="obsolete">A&amp;fficher le mot de passe en clair par défaut</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="953"/>
        <source>Alt+O</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation type="obsolete">Appa&amp;rence</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="477"/>
        <source>Banner Color</source>
        <translation>Couleur du bandeau</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="491"/>
        <source>Text Color:</source>
        <translation>Couleur du texte:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="623"/>
        <source>Change...</source>
        <translation>Changer...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="548"/>
        <source>Color 2:</source>
        <translation>Couleur 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="676"/>
        <source>C&amp;hange...</source>
        <translation>C&amp;hanger...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="679"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="692"/>
        <source>Color 1:</source>
        <translation>Couleur 1:</translation>
    </message>
    <message>
        <source>Expand group tree when opening a database</source>
        <translation type="obsolete">Développer l&apos;arborescence du groupe à l&apos;ouverture de la base de données</translation>
    </message>
    <message>
        <source>&amp;Other</source>
        <translation type="obsolete">Au&amp;tre</translation>
    </message>
    <message>
        <source>Remember last opend file</source>
        <translation type="obsolete">Se souvenir du dernier fichier ouvert</translation>
    </message>
    <message>
        <source>Browser Command:</source>
        <translation type="obsolete">Commande du navigateur:</translation>
    </message>
    <message>
        <source>Securi&amp;ty</source>
        <translation type="obsolete">Séc&amp;urité</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="720"/>
        <source>Alternating Row Colors</source>
        <translation>Couleurs alternées pour les rangées </translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1132"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="222"/>
        <source>Remember last key type and location</source>
        <translation>Se souvenir de la dernière  saisie de clé et  du dernier emplacement</translation>
    </message>
    <message>
        <source>Mounting Root:</source>
        <translation type="obsolete">Point de montage:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="191"/>
        <source>Remember last opened file</source>
        <translation>Se souvenir du dernier fichier ouvert</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="122"/>
        <source>Show system tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="180"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="298"/>
        <source>Save recent directories of file dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="761"/>
        <source>Group tree at start-up:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="768"/>
        <source>Restore last state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="775"/>
        <source>Expand all items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="782"/>
        <source>Do not expand any item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="94"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="950"/>
        <source>Edit Entry Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1333"/>
        <source>Plug-Ins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1342"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1349"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1356"/>
        <source>KDE 4 Desktop Integration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1371"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1391"/>
        <source>Configure...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="99"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="321"/>
        <source>Clear History Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="343"/>
        <source>Always ask before deleting entries or groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="732"/>
        <source>Customize Entry Detail View...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1285"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1295"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1151"/>
        <source>Auto-Type Fine Tuning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1160"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1209"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1173"/>
        <source>Pre-Gap:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1196"/>
        <source>Key Stroke Delay:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1206"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1125"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1115"/>
        <source>Media Root:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1141"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1144"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="150"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="252"/>
        <source>Start minimized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="282"/>
        <source>Start locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1019"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1224"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1091"/>
        <source>Custom Browser Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1103"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="438"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="941"/>
        <source>Show plain text passwords in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="960"/>
        <source>Database Key Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1057"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1028"/>
        <source>Lock database after inactivity of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1253"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="74"/>
        <source>General (1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="79"/>
        <source>General (2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="84"/>
        <source>Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="89"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="375"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="403"/>
        <source>Delete backup entries older than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="429"/>
        <source>days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="445"/>
        <source>Automatically save database after every change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="837"/>
        <source>System Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="842"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="867"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="874"/>
        <source>Author:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Entrer votre mot de passe</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Mot de passe:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
</context>
<context>
    <name>StandardDatabase</name>
    <message>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Taille de fichier inattendue (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation type="obsolete">Mauvaise signature</translation>
    </message>
    <message>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Version de fichier non supportée.</translation>
    </message>
    <message>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Algorithme d&apos;encryptage inconnu.</translation>
    </message>
    <message>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Le décryptage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Le test de hachage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E3]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E3]</translation>
    </message>
    <message>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier pour écriture.</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../lib/tools.cpp" line="338"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="unfinished">&lt;br&gt;Djellel DIDA</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="unfinished"> &lt;b&gt;Courriel:&lt;/b&gt;  &lt;br&gt; djellel@free.fr
</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="337"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <source>Title</source>
        <translation type="obsolete">Títre</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Nom d&apos;utilisateur</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation type="unfinished">Fermer la BD</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <source>Database Settings</source>
        <translation type="obsolete">Préférences de la Base de Données</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="obsolete">Encryptage</translation>
    </message>
    <message>
        <source>Algorithm:</source>
        <translation type="obsolete">Algorithme:</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <source>Encryption Rounds:</source>
        <translation type="obsolete">Nombre de passes:</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Ctrl+K</source>
        <translation type="obsolete">Ctrl+A<byte value="x9"/></translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation type="obsolete">Ctrl+N</translation>
    </message>
</context>
</TS>
