<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="ja_JP">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;現在の翻訳: 日本語&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;作者:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Nardog</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">http://nardog.takoweb.com</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source><byte value="x9"/>Information on how to translate KeePassX can be found under:
<byte value="x9"/>http://keepassx.sourceforge.net/</source>
        <translation type="obsolete"><byte value="x9"/>KeePassX を翻訳する方法の情報は下でご覧になれます:
<byte value="x9"/>http://keepassx.sourceforge.net/</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>チーム</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Tarek Saidi</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation>開発者、プロジェクト管理者</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>tariq@users.berlios.de</source>
        <translation type="obsolete">tariq@users.berlios.de</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Eugen Gorschenin</source>
        <translation type="obsolete">Eugen Gorschenin</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation>Web デザイナ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation>謝辞</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation>よりよい MacOS X サポートのパッチ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">www.outofhanwell.com</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>James Nicholls</source>
        <translation type="obsolete">James Nicholls</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation>メイン アプリケーションのアイコン</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Constantin Makshin</source>
        <translation type="obsolete">Constantin Makshin</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation>さまざまな修正と向上</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>dinosaur-rus@users.sourceforge.net</source>
        <translation type="obsolete">dinosaur-rus@users.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>ファイル &apos;%1&apos; が見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>プログラムが正しくインストールされていることを確実にしてください。</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">ファイル &apos;%1&apos; を開けませんでした</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">以下のエラーが発生しました:
%1</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>KeePassX を翻訳する方法についての情報は次の下に見つかります:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation>開発者</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="unfinished">なし</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="50"/>
        <source>About</source>
        <translation>バージョン情報</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="213"/>
        <source>License</source>
        <translation>ライセンス</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="188"/>
        <source>Translation</source>
        <translation>翻訳</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - クロス プラットフォーム パスワード マネージャ&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="163"/>
        <source>Credits</source>
        <translation>クレジット</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>http://keepassx.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2 or later.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX は General Public License (GPL) version 2 以降の
条件の下で配布されています。</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="74"/>
        <source>AppName</source>
        <translation>AppName</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="92"/>
        <source>AppFunc</source>
        <translation>AppFunc</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Copyright (C) 2005 - 2007 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2007 KeePassX Team 
KeePassX は General Public License (GPL) 
version 2 の条件の下に配布されています。</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="151"/>
        <source>Copyright (C) 2005 - 2008 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2007 KeePassX Team 
KeePassX は General Public License (GPL) 
version 2 の条件の下に配布されています。 {2005 ?} {2008 ?} {2.?}</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2007 KeePassX Team 
KeePassX は General Public License (GPL) 
version 2 の条件の下に配布されています。 {2005 ?} {2008 ?} {2.?} {2005 ?} {2009 ?} {2.?}</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>ブックマークの追加</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>タイトル:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>ファイル:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>参照...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>ブックマークの編集</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass データベース (*.kdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="68"/>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation type="obsolete">1 つより多くの &apos;自動入力:&apos; キー シーケンスが見つかりました。
許可されているのは 1 つのエントリあたり 1 つのみです。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Error</source>
        <translation type="obsolete">エラー</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="95"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="obsolete">文字 %1 に近い [自動入力] シーケンスでの構文エラーです
<byte value="x9"/>閉じ &apos;}&apos; のない &apos;{&apos; が見つかりました</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Auto-Type string contains illegal characters</source>
        <translation type="obsolete">自動入力の文字列は不法な文字を含みます</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - 自動入力</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>自動入力するにはエントリ上でクリックします。</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="89"/>
        <source>Auto-Type</source>
        <translation>自動入力</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 ビット   (既定)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 ビット</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>暗号化の周囲数を決定してください。</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="71"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; は有効な整数値ではありません。</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>暗号化の周囲数は 0 より多い必要があります。</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>パスワードとパスワードの反復が同じではありません。
ご入力をチェックしてください。</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="334"/>
        <source>Save Attachment...</source>
        <translation>添付の保存...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>Yes</source>
        <translation>はい</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Error while writing the file.</source>
        <translation>ファイルの書き込み中のエラーです。</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>Delete Attachment?</source>
        <translation>添付を削除しますか?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>このエントリの添付を削除しようとしています。
よろしいですか?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>No, Cancel</source>
        <translation>いいえ、キャンセル</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="341"/>
        <source>Could not open file.</source>
        <translation>ファイルを開けませんでした。</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 ビット</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="304"/>
        <source>Add Attachment...</source>
        <translation>添付の追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Test 2</source>
        <translation type="obsolete">テスト 2</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>選択されたエントリは添付がないか空です。</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation>今日</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation>1 週間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation>2 週間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation>3 週間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation>1 ヶ月間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation>3 ヶ月間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation>6 ヶ月間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation>1 年間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation>カレンダー...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation>エントリの編集</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="259"/>
        <source>[Untitled Entry]</source>
        <translation>[無題のエントリ]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation type="obsolete">通知</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation type="obsolete">少なくとも 1 文字入力する必要があります</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="121"/>
        <source>Password Generator</source>
        <translation>パスワード ジェネレータ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Accept</source>
        <translation type="obsolete">承認</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="270"/>
        <source>%1 Bits</source>
        <translation>%1 ビット</translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Error</source>
        <translation type="obsolete">エラー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Please enter a Password.</source>
        <translation type="obsolete">パスワードを入力してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Please choose a key file.</source>
        <translation type="obsolete">キー ファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">パスワードを入力するかキー ファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Database Key</source>
        <translation type="obsolete">データベース キー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Select a Key File</source>
        <translation type="obsolete">キー ファイルの選択</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The selected key file or directory does not exist.</source>
        <translation type="obsolete">選択されたキー ファイルまたはディレクトリが存在しません。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="obsolete">ディレクトリがキー ファイルを含みません。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The given directory contains more then one key file.
Please specify the key file directly.</source>
        <translation type="obsolete">ディレクトリは 1 つより多くのキー ファイルを含みます。
直接キー ファイルを指定してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The key file found in the given directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">ディレクトリに見つかったキー ファイルは読み込み可能ではありません。
権限をチェックしてください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key file could not be found.</source>
        <translation type="obsolete">キー ファイルが見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key file is not readable.
Please check your permissions.</source>
        <translation type="obsolete">キー ファイルは読み込み可能ではありません。
権限をチェックしてください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Warning</source>
        <translation type="obsolete">警告</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">パスワードとパスワードの反復が同じではありません。
ご入力をチェックしてください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">パスワードを入力するかキー ファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">選択されたキー ファイルまたはディレクトリは読み込み可能ではありません。
権限をチェックしてください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>All Files (*)</source>
        <translation type="obsolete">すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key Files (*.key)</source>
        <translation type="obsolete">キー ファイル (*.key)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>File exists.</source>
        <translation type="obsolete">ファイルは存在します。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>A file with the selected name already exists, should this file be used as key file or do you want to overwrite it with a newly generated one?</source>
        <translation type="obsolete">選択された名前のファイルはすでに存在します、このファイルをキー ファイルとして使用するか新しく生成されたもので上書きしますか?</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Use</source>
        <translation type="obsolete">使用</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Overwrite</source>
        <translation type="obsolete">上書き</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key file could not be created.
%1</source>
        <translation type="obsolete">キー ファイルは作成できませんでした。
%1</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Last File</source>
        <translation type="obsolete">最後のファイル</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Add Icons...</source>
        <translation>アイコンの追加...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="94"/>
        <source>Images (%1)</source>
        <translation>イメージ (%1)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: ファイルは読み込めませんでした。
</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>置換...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">アイコンの読み込み中にエラーが発生しました:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>An error occured while loading the icon.</source>
        <translation>アイコンの読み込み中にエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>カスタム アイコンの追加</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>抽出</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: ファイルは読み込めませんでした。</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="68"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>アイコンの読み込み中にエラーが発生しました:</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="349"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="354"/>
        <source>Select a directory...</source>
        <translation>ディレクトリの選択...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Error: %1</source>
        <translation type="obsolete">エラー: %1</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="361"/>
        <source>Select an executable...</source>
        <translation>実行ファイルの選択...</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>カレンダー</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>エントロピー コレクション</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>ランダム数字ジェネレータ</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>エントロピーを収集しています...
ランダム数字ジェネレータの再シードに十分なエントロピーが収集されるまでマウスを動かすか何かキーを押してください。</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;ランダム プールが正常に再シードされました!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>コメント</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>添付名</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>作成日</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>最終アクセス日</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>最終変更日</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>満了日</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>満了まで</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>ダイアログ</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation>リッチ テキスト エディタ</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation>太字</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation>太</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation>斜体</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation>斜</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation>下線</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation>左揃え</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation>中央揃え</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation>右揃え</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation>両端揃え</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation>テキストの色</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation>フォント サイズ</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation>テンプレート</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation>テ</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Restore Default</source>
        <translation type="obsolete">既定の復元</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Save</source>
        <translation type="obsolete">保存</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>しない</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>データベースの設定</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>暗号化</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>アルゴリズム:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>暗号化の周囲:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation>作成</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation>最終アクセス</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation>最終変更</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation>満了</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation>コメント</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>エントリの編集</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="140"/>
        <source>Username:</source>
        <translation>ユーザー名:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="198"/>
        <source>Password Repet.:</source>
        <translation>パスワードの反復:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="127"/>
        <source>Title:</source>
        <translation>タイトル:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="166"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="211"/>
        <source>Password:</source>
        <translation>パスワード:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="101"/>
        <source>Quality:</source>
        <translation>品質:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="153"/>
        <source>Comment:</source>
        <translation>コメント:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="224"/>
        <source>Expires:</source>
        <translation>満了:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="179"/>
        <source>Group:</source>
        <translation>グループ:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="338"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="450"/>
        <source>Icon:</source>
        <translation>アイコン:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="52"/>
        <source>Ge&amp;n.</source>
        <translation>生成(&amp;N)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="371"/>
        <source>Never</source>
        <translation>しない</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="114"/>
        <source>Attachment:</source>
        <translation>添付:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="407"/>
        <source>%1 Bit</source>
        <translation>%1 ビット</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>グループのプロパティ</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>タイトル:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>アイコン:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">キャンセル(&amp;C)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>O&amp;K</source>
        <translation type="obsolete">OK(&amp;K)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>Expried Entries of the Database</source>
        <translation type="obsolete">データベースの満了済みエントリ</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>満了済みエントリ</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>ジャンプするエントリ上でダブル クリックします。</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>満了</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>データベースの満了済みエントリ</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML ファイル (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>KeePassX XML ファイル</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>テキスト ファイル (*.txt)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>テキスト ファイル</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="obsolete">ファイルのインポート...</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>エクスポートが失敗しました</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="59"/>
        <source>No error occurred.</source>
        <translation>エラーが発生しませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="60"/>
        <source>An error occurred while reading from the file.</source>
        <translation>ファイルからの読み込み中にエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="61"/>
        <source>An error occurred while writing to the file.</source>
        <translation>ファイルへの書き込み中にエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>A fatal error occurred.</source>
        <translation>致命的なエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An resource error occurred.</source>
        <translation>リソース エラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>The file could not be opened.</source>
        <translation>ファイルは開けませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>The operation was aborted.</source>
        <translation>操作は中止されました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>A timeout occurred.</source>
        <translation>タイムアウトが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>An unspecified error occurred.</source>
        <translation>予期しないエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The file could not be removed.</source>
        <translation>ファイルは削除できませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>The file could not be renamed.</source>
        <translation>ファイルは名前を変更できませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>The position in the file could not be changed.</source>
        <translation>ファイルの位置は変更できませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be resized.</source>
        <translation>ファイルはサイズを変更できませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be accessed.</source>
        <translation>ファイルはアクセスできませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The file could not be copied.</source>
        <translation>ファイルはコピーできませんでした。</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="141"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="118"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="399"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="95"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>パスワード ジェネレータ</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="472"/>
        <source>Generate</source>
        <translation>生成</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="455"/>
        <source>New Password:</source>
        <translation>新しいパスワード:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="369"/>
        <source>Quality:</source>
        <translation>品質:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="319"/>
        <source>Options</source>
        <translation>オプション</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="138"/>
        <source>&amp;Upper Letters</source>
        <translation>大文字(&amp;U)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="92"/>
        <source>&amp;Lower Letters</source>
        <translation>小文字(&amp;L)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="115"/>
        <source>&amp;Numbers</source>
        <translation>数字(&amp;N)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="158"/>
        <source>&amp;Special Characters</source>
        <translation>特殊文字(&amp;S)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation type="obsolete">マイナス</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation type="obsolete">下線(&amp;N)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="169"/>
        <source>Use &amp;only following characters:</source>
        <translation>以下の文字のみ使用する(&amp;O):</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="172"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="333"/>
        <source>Length:</source>
        <translation>長さ:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="56"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>以下の文字グループを使用する(&amp;W):</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="59"/>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation type="obsolete">空白(&amp;S)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="131"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="396"/>
        <source>Enable entropy collection</source>
        <translation>エントロピーの収集を有効にする</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="425"/>
        <source>Collect only once per session</source>
        <translation>収集は 1 セッションあたり 1 回のみ</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="105"/>
        <source>&amp;Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="128"/>
        <source>&amp;White Spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>&amp;Minus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="209"/>
        <source>Exclude look-alike characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="216"/>
        <source>Ensure that password contains characters from every group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="232"/>
        <source>Pronounceable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="256"/>
        <source>Lower Letters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="263"/>
        <source>Upper Letters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="270"/>
        <source>Numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="277"/>
        <source>Special Characters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML ファイル (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>インポートが失敗しました</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>不正な XML データです (詳細は stdout をご覧ください)。</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>不正な XML データです。</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>ドキュメントがデータを含みません。</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>KeePass XML ファイル (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>インポートが失敗しました</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>行 %1 列 %2 での XML 構文解析エラー:
%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>構文解析エラー: ファイルは有効な KeePassX XML ファイルではありません。</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>PwManager ファイル (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation>インポートが失敗しました</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation>ファイルは空です。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation>ファイルは有効な PwManager ファイルではありません。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation>未サポートのファイル バージョンです。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation>未サポートのハッシュ アルゴリズムです。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>未サポートの暗号化アルゴリズムです。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation>圧縮ファイルはまだサポートされていません。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation>間違ったパスワードです。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation>ファイルは損害を受けています (ハッシュ テストが失敗しました)。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>不正な XML データです (詳細は stdout をご覧ください)。</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>ファイルのインポート...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>インポートが失敗しました</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="511"/>
        <source>Could not open file.</source>
        <translation>ファイルを開けませんでした。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="522"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>予期しないファイル サイズです (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="539"/>
        <source>Wrong Signature</source>
        <translation>間違った署名</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="544"/>
        <source>Unsupported File Version.</source>
        <translation>未サポートのファイル バージョンです。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="553"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>不明な暗号化アルゴリズムです。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="588"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>複合化が失敗しました。
キーが間違っているかファイルが損害を受けています。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="608"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>ハッシュ テストが失敗しました。
キーが間違っているかファイルが損害を受けています。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [G1]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[G1]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [G2]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[G２]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [E1]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[E1]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [E2]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[E2]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [E3]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[E3]</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="693"/>
        <source>Invalid group tree.</source>
        <translation>不正なグループ ツリーです。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="890"/>
        <source>Key file is empty.</source>
        <translation>キー ファイルは空です。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1271"/>
        <source>The database must contain at least one group.</source>
        <translation>データベースは少なくとも 1 つのグループを含む必要があります。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1295"/>
        <source>Could not open file for writing.</source>
        <translation>書き込み用のファイルを開けませんでした。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="687"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>予期しないエラー: オフセットは範囲外です。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="580"/>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1135"/>
        <source>Bytes</source>
        <translation>バイト</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1143"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1149"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1154"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="481"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="483"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="485"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="487"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="489"/>
        <source>Comments</source>
        <translation>コメント</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="491"/>
        <source>Expires</source>
        <translation>満了</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="493"/>
        <source>Creation</source>
        <translation>作成</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="495"/>
        <source>Last Change</source>
        <translation>最終変更</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="497"/>
        <source>Last Access</source>
        <translation>最終アクセス</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="499"/>
        <source>Attachment</source>
        <translation>添付</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Are you sure you want delete this entry?</source>
        <translation type="obsolete">このエントリを削除してもよろしいですか?</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Are you sure you want delete these %1 entries?</source>
        <translation type="obsolete">これら %1 個のエントリを削除してもよろしいですか?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="148"/>
        <source>Delete?</source>
        <translation>削除しますか?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="501"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>少なくとも 1 のグループはエントリの追加前に存在する必要があります。</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="145"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="57"/>
        <source>Search Results</source>
        <translation>検索結果</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Groups</source>
        <translation type="obsolete">グループ</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="88"/>
        <source>Delete?</source>
        <translation>削除しますか?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="86"/>
        <source>Are you sure you want to delete this group, all it&apos;s child groups and all their entries?</source>
        <translation type="obsolete">このグループ、すべての子グループ、およびそれらのエントリをすべて削除してもよろしいですか?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="89"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="353"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="374"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="375"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="515"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The following error occured while opening the database:
%1</source>
        <translation type="obsolete">データベースを開いている間に以下のエラーが発生しました:
%1</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="501"/>
        <source>Save modified file?</source>
        <translation>変更されたファイルを保存しますか?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation type="obsolete">現在のファイルは変更されました。変更を
保存しますか?</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Yes</source>
        <translation type="obsolete">はい</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>No</source>
        <translation type="obsolete">いいえ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="834"/>
        <source>Clone Entry</source>
        <translation>エントリを閉じる</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="836"/>
        <source>Delete Entry</source>
        <translation>エントリの削除</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="849"/>
        <source>Clone Entries</source>
        <translation>エントリのクローン</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="851"/>
        <source>Delete Entries</source>
        <translation>エントリの削除</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">ファイルは保存できませんでした。
%1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Ready</source>
        <translation>レディ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>[new]</source>
        <translation type="obsolete">[新規]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="580"/>
        <source>Open Database...</source>
        <translation>データベースを開く...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="439"/>
        <source>Loading Database...</source>
        <translation>データベースを読み込んでいます...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="461"/>
        <source>Loading Failed</source>
        <translation>読み込みが失敗しました</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">ツール バーの表示</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>%1 - KeePassX</source>
        <translation type="obsolete">%1 - KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="463"/>
        <source>Unknown error while loading database.</source>
        <translation>データベースの読み込み中の不明なエラーです。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass データベース (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>Save Database...</source>
        <translation>データベースの保存...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>KeePassX - [unsaved]</source>
        <translation type="obsolete">KeePassX - [未保存]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>New Database</source>
        <translation type="obsolete">新しいデータベース</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>expired</source>
        <translation type="obsolete">満了済み</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="722"/>
        <source>1 Month</source>
        <translation>1 ヶ月間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="724"/>
        <source>%1 Months</source>
        <translation>%1 ヶ月間</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>, </source>
        <translation type="obsolete">、</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>1 Year</source>
        <translation>1 年間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="732"/>
        <source>%1 Years</source>
        <translation>%1 年間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="738"/>
        <source>1 Day</source>
        <translation>1 日間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="740"/>
        <source>%1 Days</source>
        <translation>%1 日間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="744"/>
        <source>less than 1 day</source>
        <translation>1 日未満</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Set Master Key</source>
        <translation type="obsolete">マスター キーの設定</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>* - KeePassX</source>
        <translation type="obsolete">* - KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1301"/>
        <source>Locked</source>
        <translation>ロック済み</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1316"/>
        <source>Unlocked</source>
        <translation>未ロック</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="484"/>
        <source>The database file does not exist.</source>
        <translation>データベース ファイルが存在しません。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="465"/>
        <source>The following error occured while opening the database:</source>
        <translation>データベースを開いている間に以下のエラーが発生ました:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1090"/>
        <source>new</source>
        <translation>新規</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="708"/>
        <source>Expired</source>
        <translation>満了済み</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="890"/>
        <source>File could not be saved.</source>
        <translation>ファイルは保存できませんでした。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1302"/>
        <source>Un&amp;lock Workspace</source>
        <translation>ワークスペースのロック解除(&amp;L)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>&amp;Lock Workspace</source>
        <translation>ワークスペースのロック(&amp;L)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="302"/>
        <source>Show &amp;Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="401"/>
        <source>Database locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="405"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="445"/>
        <source>Couldn&apos;t create database lock file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="502"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="515"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="144"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>ファイル &apos;%1&apos; は見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Columns</source>
        <translation type="obsolete">列</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="593"/>
        <source>Add New Group...</source>
        <translation type="unfinished">新しいグループの追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Edit Group...</source>
        <translation type="obsolete">グループの編集...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Delete Group</source>
        <translation type="obsolete">グループの削除</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">クリップボードへパスワードをコピー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">クリップボードへユーザー名をコピー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Open URL</source>
        <translation type="obsolete">URL を開く</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Save Attachment As...</source>
        <translation type="obsolete">名前を付けて添付を保存...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Add New Entry...</source>
        <translation type="obsolete">新しいエントリの追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">エントリの表示/編集...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Delete Entry</source>
        <translation type="obsolete">エントリの削除</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Clone Entry</source>
        <translation type="obsolete">エントリのクローン</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search In Database...</source>
        <translation type="obsolete">データベースから検索...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search in this group...</source>
        <translation type="obsolete">このグループから検索...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Entry Details</source>
        <translation type="obsolete">エントリの詳細の表示</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Hide Usernames</source>
        <translation type="obsolete">ユーザー名を隠す</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Hide Passwords</source>
        <translation type="obsolete">パスワードを隠す</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Title</source>
        <translation type="obsolete">タイトル</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Username</source>
        <translation type="obsolete">ユーザー名</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>URL</source>
        <translation type="obsolete">URL</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Password</source>
        <translation type="obsolete">パスワード</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Comment</source>
        <translation type="obsolete">コメント</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Expires</source>
        <translation type="obsolete">満了</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Creation</source>
        <translation type="obsolete">作成</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Last Change</source>
        <translation type="obsolete">最終変更</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Last Access</source>
        <translation type="obsolete">最終アクセス</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Attachment</source>
        <translation type="obsolete">添付</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Statusbar</source>
        <translation type="obsolete">ステータス バーの表示</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="500"/>
        <source>Hide</source>
        <translation>非表示</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Perform AutoType</source>
        <translation type="obsolete">自動入力を行う</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">ツール バー アイコンのサイズ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>16x16</source>
        <translation type="obsolete">16x16</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>22x22</source>
        <translation type="obsolete">22x22</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>28x28</source>
        <translation type="obsolete">28x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="196"/>
        <source>&amp;View</source>
        <translation>表示(&amp;V)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="136"/>
        <source>&amp;File</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="145"/>
        <source>&amp;Import from...</source>
        <translation>インポート(&amp;I)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="140"/>
        <source>&amp;Export to...</source>
        <translation>エクスポート(&amp;E)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Edit</source>
        <translation>編集(&amp;E)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="235"/>
        <source>E&amp;xtras</source>
        <translation>追加(&amp;X)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;Help</source>
        <translation>ヘルプ(&amp;H)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="256"/>
        <source>&amp;Open Database...</source>
        <translation>データベースを開く(&amp;O)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="261"/>
        <source>&amp;Close Database</source>
        <translation>データベースを閉じる(&amp;C)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="266"/>
        <source>&amp;Save Database</source>
        <translation>データベースの上書き保存(&amp;S)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="271"/>
        <source>Save Database &amp;As...</source>
        <translation>名前を付けてデータベースを保存(&amp;A)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="276"/>
        <source>&amp;Database Settings...</source>
        <translation>データベースの設定(&amp;D)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="281"/>
        <source>Change &amp;Master Key...</source>
        <translation>マスター キーの変更(&amp;M)...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>E&amp;xit</source>
        <translation type="obsolete">終了(&amp;X)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="468"/>
        <source>&amp;Settings...</source>
        <translation>設定(&amp;S)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="476"/>
        <source>&amp;About...</source>
        <translation>バージョン情報(&amp;A)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="492"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>KeePassX ハンドブック(&amp;K)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="534"/>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation>スタンダード KeePass シングル ユーザー データベース (*.kdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="539"/>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation>アドバンスド KeePass データベース (*.kdb)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>New Database...</source>
        <translation type="obsolete">新しいデータベース...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Password Generator...</source>
        <translation type="obsolete">パスワード ジェネレータ...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Group (search results only)</source>
        <translation type="obsolete">グループ (検索結果のみ)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Expired Entries...</source>
        <translation type="obsolete">満了済みエントリの表示...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Expired Entries</source>
        <translation type="obsolete">満了済みエントリの表示</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="570"/>
        <source>Recycle Bin...</source>
        <translation>ごみ箱...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Lock Workspace</source>
        <translation type="obsolete">ワークスペースのロック</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="55"/>
        <source>Groups</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Bookmarks</source>
        <translation type="obsolete">ブックマーク</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Manage Bookmarks...</source>
        <translation type="obsolete">ブックマークの管理...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Lock Workspace</source>
        <translation>ワークスペースのロック(&amp;L)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Q&amp;uit</source>
        <translation type="obsolete">終了(&amp;U)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search in Database...</source>
        <translation type="obsolete">データベースから検索...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search in this Group...</source>
        <translation type="obsolete">このグループから検索...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Add Bookmark...</source>
        <translation type="obsolete">ブックマークの追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Bookmark this Database...</source>
        <translation type="obsolete">このデータベースをブックマーク...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="150"/>
        <source>&amp;Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="200"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="208"/>
        <source>&amp;Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="251"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="291"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="304"/>
        <source>&amp;Edit Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="309"/>
        <source>&amp;Delete Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="314"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="319"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="324"/>
        <source>&amp;Open URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="329"/>
        <source>&amp;Save Attachment As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="334"/>
        <source>Add &amp;New Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="339"/>
        <source>&amp;View/Edit Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="344"/>
        <source>De&amp;lete Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="349"/>
        <source>&amp;Clone Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="354"/>
        <source>Search &amp;in Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="359"/>
        <source>Search in this &amp;Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="367"/>
        <source>Show &amp;Entry Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="375"/>
        <source>Hide &amp;Usernames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="383"/>
        <source>Hide &amp;Passwords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="391"/>
        <source>&amp;Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="399"/>
        <source>User&amp;name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="407"/>
        <source>&amp;URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="415"/>
        <source>&amp;Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="423"/>
        <source>&amp;Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="431"/>
        <source>E&amp;xpires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="439"/>
        <source>C&amp;reation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="447"/>
        <source>&amp;Last Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="455"/>
        <source>Last &amp;Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="463"/>
        <source>A&amp;ttachment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="487"/>
        <source>Show &amp;Statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="505"/>
        <source>&amp;Perform AutoType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="513"/>
        <source>&amp;16x16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="521"/>
        <source>&amp;22x22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="529"/>
        <source>2&amp;8x28</source>
        <translation type="unfinished">28x28 {2&amp;8x?}</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="544"/>
        <source>&amp;New Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="549"/>
        <source>&amp;Password Generator...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="560"/>
        <source>&amp;Group (search results only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="565"/>
        <source>Show &amp;Expired Entries...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="578"/>
        <source>&amp;Add Bookmark...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="583"/>
        <source>Bookmark &amp;this Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="299"/>
        <source>&amp;Add New Subgroup...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="588"/>
        <source>Copy URL to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>ブックマークの管理</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="31"/>
        <source>Enter Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="34"/>
        <source>Set Master Key</source>
        <translation type="unfinished">マスター キーの設定</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="37"/>
        <source>Change Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="41"/>
        <source>Database Key</source>
        <translation type="unfinished">データベース キー</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="131"/>
        <source>Last File</source>
        <translation type="unfinished">最後のファイル</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="177"/>
        <source>Select a Key File</source>
        <translation type="unfinished">キー ファイルの選択</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="341"/>
        <source>All Files (*)</source>
        <translation type="unfinished">すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="341"/>
        <source>Key Files (*.key)</source>
        <translation type="unfinished">キー ファイル (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="205"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="unfinished">パスワードを入力するかキー ファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="210"/>
        <source>Please enter a Password.</source>
        <translation type="unfinished">パスワードを入力してください。</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="215"/>
        <source>Please provide a key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="261"/>
        <source>%1:
No such file or directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="227"/>
        <source>The selected key file or directory is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="246"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="unfinished">ディレクトリがキー ファイルを含みません。</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="252"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="265"/>
        <source>%1:
File is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="339"/>
        <source>Create Key File...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="" line="279"/>
        <source>TextLabel</source>
        <translation type="obsolete">TextLabel</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>パスワードを入力するかキー ファイルを選択します。</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>キー</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>パスワード:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key file or directory:</source>
        <translation type="obsolete">キー ファイルまたはディレクトリ:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="185"/>
        <source>&amp;Browse...</source>
        <translation>参照(&amp;B)...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="188"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">パスワードとキー ファイルを使用する</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Exit</source>
        <translation type="obsolete">終了</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Password Repet.:</source>
        <translation type="obsolete">パスワードの反復:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>最後のファイル</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="157"/>
        <source>Key File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="197"/>
        <source>Generate Key File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="229"/>
        <source>Please repeat your password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="241"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="268"/>
        <source>Passwords are not equal.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="279"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">ファイル &apos;%1&apos; が見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Error</source>
        <translation type="obsolete">エラー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Never</source>
        <translation type="obsolete">しない</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Initialization failed.</source>
        <translation type="obsolete">初期化が失敗しました。</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="120"/>
        <source>Could not locate library file.</source>
        <translation type="obsolete">ライブラリ ファイルを検索できませんでした。</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>検索</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="90"/>
        <source>A&amp;nhang</source>
        <translation>アンハング(&amp;N)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>検索...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="180"/>
        <source>Search For:</source>
        <translation>検索する文字列:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="158"/>
        <source>Regular E&amp;xpression</source>
        <translation>正規表現(&amp;X)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="148"/>
        <source>&amp;Case Sensitive</source>
        <translation>大文字と小文字を区別する(&amp;C)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="68"/>
        <source>Include:</source>
        <translation>範囲:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="130"/>
        <source>&amp;Titles</source>
        <translation>タイトル(&amp;T)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="120"/>
        <source>&amp;Usernames</source>
        <translation>ユーザー名(&amp;U)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="110"/>
        <source>C&amp;omments</source>
        <translation>コメント(&amp;O)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="100"/>
        <source>U&amp;RLs</source>
        <translation>URL(&amp;R)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="80"/>
        <source>Pass&amp;words</source>
        <translation>パスワード(&amp;W)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search</source>
        <translation type="obsolete">検索</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Clo&amp;se</source>
        <translation type="obsolete">閉じる(&amp;S)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="168"/>
        <source>Include Subgroups (recursive)</source>
        <translation>サブグループを含める (再帰的)</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>アイコンの選択</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">カスタム アイコンの追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Pick</source>
        <translation type="obsolete">抽出</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="194"/>
        <source>Alt+Ö</source>
        <translation>Shift+Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="981"/>
        <source>Clear clipboard after:</source>
        <translation>クリップボードをクリアする:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Seconds</source>
        <translation type="obsolete">秒後</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="953"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation type="obsolete">外観(&amp;R)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="477"/>
        <source>Banner Color</source>
        <translation>バナーの色</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="491"/>
        <source>Text Color:</source>
        <translation>テキストの色:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="623"/>
        <source>Change...</source>
        <translation>変更...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="548"/>
        <source>Color 2:</source>
        <translation>色 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="676"/>
        <source>C&amp;hange...</source>
        <translation>変更(&amp;H)...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="679"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="692"/>
        <source>Color 1:</source>
        <translation>色 1:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Browser Command:</source>
        <translation type="obsolete">ブラウザ コマンド:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="720"/>
        <source>Alternating Row Colors</source>
        <translation>交互の列の色</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1132"/>
        <source>Browse...</source>
        <translation>参照...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="222"/>
        <source>Remember last key type and location</source>
        <translation>最後のキーの種類と場所を記憶する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="191"/>
        <source>Remember last opened file</source>
        <translation>最後に開かれたファイルを記憶する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="51"/>
        <source>The integration plugins provide features like usage of the native file dialogs and message boxes of the particular desktop environments.</source>
        <translation type="obsolete">統合プラグインは特定のデスクトップ環境のネイティブのファイル ダイアログとメッセージボックスの使用のような機能を供給します。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="54"/>
        <source>General</source>
        <translation type="obsolete">全般</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="122"/>
        <source>Show system tray icon</source>
        <translation>システム トレイ アイコンを表示する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="180"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>メイン ウィンドウの閉じるボタンのクリック時にトレイへ最小化する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="298"/>
        <source>Save recent directories of file dialogs</source>
        <translation>ファイル ダイアログの最近のディレクトリを保存する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="761"/>
        <source>Group tree at start-up:</source>
        <translation>起動時のグループ ツリー:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="768"/>
        <source>Restore last state</source>
        <translation>最後の状態を復元する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="775"/>
        <source>Expand all items</source>
        <translation>すべてのアイテムを展開する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="782"/>
        <source>Do not expand any item</source>
        <translation>すべてのアイテムを展開しない</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="94"/>
        <source>Security</source>
        <translation>セキュリティ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show passwords in plain text in:</source>
        <translation type="obsolete">プレーン テキストでパスワードを表示する:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="950"/>
        <source>Edit Entry Dialog</source>
        <translation>[エントリの編集] ダイアログ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key Dialogs</source>
        <translation type="obsolete">[キー] ダイアログ</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="864"/>
        <source>Desktop Integration</source>
        <translation type="obsolete">デスクトップ統合</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1333"/>
        <source>Plug-Ins</source>
        <translation>プラグイン</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1342"/>
        <source>None</source>
        <translation>なし</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1349"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Gnome デスクトップ統合 (Gtk 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1356"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>KDE 4 デスクトップ統合</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1371"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>変更を影響させる前にプログラムを再起動する必要があります。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1391"/>
        <source>Configure...</source>
        <translation>構成...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="99"/>
        <source>Advanced</source>
        <translation>詳細設定</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="321"/>
        <source>Clear History Now</source>
        <translation>今すぐ履歴をクリア</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="343"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>常にエントリまたはグループの削除前に質問する</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unified Title and Toolbar</source>
        <translation type="obsolete">統一タイトルとツール バー</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="732"/>
        <source>Customize Entry Detail View...</source>
        <translation>エントリの詳細表示のカスタマイズ...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="827"/>
        <source>Features</source>
        <translation type="obsolete">機能</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1285"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>ユーザー インターフェイスをスリムに維持する際は必要に応じてここで KeePassX のいくつかの機能を無効にできます。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1295"/>
        <source>Bookmarks</source>
        <translation>ブックマーク</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1151"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>自動入力の微調整</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1160"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>ユーザーによる自動入力のアクティブ化と最初のシミュレート済みキー ストロークの間の時間です。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1209"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1173"/>
        <source>Pre-Gap:</source>
        <translation>プリギャップ:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1196"/>
        <source>Key Stroke Delay:</source>
        <translation>キー ストロークの遅延:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1206"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>2 つのシミュレート済みキー ストロークの間の遅延です。自動入力がランダムに文字をスキップする場合はこれを上げます。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1125"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>CD やメモリ スティックのようなストレージ デバイスが通常マウントされるディレクトリです。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1115"/>
        <source>Media Root:</source>
        <translation>メディア ルート:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>System Default</source>
        <translation type="obsolete">システム既定</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1141"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>ブックマークとそれらの絶対パスから独立した最後に開かれたファイルを使用したい場合はこれを有効にします。これは特にポータブルに KeePassX を使用しそのためファイル システムのマウント ポイントの変更があるときに有用です。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1144"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>相対パス (ブックマークと最後のファイル) を保存する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="150"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>タスク バーの代わりにトレイへ最小化する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="252"/>
        <source>Start minimized</source>
        <translation>最小化済みで起動する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="282"/>
        <source>Start locked</source>
        <translation>ロック済みで起動する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1019"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>メイン ウィンドウの最小化時にワークスペースをロックする</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1091"/>
        <source>Custom Browser Command</source>
        <translation>カスタム ブラウザ コマンド</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1103"/>
        <source>Browse</source>
        <translation>参照</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1224"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>グローバル自動入力ショートカット:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Use entry titles to match the window for Global Auto-Type</source>
        <translation type="obsolete">グローバグ自動入力のウィンドウへの一致にエントリのタイトルを使用する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="438"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="941"/>
        <source>Show plain text passwords in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="960"/>
        <source>Database Key Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1057"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1028"/>
        <source>Lock database after inactivity of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1253"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="74"/>
        <source>General (1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="79"/>
        <source>General (2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="84"/>
        <source>Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="89"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="375"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="403"/>
        <source>Delete backup entries older than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="429"/>
        <source>days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="445"/>
        <source>Automatically save database after every change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="837"/>
        <source>System Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="842"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="867"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="874"/>
        <source>Author:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>パスワードを入力します</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>パスワード:</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../lib/tools.cpp" line="338"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="unfinished">Nardog</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="unfinished">http://nardog.takoweb.com</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="337"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>Recycle Bin</source>
        <translation type="obsolete">ごみ箱</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Double click on an entry to restore it.</source>
        <translation type="obsolete">復元するエントリ上でダブル クリックします。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Group</source>
        <translation type="obsolete">グループ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Title</source>
        <translation type="obsolete">タイトル</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Username</source>
        <translation type="obsolete">ユーザー名</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Expired</source>
        <translation type="obsolete">満了</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Empty Recycle Bin</source>
        <translation type="obsolete">ごみ箱を空にする</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Close</source>
        <translation type="obsolete">閉じる</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>フォーム</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;ワークスペースはロックされています。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>ロック解除</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>データベースを閉じる</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <location filename="" line="279"/>
        <source>Database Settings</source>
        <translation type="obsolete">データベースの設定</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Encryption</source>
        <translation type="obsolete">暗号化</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Algorithm:</source>
        <translation type="obsolete">アルゴリズム:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Encryption Rounds:</source>
        <translation type="obsolete">暗号化の周囲:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>O&amp;K</source>
        <translation type="obsolete">OK(&amp;K)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Ctrl+K</source>
        <translation type="obsolete">Ctrl+K</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">キャンセル(&amp;C)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Ctrl+C</source>
        <translation type="obsolete">Ctrl+C</translation>
    </message>
</context>
</TS>
