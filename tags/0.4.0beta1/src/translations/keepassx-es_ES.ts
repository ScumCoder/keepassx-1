<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="en_US">
<defaultcodec></defaultcodec>
<context>
    <name>@default</name>
    <message>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">No se pudo abrir el archivo (FileError=%1)</translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Traducción actual: Español&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Autor:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Jaroslaw Filiochowski, Marcos del Puerto</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">jarfil@jarfil.net, mpuertog@gmail.com</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>Equipo</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation>Desarrollador, Administrador del Proyecto</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation>Diseñador web</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation>Agradecimientos</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation>Parches para mejorar la compatibilidad con MacOS X</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation>Icono de la aplicación principal</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation>Múltiples correcciones y mejoras</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Archivo &apos;%1&apos; no encontrado.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>Asegúrese de que el programa está instalado correctamente.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">No se pudo abrir fichero &apos;%1&apos;</translation>
    </message>
    <message>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">Ha ocurrido el siguiente error:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation>Desarrollador</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>Se puede encontrar información sobre cómo traducir KeePassX en:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation>Traducción actual</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation>Por favor, sustituir &apos;Ninguno&apos; con el idioma de la traducción</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="50"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <source>Thanks To</source>
        <translation type="obsolete">Agradecimientos</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="213"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="188"/>
        <source>Translation</source>
        <translation>Traducción</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="163"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>http://keepassx.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="74"/>
        <source>AppName</source>
        <translation>NombreApl</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="92"/>
        <source>AppFunc</source>
        <translation>NombreFun</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="151"/>
        <source>Copyright (C) 2005 - 2008 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2008 KeePassX Team KeePassX se distribuye bajo los términos de la  General Public License (GPL) versión 2.</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2008 KeePassX Team KeePassX se distribuye bajo los términos de la  General Public License (GPL) versión 2. {2005 ?} {2009 ?} {2.?}</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX se distribuye bajo los términos de la
General Public License (GPL) versión 2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>Añadir marcador</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>Archivo:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>Navegar...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>Editar marcador</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>Base de datos de KeePass (*.kdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="68"/>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation type="obsolete">Se ha encontrado más de una secuencia de teclas para &apos;auto-escritura:&apos;.Sólo se permite una por entrada.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="95"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="obsolete">Error sintáctico en la secuencia de auto-escritura cerca del carácter %1
<byte value="x9"/>Se encontró &apos;{&apos; sin un &apos;}&apos; de cierrre.</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="133"/>
        <source>Auto-Type string contains invalid characters</source>
        <translation type="obsolete">Alguno de los caracteres de la cadena de auto-escritura no son válidos.</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - Auto-escritura</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>Hacer click en una entrada para auto-escribirla.</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="89"/>
        <source>Auto-Type</source>
        <translation>Auto-escritura</translation>
    </message>
</context>
<context>
    <name>CAboutDialog</name>
    <message>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Archivo &apos;%1&apos; no encontrado.</translation>
    </message>
    <message>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="obsolete">Asegúrese de que el programa está instalado correctamente.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">No se pudo abrir fichero &apos;%1&apos;</translation>
    </message>
    <message>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">Ha ocurrido el siguiente error:
%1</translation>
    </message>
    <message>
        <source>http://keepass.berlios.de/index.php</source>
        <translation type="obsolete">http://keepass.berlios.de/index.php</translation>
    </message>
    <message>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Traducción actual: Español&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Autor:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <source>$TRANSALTION_AUTHOR</source>
        <translation type="obsolete">Jaroslaw Filiochowski</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">jarfil@jarfil.net</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/translation-howto.html</source>
        <translation type="obsolete">Puede encontrar información sobre cómo traducir KeePassX en esta dirección:
http://keepass.berlios.de/translation-howto.html</translation>
    </message>
    <message>
        <source>Thanks To</source>
        <translation type="obsolete">Agradecimientos</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 Bits   (por defecto)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 Bits</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Defina el número de iteraciones de cifrado.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="71"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; no es un valor entero válido.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>El númer de iteraciones de cifrado debe ser mayor que 0.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Preferencias</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Las contraseñas no coinciden.
Compruebe que las ha introducido correctamente.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="334"/>
        <source>Save Attachment...</source>
        <translation>Guardar Adjunto...</translation>
    </message>
    <message>
        <source>Overwrite?</source>
        <translation type="obsolete">Sobreescribir?</translation>
    </message>
    <message>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation type="obsolete">Ya existe un archivo con este nombre.
¿Desea reemplazarlo?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">No</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Could not remove old file.</source>
        <translation type="obsolete">No se pudo eliminar el archivo anterior.</translation>
    </message>
    <message>
        <source>Could not create new file.</source>
        <translation type="obsolete">No se pudo crear el nuevo archivo.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Error while writing the file.</source>
        <translation>Error al escribir el archivo.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>Delete Attachment?</source>
        <translation>Eliminar Adjunto?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Está a punto de eliminar el adjunto de esta entrada.
¿Está seguro?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="362"/>
        <source>No, Cancel</source>
        <translation>No, Cancelar</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation>Editar entrada</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="341"/>
        <source>Could not open file.</source>
        <translation>No se pudo abrir el archivo.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="304"/>
        <source>Add Attachment...</source>
        <translation>Adjuntar archivo...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>La entrada seleccionada no contiene un archivo adjunto o está vacio.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation>Hoy</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation>1 Semana</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation>2 Semanas</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation>3 Semanas</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation>1 Mes</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation>3 Meses</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation>6 Meses</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation>1 Año</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation>Calendario...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="259"/>
        <source>[Untitled Entry]</source>
        <translation>[Entrada sin nombre]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation>Nueva entrada</translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation type="obsolete">Notificación</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation type="obsolete">Debe introducir al menos un carácter</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation type="obsolete">Aceptar</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Could not open &apos;/dev/random&apos; or &apos;/dev/urandom&apos;.</source>
        <translation type="obsolete">No se pudo abrir &apos;/dev/random&apos; o &apos;/dev/urandom&apos;.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="121"/>
        <source>Password Generator</source>
        <translation>Generador de contraseñas</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="270"/>
        <source>%1 Bits</source>
        <translation>%1 Bits</translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">Aceptar</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Please enter a Password.</source>
        <translation type="obsolete">Introduzca una Contraseña.</translation>
    </message>
    <message>
        <source>Please choose a key file.</source>
        <translation type="obsolete">Seleccione un archivo de clave.</translation>
    </message>
    <message>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">Introduzca una Contraseña o seleccione un archivo de clave.</translation>
    </message>
    <message>
        <source>Database Key</source>
        <translation type="obsolete">Contraseña de la Base de Datos</translation>
    </message>
    <message>
        <source>Select a Key File</source>
        <translation type="obsolete">Seleccione un Archivo de Clave</translation>
    </message>
    <message>
        <source>*.key</source>
        <translation type="obsolete">*.key</translation>
    </message>
    <message>
        <source>Unexpected Error: File does not exist.</source>
        <translation type="obsolete">Error Inesperado: Archivo no existe.</translation>
    </message>
    <message>
        <source>The selected key file or directory does not exist.</source>
        <translation type="obsolete">El archivo de clave seleccionado o el directorio no existen.</translation>
    </message>
    <message>
        <source>The given directory does not contain any key files.</source>
        <translation type="obsolete">El directorio no contiene ningún archivo de clave.</translation>
    </message>
    <message>
        <source>The given directory contains more then one key file.
Please specify the key file directly.</source>
        <translation type="obsolete">El directorio contiene más de un archivo de clave.
Especifique un archivo de clave concreto.</translation>
    </message>
    <message>
        <source>The key file found in the given directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">El archivo de clave encontrado en el directorio no se puede leer.
Compruebe sus permisos de acceso.</translation>
    </message>
    <message>
        <source>Key file could not be found.</source>
        <translation type="obsolete">Archivo de clave no encontrado.</translation>
    </message>
    <message>
        <source>Key file is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Archivo de clave no se puede leer.
Compruebe sus permisos de acceso.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Advertencia</translation>
    </message>
    <message>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">Las contraseñas no coinciden.
Compruebe que las ha introducido correctamente.</translation>
    </message>
    <message>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">Introduzca una contraseña o seleccione un archivo de clave.</translation>
    </message>
    <message>
        <source>A file with the name &apos;pwsafe.key&apos; already exisits in the given directory.
Do you want to replace it?</source>
        <translation type="obsolete">El archivo &apos;pwsafe.key&apos; ya existe en el directorio.
¿Desea reemplazarlo?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Sí</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">No</translation>
    </message>
    <message>
        <source>The exisiting file is not writable.</source>
        <translation type="obsolete">El archivo existente no se puede escribir.</translation>
    </message>
    <message>
        <source>A file with the this name already exisits.
Do you want to replace it?</source>
        <translation type="obsolete">Existe un archivo con ese nombre.
¿Desea reemplazarlo?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
</context>
<context>
    <name>CSearchDlg</name>
    <message>
        <source>Notice</source>
        <translation type="obsolete">Aviso</translation>
    </message>
    <message>
        <source>Please enter a search string.</source>
        <translation type="obsolete">Introduzca una cadena de búsqueda.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Aceptar</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Buscar</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Add Icons...</source>
        <translation>Añadir iconos...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="94"/>
        <source>Images (%1)</source>
        <translation>Imágenes (%1)</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Sustituir...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>An error occured while loading the icon.</source>
        <translation>Se produjo un error al cargar el icono.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>Añadir icono personalizado</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>Seleccionar</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: No se puede cargar el archivo.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="68"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>Se produjo un error al cargar los iconos:</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="349"/>
        <source>Settings</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="354"/>
        <source>Select a directory...</source>
        <translation>Seleccione un directorio...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="361"/>
        <source>Select an executable...</source>
        <translation>Seleccionar un ejecutable...</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>Calendario</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>Recopilar entropía</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>Generador de números aleatorios</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>Recopilando entropía...Por favor, mueve el ratón y/o pulsa algunas teclas hasta que se recopile suficiente entropía para la semilla del generador de números aleatorios.</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;¡Creada la semilla del fondo aleatorio con éxito!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>Nombre del adjunto</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>Fecha de creación</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>Fecha de último acceso</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>Fecha de última modificación</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>Fecha de expiración</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>Tiempo hasta la expiración</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation>Editor de texto enriquecido</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation>Cursiva</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation>Subrayado</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation>Alineado a la izquierda</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation>Centrado</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation>Alineado a la derecha</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation>Justificado</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation>Color del texto</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation>Tamaño de fuente</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation>Plantillas</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>Configuración de la base de datos</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>Cifrado</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>Algoritmo:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>Iteraciones de cifrado:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation>Calcular iteraciones de 1 segundo de espera en este ordenador</translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation>Creación</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation>Último Acceso</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation>Última modificación</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation>Expiración</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Editar Entrada</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="140"/>
        <source>Username:</source>
        <translation>Usuario:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="198"/>
        <source>Password Repet.:</source>
        <translation>Contraseña (repetida):</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="127"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="166"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="211"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="101"/>
        <source>Quality:</source>
        <translation>Calidad:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="153"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="224"/>
        <source>Expires:</source>
        <translation>Expira:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="179"/>
        <source>Group:</source>
        <translation>Grupo:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="338"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="450"/>
        <source>Icon:</source>
        <translation>Icono:</translation>
    </message>
    <message>
        <source>% Bit</source>
        <translation type="obsolete">% Bits</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="52"/>
        <source>Ge&amp;n.</source>
        <translation>Ge&amp;n.</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Aceptar</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="371"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="114"/>
        <source>Attachment:</source>
        <translation>Adjunto:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="407"/>
        <source>%1 Bit</source>
        <translation>%1 Bit</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Propiedades del Grupo</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>Icono:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Aceptar</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>Entidades expiradas</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>Doble click en una entrada para ir a ella.</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>Expirada</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>Entradas expiradas en la base de datos</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>Archivos XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>Archivo XML de KeePassX</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">No se pudo abrir el archivo (FileError=%1)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>Archivos de texto (*.txt)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>Archivo de texto</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="obsolete">Importar archivo...</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>Falló la exportación</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="59"/>
        <source>No error occurred.</source>
        <translation>No se produjeron errores.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="60"/>
        <source>An error occurred while reading from the file.</source>
        <translation>Se produjo un error al leer el archivo.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="61"/>
        <source>An error occurred while writing to the file.</source>
        <translation>Se produjo un error al escribir en el archivo.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>A fatal error occurred.</source>
        <translation>Se produjo un error fatal.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An resource error occurred.</source>
        <translation>Se produjo un error de recursos.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>The file could not be opened.</source>
        <translation>No se puede abrir el archivo.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>The operation was aborted.</source>
        <translation>Se abortó la operación.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>A timeout occurred.</source>
        <translation>Venció un temporizador.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>An unspecified error occurred.</source>
        <translation>Se produjo un error no especificado.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The file could not be removed.</source>
        <translation>No se puede eliminar el archivo.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>The file could not be renamed.</source>
        <translation>No se puede renombrar el archivo.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>The position in the file could not be changed.</source>
        <translation>No se puede cambiar la posición en el archivo.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be resized.</source>
        <translation>No se puede redimensionar el archivo.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be accessed.</source>
        <translation>No se puede acceder al archivo.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The file could not be copied.</source>
        <translation>No se puede copiar el archivo.</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="141"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="118"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="399"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="95"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Generador de Contraseña</translation>
    </message>
    <message>
        <source>Accep&amp;t</source>
        <translation type="obsolete">&amp;Aceptar</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="472"/>
        <source>Generate</source>
        <translation>Generar</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="455"/>
        <source>New Password:</source>
        <translation>Nueva Contraseña:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="369"/>
        <source>Quality:</source>
        <translation>Calidad:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="319"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="138"/>
        <source>&amp;Upper Letters</source>
        <translation>Mayúsc&amp;ulas</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="92"/>
        <source>&amp;Lower Letters</source>
        <translation>Minúscu&amp;las</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="115"/>
        <source>&amp;Numbers</source>
        <translation>&amp;Números</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="158"/>
        <source>&amp;Special Characters</source>
        <translation>Carácteres E&amp;speciales</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation type="obsolete">Guión</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation type="obsolete">S&amp;ubrayado</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="169"/>
        <source>Use &amp;only following characters:</source>
        <translation>Usar sól&amp;o carácteres siguientes:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="172"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="333"/>
        <source>Length:</source>
        <translation>Longitud:</translation>
    </message>
    <message>
        <source>Use &quot;/dev/rando&amp;m&quot;</source>
        <translation type="obsolete">Usar &quot;/dev/rando&amp;m&quot;</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="56"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>Usar &amp;grupos de carácteres siguientes:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="59"/>
        <source>Alt+W</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation type="obsolete">E&amp;spacios</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="131"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="396"/>
        <source>Enable entropy collection</source>
        <translation>Habilitar la recopilación de entropía</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="425"/>
        <source>Collect only once per session</source>
        <translation>Recopilar sólo una vez por sesión</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="105"/>
        <source>&amp;Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="128"/>
        <source>&amp;White Spaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>&amp;Minus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="209"/>
        <source>Exclude look-alike characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="216"/>
        <source>Ensure that password contains characters from every group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="232"/>
        <source>Pronounceable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="256"/>
        <source>Lower Letters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="263"/>
        <source>Upper Letters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="270"/>
        <source>Numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="277"/>
        <source>Special Characters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>Archivos XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>Falló la importación</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Datos XML no válidos (ver stdout para más detalles).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>Archivo XML no válido.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>El documento no contiene datos.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>Archivo XML de KeePassX</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>Falló la importación</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>Error de análisis sintáctico de XML en la línea %1 columna %2:%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>Error de análisis sintáctico: El archivo no es un archivo XML de KeePassX válido.</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>Archivo de PwManager (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation>Falló la importación</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation>Archivo vacío.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation>El archivo no es un archivo PwManager válido.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation>Version de archivo no compatible.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation>Algoritmo hash no compatible.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>Algoritmo de cifrado no compatible.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation>No se admiten todavía archivos comprimidos.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation>Contraseña incorrecta.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation>El archivo está dañado (comprobación hash fallida).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Datos XML no válidos (ver stdout para más detalles).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>Importar archivo...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>Falló la importación</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="511"/>
        <source>Could not open file.</source>
        <translation>No se pudo abrir el archivo.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="522"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>Tamaño de fichero no esperado (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="539"/>
        <source>Wrong Signature</source>
        <translation>Firma Incorrecta</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="544"/>
        <source>Unsupported File Version.</source>
        <translation>Versión de archivo no compatible.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="553"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>Algoritmo de cifrado desconocido.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="588"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>Falló el descifrado.La contraseña es incorrecta o el archivo está dañado.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="608"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>Comprobación hash fallida.
La clave es incorecta o el fichero está dañado.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="693"/>
        <source>Invalid group tree.</source>
        <translation>Árbol de grupos no válido.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="890"/>
        <source>Key file is empty.</source>
        <translation>El archivo de claves está vacio.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1271"/>
        <source>The database must contain at least one group.</source>
        <translation>La base de datos tiene que contener al menos un grupo.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1295"/>
        <source>Could not open file for writing.</source>
        <translation>No se puede abrir el archivo para escritura.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="687"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>Error inesperado: El desplazamiento está fuera del rango.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="580"/>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1135"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1143"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1149"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1154"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="481"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="483"/>
        <source>Username</source>
        <translation>Usuario</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="485"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="487"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="489"/>
        <source>Comments</source>
        <translation>Comentarios</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="491"/>
        <source>Expires</source>
        <translation>Expira</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="493"/>
        <source>Creation</source>
        <translation>Creación</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="495"/>
        <source>Last Change</source>
        <translation>Último Cambio</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="497"/>
        <source>Last Access</source>
        <translation>Último Acceso</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="499"/>
        <source>Attachment</source>
        <translation>Adjunto</translation>
    </message>
    <message>
        <source>%1 items</source>
        <translation type="obsolete">%1 elementos</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="148"/>
        <source>Delete?</source>
        <translation>¿Borrar?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="501"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>Tiene que existir al menos un grupo antes de añadir una entrada.</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="255"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="145"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation>¿Está seguro de que quiere borrar esta entrada?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation>¿Está seguro de que quiere borrar estas %1 entradas?</translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="57"/>
        <source>Search Results</source>
        <translation>Resultados de Búsqueda</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation type="obsolete">Grupos</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="88"/>
        <source>Delete?</source>
        <translation>¿Borrar?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="86"/>
        <source>Are you sure you want to delete this group, all it&apos;s child groups and all their entries?</source>
        <translation type="obsolete">¿Está seguro de que quiere borrar este grupo, todos los grupos hijos y todas las entradas?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="89"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="353"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="374"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="375"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="515"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>The following error occured while opening the database:
%1</source>
        <translation type="obsolete">Ocurrió el siguiente error al intentar abrir la base de datos:
%1</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Aceptar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="501"/>
        <source>Save modified file?</source>
        <translation>¿Guardar archivo modificado?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation type="obsolete">El archivo actual ha sido modificado.
¿Desea guardar los cambios?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Sí</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">No</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <source>&lt;B&gt;Group: &lt;/B&gt;%1  &lt;B&gt;Title: &lt;/B&gt;%2  &lt;B&gt;Username: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Password: &lt;/B&gt;%5  &lt;B&gt;Creation: &lt;/B&gt;%6  &lt;B&gt;Last Change: &lt;/B&gt;%7  &lt;B&gt;LastAccess: &lt;/B&gt;%8  &lt;B&gt;Expires: &lt;/B&gt;%9</source>
        <translation type="obsolete">&lt;b&gt;Grupo: &lt;/b&gt;%1  &lt;b&gt;Título: &lt;/b&gt;%2  &lt;b&gt;Usuario: &lt;/b&gt;%3  &lt;b&gt;URL: &lt;/b&gt;&lt;a href=&quot;%4&quot;&gt;%4&lt;/a&gt;  &lt;b&gt;Contraseña: &lt;/b&gt;%5  &lt;b&gt;Creación: &lt;/b&gt;%6  &lt;b&gt;Último Cambio: &lt;/b&gt;%7  &lt;b&gt;Último Acceso: &lt;/b&gt;%8  &lt;b&gt;Expira: &lt;/b&gt;%9</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="834"/>
        <source>Clone Entry</source>
        <translation>Duplicar Entrada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="836"/>
        <source>Delete Entry</source>
        <translation>Eliminar Entrada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="849"/>
        <source>Clone Entries</source>
        <translation>Duplicar Entradas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="851"/>
        <source>Delete Entries</source>
        <translation>Eliminar Entradas</translation>
    </message>
    <message>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">Archivo no ha podido ser guardado.
%1</translation>
    </message>
    <message>
        <source>Save Database As...</source>
        <translation type="obsolete">Guardar Base de Datos Como...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Ready</source>
        <translation>Listo</translation>
    </message>
    <message>
        <source>[new]</source>
        <translation type="obsolete">[nuevo]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="580"/>
        <source>Open Database...</source>
        <translation>Abrir Base de Datos...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="439"/>
        <source>Loading Database...</source>
        <translation>Cargando Base de Datos...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="461"/>
        <source>Loading Failed</source>
        <translation>Carga Fallida</translation>
    </message>
    <message>
        <source>Could not create key file. The following error occured:
%1</source>
        <translation type="obsolete">No se ha podido crear archivo de clave. Ha ocurrido el siguiente error:
%1</translation>
    </message>
    <message>
        <source>Export To...</source>
        <translation type="obsolete">Exportar A...</translation>
    </message>
    <message>
        <source>KeePassX [new]</source>
        <translation type="obsolete">KeePassX [nuevo]</translation>
    </message>
    <message>
        <source>Unknown error in Import_PwManager::importFile()()</source>
        <translation type="obsolete">Error desconocido en Import_PwManager::importFile()()</translation>
    </message>
    <message>
        <source>Unknown error in Import_KWalletXml::importFile()</source>
        <translation type="obsolete">Error desconocido en Import_KWalletXml::importFile()</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <source>Show Toolbar</source>
        <translation type="obsolete">Mostrar Barra de herramientas</translation>
    </message>
    <message>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="463"/>
        <source>Unknown error while loading database.</source>
        <translation>Error desconocido al cargar la base de datos.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>Base de datos de KeePass (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="886"/>
        <source>Save Database...</source>
        <translation>Guardar base de datos</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="722"/>
        <source>1 Month</source>
        <translation>1 Mes</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="724"/>
        <source>%1 Months</source>
        <translation>%1 Meses</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>1 Year</source>
        <translation>1 Año</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="732"/>
        <source>%1 Years</source>
        <translation>%1 Años</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="738"/>
        <source>1 Day</source>
        <translation>1 Día</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="740"/>
        <source>%1 Days</source>
        <translation>%1 Días</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="744"/>
        <source>less than 1 day</source>
        <translation>menos de 1 día</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1301"/>
        <source>Locked</source>
        <translation>Bloqueado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1316"/>
        <source>Unlocked</source>
        <translation>Desbloqueado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="484"/>
        <source>The database file does not exist.</source>
        <translation>El archivo de la base de datos no existe.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1090"/>
        <source>new</source>
        <translation>nuevo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="708"/>
        <source>Expired</source>
        <translation>Expirada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1302"/>
        <source>Un&amp;lock Workspace</source>
        <translation>Desb&amp;loquear área de trabajo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>&amp;Lock Workspace</source>
        <translation>B&amp;loquear área de trabajo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="465"/>
        <source>The following error occured while opening the database:</source>
        <translation>Se produjo el siguiente errore al abrir la base de datos:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="890"/>
        <source>File could not be saved.</source>
        <translation>No se puedo guardar el archivo.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="302"/>
        <source>Show &amp;Toolbar</source>
        <translation>Mostrar &amp;Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="401"/>
        <source>Database locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="405"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="445"/>
        <source>Couldn&apos;t create database lock file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="502"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="515"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="144"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Archivo &apos;%1&apos; no encontrado.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Archivo</translation>
    </message>
    <message>
        <source>Import from...</source>
        <translation type="obsolete">Importar desde...</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="obsolete">Ver</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="obsolete">Columnas</translation>
    </message>
    <message>
        <source>Extras</source>
        <translation type="obsolete">Preferencias</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Ayuda</translation>
    </message>
    <message>
        <source>New Database...</source>
        <translation type="obsolete">Nueva Base de Datos...</translation>
    </message>
    <message>
        <source>Open Database...</source>
        <translation type="obsolete">Abrir Base de Datos...</translation>
    </message>
    <message>
        <source>Close Database</source>
        <translation type="obsolete">Cerrar Base de Datos</translation>
    </message>
    <message>
        <source>Save Database</source>
        <translation type="obsolete">Guardar Base de Datos</translation>
    </message>
    <message>
        <source>Save Database As...</source>
        <translation type="obsolete">Guardar Base de Datos Como...</translation>
    </message>
    <message>
        <source>Database Settings...</source>
        <translation type="obsolete">Preferencias de Base de Datos...</translation>
    </message>
    <message>
        <source>Change Master Key...</source>
        <translation type="obsolete">Cambiar Clave Maestra...</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Salir</translation>
    </message>
    <message>
        <source>PwManager File (*.pwm)</source>
        <translation type="obsolete">PwManager (*.pwm)</translation>
    </message>
    <message>
        <source>KWallet XML-File (*.xml)</source>
        <translation type="obsolete">KWallet, archivo XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="593"/>
        <source>Add New Group...</source>
        <translation type="unfinished">Añadir Nuevo Grupo...</translation>
    </message>
    <message>
        <source>Edit Group...</source>
        <translation type="obsolete">Editar Grupo...</translation>
    </message>
    <message>
        <source>Delete Group</source>
        <translation type="obsolete">Eliminar Grupo</translation>
    </message>
    <message>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">Copiar Contraseña al Portapapeles</translation>
    </message>
    <message>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">Copiar Usuario al Portapapeles</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation type="obsolete">Abrir URL</translation>
    </message>
    <message>
        <source>Save Attachment As...</source>
        <translation type="obsolete">Guardar Adjunto Como...</translation>
    </message>
    <message>
        <source>Add New Entry...</source>
        <translation type="obsolete">Añadir Nueva Entrada...</translation>
    </message>
    <message>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">Ver/Editar Entrada...</translation>
    </message>
    <message>
        <source>Delete Entry</source>
        <translation type="obsolete">Eliminar Entrada</translation>
    </message>
    <message>
        <source>Clone Entry</source>
        <translation type="obsolete">Duplicar Entrada</translation>
    </message>
    <message>
        <source>Search In Database...</source>
        <translation type="obsolete">Buscar en Base de Datos...</translation>
    </message>
    <message>
        <source>Search in this group...</source>
        <translation type="obsolete">Buscar en este grupo...</translation>
    </message>
    <message>
        <source>Show Toolbar</source>
        <translation type="obsolete">Mostrar Barra de herramientas</translation>
    </message>
    <message>
        <source>Show Entry Details</source>
        <translation type="obsolete">Mostrar Detalles de Entradas</translation>
    </message>
    <message>
        <source>Hide Usernames</source>
        <translation type="obsolete">Ocultar Usuarios</translation>
    </message>
    <message>
        <source>Hide Passwords</source>
        <translation type="obsolete">Ocultar Contraseñas</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="obsolete">Título</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Usuario</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="obsolete">URL</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Contraseña</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="obsolete">Comentario</translation>
    </message>
    <message>
        <source>Expires</source>
        <translation type="obsolete">Expira</translation>
    </message>
    <message>
        <source>Creation</source>
        <translation type="obsolete">Creación</translation>
    </message>
    <message>
        <source>Last Change</source>
        <translation type="obsolete">Último Cambio</translation>
    </message>
    <message>
        <source>Last Access</source>
        <translation type="obsolete">Último Acceso</translation>
    </message>
    <message>
        <source>Attachment</source>
        <translation type="obsolete">Adjunto</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation type="obsolete">Preferencias...</translation>
    </message>
    <message>
        <source>About...</source>
        <translation type="obsolete">Acerca de...</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Editar</translation>
    </message>
    <message>
        <source>Show Statusbar</source>
        <translation type="obsolete">Mostrar Barra de estado</translation>
    </message>
    <message>
        <source>Export to...</source>
        <translation type="obsolete">Exportar a...</translation>
    </message>
    <message>
        <source>KeePassX Handbook...</source>
        <translation type="obsolete">Manual de KeePassX...</translation>
    </message>
    <message>
        <source>Plain Text (*.txt)</source>
        <translation type="obsolete">Texto Plano (*.txt)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="500"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="196"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="136"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="145"/>
        <source>&amp;Import from...</source>
        <translation>&amp;Importar desde...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="140"/>
        <source>&amp;Export to...</source>
        <translation>&amp;Exportar a...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="235"/>
        <source>E&amp;xtras</source>
        <translation>E&amp;xtras</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="256"/>
        <source>&amp;Open Database...</source>
        <translation>&amp;Abrir base de datos...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="261"/>
        <source>&amp;Close Database</source>
        <translation>&amp;Cerrar bases de datos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="266"/>
        <source>&amp;Save Database</source>
        <translation>&amp;Guardar base de datos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="271"/>
        <source>Save Database &amp;As...</source>
        <translation>Guardar base de datos &amp;como...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="276"/>
        <source>&amp;Database Settings...</source>
        <translation>Configuración de la base de &amp;datos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="281"/>
        <source>Change &amp;Master Key...</source>
        <translation>Cambiar contraseña &amp;maestra...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="468"/>
        <source>&amp;Settings...</source>
        <translation>&amp;Preferencias...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="476"/>
        <source>&amp;About...</source>
        <translation>&amp;Acerca de...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="492"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>Manual de &amp;KeePassX...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="534"/>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation>Base de datos estándar de KeePass para un único usuario (*.kdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="539"/>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation>Base de datos avanzada de KeePass (*.kxdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="570"/>
        <source>Recycle Bin...</source>
        <translation>Papelera de reciclaje...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="55"/>
        <source>Groups</source>
        <translation>Grupos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Lock Workspace</source>
        <translation>B&amp;loquear área de trabajo</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="150"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Marcadores</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="200"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation>Tamaño de los &amp;iconos de la barra de herramientas</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="208"/>
        <source>&amp;Columns</source>
        <translation>&amp;Columnas</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="251"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation>&amp;Gestionar marcadores</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="291"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Add New Group...</source>
        <translation type="obsolete">&amp;Añadir nuevo grupo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="304"/>
        <source>&amp;Edit Group...</source>
        <translation>&amp;Editar Grupo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="309"/>
        <source>&amp;Delete Group</source>
        <translation>&amp;Borrar grupo</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="314"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation>Copiar la con&amp;traseña al portapapeles</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="319"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation>Copiar la nombre de &amp;usuario al portapapeles</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="324"/>
        <source>&amp;Open URL</source>
        <translation>Abrir URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="329"/>
        <source>&amp;Save Attachment As...</source>
        <translation>&amp;Guardar archivo adjunto como...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="334"/>
        <source>Add &amp;New Entry...</source>
        <translation>Añadir &amp;nueva entrada...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="339"/>
        <source>&amp;View/Edit Entry...</source>
        <translation>&amp;Ver/Editar entrada...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="344"/>
        <source>De&amp;lete Entry</source>
        <translation>Bo&amp;rrar entrada</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="349"/>
        <source>&amp;Clone Entry</source>
        <translation>&amp;Clonar entrada</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="354"/>
        <source>Search &amp;in Database...</source>
        <translation>Buscar e&amp;n la base de datos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="359"/>
        <source>Search in this &amp;Group...</source>
        <translation>Buscar en este &amp;grupo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="367"/>
        <source>Show &amp;Entry Details</source>
        <translation>Mostrar detalles de la &amp;entrada</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="375"/>
        <source>Hide &amp;Usernames</source>
        <translation>Ocultar nombres de &amp;usuario</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="383"/>
        <source>Hide &amp;Passwords</source>
        <translation>Ocultar &amp;contraseñas</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="391"/>
        <source>&amp;Title</source>
        <translation>&amp;Título</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="399"/>
        <source>User&amp;name</source>
        <translation>&amp;Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="407"/>
        <source>&amp;URL</source>
        <translation>&amp;URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="415"/>
        <source>&amp;Password</source>
        <translation>&amp;Contraseña</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="423"/>
        <source>&amp;Comment</source>
        <translation>&amp;Comentario</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="431"/>
        <source>E&amp;xpires</source>
        <translation>E&amp;xpira</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="439"/>
        <source>C&amp;reation</source>
        <translation>C&amp;reación</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="447"/>
        <source>&amp;Last Change</source>
        <translation>Ú&amp;ltimo cambio</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="455"/>
        <source>Last &amp;Access</source>
        <translation>Último &amp;acceso</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="463"/>
        <source>A&amp;ttachment</source>
        <translation>Adjun&amp;to</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="487"/>
        <source>Show &amp;Statusbar</source>
        <translation>Mostrar barra de e&amp;stado</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="505"/>
        <source>&amp;Perform AutoType</source>
        <translation>%Utilizar Auto-escritura</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="513"/>
        <source>&amp;16x16</source>
        <translation>&amp;16x16</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="521"/>
        <source>&amp;22x22</source>
        <translation>&amp;22x22</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="529"/>
        <source>2&amp;8x28</source>
        <translation>2&amp;8x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="544"/>
        <source>&amp;New Database...</source>
        <translation>&amp;Nueva base de datos...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="549"/>
        <source>&amp;Password Generator...</source>
        <translation>Generador de &amp;contraseñas...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="560"/>
        <source>&amp;Group (search results only)</source>
        <translation>&amp;Grupo (sólo resultados de búsqueda)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="565"/>
        <source>Show &amp;Expired Entries...</source>
        <translation>Mostar &amp;entidades expiradas</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="578"/>
        <source>&amp;Add Bookmark...</source>
        <translation>&amp;Añadir marcador...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="583"/>
        <source>Bookmark &amp;this Database...</source>
        <translation>Añadir marcador a es&amp;ta base de datos...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="299"/>
        <source>&amp;Add New Subgroup...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="588"/>
        <source>Copy URL to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>Gestionar marcadores</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="31"/>
        <source>Enter Master Key</source>
        <translation>Introducir clave maestra</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="34"/>
        <source>Set Master Key</source>
        <translation>Establecer clave maestra</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="37"/>
        <source>Change Master Key</source>
        <translation>Cambiar clave maestra</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="41"/>
        <source>Database Key</source>
        <translation>Contraseña de la Base de Datos</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="131"/>
        <source>Last File</source>
        <translation>Último archivo</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="177"/>
        <source>Select a Key File</source>
        <translation>Seleccione un archivo de contraseñas</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="341"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="341"/>
        <source>Key Files (*.key)</source>
        <translation>Archivos de contraseñas (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="205"/>
        <source>Please enter a Password or select a key file.</source>
        <translation>Introduzca una contraseña o seleccione un archivo de contraseñas.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="210"/>
        <source>Please enter a Password.</source>
        <translation>Introduzca una Contraseña.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="215"/>
        <source>Please provide a key file.</source>
        <translation>Introduzca un archivo de contraseñas.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="261"/>
        <source>%1:
No such file or directory.</source>
        <translation>%1:No existe el archivo o directorio.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="227"/>
        <source>The selected key file or directory is not readable.</source>
        <translation>No se puede leer el archivo o directorio de las contraseñas.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="246"/>
        <source>The given directory does not contain any key files.</source>
        <translation>El directorio especificado no contiene ningún archivo de contraseñas.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="252"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation>El directorio especificado contiene más de un archivo de contraseñas.Por favor, especifique el archivo de contraseñas directamente.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="265"/>
        <source>%1:
File is not readable.</source>
        <translation>%1:No se puede leer el archivo.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="339"/>
        <source>Create Key File...</source>
        <translation>Crear un archivo de contraseñas...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">Aceptar</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Introduzca una Contraseña y/o seleccione un archivo de clave.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Clave</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <source>Key file or directory:</source>
        <translation type="obsolete">Archivo o directorio de clave:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="185"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Navegar...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="188"/>
        <source>Alt+B</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">Usar Contraseña Y Archivo de Clave</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Salir</translation>
    </message>
    <message>
        <source>Password Repet.:</source>
        <translation type="obsolete">Contraseña (repetida):</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>Último archivo</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="157"/>
        <source>Key File:</source>
        <translation>Archivo de contraseñas:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="197"/>
        <source>Generate Key File...</source>
        <translation>Generar archivo de contraseñas...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="229"/>
        <source>Please repeat your password:</source>
        <translation>Por favor, repita la contraseña:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="241"/>
        <source>Back</source>
        <translation>Volver</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="268"/>
        <source>Passwords are not equal.</source>
        <translation>Las contraseñas no son iguales.</translation>
    </message>
</context>
<context>
    <name>PwDatabase</name>
    <message>
        <source>Unknown Error</source>
        <translation type="obsolete">Error Desconocido</translation>
    </message>
    <message>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Tamaño de fichero inesperado (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation type="obsolete">Firma Incorrecta</translation>
    </message>
    <message>
        <source>AES-Init Failed</source>
        <translation type="obsolete">Falló inicialización de AES</translation>
    </message>
    <message>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Comprobación hash fallida.
La clave es incorecta o el fichero está dañado.</translation>
    </message>
    <message>
        <source>Could not open key file.</source>
        <translation type="obsolete">No se pudo abrir archivo de clave.</translation>
    </message>
    <message>
        <source>Key file could not be written.</source>
        <translation type="obsolete">No se pudo escribir archivo de clave.</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">No se pudo abrir el archivo.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Advertencia</translation>
    </message>
    <message>
        <source>Could not save configuration file.
Make sure you have write access to &apos;~/.keepass&apos;.</source>
        <translation type="obsolete">No se pudo guardar el archivo de configuración.
Asegúrese de tener acceso para escritura en &apos;~/.keepass&apos;.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Aceptar</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Archivo &apos;%1&apos; no encontrado.</translation>
    </message>
    <message>
        <source>File not found.</source>
        <translation type="obsolete">Archivo no encontrado.</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">No se pudo abrir el archivo.</translation>
    </message>
    <message>
        <source>File is no valid PwManager file.</source>
        <translation type="obsolete">El archivo no es un archivo PwManager válido.</translation>
    </message>
    <message>
        <source>Unsupported file version.</source>
        <translation type="obsolete">Version de archivo no soportada.</translation>
    </message>
    <message>
        <source>Unsupported hash algorithm.</source>
        <translation type="obsolete">Algoritmo hash no soportado.</translation>
    </message>
    <message>
        <source>Unsupported encryption algorithm.</source>
        <translation type="obsolete">Algoritmo de cifrado no soportado.</translation>
    </message>
    <message>
        <source>Compressed files are not supported yet.</source>
        <translation type="obsolete">Los archivos comprimidos todavía no están soportados.</translation>
    </message>
    <message>
        <source>Wrong password.</source>
        <translation type="obsolete">Contraseña incorrecta.</translation>
    </message>
    <message>
        <source>File is damaged (hash test failed).</source>
        <translation type="obsolete">El archivo está dañado (comprobación hash fallida).</translation>
    </message>
    <message>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="obsolete">Datos XML no válidos (ver stdout para más detalles).</translation>
    </message>
    <message>
        <source>File is empty.</source>
        <translation type="obsolete">Archivo vacío.</translation>
    </message>
    <message>
        <source>Invalid XML file (see stdout for details).</source>
        <translation type="obsolete">Archivo XML no válido (ver stdout para más detalles).</translation>
    </message>
    <message>
        <source>Invalid XML file.</source>
        <translation type="obsolete">Archivo XML no válido.</translation>
    </message>
    <message>
        <source>Document does not contain data.</source>
        <translation type="obsolete">El documento no contiene datos.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Error</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="obsolete">Nunca</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="120"/>
        <source>Could not locate library file.</source>
        <translation type="obsolete">No se puede localizar el archivo de la biblioteca.</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="90"/>
        <source>A&amp;nhang</source>
        <translation>&amp;Adjunto</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="180"/>
        <source>Search For:</source>
        <translation>Buscar:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="158"/>
        <source>Regular E&amp;xpression</source>
        <translation>E&amp;xpresión Regular</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="148"/>
        <source>&amp;Case Sensitive</source>
        <translation>Distinguir &amp;Mayúsculas</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="68"/>
        <source>Include:</source>
        <translation>Incluir:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="130"/>
        <source>&amp;Titles</source>
        <translation>&amp;Títulos</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="120"/>
        <source>&amp;Usernames</source>
        <translation>&amp;Usuarios</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="110"/>
        <source>C&amp;omments</source>
        <translation>C&amp;omentarios</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="100"/>
        <source>U&amp;RLs</source>
        <translation>U&amp;RLs</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="80"/>
        <source>Pass&amp;words</source>
        <translation>&amp;Contraseñas</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Buscar</translation>
    </message>
    <message>
        <source>Clo&amp;se</source>
        <translation type="obsolete">C&amp;errar</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+E</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="168"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Incluir Subgrupos (recursivo)</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Selección de icono</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancelar</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="194"/>
        <source>Alt+Ö</source>
        <translation>Alt+Ö</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Aceptar</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="981"/>
        <source>Clear clipboard after:</source>
        <translation>Borrar portapapeles después de:</translation>
    </message>
    <message>
        <source>Seconds</source>
        <translation type="obsolete">Segundos</translation>
    </message>
    <message>
        <source>Sh&amp;ow passwords in plain text by default</source>
        <translation type="obsolete">M&amp;ostrar contraseñas en texto plano por defecto</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="953"/>
        <source>Alt+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation type="obsolete">Apa&amp;riencia</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="477"/>
        <source>Banner Color</source>
        <translation>Color de las cabeceras </translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="491"/>
        <source>Text Color:</source>
        <translation>Color de Texto:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="623"/>
        <source>Change...</source>
        <translation>Cambiar...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="548"/>
        <source>Color 2:</source>
        <translation>Color 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="676"/>
        <source>C&amp;hange...</source>
        <translation>Ca&amp;mbiar...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="679"/>
        <source>Alt+H</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="692"/>
        <source>Color 1:</source>
        <translation>Color 1:</translation>
    </message>
    <message>
        <source>Expand group tree when opening a database</source>
        <translation type="obsolete">Expandir árbol de grupo al abrir la base de datos</translation>
    </message>
    <message>
        <source>&amp;Other</source>
        <translation type="obsolete">&amp;Otros</translation>
    </message>
    <message>
        <source>Remember last opend file</source>
        <translation type="obsolete">Recordar último archivo abierto</translation>
    </message>
    <message>
        <source>Browser Command:</source>
        <translation type="obsolete">Comando del Navegador:</translation>
    </message>
    <message>
        <source>Securi&amp;ty</source>
        <translation type="obsolete">Seguri&amp;dad</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="720"/>
        <source>Alternating Row Colors</source>
        <translation>Alternar Colores de Columna</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1132"/>
        <source>Browse...</source>
        <translation>Navegar...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="222"/>
        <source>Remember last key type and location</source>
        <translation>Recordar último tipo de clave y localización</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="191"/>
        <source>Remember last opened file</source>
        <translation>Recordar el último archivo abierto</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="51"/>
        <source>The integration plugins provide features like usage of the native file dialogs and message boxes of the particular desktop environments.</source>
        <translation type="obsolete">Las extensiones de integración ofrecen funcionalidades como el uso de cuadros de diálogo y mensajes nativos entornos de escritorio particulares.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="54"/>
        <source>General</source>
        <translation type="obsolete">General</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="122"/>
        <source>Show system tray icon</source>
        <translation>Mostrar icono en la bandeja del sistema</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="180"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>Minimizar a la bandeja al hacer click en el botón de cerrar de la ventana principal</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="298"/>
        <source>Save recent directories of file dialogs</source>
        <translation>Guardar los directorios recientes de los cuadros de diálogo de archivos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="761"/>
        <source>Group tree at start-up:</source>
        <translation>Árbol de grupos al inicio:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="768"/>
        <source>Restore last state</source>
        <translation>Restaurar el último estado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="775"/>
        <source>Expand all items</source>
        <translation>Expandir todos los elementos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="782"/>
        <source>Do not expand any item</source>
        <translation>No expandir ningún elemento</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="94"/>
        <source>Security</source>
        <translation>Seguridad</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="950"/>
        <source>Edit Entry Dialog</source>
        <translation>Editar cuadro de diálogo de entradas</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="864"/>
        <source>Desktop Integration</source>
        <translation type="obsolete">Integración con el escritorio</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1333"/>
        <source>Plug-Ins</source>
        <translation>Extensiones</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1342"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1349"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Integración con Gnome (Gtk 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1356"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>Integración con KDE 4</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1371"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>Se necesita reiniciar el programa antes de que los cambios tengan efecto.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1391"/>
        <source>Configure...</source>
        <translation>Configurar...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="99"/>
        <source>Advanced</source>
        <translation>Avanzado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="321"/>
        <source>Clear History Now</source>
        <translation>Borrar ahora el historial</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="343"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>Preguntar siempre antes de borrar entradas o grupos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="732"/>
        <source>Customize Entry Detail View...</source>
        <translation>Personalizar la vista detallada de entradas</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="827"/>
        <source>Features</source>
        <translation type="obsolete">Funcionalidades</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1285"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>Aquí se pueden deshabilitar funcionalidades de KeePassX de acuerdo a tus prefencias para mantener la interfaz símple</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1295"/>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1151"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>Ajuste fino de auto-escritura</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1160"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>Tiempo entre la activación por el usuario de una acción de auto-completar y la primera pulsación de tecla simulada.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1209"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1173"/>
        <source>Pre-Gap:</source>
        <translation>Hueco previo:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1196"/>
        <source>Key Stroke Delay:</source>
        <translation>Retraso en la pulsación de teclas:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1206"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>Retraso ente dos pulsaciones de tecla simuladas. Incrementar en caso de que auto-completar se salte caracteres aleatoriamente.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1125"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>El directorio en el que se suelen monar los dispositivos como CDs y unidades de memoria.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1115"/>
        <source>Media Root:</source>
        <translation>Raíz de los medios:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1141"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>Habilitar si se quiere usar los marcadores y el último archivo abierto de forma independiente de las rutas absolutas. Esto es especialmente útil cuando se usa KeePassX en varios equipos y por lo tanto cambian los puntos de montaje del sistema de archivos.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1144"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>Guardar rutas relativas (marcadores y último archivo)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="150"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>Minimizar a la bandeja en vez de a la barra de tareas</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="252"/>
        <source>Start minimized</source>
        <translation>Iniciar minimizado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="282"/>
        <source>Start locked</source>
        <translation>Iniciar bloqueado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1019"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>Bloquear el área de trabajo cuando se minimice la ventana principal</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1224"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>Combinación de teclas para la Auto-escritura global:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1091"/>
        <source>Custom Browser Command</source>
        <translation>Navegador web personalizado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1103"/>
        <source>Browse</source>
        <translation>Navegar</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="438"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation>Guardar la base de datos automáticamente al salir y al bloquear el área de trabajo.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="941"/>
        <source>Show plain text passwords in:</source>
        <translation>Mostrar las contraseñas con texto sin formato en:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="960"/>
        <source>Database Key Dialog</source>
        <translation>Diálogo de claves de la base de datos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1057"/>
        <source>seconds</source>
        <translation>segundos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1028"/>
        <source>Lock database after inactivity of</source>
        <translation>Bloquear la base de datos tras una inactividad de</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1253"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation>Usar el título de las entradas para buscar la ventana correspondiente para la auto-escritura global</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="74"/>
        <source>General (1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="79"/>
        <source>General (2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="84"/>
        <source>Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="89"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="375"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="403"/>
        <source>Delete backup entries older than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="429"/>
        <source>days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="445"/>
        <source>Automatically save database after every change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="837"/>
        <source>System Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="842"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="867"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="874"/>
        <source>Author:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Aceptar</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Introduzca su Contraseña</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
</context>
<context>
    <name>StandardDatabase</name>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">No se pudo abrir el archivo.</translation>
    </message>
    <message>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Tamaño de fichero inesperado (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation type="obsolete">Firma Incorrecta</translation>
    </message>
    <message>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Comprobación hash fallida.
La clave es incorecta o el fichero está dañado.</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../lib/tools.cpp" line="338"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="unfinished">Jaroslaw Filiochowski, Marcos del Puerto</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="337"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <source>Title</source>
        <translation type="obsolete">Título</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Usuario</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;El área de trabajo está bloqueada.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>Bloquear</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>Cerrar base de datos</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <source>Database Settings</source>
        <translation type="obsolete">Preferencias de Base de Datos</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="obsolete">Cifrado</translation>
    </message>
    <message>
        <source>Algorithm:</source>
        <translation type="obsolete">Algoritmo:</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <source>Encryption Rounds:</source>
        <translation type="obsolete">Iteraciones de Cifrado:</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Aceptar</translation>
    </message>
    <message>
        <source>Ctrl+K</source>
        <translation type="obsolete">Ctrl+A</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Cancelar</translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation type="obsolete">Ctrl+C</translation>
    </message>
</context>
</TS>
