---------------
    0.2.2.6
---------------
-bookmarks and the Detail View are saved in the config file
-fixed bug that caused unlock not to work when 'Remember last opened file' is disabled or the the search is used
-loading default Detail View didn't work

---------------
    0.2.2.5
---------------
-fixed critical bug that caused the database to be only opened/saved with the keyfile when keyfile and password were selected
-fixed crash that could occur when locking a database that has unsaved changes
-don't hide window to tray if a modal window is opened
-auto-Type window is always on top and columns are automatically resized

---------------
    0.2.2.4
---------------
-window is now minimized to tray even if the unlock dialog is shown
-fixed problem when restoring window using the tray icon
-support custom commands and placeholders in entry urls
-improved custom browser selection in the settings dialog
-all dialogs use the native button order
-window title indicates if there are unsaved changes
-improved many translation strings

---------------
    0.2.2.3
---------------
-fixed a crash when the database is locked and KeePassX is restored
-fixed KeePassX is not always properly minimized after auto-type
-fixed global auto-type not working on non-ascii window titles
-added a setting to optionally use entry titles to match window for global auto-type
-added support for the delay command in auto-type
-expired entries are not used for global auto-type
-comparison of window titles is now always case-insensitive
-fixed cmd option -help didn't work
-output help on unrecognized argument
-fixed typo in desktop file

---------------
    0.2.2.2
---------------
-added support for multiple Auto-Type-Window definitions
 fully compatible to KeePass (http://keepass.info/help/base/autotype.html#autoglobal)
-added settings and command line options to start minimized and/or locked
-fixed window not always hidden when using 'Minimize to tray instead of taskbar'
-added pixmap and desktop file
-fixed a bunch of compiler warnings

---------------
    0.2.2.1
---------------
Changes by Felix Geyer:
-added Global Auto-Type feature
-added database locking feature
-saves the sort column and order of the entry view
-added options 'Minimize to tray instead of taskbar', 'Lock workspace when minimizing the main window' and 'Global Auto-Type Shortcut'
-fixed Auto-Type while still holding modifier keys (ctrl, shift, ...) and caps lock
-fixed Auto-Type puts space at the beginning when using custom Auto-Type format
-fixed auto-typing of some non-Latin1 chars
-fixed crash when trying to auto-type invalid chars
-left click on the tray icon brings the main window to front when it's not the active window, otherwise hides it
-fixed a bunch of compiler warnings
-disabled all not yet working features

Official changes:
-system tray icon
-fine tuning options for auto-type
-fully customizable HTML based detail view
-secure random number generator based on Yarrow including an optional entropy collector
-new dialog to view expiered entries
-calendar widget to edit expiration dates in a more confortable way
-restructured password generator
-much better appearance under MacOS X
-final program icon
-misc. visual improvements
-new more robust and flexible database back-end api
-replaced old AES implementation by the latest version of Brian Gladman's implementation.
-(maybe incomplete)

---------------
    0.2.2
---------------
-fixed crash when parsing config file under Win32
-fixed loss of entry icons when saving a database which was not created with KeePassX (no KPX_CUSTOM_ICONS metastream)
-introduces new metastream format for custom icons
-removed all old Qt3 support dependecies
-QtNetwork and QtSql are not longer requiered (when using Qt 4.1.3 or later)
-implemented correct UUID management for entries
-added delay of 0.3s before performing auto-type
-metastreams now get valid group IDs (KeePass/Win compatibility)
-fixed drawing errors when performing drag and drop operations in group view
-when there is no translation installed for the system's country preference but one for the same language the program will use it now
-when canceling the file dialog for the opening of an existing database a already openend database will not longer be closed
-same for the creation of a new database
-alpha blending for banner icons
-new standard banner icon
-MacOS packages: all Qt libraries are now included in the application bundle (extra installation is not longer necessary)

---------------
    0.2.1
---------------
-added AutoType feature (experimental!)
-added custom icons feature
-new command line option for manual language selection (-lang <LOCALE-CODE>)
-when saving an attachment the original filename is adopted by the file dialog
-fixed strange sorting behavior of entries (Bug #7083)
-sorting by dates now works as expected
-the 'Expires' column and the detailed entry view now also show the string 'never' 
 for entries which don't expire
-entry view now gets updated after changing the column setup
-added menu entry to change the size of the toolbar icons

---------------
    0.2.0
---------------
-ported whole application from Qt3 to Qt4
(better performance, less memory usage, ready for KDE4)
-improved Mac OS X support
-added Drag&Drop support
-multiple seclection mode for entries
-improved loading performance for large databases
-faster in-memory encryption
-search field in toolbar now works
-mainwindow size, splitter position and column sizes are restored at start-up
-added option for alternating row colors
-improved key/password dialog
-removed language dialog - program now uses system's default language
-loading translation files for Qt (e.g. file dialogs)
-added text export function
-added option "Never" for expire dates.
-fixed problem with hex. key files
-fixed problem with damaged file attachments after various entry operations
-fixed segmentation fault when using new icons with higher index
-fixed error when saving empty databases