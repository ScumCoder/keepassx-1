<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>@default</name>
    <message>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Ne peut ouvrir le fichier (FileError=%1)</translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;La présente traduction française&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt; À pour auteur:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">&lt;br&gt;Djellel DIDA</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete"> &lt;b&gt;Courriel:&lt;/b&gt;  &lt;br&gt; djellel@free.fr
</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>Équipe</translation>
    </message>
    <message>
        <source>Tarek Saidi</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="46"/>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation>Développeur et Administrateur du Projet</translation>
    </message>
    <message>
        <source>tariq@users.berlios.de</source>
        <translation type="obsolete">tariq@users.berlios.de</translation>
    </message>
    <message>
        <source>Eugen Gorschenin</source>
        <translation type="obsolete">Eugen Gorschenin
</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation>Concepteur du site Internet</translation>
    </message>
    <message>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation>Remerciement à</translation>
    </message>
    <message>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation>Pour les rustines ayant permis un meilleur support de MacOS X</translation>
    </message>
    <message>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">&lt;ADDRESS&gt;www.outofhanwell.com&lt;ADDRESS&gt;</translation>
    </message>
    <message>
        <source>James Nicholls</source>
        <translation type="obsolete">James Nicholls</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation>Icône principale de l&apos;application</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation>Rustines diverses et améliorations</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Le fichier &apos;%1&apos; n&apos;a pu être  trouvé.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>S&apos;assurer que l&apos;application est correctement installée.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">N&apos;a pas pu ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">L&apos;erreur suivante est survenue:
%1</translation>
    </message>
    <message>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="50"/>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation>Développeur</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="41"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>Toutes les informations concernant la méthode pour traduire KeePassX peuvent être obtenues à l&apos;adresse suivante:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation>Traduction courante</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="14"/>
        <location filename="../forms/AboutDlg.ui" line="43"/>
        <source>About</source>
        <translation>À propos </translation>
    </message>
    <message>
        <source>Thanks To</source>
        <translation type="obsolete">Remerciement à</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="182"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="165"/>
        <source>Translation</source>
        <translation>Traduction</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Gest. de mot de passe multiplateforme&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">&lt;span style=&quot; font-size:9pt; font-weight:400;&quot;&gt;Copyright (c)  2005 - 2006     Tarek Saidi &lt;br&gt;
KeePassX  est  distribué  sous  les  termes  de  la&lt;br&gt; Licence Publique Générale GNU v2  (GPL v2).&lt;/span&gt;</translation>
    </message>
    <message>
        <source>tarek.saidi@arcor.de</source>
        <translation type="obsolete">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <source>http://keepass.berlios.de/</source>
        <translation type="obsolete">http://keepass.berlios.de/</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="148"/>
        <source>Credits</source>
        <translation>Crédits </translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="120"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>http://keepassx.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="67"/>
        <source>AppName</source>
        <translation>AppName</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="85"/>
        <source>AppFunc</source>
        <translation>AppFunc</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX est distribué sous les termes de la
Licence Publique Générale (GPL) v2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="32"/>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="56"/>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>Ajouter un Signet</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>Títre :</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>Fichier :</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="40"/>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>Modifier un Signet</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>Bases de Données KeePass (*.kdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>Tous les Fichiers (*)</translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation type="obsolete">Plus d&apos;une saisie automatique : séquence clé trouvée.
Seulement une autorisée par entrée.</translation>
    </message>
    <message>
        <source>Syntax Error in Auto-Type sequence near character %1
					Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="obsolete">Erreur de syntaxe à l&apos;intérieur de la séquence d&apos;auto-saisie près du caractère %1
     Trouvé &apos;{&apos;  sans accolade fermante &apos;}&apos;</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - Saisie Automatique</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>Cliquer sur une entrée pour la saisir automatiquement.</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="37"/>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="39"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="37"/>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>Títre</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="39"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="93"/>
        <source>Auto-Type</source>
        <translation>Saisie Automatique</translation>
    </message>
</context>
<context>
    <name>CAboutDialog</name>
    <message>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Le fichier &apos;%1&apos; n&apos;a pu être  trouvé.</translation>
    </message>
    <message>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="obsolete">S&apos;assurer que l&apos;application est correctement installée.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Ok</translation>
    </message>
    <message>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">N&apos;a pas pu ouvrir le fichier &apos;%1&apos;</translation>
    </message>
    <message>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">L&apos;erreur suivante est survenue:
%1</translation>
    </message>
    <message>
        <source>http://keepass.berlios.de/index.php</source>
        <translation type="obsolete">http://keepass.berlios.de/index.php </translation>
    </message>
    <message>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;La présente traduction française&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt; À pour auteur:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <source>$TRANSALTION_AUTHOR</source>
        <translation type="obsolete">&lt;br&gt;Djellel DIDA</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete"> &lt;b&gt;Courriel:&lt;/b&gt;  &lt;br&gt; djellel@free.fr
</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/translation-howto.html</source>
        <translation type="obsolete">Les informations concernant la méthode de traduction de KeePassX peut être trouvé à l&apos;adresse suivante:&lt;br&gt;
 &lt;ADDRESS&gt;http://keepass.berlios.de/translation-howto.html&lt;/ADDRESS&gt;</translation>
    </message>
    <message>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <source>http://www.outofhanwell.com/&lt;br&gt;Mac OS X Support</source>
        <translation type="obsolete">&lt;ADDRESS&gt;http://www.outofhanwell.com &lt;/ADDRESS&gt;</translation>
    </message>
    <message>
        <source>Eugen Gorschenin</source>
        <translation type="obsolete">Eugen Gorschenin
</translation>
    </message>
    <message>
        <source>geugen@users.berlios.de&lt;br&gt;New Website</source>
        <translation type="obsolete">&lt;ADDRESS&gt;geugen@users.berlios.de&lt;/ADDRESS&gt;</translation>
    </message>
    <message>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">&lt;br&gt;Djellel DIDA</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/</source>
        <translation type="obsolete">Toutes les informations concernant la méthode pour traduire KeePassX peuvent être obtenues à l&apos;adresse suivante:&lt;br&gt;
&lt;ADDRESS&gt;http://keepass.berlios.de/&lt;ADDRESS&gt;</translation>
    </message>
    <message>
        <source>Team</source>
        <translation type="obsolete">Équipe</translation>
    </message>
    <message>
        <source>Tarek Saidi</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <source>Developer, Project Admin</source>
        <translation type="obsolete">Développeur et Administrateur du Projet</translation>
    </message>
    <message>
        <source>tariq@users.berlios.de</source>
        <translation type="obsolete">tariq@users.berlios.de</translation>
    </message>
    <message>
        <source>Web Designer</source>
        <translation type="obsolete">Concepteur du site Internet</translation>
    </message>
    <message>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <source>Thanks To</source>
        <translation type="obsolete">Remerciement à</translation>
    </message>
    <message>
        <source>Patches for better MacOS X support</source>
        <translation type="obsolete">Pour les rustines ayant permis un meilleur support de MacOS X</translation>
    </message>
    <message>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">&lt;ADDRESS&gt;www.outofhanwell.com&lt;ADDRESS&gt;</translation>
    </message>
    <message>
        <source>Information on how to translate KeePassX can be found under:
http://keepassx.sourceforge.net/</source>
        <translation type="obsolete">Les informations concernant la méthode de traduction de KeePassX peuvent être trouvées à l&apos;adresse suivante:&lt;br&gt;
 &lt;ADDRESS&gt;http://keepass.berlios.de/translation-howto.html&lt;/ADDRESS&gt;</translation>
    </message>
    <message>
        <source>James Nicholls</source>
        <translation type="obsolete">James Nicholls</translation>
    </message>
    <message>
        <source>Main Application Icon</source>
        <translation type="obsolete">Pour le logo de KeepassX</translation>
    </message>
    <message>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 Bits   (défaut)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish : 256 Bits</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Définissez le nombre de passes.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="70"/>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>OK</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="70"/>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="70"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; n&apos;est pas un nombre entier valide.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>Le nombre de passes doit être supérieur à 0.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Le mot de passe et sa confirmation ne sont pas identiques !
Veuillez vérifier votre saisie.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="299"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="321"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="338"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="324"/>
        <source>Save Attachment...</source>
        <translation>Enregistrer la pièce jointe...</translation>
    </message>
    <message>
        <source>Overwrite?</source>
        <translation type="obsolete">Écraser ?</translation>
    </message>
    <message>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation type="obsolete">Un fichier ayant le même nom existe déjà.
Voulez-vous le remplacer ?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="299"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="321"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="338"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>Could not remove old file.</source>
        <translation type="obsolete">N&apos;a pas pu enlever l&apos;ancien fichier.</translation>
    </message>
    <message>
        <source>Could not create new file.</source>
        <translation type="obsolete">N&apos;a pas pu créer un nouveau fichier.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="338"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error while writing the file.</source>
        <translation>Erreur lors de l&apos;écriture du fichier.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="351"/>
        <source>Delete Attachment?</source>
        <translation>Supprimer la pièce jointe ?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="352"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Vous êtes sur le point de supprimer la pièce jointe de cette entrée.
En êtes-vous sûr ?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>No, Cancel</source>
        <translation>Non, annuler</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation>Modification de l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="299"/>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>Could not open file.</source>
        <translation>N&apos;a pas pu ouvrir le fichier.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 Bits</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="294"/>
        <source>Add Attachment...</source>
        <translation>Ajouter une pièce jointe...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="321"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>L&apos;entrée choisie n&apos;a pas d&apos;attachement ou est vide.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation>Aujourd&apos;hui</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation>1 semaine</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation>2 semaines</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation>3 semaines</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation>1 mois</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation>3 mois</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation>6 mois</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation>1 an</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation>Calendrier...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="256"/>
        <source>[Untitled Entry]</source>
        <translation>[Entrée sans Nom]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation>Nouvelle Entrée</translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <source>Notice</source>
        <translation type="obsolete">Notification</translation>
    </message>
    <message>
        <source>You need to enter at least one character</source>
        <translation type="obsolete">Vous devez au moins entrer un caractère</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>Could not open &apos;/dev/random&apos; or &apos;/dev/urandom&apos;.</source>
        <translation type="obsolete">N&apos;a pas pu ouvrir &apos;/dev/random&apos; ou &apos;/dev/urandom&apos;.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="115"/>
        <source>Password Generator</source>
        <translation>Générateur de mots de passe</translation>
    </message>
    <message>
        <source>%1 Bit</source>
        <translation type="obsolete">%1 Bits</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="244"/>
        <source>%1 Bits</source>
        <translation>%1 Bits</translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>Please enter a Password.</source>
        <translation type="obsolete">Entrer un mot de passe.</translation>
    </message>
    <message>
        <source>Please choose a key file.</source>
        <translation type="obsolete">Sélectionner un fichier clé.</translation>
    </message>
    <message>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">Entrer un mot de passe ou sélectionner un fichier clé.</translation>
    </message>
    <message>
        <source>Database Key</source>
        <translation type="obsolete">Base de données des clés</translation>
    </message>
    <message>
        <source>Select a Key File</source>
        <translation type="obsolete">Selectionner un fichier clé</translation>
    </message>
    <message>
        <source>*.key</source>
        <translation type="obsolete">*.key</translation>
    </message>
    <message>
        <source>Unexpected Error: File does not exist.</source>
        <translation type="obsolete">Erreur inattendue: Le fichier n&apos;existe pas.</translation>
    </message>
    <message>
        <source>The selected key file or directory does not exist.</source>
        <translation type="obsolete">Le fichier clé ou le répertoire n&apos;existe pas.</translation>
    </message>
    <message>
        <source>The given directory does not contain any key files.</source>
        <translation type="obsolete">Le répertoire désigné ne contient aucun fichier clé.</translation>
    </message>
    <message>
        <source>The given directory contains more then one key file.
Please specify the key file directly.</source>
        <translation type="obsolete">Le répertoire désigné contient plus d&apos;un fichier clé.
Veuillez sélectionner le fichier clé directement.</translation>
    </message>
    <message>
        <source>The key file found in the given directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Le fichier clé choisi dans le répertoire n&apos;est pas lisible.
Veuillez vérifier vos permissions.</translation>
    </message>
    <message>
        <source>Key file could not be found.</source>
        <translation type="obsolete">Le fichier clé n&apos;a pu être trouvé.</translation>
    </message>
    <message>
        <source>Key file is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Le fichier clé n&apos;est pas lisible.
Veuillez vérifier vos permissions.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Avertissement</translation>
    </message>
    <message>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">Le mot de passe et sa confirmation ne sont pas identiques !
S&apos;il vous plait, vérifier votre saisie.</translation>
    </message>
    <message>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">Entrer un mot de passe ou sélectionner un fichier clé.</translation>
    </message>
    <message>
        <source>A file with the name &apos;pwsafe.key&apos; already exisits in the given directory.
Do you want to replace it?</source>
        <translation type="obsolete">Un fichier avec le nom &apos;pwsafe.key&apos; existe déjà dans le répertoire selectionné.
Voulez-vous le remplacer ?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <source>The exisiting file is not writable.</source>
        <translation type="obsolete">Le fichier existant est protégé en écriture .</translation>
    </message>
    <message>
        <source>A file with the this name already exisits.
Do you want to replace it?</source>
        <translation type="obsolete">Un fichier avec un nom identique existe déjà.
Désirez-vous le remplacer ?</translation>
    </message>
    <message>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Le fichier clé choisi n&apos;est pas lisible.
Vérifiez vérifier vos permissions.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
</context>
<context>
    <name>CSearchDlg</name>
    <message>
        <source>Notice</source>
        <translation type="obsolete">Notification</translation>
    </message>
    <message>
        <source>Please enter a search string.</source>
        <translation type="obsolete">Saisissez une recherche.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Recherche</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="60"/>
        <location filename="../dialogs/SelectIconDlg.cpp" line="97"/>
        <source>Add Icons...</source>
        <translation>Ajouter une icône...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="60"/>
        <location filename="../dialogs/SelectIconDlg.cpp" line="97"/>
        <source>Images (%1)</source>
        <translation>Images (%1)</translation>
    </message>
    <message>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: Le fichier n&apos;a pu être chargé.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="72"/>
        <location filename="../dialogs/SelectIconDlg.cpp" line="102"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>An error occured while loading the icon(s):
</source>
        <translation type="obsolete">Une erreur est survenue lors du chargement (des) de l&apos;icône(s):</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Remplacer... </translation>
    </message>
    <message>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">Une erreur est survenue lors du chargement (des) de l&apos;icône(s): %1</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="102"/>
        <source>An error occured while loading the icon.</source>
        <translation>Une erreur est survenue lors du chargement de l&apos;icône.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>Ajouter une icône personnalisée</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="66"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: Le fichier n&apos;a pu être chargé.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="72"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>Une erreur est survenue lors du chargement (des) de l&apos;icône(s) :</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="213"/>
        <location filename="../dialogs/SettingsDlg.cpp" line="321"/>
        <location filename="../dialogs/SettingsDlg.cpp" line="335"/>
        <location filename="../dialogs/SettingsDlg.cpp" line="349"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="354"/>
        <source>Select a directory...</source>
        <translation>Sélectionner un répertoire....</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="361"/>
        <source>Select an executable...</source>
        <translation>Sélectionner un exécutable....</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="423"/>
        <source>System Language</source>
        <translation type="unfinished">Langue du Système</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>Calendrier</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>Collecte d&apos;aléas</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>Générateur aléatoire de nombre</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>Collecte d&apos;aléas...￼ Bougez la souris et/ou appuyez sur des touches jusqu&apos;à en avoir collecté suffisamment pour une génération de nombre aléatoire.</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;￼p, li { white-space: pre-wrap; }￼&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;￼&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Groupe d&apos;aléas généré avec succès !&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>Títre</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>Nom de la Pièce jointe</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>Date de création</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>Date de dernier accès</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>Date de dernière modification</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>Date d&apos;expiration</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>Temps restant avant expiration</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation>Editeur de texte avancé</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="51"/>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation>Souligné</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation>Aligné à Gauche</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation>Centré</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="109"/>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation>Aligné à Droite</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation>Justifié</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation>Couleur du texte</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation>Taille de Police</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation>Modèles</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="78"/>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>Jamais</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>Préférences de la base de données</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>Chiffrage</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>Algorithme :</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>Nombre de passes :</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation>Calcul des passes pour un délai d&apos;une seconde sur cet ordinateur</translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation>Títre</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation>Création</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation>Dernier accès</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation>Dernière modification</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation>Expiration</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="15"/>
        <source>Edit Entry</source>
        <translation>Modifier l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="153"/>
        <source>Username:</source>
        <translation>Nom d&apos;utilisateur :</translation>
    </message>
    <message>
        <source>Password Repet.:</source>
        <translation type="obsolete">Confirmation :</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="140"/>
        <source>Title:</source>
        <translation>Títre :</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="179"/>
        <source>URL:</source>
        <translation>URL :</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="211"/>
        <source>Repeat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="224"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="114"/>
        <source>Quality:</source>
        <translation>Qualité</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="166"/>
        <source>Comment:</source>
        <translation>Commentaire :</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="237"/>
        <source>Expires:</source>
        <translation>Expire le :</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="192"/>
        <source>Group:</source>
        <translation>Groupe :</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="351"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="463"/>
        <source>Icon:</source>
        <translation>Icône :</translation>
    </message>
    <message>
        <source>% Bit</source>
        <translation type="obsolete">% Bits</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="53"/>
        <source>Ge&amp;n.</source>
        <translation>&amp;Gen.</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="384"/>
        <source>Never</source>
        <translation>Jamais</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="127"/>
        <source>Attachment:</source>
        <translation>Pièce jointe :</translation>
    </message>
    <message>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="420"/>
        <source>%1 Bit</source>
        <translation>%1 Bits</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Propriétés du groupe</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>Títre :</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>Icône :</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>Entrées expirées</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>Double cliquez sur une entrée pour l&apos;ouvrir.</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>Títre</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>Expiré</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>Entrées expirées dans la base de données</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>Fichiers XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Tous les Fichiers (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>Fichier XML KeePass</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">N&apos;a pas pu ouvrir le fichier (FileError=%1)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>Tous les Fichiers (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>Fichiers texte (*.txt)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>Fichier texte</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>Exportation échouée</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation>Exporter Fichier...</translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>No error occurred.</source>
        <translation>Aucune erreur.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An error occurred while reading from the file.</source>
        <translation>Une erreur est survenue lors de la lecture du fichier.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>An error occurred while writing to the file.</source>
        <translation>Une erreur est survenue lors de l&apos;écriture du fichier.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>A fatal error occurred.</source>
        <translation>Une erreur fatale est survenue.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>An resource error occurred.</source>
        <translation>Une erreur de ressource est survenue.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>The file could not be opened.</source>
        <translation>Le fichier n&apos;a pu être ouvert.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The operation was aborted.</source>
        <translation>L&apos;opération a été annulée.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>A timeout occurred.</source>
        <translation>Délai dépassé.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>An unspecified error occurred.</source>
        <translation>Une erreur non spécifiée est survenue.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be removed.</source>
        <translation>Le fichier n&apos;a pu être enlevé.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be renamed.</source>
        <translation>Le fichier n&apos;a pu être renommé.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The position in the file could not be changed.</source>
        <translation>La position dans le fichier n&apos;a pu être changée.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="74"/>
        <source>The file could not be resized.</source>
        <translation>Le fichier ne peut être redimensionné.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="75"/>
        <source>The file could not be accessed.</source>
        <translation>Le fichier n&apos;est pas accessible.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="76"/>
        <source>The file could not be copied.</source>
        <translation>Le fichier n&apos;a pu être copié.</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Alt+M</source>
        <translation type="obsolete">Alt+M</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation type="obsolete">Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="14"/>
        <source>Password Generator</source>
        <translation>Générateur de mots de passe</translation>
    </message>
    <message>
        <source>Accep&amp;t</source>
        <translation type="obsolete">Accep&amp;ter</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="454"/>
        <source>Generate</source>
        <translation>Générer</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="431"/>
        <source>New Password:</source>
        <translation>Nouveau mot de passe :</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="227"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="246"/>
        <source>Use the following characters:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="348"/>
        <source>Quality:</source>
        <translation>Qualité:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="298"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="116"/>
        <source>&amp;Upper Letters</source>
        <translation>Lettres majusc&amp;ules</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="82"/>
        <source>&amp;Lower Letters</source>
        <translation>Lettres minuscu&amp;les</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>&amp;Numbers</source>
        <translation>&amp;Nombres</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="133"/>
        <source>&amp;Special Characters</source>
        <translation>Caractères &amp;Spéciaux</translation>
    </message>
    <message>
        <source>Minus</source>
        <translation type="obsolete">Moins</translation>
    </message>
    <message>
        <source>U&amp;nderline</source>
        <translation type="obsolete">Soulig&amp;né</translation>
    </message>
    <message>
        <source>h&amp;igher ANSI-Characters</source>
        <translation type="obsolete">Caractères ANS&amp;I étendus</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <source>Use &amp;only following characters:</source>
        <translation type="obsolete">Utiliser s&amp;eulement les caractères suivant:</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+E</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="312"/>
        <source>Length:</source>
        <translation>Longueur:</translation>
    </message>
    <message>
        <source>Use &quot;/dev/rando&amp;m&quot;</source>
        <translation type="obsolete">Utiliser &quot;/dev/rando&amp;m&quot;</translation>
    </message>
    <message>
        <source>Use follo&amp;wing character groups:</source>
        <translation type="obsolete">Utiliser le &amp;groupe de caractères suivant:</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+G</translation>
    </message>
    <message>
        <source>White &amp;Spaces</source>
        <translation type="obsolete">E&amp;space</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="375"/>
        <source>Enable entropy collection</source>
        <translation>Activer la collecte d&apos;aléas</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="401"/>
        <source>Collect only once per session</source>
        <translation>Collecter seulement une fois par session</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="43"/>
        <source>Random</source>
        <translation>Aléatoire</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="49"/>
        <source>Use following character groups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="92"/>
        <source>&amp;Underline</source>
        <translation>So&amp;uligné</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="109"/>
        <source>&amp;White Spaces</source>
        <translation>Espaces blancs</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="126"/>
        <source>&amp;Minus</source>
        <translation>&amp;Moins</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="144"/>
        <source>Exclude look-alike characters</source>
        <translation>Exclure les caractères ressemblants</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>Ensure that password contains characters from every group</source>
        <translation>S&apos;assurer que le mot de passe contienne des caractères de chaque groupe</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="159"/>
        <source>Pronounceable</source>
        <translation>Prononçable</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="183"/>
        <source>Lower Letters</source>
        <translation>Minuscules</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="190"/>
        <source>Upper Letters</source>
        <translation>Majuscules</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="197"/>
        <source>Numbers</source>
        <translation>Nombres</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="204"/>
        <source>Special Characters</source>
        <translation>Caractères spéciaux</translation>
    </message>
</context>
<context>
    <name>HelpDlg</name>
    <message>
        <location filename="../forms/HelpDlg.ui" line="14"/>
        <source>Help Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/HelpDlg.ui" line="22"/>
        <location filename="../forms/HelpDlg.ui" line="25"/>
        <source>Previous Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/HelpDlg.ui" line="32"/>
        <location filename="../forms/HelpDlg.ui" line="35"/>
        <source>Next Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/HelpDlg.ui" line="42"/>
        <location filename="../forms/HelpDlg.ui" line="45"/>
        <source>First Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>Fichiers XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Tous les Fichiers (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <location filename="../import/Import_KWalletXml.cpp" line="43"/>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <location filename="../import/Import_KWalletXml.cpp" line="47"/>
        <location filename="../import/Import_KWalletXml.cpp" line="49"/>
        <location filename="../import/Import_KWalletXml.cpp" line="55"/>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>Importation échouée</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Donnée XML invalide (voir &apos;stdout pour plus de détails).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="43"/>
        <location filename="../import/Import_KWalletXml.cpp" line="47"/>
        <location filename="../import/Import_KWalletXml.cpp" line="49"/>
        <location filename="../import/Import_KWalletXml.cpp" line="55"/>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>Fichier XML invalide.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>Le document  ne contient pas de donnée.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>Fichiers XML KeePass (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>Tous les Fichiers (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="41"/>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="49"/>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>Importation échouée</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>Erreur d&apos;analyse XML à la ligne %1 colonne %2 :￼
%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="41"/>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="49"/>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>Erreur d&apos;analyse : le fichier n&apos;est pas un fichier XML KeePassX valide.</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>Fichiers PwManager (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation>Tous les Fichiers (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <location filename="../import/Import_PwManager.cpp" line="54"/>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation>Importation échouée</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation>Le fichier est vide.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation>Le fichier n&apos;est pas un fichier PwManager valide.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation>Version de fichier non supportée.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="54"/>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation>Algorithme de hachage non supporté. </translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>Algorithme d&apos;encryptage non supporté.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation>Les fichiers compressés ne sont pas encore supportés.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation>Mauvais mot de passe.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation>Le fichier est endommagé (le test de hachage a échoué).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Donnée XML invalide (voir &apos;stdout pour plus de détails).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>Importer fichier...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>Importation échouée</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="516"/>
        <location filename="../Kdb3Database.cpp" line="525"/>
        <source>Could not open file.</source>
        <translation>N&apos;a pu ouvrir le fichier.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="541"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>Taille de fichier inattendue (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="558"/>
        <source>Wrong Signature</source>
        <translation>Mauvaise signature</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="563"/>
        <source>Unsupported File Version.</source>
        <translation>Version de fichier non supportée.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="572"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>Algorithme de cryptage inconnu.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="599"/>
        <source>Unable to initialize the twofish algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="606"/>
        <source>Unknown encryption algorithm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="611"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>Le décryptage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="637"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>Le test de hachage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1338"/>
        <source>The database has been opened read-only.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E3]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E3]</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="722"/>
        <source>Invalid group tree.</source>
        <translation>Arborescence de groupe invalide.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="938"/>
        <source>Key file is empty.</source>
        <translation>Le fichier clé est vide.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1318"/>
        <source>The database must contain at least one group.</source>
        <translation>La base de données doit contenir au moins un groupe.</translation>
    </message>
    <message>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier pour écriture.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="660"/>
        <location filename="../Kdb3Database.cpp" line="667"/>
        <location filename="../Kdb3Database.cpp" line="679"/>
        <location filename="../Kdb3Database.cpp" line="693"/>
        <location filename="../Kdb3Database.cpp" line="700"/>
        <location filename="../Kdb3Database.cpp" line="716"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>Erreur inattendue : Le décalage est hors limite.</translation>
    </message>
    <message>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation type="obsolete">Impossible d&apos;initialiser l&apos;algorithme twofish.</translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1182"/>
        <source>Bytes</source>
        <translation>Octets</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1190"/>
        <source>KiB</source>
        <translation>kio</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1196"/>
        <source>MiB</source>
        <translation>Mio</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1201"/>
        <source>GiB</source>
        <translation>Gio</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="64"/>
        <source>Title</source>
        <translation>Títre</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="64"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="64"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="64"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="64"/>
        <source>Comments</source>
        <translation>Commentaires</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="65"/>
        <source>Expires</source>
        <translation>Expire le</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="65"/>
        <source>Creation</source>
        <translation>Créé le</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="65"/>
        <source>Last Change</source>
        <translation>Dernier changement</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="65"/>
        <source>Last Access</source>
        <translation>Dernier accès</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="65"/>
        <source>Attachment</source>
        <translation>Pièce jointe</translation>
    </message>
    <message>
        <source>%1 items</source>
        <translation type="obsolete">%1 élements</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="240"/>
        <source>Delete?</source>
        <translation>Effacer ?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="65"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="376"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="376"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>Au moins un groupe doit exister avant d&apos;ajouter une entrée.</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="376"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="237"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation>Etes vous sûr de vouloir effacer cette entrée ?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="239"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation>Etes vous sûr de vouloir effacer ces %1 entrées ?</translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="61"/>
        <source>Search Results</source>
        <translation>Résultats de la recherche</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation type="obsolete">Groupes</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="92"/>
        <source>Delete?</source>
        <translation>Effacer ?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="93"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation>Etes vous sûr de vouloir effacer ce groupe, ses groupes enfants et toutes leurs entrées ?</translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="349"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="350"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1458"/>
        <source>Couldn&apos;t create lock file. Opening the database read-only.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1472"/>
        <location filename="../mainwindow.cpp" line="1486"/>
        <source>locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="351"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="405"/>
        <location filename="../mainwindow.cpp" line="485"/>
        <location filename="../mainwindow.cpp" line="508"/>
        <location filename="../mainwindow.cpp" line="544"/>
        <location filename="../mainwindow.cpp" line="927"/>
        <location filename="../mainwindow.cpp" line="939"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <source>The following error occured while opening the database:
%1</source>
        <translation type="obsolete">l&apos;erreur suivante est survenue à l&apos;ouverture de la base de données:
%1</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="525"/>
        <source>Save modified file?</source>
        <translation>Enregistrer le fichier modifié ?</translation>
    </message>
    <message>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation type="obsolete">Le fichier courant a été modifié.
Désirez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Oui</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Non</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <source>KeePassX - %1</source>
        <translation type="obsolete">KeePassX - %1</translation>
    </message>
    <message>
        <source>&lt;B&gt;Group: &lt;/B&gt;%1  &lt;B&gt;Title: &lt;/B&gt;%2  &lt;B&gt;Username: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Password: &lt;/B&gt;%5  &lt;B&gt;Creation: &lt;/B&gt;%6  &lt;B&gt;Last Change: &lt;/B&gt;%7  &lt;B&gt;LastAccess: &lt;/B&gt;%8  &lt;B&gt;Expires: &lt;/B&gt;%9</source>
        <translation type="obsolete">&lt;B&gt;Groupe: &lt;/B&gt;%1  &lt;B&gt;Titre:  &lt;/B&gt;%2   &lt;B&gt;Nom d&apos;utilisateur:  &lt;/B&gt;%3   &lt;B&gt;URL:   &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;   &lt;B&gt;Mot de passe:  &lt;/B&gt;%5   &lt;B&gt;Date de création:   &lt;/B&gt;%6  &lt;B&gt;Dernier changement:   &lt;/B&gt;%7  &lt;B&gt;Dernier accès:   &lt;/B&gt;%8  &lt;B&gt;Date d&apos;expiration:   &lt;/B&gt;%9</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="802"/>
        <location filename="../mainwindow.cpp" line="817"/>
        <location filename="../mainwindow.cpp" line="853"/>
        <location filename="../mainwindow.cpp" line="868"/>
        <source>Clone Entry</source>
        <translation>Dupliquer l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="804"/>
        <location filename="../mainwindow.cpp" line="819"/>
        <location filename="../mainwindow.cpp" line="855"/>
        <location filename="../mainwindow.cpp" line="870"/>
        <source>Delete Entry</source>
        <translation>Effacer l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="832"/>
        <location filename="../mainwindow.cpp" line="883"/>
        <source>Clone Entries</source>
        <translation>Dupliquer les entrées</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="834"/>
        <location filename="../mainwindow.cpp" line="885"/>
        <source>Delete Entries</source>
        <translation>Effacer les entrées</translation>
    </message>
    <message>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">Le fichier n&apos;a pu être enregistré.
%1</translation>
    </message>
    <message>
        <source>Save Database As...</source>
        <translation type="obsolete">Enregistrer la base de données sous...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1449"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <source>[new]</source>
        <translation type="obsolete">[nouveau]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="611"/>
        <source>Open Database...</source>
        <translation>Ouvrir la base de données...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1452"/>
        <source>Loading Database...</source>
        <translation>Chargement de la base de données...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1455"/>
        <source>Loading Failed</source>
        <translation>Le chargement a échoué</translation>
    </message>
    <message>
        <source>Could not create key file. The following error occured:
%1</source>
        <translation type="obsolete">N&apos;a pu créer le fichier clé. L&apos;erreur suivante est survenue:
%1</translation>
    </message>
    <message>
        <source>Export To...</source>
        <translation type="obsolete">Exporter vers...</translation>
    </message>
    <message>
        <source>KeePassX [new]</source>
        <translation type="obsolete">KeePassX [nouveau]</translation>
    </message>
    <message>
        <source>Unknown error in Import_PwManager::importFile()()</source>
        <translation type="obsolete">Erreur inconnue dans Import_PwManager::importFile()()</translation>
    </message>
    <message>
        <source>Unknown error in Import_KWalletXml::importFile()</source>
        <translation type="obsolete">Erreur inconnue dans Import_KWalletXml::importFile()</translation>
    </message>
    <message>
        <source>Unknown error in PwDatabase::openDatabase()</source>
        <translation type="obsolete">Erreur inconnue dans PwDatabase::openDatabase()</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <source>Show Toolbar</source>
        <translation type="obsolete">Afficher la barre outils</translation>
    </message>
    <message>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="484"/>
        <source>Unknown error while loading database.</source>
        <translation>Erreur inconnue lors du chargement de la base de données.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="611"/>
        <location filename="../mainwindow.cpp" line="921"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>Bases de données KeePass (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="611"/>
        <location filename="../mainwindow.cpp" line="921"/>
        <source>All Files (*)</source>
        <translation>Tous les Fichiers (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="921"/>
        <source>Save Database...</source>
        <translation>Enregistrer la base de données...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="756"/>
        <source>1 Month</source>
        <translation>1 mois</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="758"/>
        <source>%1 Months</source>
        <translation>%1 mois</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="764"/>
        <source>1 Year</source>
        <translation>1 an</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="766"/>
        <source>%1 Years</source>
        <translation>%1 ans</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>1 Day</source>
        <translation>1 jour</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="774"/>
        <source>%1 Days</source>
        <translation>%1 jours</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="778"/>
        <source>less than 1 day</source>
        <translation>Moins d&apos;un jour</translation>
    </message>
    <message>
        <source>Locked</source>
        <translation type="obsolete">Verrouillé</translation>
    </message>
    <message>
        <source>Unlocked</source>
        <translation type="obsolete">Déverrouillé</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="353"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="405"/>
        <location filename="../mainwindow.cpp" line="508"/>
        <source>The database file does not exist.</source>
        <translation>La base de données n&apos;existe pas.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="421"/>
        <source>Open read-only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1470"/>
        <location filename="../mainwindow.cpp" line="1484"/>
        <source>new</source>
        <translation>nouveau</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="742"/>
        <source>Expired</source>
        <translation>Expiré</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1344"/>
        <source>Un&amp;lock Workspace</source>
        <translation>Déverrouille l&apos;espace de travai&amp;l</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1359"/>
        <source>&amp;Lock Workspace</source>
        <translation>Verrouille l&apos;espace de travai&amp;l</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="486"/>
        <source>The following error occured while opening the database:</source>
        <translation>L&apos;erreur suivante est survenue lors de l&apos;ouverture de la base de données :</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="914"/>
        <location filename="../mainwindow.cpp" line="932"/>
        <source>File could not be saved.</source>
        <translation>Le fichier n&apos;a pu être enregistré.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="309"/>
        <location filename="../mainwindow.cpp" line="1125"/>
        <source>Show &amp;Toolbar</source>
        <translation>Afficher la barre ou&amp;tils</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="414"/>
        <source>Database locked</source>
        <translation>Base de données verrouillée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="415"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation>La base de données que vous essayez d&apos;ouvrir est verrouillée.￼Cela signifie soit que quelqu&apos;un d&apos;autre a ouvert le fichier soit que KeePassX a planté la dernière fois qu&apos;il a ouvert la base.￼￼Voulez-vous l&apos;ouvrir quand même ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="927"/>
        <source>Couldn&apos;t create database lock file.</source>
        <translation type="unfinished">Impossible de créer le fichier de verrouillage de la base.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="526"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation>Le fichier courant a été modifié. Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="544"/>
        <location filename="../mainwindow.cpp" line="939"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation>Impossible d&apos;enlever le fichier de verrouillage de la base.</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="140"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="147"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Le fichier &apos;%1&apos; n&apos;a pu être  trouvé.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="18"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Fichier</translation>
    </message>
    <message>
        <source>Import from...</source>
        <translation type="obsolete">Importer d&apos;un...</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="obsolete">Affichage</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="obsolete">Colonnes</translation>
    </message>
    <message>
        <source>Extras</source>
        <translation type="obsolete">Extras</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <source>New Database...</source>
        <translation type="obsolete">Nouvelle base de données...</translation>
    </message>
    <message>
        <source>Open Database...</source>
        <translation type="obsolete">Ouvrir une base de données...</translation>
    </message>
    <message>
        <source>Close Database</source>
        <translation type="obsolete">Fermer la base de données</translation>
    </message>
    <message>
        <source>Save Database</source>
        <translation type="obsolete">Enregistrer la base de données</translation>
    </message>
    <message>
        <source>Save Database As...</source>
        <translation type="obsolete">Enregistrer la base de données sous...</translation>
    </message>
    <message>
        <source>Database Settings...</source>
        <translation type="obsolete">Paramétrage de la base de données...</translation>
    </message>
    <message>
        <source>Change Master Key...</source>
        <translation type="obsolete">Changer la clé maitresse...</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
    <message>
        <source>PwManager File (*.pwm)</source>
        <translation type="obsolete">Fichier PwManager (*.pwm)</translation>
    </message>
    <message>
        <source>KWallet XML-File (*.xml)</source>
        <translation type="obsolete">Fichier XML, KWallet (*.xml)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="578"/>
        <source>Add New Group...</source>
        <translation>Ajouter un nouveau groupe...</translation>
    </message>
    <message>
        <source>Edit Group...</source>
        <translation type="obsolete">Modifier le groupe...</translation>
    </message>
    <message>
        <source>Delete Group</source>
        <translation type="obsolete">Supprimer le groupe</translation>
    </message>
    <message>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">Copier le mot de passe dans le presse-papier</translation>
    </message>
    <message>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">Copier l&apos;utilisateur dans le presse-papier</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation type="obsolete">Ouvrir l&apos;URL</translation>
    </message>
    <message>
        <source>Save Attachment As...</source>
        <translation type="obsolete">Enregistrer la pièce jointe sous...</translation>
    </message>
    <message>
        <source>Add New Entry...</source>
        <translation type="obsolete">Ajouter une nouvelle entrée...</translation>
    </message>
    <message>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">Modifier/afficher l&apos;entrée...</translation>
    </message>
    <message>
        <source>Delete Entry</source>
        <translation type="obsolete">Supprimer l&apos;entrée</translation>
    </message>
    <message>
        <source>Clone Entry</source>
        <translation type="obsolete">Dupliquer l&apos;entrée</translation>
    </message>
    <message>
        <source>Search In Database...</source>
        <translation type="obsolete">Rechercher dans la base de données...</translation>
    </message>
    <message>
        <source>Search in this group...</source>
        <translation type="obsolete">Rechercher dans ce groupe...</translation>
    </message>
    <message>
        <source>Show Toolbar</source>
        <translation type="obsolete">Afficher la barre outils</translation>
    </message>
    <message>
        <source>Show Entry Details</source>
        <translation type="obsolete">Afficher les détails de l&apos;entrée</translation>
    </message>
    <message>
        <source>Hide Usernames</source>
        <translation type="obsolete">Cacher l&apos;utilisateur</translation>
    </message>
    <message>
        <source>Hide Passwords</source>
        <translation type="obsolete">Cacher les mots de passe</translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="obsolete">Títre</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="obsolete">URL</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Mot de passe</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation type="obsolete">Commentaire</translation>
    </message>
    <message>
        <source>Expires</source>
        <translation type="obsolete">Date d&apos;expiration</translation>
    </message>
    <message>
        <source>Creation</source>
        <translation type="obsolete">Date de création</translation>
    </message>
    <message>
        <source>Last Change</source>
        <translation type="obsolete">Dernier changement</translation>
    </message>
    <message>
        <source>Last Access</source>
        <translation type="obsolete">Dernier accès</translation>
    </message>
    <message>
        <source>Attachment</source>
        <translation type="obsolete">Pièce jointe</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation type="obsolete">Préférences...</translation>
    </message>
    <message>
        <source>About...</source>
        <translation type="obsolete">À propos...</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Modification</translation>
    </message>
    <message>
        <source>Show Statusbar</source>
        <translation type="obsolete">Afficher la barre  de statuts</translation>
    </message>
    <message>
        <source>Export to...</source>
        <translation type="obsolete">Exporter vers...</translation>
    </message>
    <message>
        <source>KeePassX Handbook...</source>
        <translation type="obsolete">Le manuel de KeePassX...</translation>
    </message>
    <message>
        <source>Plain Text (*.txt)</source>
        <translation type="obsolete">Un fichier plein texte (*.txt)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="495"/>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
    <message>
        <source>Perform AutoType</source>
        <translation type="obsolete">Exécuter l&apos;auto-saisie</translation>
    </message>
    <message>
        <source>Type Here</source>
        <translation type="obsolete">Saisir ici</translation>
    </message>
    <message>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">Taille des icônes de la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="184"/>
        <source>&amp;View</source>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="129"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="138"/>
        <source>&amp;Import from...</source>
        <translation>&amp;Importer d&apos;un...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="133"/>
        <source>&amp;Export to...</source>
        <translation>&amp;Exporter vers...</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation type="obsolete">Modifi&amp;er</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="222"/>
        <source>E&amp;xtras</source>
        <translation>E&amp;xtras</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="121"/>
        <source>&amp;Help</source>
        <translation>Ai&amp;de</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="529"/>
        <source>&amp;New Database...</source>
        <translation>&amp;Nouvelle base de données...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="254"/>
        <source>&amp;Open Database...</source>
        <translation>&amp;Ouvrir une base de données...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="259"/>
        <source>&amp;Close Database</source>
        <translation>Fer&amp;mer la base de données</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="264"/>
        <source>&amp;Save Database</source>
        <translation>&amp;Enregistrer la base de données</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="269"/>
        <source>Save Database &amp;As...</source>
        <translation>Enre&amp;gistrer la base de données sous...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="274"/>
        <source>&amp;Database Settings...</source>
        <translation>Paramé&amp;trage de la base de données...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="279"/>
        <source>Change &amp;Master Key...</source>
        <translation>&amp;Changer la clé maitre...</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="466"/>
        <source>&amp;Settings...</source>
        <translation>&amp;Préférences...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="474"/>
        <source>&amp;About...</source>
        <translation>À pr&amp;opos...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="490"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>Le manuel de &amp;KeePassX...</translation>
    </message>
    <message>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation type="obsolete">Bases de Données KeePass Standard Mono-Utilisateur (*.kdb)</translation>
    </message>
    <message>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation type="obsolete">Bases de Données KeePass Avancées (*.kxdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="555"/>
        <source>Recycle Bin...</source>
        <translation>Corbeille...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="48"/>
        <source>Groups</source>
        <translation>Groupes</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="284"/>
        <source>&amp;Lock Workspace</source>
        <translation>Verrouille l&apos;espace de travai&amp;l</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="143"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Signets</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="165"/>
        <source>&amp;Entries</source>
        <translation>&amp;Entrées</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="188"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation>Taille des &amp;icônes de la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="196"/>
        <source>&amp;Columns</source>
        <translation>&amp;Colonnes</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="232"/>
        <source>&amp;Groups</source>
        <translation>&amp;Groupes</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="249"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation>&amp;Gérer les Signets...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="289"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="302"/>
        <source>&amp;Edit Group...</source>
        <translation>Modifi&amp;er le groupe...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="307"/>
        <source>&amp;Delete Group</source>
        <translation>&amp;Effacer le groupe</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="312"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation>Copier le mot de passe &amp;vers le presse-papier</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="317"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation>Copier l&apos;&amp;utilisateur dans le presse-papier</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="322"/>
        <source>&amp;Open URL</source>
        <translation>&amp;Ouvrir l&apos;URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="327"/>
        <source>&amp;Save Attachment As...</source>
        <translation>Enregi&amp;strer la pièce jointe sous...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="332"/>
        <source>Add &amp;New Entry...</source>
        <translation>&amp;Ajouter une nouvelle entrée...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="337"/>
        <source>&amp;View/Edit Entry...</source>
        <translation>&amp;Modifier/Afficher l&apos;entrée...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="342"/>
        <source>De&amp;lete Entry</source>
        <translation>&amp;Effacer l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="347"/>
        <source>&amp;Clone Entry</source>
        <translation>&amp;Dupliquer l&apos;entrée</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="352"/>
        <source>Search &amp;in Database...</source>
        <translation>&amp;Rechercher dans la base de données...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="357"/>
        <source>Search in this &amp;Group...</source>
        <translation>Rechercher dans ce &amp;groupe...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="365"/>
        <source>Show &amp;Entry Details</source>
        <translation>Afficher les détails de l&apos;&amp;entrée</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="373"/>
        <source>Hide &amp;Usernames</source>
        <translation>Masquer l&apos;&amp;utilisateur</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="381"/>
        <source>Hide &amp;Passwords</source>
        <translation>Masquer les mots de &amp;passe</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="389"/>
        <source>&amp;Title</source>
        <translation>&amp;Títre</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="397"/>
        <source>User&amp;name</source>
        <translation>&amp;Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="405"/>
        <source>&amp;URL</source>
        <translation>&amp;URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="413"/>
        <source>&amp;Password</source>
        <translation>Mot de &amp;passe</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="421"/>
        <source>&amp;Comment</source>
        <translation>&amp;Commentaire</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="429"/>
        <source>E&amp;xpires</source>
        <translation>Date d&apos;&amp;expiration</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="437"/>
        <source>C&amp;reation</source>
        <translation>C&amp;réation</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="445"/>
        <source>&amp;Last Change</source>
        <translation>&amp;Dernier changement</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="453"/>
        <source>Last &amp;Access</source>
        <translation>Dernier &amp;accès</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="461"/>
        <source>A&amp;ttachment</source>
        <translation>Pièce join&amp;te</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="485"/>
        <source>Show &amp;Statusbar</source>
        <translation>Afficher la barre  de &amp;statut</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="500"/>
        <source>&amp;Perform AutoType</source>
        <translation>Exécuter la Saisie Automatique</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="508"/>
        <source>&amp;16x16</source>
        <translation>&amp;16x16</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="516"/>
        <source>&amp;22x22</source>
        <translation>&amp;22x22</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="524"/>
        <source>2&amp;8x28</source>
        <translation>2&amp;8x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="534"/>
        <source>&amp;Password Generator...</source>
        <translation>Générateur de mots de &amp;passe...</translation>
    </message>
    <message>
        <source>&amp;Group (search results only)</source>
        <translation type="obsolete">&amp;Groupe (résultats de recherche seulement)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="550"/>
        <source>Show &amp;Expired Entries...</source>
        <translation>Afficher &amp;Entrées expirées...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="563"/>
        <source>&amp;Add Bookmark...</source>
        <translation>&amp;Ajouter un Signet...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="568"/>
        <source>Bookmark &amp;this Database...</source>
        <translation>Créer un Signe&amp;t pour cette base de données...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="583"/>
        <source>Sort groups</source>
        <translation>Trier les groupes</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="297"/>
        <source>&amp;Add New Subgroup...</source>
        <translation>&amp;Ajouter un nouveau sous-groupe...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="542"/>
        <source>&amp;Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="573"/>
        <source>Copy URL to Clipboard</source>
        <translation>Copier l&apos;URL vers le presse-papiers</translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../dialogs/ManageBookmarksDlg.cpp" line="56"/>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>Gérer les Signets</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="32"/>
        <source>Enter Master Key</source>
        <translation>Entrer la clé maitre</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="35"/>
        <source>Set Master Key</source>
        <translation>Définir la clé maitre</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="38"/>
        <source>Change Master Key</source>
        <translation>Changer la clé maitre</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="42"/>
        <source>Database Key</source>
        <translation>Clé de la base de données</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="132"/>
        <source>Last File</source>
        <translation>Dernier Fichier</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="178"/>
        <source>Select a Key File</source>
        <translation>Selectionner  un fichier clé</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="179"/>
        <location filename="../dialogs/PasswordDlg.cpp" line="341"/>
        <source>All Files (*)</source>
        <translation>Tous les Fichiers (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="180"/>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>Key Files (*.key)</source>
        <translation>Fichiers clé (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="206"/>
        <source>Please enter a Password or select a key file.</source>
        <translation>Entrer un mot de passe ou sélectionner un fichier clé.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="211"/>
        <source>Please enter a Password.</source>
        <translation>Entrer un mot de passe.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="216"/>
        <source>Please provide a key file.</source>
        <translation>Sélectionner un fichier clé.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="224"/>
        <location filename="../dialogs/PasswordDlg.cpp" line="262"/>
        <source>%1:
No such file or directory.</source>
        <translation>%1 : Fichier ou répertoire inexistant.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="228"/>
        <source>The selected key file or directory is not readable.</source>
        <translation>Le fichier clé choisi n&apos;est pas lisible.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="247"/>
        <source>The given directory does not contain any key files.</source>
        <translation>Le répertoire désigné ne contient aucun fichier clé.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="252"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation>Le répertoire désigné contient plus d&apos;un fichier clé.
Pourriez-vous sélectionner le fichier clé désiré.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="266"/>
        <source>%1:
File is not readable.</source>
        <translation>%1 : Fichier illisible.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="340"/>
        <source>Create Key File...</source>
        <translation>Créer le fichier clé...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Entrer un mot de passe et/ou sélectionner un fichier clé.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Clé maitre</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <source>Key file or directory:</source>
        <translation type="obsolete">fichier clé ou répertoire:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="191"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Parcourir...</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation type="obsolete">Alt+P</translation>
    </message>
    <message>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">Utiliser  un mot de passe ET un fichier clé</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
    <message>
        <source>Password Repet.:</source>
        <translation type="obsolete">Confirmation:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>Dernier Fichier</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="163"/>
        <source>Key File:</source>
        <translation>Fichier  clé :</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="200"/>
        <source>Generate Key File...</source>
        <translation>Générer le fichier clé...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="232"/>
        <source>Please repeat your password:</source>
        <translation>Ré-entrer votre mot de passe :</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="250"/>
        <source>Back</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="277"/>
        <source>Passwords are not equal.</source>
        <translation>Les mots de passe ne sont pas identiques.</translation>
    </message>
</context>
<context>
    <name>PwDatabase</name>
    <message>
        <source>Unknown Error</source>
        <translation type="obsolete">Erreur inconnue</translation>
    </message>
    <message>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Taille de fichier inattendue (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation type="obsolete">Mauvaise signature</translation>
    </message>
    <message>
        <source>AES-Init Failed</source>
        <translation type="obsolete">L&apos;initialisation de AES a échoué</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E3]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E3]</translation>
    </message>
    <message>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Le test de hachage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Could not open key file.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier clé.</translation>
    </message>
    <message>
        <source>Key file could not be written.</source>
        <translation type="obsolete">Le fichier clé n&apos;a pas pu être écrit.</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier.</translation>
    </message>
    <message>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Version de fichier non supportée.</translation>
    </message>
    <message>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Algorithme d&apos;encryptage inconnu.</translation>
    </message>
    <message>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Le décryptage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier pour écriture.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Avertissement</translation>
    </message>
    <message>
        <source>Could not save configuration file.
Make sure you have write access to &apos;~/.keepass&apos;.</source>
        <translation type="obsolete">N&apos;a pu enregistrer le fichier de configuration.
Étes-vous sûr de posséder le droit en écriture sur &apos;~/.keepass&apos;.</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Accepter</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Le fichier &apos;%1&apos; n&apos;a pu être trouvé.</translation>
    </message>
    <message>
        <source>File not found.</source>
        <translation type="obsolete">Fichier non trouvé.</translation>
    </message>
    <message>
        <source>Could not open file.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier.</translation>
    </message>
    <message>
        <source>File is no valid PwManager file.</source>
        <translation type="obsolete">Le fichier n&apos;est pas un fichier PwManager valide.</translation>
    </message>
    <message>
        <source>Unsupported file version.</source>
        <translation type="obsolete">Version de fichier non supportée.</translation>
    </message>
    <message>
        <source>Unsupported hash algorithm.</source>
        <translation type="obsolete">L&apos;algorithme de hachage non supporté. </translation>
    </message>
    <message>
        <source>Unsupported encryption algorithm.</source>
        <translation type="obsolete">Algorithme d&apos;encryptage non supporté.</translation>
    </message>
    <message>
        <source>Compressed files are not supported yet.</source>
        <translation type="obsolete">Fichiers de compression non supportés encore.</translation>
    </message>
    <message>
        <source>Wrong password.</source>
        <translation type="obsolete">Mauvais mot de passe.</translation>
    </message>
    <message>
        <source>File is damaged (hash test failed).</source>
        <translation type="obsolete">Le fichier est endommagé (Le test de hachage a échoué).</translation>
    </message>
    <message>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="obsolete">Donnée XML invalide (voir &apos;stdout pour plus de détails).</translation>
    </message>
    <message>
        <source>File is empty.</source>
        <translation type="obsolete">Le fichier est vide.</translation>
    </message>
    <message>
        <source>Invalid XML file (see stdout for details).</source>
        <translation type="obsolete">Fichier XML invalide (voir &apos;stdout&apos; pour plus de détails).</translation>
    </message>
    <message>
        <source>Invalid XML file.</source>
        <translation type="obsolete">Fichier XML invalide.</translation>
    </message>
    <message>
        <source>Document does not contain data.</source>
        <translation type="obsolete">Le document  ne contient pas de donnée.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Erreur</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation type="obsolete">Avertissement:</translation>
    </message>
    <message>
        <source>Invalid RGB color value.
</source>
        <translation type="obsolete">Valeur de la couleur RGB  invalide.</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="obsolete">Jamais</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="28"/>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <source>Alt+T</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="81"/>
        <source>A&amp;nhang</source>
        <translation>A&amp;nnexe</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+P</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Rechercher...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="150"/>
        <source>Search For:</source>
        <translation>Recherche de :</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="131"/>
        <source>Regular E&amp;xpression</source>
        <translation>E&amp;xpression Régulière</translation>
    </message>
    <message>
        <source>Alt+X</source>
        <translation type="obsolete">Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="124"/>
        <source>&amp;Case Sensitive</source>
        <translation>Respecter la &amp;casse</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="65"/>
        <source>Include:</source>
        <translation>Inclure :</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="109"/>
        <source>&amp;Titles</source>
        <translation>&amp;Títres</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="102"/>
        <source>&amp;Usernames</source>
        <translation>Nom d&apos;&amp;utilisateurs</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="95"/>
        <source>C&amp;omments</source>
        <translation>C&amp;ommentaires</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="88"/>
        <source>U&amp;RLs</source>
        <translation>U&amp;RLs</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="74"/>
        <source>Pass&amp;words</source>
        <translation>Mot de &amp;passe</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Chercher</translation>
    </message>
    <message>
        <source>Clo&amp;se</source>
        <translation type="obsolete">&amp;Quitter</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+Q</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="138"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Inclure les sous-groupes (récursif)</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Choix d&apos;icônes</translation>
    </message>
    <message>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">Ajouter une icône... </translation>
    </message>
    <message>
        <source>Pick</source>
        <translation type="obsolete">Sélectionner</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annuler</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message utf8="true">
        <source>Alt+Ö</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="14"/>
        <source>Settings</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="894"/>
        <source>Clear clipboard after:</source>
        <translation>Effacer le presse-papier après:</translation>
    </message>
    <message>
        <source>Seconds</source>
        <translation type="obsolete">Secondes</translation>
    </message>
    <message>
        <source>Sh&amp;ow passwords in plain text by default</source>
        <translation type="obsolete">A&amp;fficher le mot de passe en clair par défaut</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+F</translation>
    </message>
    <message>
        <source>Appea&amp;rance</source>
        <translation type="obsolete">Appa&amp;rence</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="421"/>
        <source>Banner Color</source>
        <translation>Couleur du bandeau</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="435"/>
        <source>Text Color:</source>
        <translation>Couleur du texte :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="476"/>
        <location filename="../forms/SettingsDlg.ui" line="564"/>
        <source>Change...</source>
        <translation>Changer...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="489"/>
        <source>Color 2:</source>
        <translation>Couleur 2 :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="614"/>
        <source>C&amp;hange...</source>
        <translation>C&amp;hanger...</translation>
    </message>
    <message>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="627"/>
        <source>Color 1:</source>
        <translation>Couleur 1 :</translation>
    </message>
    <message>
        <source>Expand group tree when opening a database</source>
        <translation type="obsolete">Développer l&apos;arborescence du groupe à l&apos;ouverture de la base de données</translation>
    </message>
    <message>
        <source>&amp;Other</source>
        <translation type="obsolete">Au&amp;tre</translation>
    </message>
    <message>
        <source>Remember last opend file</source>
        <translation type="obsolete">Se souvenir du dernier fichier ouvert</translation>
    </message>
    <message>
        <source>Browser Command:</source>
        <translation type="obsolete">Commande du navigateur:</translation>
    </message>
    <message>
        <source>Securi&amp;ty</source>
        <translation type="obsolete">Séc&amp;urité</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="662"/>
        <source>Alternating Row Colors</source>
        <translation>Couleurs alternées pour les rangées </translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1037"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="182"/>
        <source>Remember last key type and location</source>
        <translation>Se souvenir de la dernière  saisie de clé et  du dernier emplacement</translation>
    </message>
    <message>
        <source>Mounting Root:</source>
        <translation type="obsolete">Point de montage:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="154"/>
        <source>Remember last opened file</source>
        <translation>Se souvenir du dernier fichier ouvert</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="115"/>
        <source>Show system tray icon</source>
        <translation>Affiche l&apos;icône dans la zone de notification</translation>
    </message>
    <message>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation type="obsolete">Réduit dans la zone de notification au lieu de quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="258"/>
        <source>Save recent directories of file dialogs</source>
        <translation>Enregistre les répertoires récents des dialogues de fichiers</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="703"/>
        <source>Group tree at start-up:</source>
        <translation>Arborescence de groupe au démarrage :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="710"/>
        <source>Restore last state</source>
        <translation>Restaure l&apos;état précédent</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="717"/>
        <source>Expand all items</source>
        <translation>Développer tous les éléments</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="724"/>
        <source>Do not expand any item</source>
        <translation>Ne développe aucun élément</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="95"/>
        <source>Security</source>
        <translation>Sécurité</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="655"/>
        <source>Show window always on top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="866"/>
        <source>Edit Entry Dialog</source>
        <translation>Dialogue de modification d&apos;entrée</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1222"/>
        <source>Plug-Ins</source>
        <translation>Greffons</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1231"/>
        <source>None</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1238"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Intégration à Gnome (Gtk 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1245"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>Intégration à KDE4</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1260"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>Vous devez redémarrer le programme pour que les changements prennent effet.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1280"/>
        <source>Configure...</source>
        <translation>Configurer...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="100"/>
        <source>Advanced</source>
        <translation>Avancé</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="281"/>
        <source>Clear History Now</source>
        <translation>Vide l&apos;historique maintenant</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="303"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>Toujours demander avant d&apos;effacer les entrées ou les groupes</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="674"/>
        <source>Customize Entry Detail View...</source>
        <translation>Personnaliser la vue de détail de l&apos;entrée...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1182"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>Vous pouvez désactiver plusieurs fonctions de KeePassX ici selon vos besoins pour conserver l&apos;interface utilisateur claire.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1192"/>
        <source>Bookmarks</source>
        <translation>Signets</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1056"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>Réglage fin de la saisie automatique</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1065"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>Temps entre l&apos;activation d&apos;une action de saisie automatique par l&apos;utilisateur et le première frappe de touche simulée.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1068"/>
        <location filename="../forms/SettingsDlg.ui" line="1114"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1078"/>
        <source>Pre-Gap:</source>
        <translation>Pre-Gap :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1101"/>
        <source>Key Stroke Delay:</source>
        <translation>Délai de frappe des touches :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1111"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>Délai entre deux frappes de touches simulées. Augmenter le si la Saisie Automatique saute aléatoirement des caractères.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1030"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>Le répertoire où sont normalement montés les périphériques tels que les CD et les clés USB.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1020"/>
        <source>Media Root:</source>
        <translation>Racine des Media :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1046"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>Activer ceci si vous voulez utiliser vos signets et le dernier fichier ouvert indépendamment de leurs chemins absolus. Particulièrement utile en cas d&apos;utilisation portable de KeePassX et par conséquent avec des points de montage variables.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1049"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>Enregistrert les chemins relatifs (signets et dernier fichier)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="143"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>Réduit dans la zone de notification au lieu de la barre des tâches</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="212"/>
        <source>Start minimized</source>
        <translation>Démarre réduit</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="242"/>
        <source>Start locked</source>
        <translation>Démarre verrouillé</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="932"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>Verrouille l&apos;espace de travail lors de la minimisation de la fenêtre</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1129"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>Raccourci global de Saisie Automatique :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="996"/>
        <source>Custom Browser Command</source>
        <translation>Commande personnalisée du navigateur</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1008"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="390"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation>Sauvegarde automatique de la base de données à la sortie et au verrouillage de l&apos;espace de travail</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="857"/>
        <source>Show plain text passwords in:</source>
        <translation>Afficher les mots de passe en clair dans :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="873"/>
        <source>Database Key Dialog</source>
        <translation>Dialogue de clé de base de données</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="923"/>
        <location filename="../forms/SettingsDlg.ui" line="970"/>
        <source>seconds</source>
        <translation>secondes</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="941"/>
        <source>Lock database after inactivity of</source>
        <translation>Verrouille la base de données après une inactivité de </translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1158"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation>Utiliser le titre de l&apos;entrée pour correspondre à la fenêtre globale de Saisie Automatique</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="75"/>
        <source>General (1)</source>
        <translation>Général (1)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="80"/>
        <source>General (2)</source>
        <translation>Général (2)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="85"/>
        <source>Appearance</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="90"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="327"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation>Enregistrer les sauvegardes des entrées modifiées dans le groupe &apos;Sauvegarde&apos;</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="355"/>
        <source>Delete backup entries older than:</source>
        <translation>Effacer les entrées de sauvegarde de plus de :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="381"/>
        <source>days</source>
        <translation>jours</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="397"/>
        <source>Automatically save database after every change</source>
        <translation>Enregistrer automatiquement la base de données après chaque changement</translation>
    </message>
    <message>
        <source>System Language</source>
        <translation type="obsolete">Langue du Système</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="obsolete">Anglais</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="791"/>
        <source>Language:</source>
        <translation>Langue :</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="798"/>
        <source>Author:</source>
        <translation>Auteur :</translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation>Super_L</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Entrer votre mot de passe</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
</context>
<context>
    <name>StandardDatabase</name>
    <message>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Taille de fichier inattendue (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <source>Wrong Signature</source>
        <translation type="obsolete">Mauvaise signature</translation>
    </message>
    <message>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Version de fichier non supportée.</translation>
    </message>
    <message>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Algorithme d&apos;encryptage inconnu.</translation>
    </message>
    <message>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Le décryptage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Le test de hachage a échoué.
La clé est mauvaise ou le fichier est endommagé.</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [G2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[G2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E1]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E1]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E2]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E2]</translation>
    </message>
    <message>
        <source>Unexpected error: Offset is out of range. [E3]</source>
        <translation type="obsolete">Erreur inattendue: Le décalage est hors limite.[E3]</translation>
    </message>
    <message>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">N&apos;a pu ouvrir le fichier pour écriture.</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation>Saisie Automatique : Sélectionnez la fenêtre de destination</translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation>Pour spécifier la fenêtre de destination, sélectionnez une fenêtre actuellement
ouverte dans le menu déroulant ou entrez le nom de la fenêtre manuellement :</translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <location filename="../lib/tools.cpp" line="352"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation>fat115</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation> &lt;b&gt;Courriel:&lt;/b&gt;  fat115@free.fr
</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="351"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation>Français (France)</translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <source>Title</source>
        <translation type="obsolete">Títre</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Nom d&apos;utilisateur</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;￼p, li { white-space: pre-wrap; }￼&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;￼&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;L&apos;espace de travail est verrouillé.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>Déverrouiller</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>Fermer la base de données</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <source>Database Settings</source>
        <translation type="obsolete">Préférences de la Base de Données</translation>
    </message>
    <message>
        <source>Encryption</source>
        <translation type="obsolete">Encryptage</translation>
    </message>
    <message>
        <source>Algorithm:</source>
        <translation type="obsolete">Algorithme:</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <source>Encryption Rounds:</source>
        <translation type="obsolete">Nombre de passes:</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation type="obsolete">&amp;Accepter</translation>
    </message>
    <message>
        <source>Ctrl+K</source>
        <translation type="obsolete">Ctrl+A	</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">A&amp;nnuler</translation>
    </message>
    <message>
        <source>Ctrl+C</source>
        <translation type="obsolete">Ctrl+N</translation>
    </message>
</context>
</TS>
