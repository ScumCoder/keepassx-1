<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="fi_FI">
<defaultcodec></defaultcodec>
<context>
    <name>PPDOptionsModel</name>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="1595"/>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="1597"/>
        <source>Value</source>
        <translation>Arvo</translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <location filename="../src/qt3support/other/q3accel.cpp" line="462"/>
        <source>%1, %2 not defined</source>
        <translation>%1, %2 ei määritelty</translation>
    </message>
    <message>
        <location filename="../src/qt3support/other/q3accel.cpp" line="497"/>
        <source>Ambiguous %1 not handled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="253"/>
        <source>True</source>
        <translation>Tosi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="254"/>
        <source>False</source>
        <translation>Epätosi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="766"/>
        <source>Insert</source>
        <translation>Lisää</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="767"/>
        <source>Update</source>
        <translation>Päivitä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="768"/>
        <source>Delete</source>
        <translation>Poista</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="834"/>
        <source>Copy or Move a File</source>
        <translation>Kopioi tai siirrä tiedosto</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="842"/>
        <source>Read: %1</source>
        <translation>Lue: %1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="878"/>
        <source>Write: %1</source>
        <translation>Kirjoita: %1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2470"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_mac.cpp" line="80"/>
        <source>All Files (*)</source>
        <translation>Kaikki tiedostot (*)</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2426"/>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2427"/>
        <source>Size</source>
        <translation>Koko</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2429"/>
        <source>Type</source>
        <translation>Tyyppi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2430"/>
        <source>Date</source>
        <translation>Päiväys</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2431"/>
        <source>Attributes</source>
        <translation>Attribuutit</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4530"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2506"/>
        <source>Look &amp;in:</source>
        <translation>Etsi &amp;kohteista:</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4537"/>
        <source>File &amp;name:</source>
        <translation>Tiedosto&amp;nimi:</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2508"/>
        <source>File &amp;type:</source>
        <translation>Tiedosto&amp;tyyppi:</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2515"/>
        <source>Back</source>
        <translation>Takaisin</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2522"/>
        <source>One directory up</source>
        <translation>Yksin kansio ylös</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2531"/>
        <source>Create New Folder</source>
        <translation>Luo uusi kansio</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2549"/>
        <source>List View</source>
        <translation>Listanäkymä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2557"/>
        <source>Detail View</source>
        <translation>Yksityiskohtanäkymä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2566"/>
        <source>Preview File Info</source>
        <translation>Esikatsele tiedoston tietoja</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2589"/>
        <source>Preview File Contents</source>
        <translation>Esikatsele tiedoston sisältöä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2677"/>
        <source>Read-write</source>
        <translation>Luku-kirjoitus</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2678"/>
        <source>Read-only</source>
        <translation>Vain luku</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2679"/>
        <source>Write-only</source>
        <translation>Vain kirjoitus</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2680"/>
        <source>Inaccessible</source>
        <translation>Ei oikeuksia</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2682"/>
        <source>Symlink to File</source>
        <translation>Symbolinen linkki tiedostoon</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2683"/>
        <source>Symlink to Directory</source>
        <translation>Symbolinen linkki kansioon</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2684"/>
        <source>Symlink to Special</source>
        <translation>Symbolinen linkki erikoistiedostoon</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2685"/>
        <source>File</source>
        <translation>Tiedosto</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2686"/>
        <source>Dir</source>
        <translation>Kansio</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="2687"/>
        <source>Special</source>
        <translation>Erikoistiedosto</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="308"/>
        <source>Open</source>
        <translation>Avaa</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="392"/>
        <source>Save As</source>
        <translation>Tallenna nimellä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4536"/>
        <source>&amp;Open</source>
        <translation>&amp;Avaa</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4520"/>
        <source>&amp;Save</source>
        <translation>&amp;Tallenna</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4179"/>
        <source>&amp;Rename</source>
        <translation>Nimeä &amp;uudestaan</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4180"/>
        <source>&amp;Delete</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4200"/>
        <source>R&amp;eload</source>
        <translation>Lataa uud&amp;estaan</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4204"/>
        <source>Sort by &amp;Name</source>
        <translation>Järjestä &amp;nimen mukaan</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4206"/>
        <source>Sort by &amp;Size</source>
        <translation>Järjestä &amp;koon mukaan</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4207"/>
        <source>Sort by &amp;Date</source>
        <translation>Järjestä &amp;päiväyksen mukaan</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4209"/>
        <source>&amp;Unsorted</source>
        <translation>Ei &amp;järjestetty</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4224"/>
        <source>Sort</source>
        <translation>Järjestä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4228"/>
        <source>Show &amp;hidden files</source>
        <translation>Näytä &amp;piilotetut tiedostot</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4259"/>
        <source>the file</source>
        <translation>tiedosto</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4261"/>
        <source>the directory</source>
        <translation>kansio</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4263"/>
        <source>the symlink</source>
        <translation>symbolinen linkki</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4266"/>
        <source>Delete %1</source>
        <translation>Poista %1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4268"/>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;Haluatko varmasti poistaa %1 &quot;%2&quot;?&lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4269"/>
        <source>&amp;Yes</source>
        <translation>&amp;Kyllä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4269"/>
        <source>&amp;No</source>
        <translation>&amp;Ei</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4305"/>
        <source>New Folder 1</source>
        <translation>Uusi kansio 1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4310"/>
        <source>New Folder</source>
        <translation>Uusi kansio</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4315"/>
        <source>New Folder %1</source>
        <translation>Uusi kansio %1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4420"/>
        <source>Find Directory</source>
        <translation>Etsi kansio</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4533"/>
        <source>Directories</source>
        <translation>Kansiot</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4531"/>
        <source>Directory:</source>
        <translation>Kansio:</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="5716"/>
        <source>Error</source>
        <translation>Virhe</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="4572"/>
        <source>%1
File not found.
Check path and filename.</source>
        <translation>%1 tiedostoa ei ole olemassa. Tarkista polku ja tiedostonimi.</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="103"/>
        <source>All Files (*.*)</source>
        <translation>Kaikki tiedostot (*.*)</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="478"/>
        <source>Open </source>
        <translation>Avaa</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="633"/>
        <source>Select a Directory</source>
        <translation>Valitse kansio</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="113"/>
        <source>Could not read directory
%1</source>
        <translation>Kansiota %1 ei voitu lukea</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="158"/>
        <source>Could not create directory
%1</source>
        <translation>Kansiota %1 ei voitu luoda</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="192"/>
        <source>Could not remove file or directory
%1</source>
        <translation>Tiedostoa tai kansiota %1 ei voitu poistaa</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="219"/>
        <source>Could not rename
%1
to
%2</source>
        <translation>Kohdetta %1 ei voitu nimetä uudestaan nimelle %2</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="244"/>
        <source>Could not open
%1</source>
        <translation>%1 ei voitu avata</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="312"/>
        <source>Could not write
%1</source>
        <translation>%1 ei voitu kirjoittaa</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <location filename="../src/qt3support/widgets/q3mainwindow.cpp" line="2050"/>
        <source>Line up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/qt3support/widgets/q3mainwindow.cpp" line="2052"/>
        <source>Customize...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <location filename="../src/qt3support/network/q3networkprotocol.cpp" line="827"/>
        <source>Operation stopped by the user</source>
        <translation>Käyttäjä keskeytti toiminnon</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3progressdialog.cpp" line="292"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="973"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="617"/>
        <source>Apply</source>
        <translation>Toteuta</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="660"/>
        <source>Help</source>
        <translation>Ohje</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="705"/>
        <source>Defaults</source>
        <translation>Oletus</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="755"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="5424"/>
        <source>&amp;Undo</source>
        <translation>&amp;Peru</translation>
    </message>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="5425"/>
        <source>&amp;Redo</source>
        <translation>&amp;Tee uudestaan</translation>
    </message>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="5430"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Leikkaa</translation>
    </message>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="5431"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopioi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="5433"/>
        <source>&amp;Paste</source>
        <translation>L&amp;iiitä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="5436"/>
        <source>Clear</source>
        <translation>Tyhjennä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="5442"/>
        <source>Select All</source>
        <translation>Valitse kaikki</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="216"/>
        <source>System</source>
        <translation>Järjestelmä</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="219"/>
        <source>Restore up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="220"/>
        <source>Minimize</source>
        <translation>Pienennä</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="223"/>
        <source>Restore down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="224"/>
        <source>Maximize</source>
        <translation>Suurenna</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="226"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="244"/>
        <source>Contains commands to manipulate the window</source>
        <translation>Sisältää ikkunan muokkaukseen liittyviä komentoja</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="247"/>
        <source>Puts a minimized back to normal</source>
        <translation>Palauttaa pienennetyn ikkunan</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="248"/>
        <source>Moves the window out of the way</source>
        <translation>Siirtää ikkunan pois tieltä</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="251"/>
        <source>Puts a maximized window back to normal</source>
        <translation>Palauttaa suurennetun ikkunan</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="252"/>
        <source>Makes the window full screen</source>
        <translation>Näytä ikkuna kokoruututilassa</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="254"/>
        <source>Closes the window</source>
        <translation>Sulkee ikkunan</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="256"/>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation>Näyttää ikkunan nimen ja sisältää ohjaimia ikkunan käsittelyyn</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <location filename="../src/qt3support/widgets/q3toolbar.cpp" line="656"/>
        <source>More...</source>
        <translation>Lisää...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="624"/>
        <source>The protocol `%1&apos; is not supported</source>
        <translation>protokolla &quot;%1&quot; ei ole tuettu</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="364"/>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation>Protokolla &quot;%1&quot; ei tue kansiolistauksia</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="367"/>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation>Protokolla &quot;%1&quot; ei tue kansioiden luontia</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="370"/>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation>Protokolla &quot;%1&quot; ei tue tiedostojen tai kansioiden poistoa</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="373"/>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation>Protokolla &quot;%1&quot; eitue tiedostojen tai kansioiden uudelleennimeämistä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="376"/>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation>Protokolla &quot;%1&quot; ei tue tiedostojen noutoa</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="379"/>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation>Protokolla &quot;%1&quot; ei tue tiedostojen lisäystä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="626"/>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation>Protokolla &quot;%1&quot; ei tue tiedostojen tai kansioiden kopiointia tai siirtoa</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="881"/>
        <source>(unknown)</source>
        <translation>(tuntematon)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="145"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Peru</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="146"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Takaisin</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="147"/>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Seuraava &gt;</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="148"/>
        <source>&amp;Finish</source>
        <translation>&amp;Valmis</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="149"/>
        <source>&amp;Help</source>
        <translation>O&amp;hje</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <location filename="../src/network/qabstractsocket.cpp" line="703"/>
        <source>Host not found</source>
        <translation>Isäntää ei löytynyt</translation>
    </message>
    <message>
        <location filename="../src/network/qhttpsocketengine.cpp" line="479"/>
        <source>Connection refused</source>
        <translation>Yhteys estettiin</translation>
    </message>
    <message>
        <location filename="../src/network/qabstractsocket.cpp" line="1449"/>
        <source>Socket operation timed out</source>
        <translation>Pistekeoperaation aikakatkaisu</translation>
    </message>
    <message>
        <location filename="../src/network/qabstractsocket.cpp" line="1831"/>
        <source>Socket is not connected</source>
        <translation>Pistoke ei ole yhdistetty</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="1148"/>
        <source>&amp;Step up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="1150"/>
        <source>Step &amp;down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="1142"/>
        <source>&amp;Select All</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="346"/>
        <source>Activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="308"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="310"/>
        <source>Incompatible Qt Library Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="2012"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="348"/>
        <source>Activates the program&apos;s main window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <location filename="../src/activeqt/container/ui_qaxselect.h" line="123"/>
        <source>Select ActiveX Control</source>
        <translation>Valitse ActiveX-ohjain</translation>
    </message>
    <message>
        <location filename="../src/activeqt/container/ui_qaxselect.h" line="124"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/activeqt/container/ui_qaxselect.h" line="125"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Peru</translation>
    </message>
    <message>
        <location filename="../src/activeqt/container/ui_qaxselect.h" line="126"/>
        <source>COM &amp;Object:</source>
        <translation>COM-&amp;olio:</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="84"/>
        <source>Uncheck</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="87"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="88"/>
        <source>Toggle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1204"/>
        <source>Hu&amp;e:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1205"/>
        <source>&amp;Sat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1206"/>
        <source>&amp;Val:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1207"/>
        <source>&amp;Red:</source>
        <translation>&amp;Punainen:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1208"/>
        <source>&amp;Green:</source>
        <translation>&amp;Vihreä:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1209"/>
        <source>Bl&amp;ue:</source>
        <translation>&amp;Sininen:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1210"/>
        <source>A&amp;lpha channel:</source>
        <translation>A&amp;lphakanava:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1410"/>
        <source>&amp;Basic colors</source>
        <translation>&amp;Perusvärit</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1411"/>
        <source>&amp;Custom colors</source>
        <translation>&amp;Omat värit</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1412"/>
        <source>&amp;Define Custom Colors &gt;&gt;</source>
        <translation>&amp;Määrittele omat värit &gt;&gt;</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1415"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1416"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1413"/>
        <source>&amp;Add to Custom Colors</source>
        <translation>&amp;Lisää omiin väreihin</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="1539"/>
        <source>Select color</source>
        <translation>Valitse väri</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="1394"/>
        <source>Open</source>
        <translation>Avaa</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qitemeditorfactory.cpp" line="515"/>
        <source>False</source>
        <translation>Epätosi</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qitemeditorfactory.cpp" line="516"/>
        <source>True</source>
        <translation>Tosi</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="1394"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="1153"/>
        <source>Unable to connect</source>
        <translation>Yhteyttä ei saatu</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="1414"/>
        <source>Unable to commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="1431"/>
        <source>Unable to rollback transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="1446"/>
        <source>Unable to set autocommit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="768"/>
        <source>Unable to execute statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="562"/>
        <source>Unable to prepare statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="758"/>
        <source>Unable to bind variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="849"/>
        <source>Unable to fetch record %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="866"/>
        <source>Unable to fetch next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="885"/>
        <source>Unable to fetch first</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="2061"/>
        <source>AM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="2061"/>
        <source>am</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="2063"/>
        <source>PM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="2063"/>
        <source>pm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="920"/>
        <source>QDial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="922"/>
        <source>SpeedoMeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="924"/>
        <source>SliderHandle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qdialog.cpp" line="490"/>
        <source>What&apos;s This?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="525"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="528"/>
        <source>Save</source>
        <translation>Tallenna</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="531"/>
        <source>Open</source>
        <translation>Avaa</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="534"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="537"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="540"/>
        <source>Apply</source>
        <translation>Toteuta</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="543"/>
        <source>Reset</source>
        <translation>Nollaa</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="546"/>
        <source>Help</source>
        <translation>Ohje</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="550"/>
        <source>Don&apos;t Save</source>
        <translation>Älä tallenna</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="554"/>
        <source>Discard</source>
        <translation>Hylkää</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="557"/>
        <source>&amp;Yes</source>
        <translation>&amp;Kyllä</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="560"/>
        <source>Yes to &amp;All</source>
        <translation>Kyllä k&amp;aikkiin</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="563"/>
        <source>&amp;No</source>
        <translation>&amp;Ei</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="566"/>
        <source>N&amp;o to All</source>
        <translation>E&amp;i kaikkiin</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="569"/>
        <source>Save All</source>
        <translation>Tallenna kaikki</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="572"/>
        <source>Abort</source>
        <translation>Keskeytä</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="575"/>
        <source>Retry</source>
        <translation>Yritä uudestaan</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="578"/>
        <source>Ignore</source>
        <translation>Älä huomioi</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="581"/>
        <source>Restore Defaults</source>
        <translation>Palauta oletukset</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="552"/>
        <source>Close without Saving</source>
        <translation>Sulje tallentamatta</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="419"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="420"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="423"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="425"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="431"/>
        <source>Date Modified</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="1198"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="1201"/>
        <source>Dock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="1201"/>
        <source>Float</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="387"/>
        <source>More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="389"/>
        <source>Less</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="131"/>
        <source>Debug Message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="134"/>
        <source>Warning:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="137"/>
        <source>Fatal Error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="287"/>
        <source>&amp;Show this message again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="288"/>
        <source>&amp;OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog_mac.cpp" line="84"/>
        <source>All Files (*)</source>
        <translation>Kaikki tiedostot (*)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="747"/>
        <source>Directories</source>
        <translation>Kansiot</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="784"/>
        <source>&amp;Open</source>
        <translation>&amp;Avaa</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="784"/>
        <source>&amp;Save</source>
        <translation>&amp;Tallenna</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="416"/>
        <source>Open</source>
        <translation>Avaa</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="1557"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1 on jo olemassa.
Haluatko korvata sen?</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="1575"/>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>%1 tiedostoa ei löydy.
Tarkista, että annoit oikean tiedostonimen.</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="823"/>
        <source>My Computer</source>
        <translation>Oma tietokone</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="442"/>
        <source>&amp;Rename</source>
        <translation>&amp;Nimeä uudestaan</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="443"/>
        <source>&amp;Delete</source>
        <translation>&amp;Poista</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="444"/>
        <source>Show &amp;hidden files</source>
        <translation>Näytä &amp;piilotiedostot</translation>
    </message>
    <message>
        <location filename="../src/gui/ui_qfiledialog.h" line="257"/>
        <source>Back</source>
        <translation>Takaisin</translation>
    </message>
    <message>
        <location filename="../src/gui/ui_qfiledialog.h" line="259"/>
        <source>Parent Directory</source>
        <translation>Yläkansio</translation>
    </message>
    <message>
        <location filename="../src/gui/ui_qfiledialog.h" line="261"/>
        <source>List View</source>
        <translation>Listanäkymä</translation>
    </message>
    <message>
        <location filename="../src/gui/ui_qfiledialog.h" line="262"/>
        <source>Detail View</source>
        <translation>Yksityiskohdat</translation>
    </message>
    <message>
        <location filename="../src/gui/ui_qfiledialog.h" line="263"/>
        <source>Files of type:</source>
        <translation>Tiedostot tyyppiä:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="749"/>
        <source>Directory:</source>
        <translation>Kansio:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="1518"/>
        <source>
File not found.
Please verify the correct file name was given</source>
        <translation>Tiedostoa ei löydy.
Varmista, että annoit oikean tiedostonimen</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="2269"/>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>%1 kansiota ei löydy.
Varmista, että annoit oikean kansion nimen.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="2104"/>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>&quot;%1&quot; on kirjoitussuojattu.
Haluatko silti poistaa sen?</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="2109"/>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation>Haluatko varmasti poistaa kohteen &quot;%1&quot;?</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="2120"/>
        <source>Could not delete directory.</source>
        <translation>Kansiota ei voitu poistaa.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog_win.cpp" line="105"/>
        <source>All Files (*.*)</source>
        <translation>Kaikki tiedostot (*.*)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="418"/>
        <source>Save As</source>
        <translation>Tallenna nimellä</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="332"/>
        <source>Drive</source>
        <translation>Asema</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="336"/>
        <source>File</source>
        <translation>Tiedosto</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="365"/>
        <source>Unknown</source>
        <translation>Tuntematon</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="420"/>
        <source>Find Directory</source>
        <translation>Etsi kansio</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="438"/>
        <source>Show </source>
        <translation>Näytä</translation>
    </message>
    <message>
        <location filename="../src/gui/ui_qfiledialog.h" line="258"/>
        <source>Forward</source>
        <translation>Eteenpäin</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="1997"/>
        <source>New Folder</source>
        <translation>Uusi kansio</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="445"/>
        <source>&amp;New Folder</source>
        <translation>&amp;Uusi kansio</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="782"/>
        <source>&amp;Choose</source>
        <translation>&amp;Valitse</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qsidebar.cpp" line="376"/>
        <source>Remove</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="752"/>
        <source>File &amp;name:</source>
        <translation>Tiedosto&amp;nimi:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui_qfiledialog.h" line="256"/>
        <source>Look in:</source>
        <translation>Etsi kohteista:</translation>
    </message>
    <message>
        <location filename="../src/gui/ui_qfiledialog.h" line="260"/>
        <source>Create New Folder</source>
        <translation>Luo uusi kansio</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="560"/>
        <source>Invalid filename</source>
        <translation>Virheellinen tiedostonimi</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="562"/>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="607"/>
        <source>Name</source>
        <translation>Nimi</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="609"/>
        <source>Size</source>
        <translation>Koko</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="613"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>Tyyppi</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="615"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>Tyyppi</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="622"/>
        <source>Date Modified</source>
        <translation>Muokattu</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel_p.h" line="261"/>
        <source>My Computer</source>
        <translation>Oma Tietokone</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel_p.h" line="263"/>
        <source>Computer</source>
        <translation>Tietokone</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="479"/>
        <source>%1 TB</source>
        <translation>%1 TB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="481"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="483"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="485"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="486"/>
        <source>%1 bytes</source>
        <translation>%1 tavua</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="786"/>
        <source>&amp;Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="787"/>
        <source>Font st&amp;yle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="788"/>
        <source>&amp;Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="789"/>
        <source>Effects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="790"/>
        <source>Stri&amp;keout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="791"/>
        <source>&amp;Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="792"/>
        <source>Sample</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="793"/>
        <source>Wr&amp;iting System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="449"/>
        <source>Select Font</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="653"/>
        <source>Not connected</source>
        <translation>Ei yhteyttä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="718"/>
        <source>Host %1 not found</source>
        <translation>Palvelinta %1 ei löydy</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="722"/>
        <source>Connection refused to host %1</source>
        <translation>Palvelin %1 esti yhteyden</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2327"/>
        <source>Connected to host %1</source>
        <translation>Yhdistetty palvelimeen %1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="985"/>
        <source>Connection refused for data connection</source>
        <translation>Datayhteyden avaus estettiin</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="1960"/>
        <source>Unknown error</source>
        <translation>Tuntematon virhe</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2037"/>
        <source>Connecting to host failed:
%1</source>
        <translation>Yhteys palvelimeen epäonnistui:
%1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2040"/>
        <source>Login failed:
%1</source>
        <translation>Kirjautuminen epäonnistui: %1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2043"/>
        <source>Listing directory failed:
%1</source>
        <translation>Kansion sisällön näyttö epäonnistui.
%1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2046"/>
        <source>Changing directory failed:
%1</source>
        <translation>Kansioon siirtyminen epäonnistui:
%1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2049"/>
        <source>Downloading file failed:
%1</source>
        <translation>Tiedoston nouto epäonnistui:
%1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2052"/>
        <source>Uploading file failed:
%1</source>
        <translation>Tiedoston vienti epäonnistui:
%1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2055"/>
        <source>Removing file failed:
%1</source>
        <translation>Tiedoston poisto epäonnistui:
%1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2058"/>
        <source>Creating directory failed:
%1</source>
        <translation>Kansion luonti epäonnistui:
%1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2061"/>
        <source>Removing directory failed:
%1</source>
        <translation>Kansion poisto epäonnistui:
%1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2336"/>
        <source>Connection closed</source>
        <translation>Yhteys suljettu</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2325"/>
        <source>Host %1 found</source>
        <translation>Palvelin %1 löytyi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2329"/>
        <source>Connection to %1 closed</source>
        <translation>Yhteys palvelimeen %1 suljettiin</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2332"/>
        <source>Host found</source>
        <translation>Palvelin löytyi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="2334"/>
        <source>Connected to host</source>
        <translation>Yhdistetty palvelimeen</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <location filename="../src/network/qhostinfo_p.h" line="136"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <location filename="../src/network/qhostinfo_win.cpp" line="207"/>
        <source>Host not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qhostinfo_win.cpp" line="202"/>
        <source>Unknown address type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qhostinfo_win.cpp" line="210"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1749"/>
        <source>Unknown error</source>
        <translation>Tuntematon virhe</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1379"/>
        <source>Request aborted</source>
        <translation>Pyyntö keskeytettiin</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1760"/>
        <source>No server set to connect to</source>
        <translation>Ei palvelinta johon ottaa yhteyttä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1816"/>
        <source>Wrong content length</source>
        <translation>Virheellinen sisällön pituus</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1820"/>
        <source>Server closed connection unexpectedly</source>
        <translation>Palvelin katkaisi yhteyden odottamatta</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1858"/>
        <source>Connection refused</source>
        <translation>Yhteys estettiin</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1861"/>
        <source>Host %1 not found</source>
        <translation>Palvelinta %1 ei löydy</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1864"/>
        <source>HTTP request failed</source>
        <translation>HTTP-pyyntö epäonnistui</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="1933"/>
        <source>Invalid HTTP response header</source>
        <translation>Virheellinen HTTP-vastausotsake</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="2020"/>
        <source>Invalid HTTP chunked body</source>
        <translation>Virheellinen HTTP-paloiteltu runko</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="2314"/>
        <source>Host %1 found</source>
        <translation>Palvelin %1 löytyi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="2317"/>
        <source>Connected to host %1</source>
        <translation>Yhdistetty palvelimeen %1</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="2320"/>
        <source>Connection to %1 closed</source>
        <translation>Yhteys palvelimeen %1 katkaistu</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="2328"/>
        <source>Host found</source>
        <translation>Palvelin löytyi</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="2331"/>
        <source>Connected to host</source>
        <translation>Yhdistetty palvelimeen</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="2334"/>
        <source>Connection closed</source>
        <translation>Yhteys suljettu</translation>
    </message>
    <message>
        <location filename="../src/network/qhttp.cpp" line="2652"/>
        <source>Proxy authentication required</source>
        <translation>Välipalvelin vaatii tunnistautumisen</translation>
    </message>
    <message>
        <location filename="../src/network/qhttp.cpp" line="2656"/>
        <source>Authentication required</source>
        <translation>Tunnistautuminen vaaditaan</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <location filename="../src/network/qhttpsocketengine.cpp" line="492"/>
        <source>Authentication required</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="1344"/>
        <source>Error opening database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="1377"/>
        <source>Could not start transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="1390"/>
        <source>Unable to commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="1403"/>
        <source>Unable to rollback transaction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="353"/>
        <source>Unable to create BLOB</source>
        <translation>BLOB:ia ei voitu luoda</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="359"/>
        <source>Unable to write BLOB</source>
        <translation>BLOB:iin ei voitu kirjoittaa</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="373"/>
        <source>Unable to open BLOB</source>
        <translation>BLOB:ia ei voi avata</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="391"/>
        <source>Unable to read BLOB</source>
        <translation>BLOB:ia ei voi lukea</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="703"/>
        <source>Could not find array</source>
        <translation>Taulukkoa ei löytynyt</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="546"/>
        <source>Could not get array data</source>
        <translation>Taulukon tietoja ei saatu</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="758"/>
        <source>Could not get query info</source>
        <translation>Kyselytietoja ei saatu</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="778"/>
        <source>Could not start transaction</source>
        <translation>Transaktiota ei voitu aloittaa</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="797"/>
        <source>Unable to commit transaction</source>
        <translation>Transaktiota ei voitu toteuttaa</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="830"/>
        <source>Could not allocate statement</source>
        <translation>Lauseketta ei voitu varata</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="835"/>
        <source>Could not prepare statement</source>
        <translation>Lauseketta ei voitu valmistella</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="848"/>
        <source>Could not describe input statement</source>
        <translation>Syötelauseketta ei voitu kuvata</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="857"/>
        <source>Could not describe statement</source>
        <translation>Lauseketta ei voitu kuvata</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="972"/>
        <source>Unable to close statement</source>
        <translation>Lauseketta ei voitu sulkea</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="980"/>
        <source>Unable to execute query</source>
        <translation>Kyselyä ei voitu suorittaa</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="1022"/>
        <source>Could not fetch next item</source>
        <translation>Seuraavaa kohdetta ei voitu noutaa</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="1182"/>
        <source>Could not get statement info</source>
        <translation>Tietoja lausekkeesta ei saatu</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="1982"/>
        <source>Permission denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="1985"/>
        <source>Too many open files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="1988"/>
        <source>No such file or directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="1991"/>
        <source>No space left on device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qiodevice.cpp" line="1484"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="202"/>
        <source>XIM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="225"/>
        <source>XIM input method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="229"/>
        <source>Windows input method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="233"/>
        <source>Mac OS X input method</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="50"/>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="76"/>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="91"/>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="355"/>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="376"/>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="383"/>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="620"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="636"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="928"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="602"/>
        <source>The shared library was not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="604"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="643"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="2489"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="2491"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="2495"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="2497"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="2499"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="2502"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="2506"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="1077"/>
        <source>Unable to open database &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="1084"/>
        <source>Unable to connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="1204"/>
        <source>Unable to begin transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="1221"/>
        <source>Unable to commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="1238"/>
        <source>Unable to rollback transaction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="444"/>
        <source>Unable to fetch data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="594"/>
        <source>Unable to execute query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="600"/>
        <source>Unable to store result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="731"/>
        <source>Unable to prepare statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="762"/>
        <source>Unable to reset statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="849"/>
        <source>Unable to bind value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="860"/>
        <source>Unable to execute statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="895"/>
        <source>Unable to bind outvalues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="883"/>
        <source>Unable to store statement results</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="255"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="547"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="550"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="553"/>
        <source>Restore Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="893"/>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="896"/>
        <source>&amp;Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="897"/>
        <source>&amp;Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="898"/>
        <source>Mi&amp;nimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="900"/>
        <source>Ma&amp;ximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="902"/>
        <source>Stay on &amp;Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="905"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="345"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="346"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="399"/>
        <source>Execute</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="308"/>
        <source>About</source>
        <translation>Tietoja</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="314"/>
        <source>Config</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="315"/>
        <source>Preference</source>
        <translation>Asetus</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="316"/>
        <source>Options</source>
        <translation>Valinnat</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="317"/>
        <source>Setting</source>
        <translation>Asetus</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="318"/>
        <source>Setup</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="320"/>
        <source>Quit</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="321"/>
        <source>Exit</source>
        <translation>Lopeta</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="356"/>
        <source>About %1</source>
        <translation>Tietoja - %1</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="358"/>
        <source>About Qt</source>
        <translation>Tietoja Qt:sta</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="360"/>
        <source>Preferences</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qmenu_mac.cpp" line="362"/>
        <source>Quit %1</source>
        <translation>Sulje %1</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/gui/dialogs/qdialog.cpp" line="585"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="274"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="1456"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="1636"/>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="1635"/>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qtopia Core.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://www.trolltech.com/qt/&quot;&gt;www.trolltech.com/qt/&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="83"/>
        <source>Show Details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="83"/>
        <source>Hide Details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="1644"/>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://www.trolltech.com/company/model/&quot;&gt;www.trolltech.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontext.cpp" line="55"/>
        <source>Select IM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontextplugin.cpp" line="66"/>
        <source>Multiple input method switcher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontextplugin.cpp" line="73"/>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="169"/>
        <source>The remote host closed the connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="172"/>
        <source>Network operation timed out</source>
        <translation>Verkkotoiminto aikakatkaistiin</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="175"/>
        <source>Out of resources</source>
        <translation>Resurssit loppuivat</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="178"/>
        <source>Unsupported socket operation</source>
        <translation>Pistoketoiminto ei ole tuettu</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="181"/>
        <source>Protocol type not supported</source>
        <translation>Protokollatyyppi ei ole tuettu</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="184"/>
        <source>Invalid socket descriptor</source>
        <translation>Virheellinen pistokkeen kuvaaja</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="190"/>
        <source>Network unreachable</source>
        <translation>Verkko ei ole saatavilla</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="193"/>
        <source>Permission denied</source>
        <translation>Lupa evätty</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="196"/>
        <source>Connection timed out</source>
        <translation>Yhteys aikakatkaistiin</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="199"/>
        <source>Connection refused</source>
        <translation>Yhteys estettiin</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="202"/>
        <source>The bound address is already in use</source>
        <translation>Sidottu osoite on jo käytössä</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="205"/>
        <source>The address is not available</source>
        <translation>Osoite ei ole saatavilla</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="208"/>
        <source>The address is protected</source>
        <translation>Osoite on suojattu</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="214"/>
        <source>Unable to send a message</source>
        <translation>Viestiä ei voitu lähettää</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="217"/>
        <source>Unable to receive a message</source>
        <translation>Viestiä ei voitu vastaanottaa</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="220"/>
        <source>Unable to write</source>
        <translation>Ei voitu kirjoittaa</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="223"/>
        <source>Network error</source>
        <translation>Verkkovirhe</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="226"/>
        <source>Another socket is already listening on the same port</source>
        <translation>Toinen pistoke kuuntelee jo samaa porttia</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="160"/>
        <source>Unable to initialize non-blocking socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="163"/>
        <source>Unable to initialize broadcast socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="166"/>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>Yritettiin käyttää IPv6-pistoketta alustalla joka ei sisällä IPv6-tukea</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="187"/>
        <source>Host unreachable</source>
        <translation>Palvelinta ei voi saavuttaa</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="211"/>
        <source>Datagram was too large to send</source>
        <translation>Paketti oli liian suuri lähetettäväksi</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="229"/>
        <source>Operation on non-socket</source>
        <translation>Toiminnon kohde ei ole pistoke</translation>
    </message>
    <message>
        <location filename="../src/network/qnativesocketengine.cpp" line="232"/>
        <source>Unknown error</source>
        <translation>Tuntematon virhe</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1919"/>
        <source>Unable to logon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1783"/>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1283"/>
        <source>Unable to bind column for batch execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1297"/>
        <source>Unable to execute batch statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1574"/>
        <source>Unable to goto next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1631"/>
        <source>Unable to alloc statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1646"/>
        <source>Unable to prepare statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1664"/>
        <source>Unable to bind value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1689"/>
        <source>Unable to execute select statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="1706"/>
        <source>Unable to execute statement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1518"/>
        <source>Unable to connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1525"/>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1708"/>
        <source>Unable to disable autocommit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1725"/>
        <source>Unable to commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1742"/>
        <source>Unable to rollback transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1757"/>
        <source>Unable to enable autocommit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1056"/>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1317"/>
        <source>Unable to execute statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="820"/>
        <source>Unable to fetch next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1073"/>
        <source>Unable to prepare statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="1308"/>
        <source>Unable to bind variable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/gui/util/qdesktopservices_mac.cpp" line="147"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="548"/>
        <source>Unable to connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="587"/>
        <source>Could not begin transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="604"/>
        <source>Could not commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="620"/>
        <source>Could not rollback transaction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="140"/>
        <source>Unable to create query</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="251"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="194"/>
        <source>The plugin was not loaded.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="186"/>
        <source>locally connected</source>
        <translation>paikallisesti yhdistetty</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="411"/>
        <source>Aliases: %1</source>
        <translation>Aliakset: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="854"/>
        <source>unknown</source>
        <translation>tuntematon</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="947"/>
        <source>Portrait</source>
        <translation>Pysty</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="948"/>
        <source>Landscape</source>
        <translation>Vaaka</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="776"/>
        <source>A0 (841 x 1189 mm)</source>
        <translation>A0 (841 x 1189 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="777"/>
        <source>A1 (594 x 841 mm)</source>
        <translation>A1 (594 x 841 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="778"/>
        <source>A2 (420 x 594 mm)</source>
        <translation>A2 (420 x 594 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="779"/>
        <source>A3 (297 x 420 mm)</source>
        <translation>A3 (297 x 420 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="780"/>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation>A4 (210 x 297 mm, 8.26 x 11.7 inches)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="781"/>
        <source>A5 (148 x 210 mm)</source>
        <translation>A5 (148 x 210 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="782"/>
        <source>A6 (105 x 148 mm)</source>
        <translation>A6 (105 x 148 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="783"/>
        <source>A7 (74 x 105 mm)</source>
        <translation>A7 (74 x 105 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="784"/>
        <source>A8 (52 x 74 mm)</source>
        <translation>A8 (52 x 74 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="785"/>
        <source>A9 (37 x 52 mm)</source>
        <translation>A9 (37 x 52 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="786"/>
        <source>B0 (1000 x 1414 mm)</source>
        <translation>B0 (1000 x 1414 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="787"/>
        <source>B1 (707 x 1000 mm)</source>
        <translation>B1 (707 x 1000 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="788"/>
        <source>B2 (500 x 707 mm)</source>
        <translation>B2 (500 x 707 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="789"/>
        <source>B3 (353 x 500 mm)</source>
        <translation>B3 (353 x 500 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="790"/>
        <source>B4 (250 x 353 mm)</source>
        <translation>B4 (250 x 353 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="791"/>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation>B5 (176 x 250 mm, 6.93 x 9.84 tuumaa)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="792"/>
        <source>B6 (125 x 176 mm)</source>
        <translation>B6 (125 x 176 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="793"/>
        <source>B7 (88 x 125 mm)</source>
        <translation>B7 (88 x 125 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="794"/>
        <source>B8 (62 x 88 mm)</source>
        <translation>B8 (62 x 88 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="795"/>
        <source>B9 (44 x 62 mm)</source>
        <translation>B9 (44 x 62 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="796"/>
        <source>B10 (31 x 44 mm)</source>
        <translation>B10 (31 x 44 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="797"/>
        <source>C5E (163 x 229 mm)</source>
        <translation>C5E (163 x 229 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="798"/>
        <source>DLE (110 x 220 mm)</source>
        <translation>DLE (110 x 220 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="799"/>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation>Executive (7.5 x 10 tuumaa, 191 x 254 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="800"/>
        <source>Folio (210 x 330 mm)</source>
        <translation>Kalvo (210 x 330 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="801"/>
        <source>Ledger (432 x 279 mm)</source>
        <translation>Ledger (432 x 279 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="802"/>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation>Legal (8.5 x 14 tuumaa, 216 x 356 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="803"/>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation>Letter (8.5 x 11 tuumaa, 216 x 279 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="804"/>
        <source>Tabloid (279 x 432 mm)</source>
        <translation>Tabloid (279 x 432 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="805"/>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation>US Common #10 Envelope (105 x 241 mm)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="155"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="156"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="130"/>
        <source>Page size:</source>
        <translation>Sivun koko:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="56"/>
        <source>Orientation:</source>
        <translation>Suunta:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="141"/>
        <source>Paper source:</source>
        <translation>Paperin lähde:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="951"/>
        <source>Print</source>
        <translation>Tulosta</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="1025"/>
        <source>File</source>
        <translation>Tiedosto</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="1050"/>
        <source>Printer</source>
        <translation>Tulostin</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="1136"/>
        <source>Print To File ...</source>
        <translation>Tulosta tiedostoon...</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="25"/>
        <source>Print dialog</source>
        <translation>Tulostusikkuna</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="37"/>
        <source>Paper format</source>
        <translation>Paperin muoto</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="49"/>
        <source>Size:</source>
        <translation>Koko:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="162"/>
        <source>Properties</source>
        <translation>Ominaisuudet</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="179"/>
        <source>Printer info:</source>
        <translation>Tietoja tulostimesta:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="239"/>
        <source>Browse</source>
        <translation>Selaa</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="259"/>
        <source>Print to file</source>
        <translation>Tulosta tiedostoon</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="347"/>
        <source>Print range</source>
        <translation>Tulostusalue</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="345"/>
        <source>Print all</source>
        <translation>Tulosta kaikki</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="307"/>
        <source>Pages from</source>
        <translation>Sivut alkaen</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="321"/>
        <source>to</source>
        <translation>saakka</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="350"/>
        <source>Selection</source>
        <translation>Valinta</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="368"/>
        <source>Copies</source>
        <translation>Kopiot</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="388"/>
        <source>Number of copies:</source>
        <translation>Kopioiden lukumäärä:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="423"/>
        <source>Collate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="430"/>
        <source>Print last page first</source>
        <translation>Tulosta viimeinen sivu ensimmäiseksi</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="448"/>
        <source>Other</source>
        <translation>Muu</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="460"/>
        <source>Print in color if available</source>
        <translation>Tulosta värillisenä, jos mahdollista</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog.ui" line="470"/>
        <source>Double side printing</source>
        <translation>Kaksipuoleinen tulostus</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="1178"/>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>Tiedosto %1 ei ole kirjoitettavissa.
Valitse toinen tiedostonimi.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="1182"/>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>%1 on jo olemassa.
Haluatko kirjoittaa sen yli?</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="118"/>
        <source>File exists</source>
        <translation>Tiedosto on olemassa</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="119"/>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;Haluatko kirjoittaa sen yli?&lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="346"/>
        <source>Print selection</source>
        <translation>Tulosta valinta</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="1174"/>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>%1 on kansio.
Valitse toinen tiedostonimi.</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertiesdialog.ui" line="27"/>
        <source>PPD Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertiesdialog.ui" line="54"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertiesdialog.ui" line="74"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprogressdialog.cpp" line="147"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="80"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="92"/>
        <source>Check</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="31"/>
        <source>no error occurred</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="32"/>
        <source>disabled feature used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="33"/>
        <source>bad char class syntax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="34"/>
        <source>bad lookahead syntax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="35"/>
        <source>bad repetition syntax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="36"/>
        <source>invalid octal value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="37"/>
        <source>missing left delim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="38"/>
        <source>unexpected end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="40"/>
        <source>met internal limit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="346"/>
        <source>Error to open database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="387"/>
        <source>Unable to begin transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="404"/>
        <source>Unable to commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="421"/>
        <source>Unable to rollback Transaction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="118"/>
        <source>Unable to fetch results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="251"/>
        <source>Unable to execute statement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="461"/>
        <source>Error opening database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="472"/>
        <source>Error closing database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="492"/>
        <source>Unable to begin transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="507"/>
        <source>Unable to commit transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="522"/>
        <source>Unable to roll back transaction</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="213"/>
        <source>Unable to fetch row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="265"/>
        <source>Unable to execute statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="284"/>
        <source>Unable to reset statement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="328"/>
        <source>Unable to bind parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="335"/>
        <source>Parameter count mismatch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="396"/>
        <source>Scroll here</source>
        <translation>Vieritä tähän</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="399"/>
        <source>Left edge</source>
        <translation>Vasen reuna</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="399"/>
        <source>Top</source>
        <translation>Ylös</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="401"/>
        <source>Right edge</source>
        <translation>Oikea reuna</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="401"/>
        <source>Bottom</source>
        <translation>Alas</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="404"/>
        <source>Page left</source>
        <translation>Sivu vasemmalle</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="532"/>
        <source>Page up</source>
        <translation>Sivu ylös</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="406"/>
        <source>Page right</source>
        <translation>Sivu oikealle</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="536"/>
        <source>Page down</source>
        <translation>Sivu alas</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="409"/>
        <source>Scroll left</source>
        <translation>Vieritä vasemmalle</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="409"/>
        <source>Scroll up</source>
        <translation>Vieritä ylös</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="411"/>
        <source>Scroll right</source>
        <translation>Vieritä oikealle</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="411"/>
        <source>Scroll down</source>
        <translation>Vieritä alas</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="530"/>
        <source>Line up</source>
        <translation>Rivi ylös</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="534"/>
        <source>Position</source>
        <translation>Sijainti</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="538"/>
        <source>Line down</source>
        <translation>Rivi alas</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="288"/>
        <source>Space</source>
        <translation>Välilyönti</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="289"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="290"/>
        <source>Tab</source>
        <translation>Sarkain</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="291"/>
        <source>Backtab</source>
        <translation>Backtab</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="292"/>
        <source>Backspace</source>
        <translation>Askelpalautin</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="293"/>
        <source>Return</source>
        <translation>Return</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="294"/>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="295"/>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="296"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="297"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="298"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="299"/>
        <source>SysReq</source>
        <translation>SysReq</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="300"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="301"/>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="302"/>
        <source>Left</source>
        <translation>Vasen</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="303"/>
        <source>Up</source>
        <translation>Ylös</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="304"/>
        <source>Right</source>
        <translation>Oikea</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="305"/>
        <source>Down</source>
        <translation>Alas</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="306"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="307"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="308"/>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="309"/>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="310"/>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="311"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="312"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="315"/>
        <source>Back</source>
        <translation>Takaisin</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="316"/>
        <source>Forward</source>
        <translation>Eteenpäin</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="317"/>
        <source>Stop</source>
        <translation>Pysäytä</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="318"/>
        <source>Refresh</source>
        <translation>Päivitä</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="319"/>
        <source>Volume Down</source>
        <translation>Äänenvoimakkuus alas</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="320"/>
        <source>Volume Mute</source>
        <translation>Vaimenna äänet</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="321"/>
        <source>Volume Up</source>
        <translation>Äänenvoimakkuus ylös</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="322"/>
        <source>Bass Boost</source>
        <translation>Basson korostus</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="323"/>
        <source>Bass Up</source>
        <translation>Basson lisäys</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="324"/>
        <source>Bass Down</source>
        <translation>Basson vähennys</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="325"/>
        <source>Treble Up</source>
        <translation>Diskantin lisäys</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="326"/>
        <source>Treble Down</source>
        <translation>Diskantin vähennys</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="327"/>
        <source>Media Play</source>
        <translation>Median - toista</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="328"/>
        <source>Media Stop</source>
        <translation>Median - pysäytä</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="329"/>
        <source>Media Previous</source>
        <translation>Media - edellinen</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="330"/>
        <source>Media Next</source>
        <translation>Media - seuraava</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="331"/>
        <source>Media Record</source>
        <translation>Media - tallenna</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="333"/>
        <source>Favorites</source>
        <translation>Suosikit</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="334"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="335"/>
        <source>Standby</source>
        <translation>Keskeytystila</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="336"/>
        <source>Open URL</source>
        <translation>Avaa URL</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="337"/>
        <source>Launch Mail</source>
        <translation>Käynnistä sähköposti</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="338"/>
        <source>Launch Media</source>
        <translation>Käynnistä mediasoitin</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="339"/>
        <source>Launch (0)</source>
        <translation>Käynnistä (0)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="340"/>
        <source>Launch (1)</source>
        <translation>Käynnistä (1)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="341"/>
        <source>Launch (2)</source>
        <translation>Käynnistä (2)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="342"/>
        <source>Launch (3)</source>
        <translation>Käynnistä (3)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="343"/>
        <source>Launch (4)</source>
        <translation>Käynnistä (4)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="344"/>
        <source>Launch (5)</source>
        <translation>Käynnistä (5)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="345"/>
        <source>Launch (6)</source>
        <translation>Käynnistä (6)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="346"/>
        <source>Launch (7)</source>
        <translation>Käynnistä (7)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="347"/>
        <source>Launch (8)</source>
        <translation>Käynnistä (8)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="348"/>
        <source>Launch (9)</source>
        <translation>Käynnistä (9)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="349"/>
        <source>Launch (A)</source>
        <translation>Käynnistä (A)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="350"/>
        <source>Launch (B)</source>
        <translation>Käynnistä (B)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="351"/>
        <source>Launch (C)</source>
        <translation>Käynnistä (C)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="352"/>
        <source>Launch (D)</source>
        <translation>Käynnistä (D)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="353"/>
        <source>Launch (E)</source>
        <translation>Käynnistä (E)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="354"/>
        <source>Launch (F)</source>
        <translation>Käynnistä (F)</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="358"/>
        <source>Print Screen</source>
        <translation>PrintScreen</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="359"/>
        <source>Page Up</source>
        <translation>Sivu ylös</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="360"/>
        <source>Page Down</source>
        <translation>Sivu alas</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="361"/>
        <source>Caps Lock</source>
        <translation>CapsLock</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="362"/>
        <source>Num Lock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="363"/>
        <source>Number Lock</source>
        <translation>Numerolukko</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="364"/>
        <source>Scroll Lock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="365"/>
        <source>Insert</source>
        <translation>Insert</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="366"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="367"/>
        <source>Escape</source>
        <translation>Escape</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="368"/>
        <source>System Request</source>
        <translation>SystemRequest</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="372"/>
        <source>Select</source>
        <translation>Valinta</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="373"/>
        <source>Yes</source>
        <translation>Kyllä</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="374"/>
        <source>No</source>
        <translation>Ei</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="378"/>
        <source>Context1</source>
        <translation>Konteksti1</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="379"/>
        <source>Context2</source>
        <translation>Konteksti2</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="380"/>
        <source>Context3</source>
        <translation>Konteksti3</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="381"/>
        <source>Context4</source>
        <translation>Konteksti4</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="382"/>
        <source>Call</source>
        <translation>Soita</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="383"/>
        <source>Hangup</source>
        <translation>Katkaise</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="384"/>
        <source>Flip</source>
        <translation>Käännä</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="1020"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="1024"/>
        <source>Shift</source>
        <translation>Vaihtonäppäin</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="1022"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="1018"/>
        <source>Meta</source>
        <translation>Meta</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="994"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="1039"/>
        <source>F%1</source>
        <translation>F%1</translation>
    </message>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="332"/>
        <source>Home Page</source>
        <translation>Kotisivu</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="689"/>
        <source>Page left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="689"/>
        <source>Page up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="691"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="694"/>
        <source>Page right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="694"/>
        <source>Page down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <location filename="../src/network/qsocks5socketengine.cpp" line="1187"/>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="120"/>
        <source>More</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="122"/>
        <source>Less</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="870"/>
        <source>Delete</source>
        <translation>Poista</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="871"/>
        <source>Delete this record?</source>
        <translation>Poistetaanko tämä tietue?</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="924"/>
        <source>Yes</source>
        <translation>Kyllä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="925"/>
        <source>No</source>
        <translation>Ei</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="881"/>
        <source>Insert</source>
        <translation>Lisää</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="883"/>
        <source>Update</source>
        <translation>Päivitä</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="887"/>
        <source>Save edits?</source>
        <translation>Tallenna muutokset?</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="890"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="922"/>
        <source>Confirm</source>
        <translation>Vahvista</translation>
    </message>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="923"/>
        <source>Cancel your edits?</source>
        <translation>Peru muutokset?</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="483"/>
        <source>Unable to write data: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="552"/>
        <source>Error while reading: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="582"/>
        <source>Error during SSL handshake: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="218"/>
        <source>Error creating SSL context (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="240"/>
        <source>Invalid or empty cipher list (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="291"/>
        <source>Error creating SSL session, %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="305"/>
        <source>Error creating SSL session: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="256"/>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="263"/>
        <source>Error loading local certificate, %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="275"/>
        <source>Error loading private key, %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/network/qsslsocket_openssl.cpp" line="282"/>
        <source>Private key do not certificate public key, %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <location filename="../src/sql/drivers/tds/qsql_tds.cpp" line="550"/>
        <source>Unable to open connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/tds/qsql_tds.cpp" line="555"/>
        <source>Unable to use database</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="1068"/>
        <source>Scroll Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="1068"/>
        <source>Scroll Right</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <location filename="../src/network/qtcpserver.cpp" line="216"/>
        <source>Socket operation unsupported</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="1861"/>
        <source>&amp;Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="1863"/>
        <source>&amp;Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="1867"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="1872"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="1879"/>
        <source>Copy &amp;Link Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="1885"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="1888"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="1895"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="352"/>
        <source>Press</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="356"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <location filename="../src/network/qudpsocket.cpp" line="132"/>
        <source>This platform does not support IPv6</source>
        <translation>Tämä alusta ei tue IPv6:ta</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <location filename="../src/gui/util/qundogroup.cpp" line="341"/>
        <source>Undo</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/gui/util/qundogroup.cpp" line="369"/>
        <source>Redo</source>
        <translation>Tee uudestaan</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <location filename="../src/gui/util/qundoview.cpp" line="71"/>
        <source>&lt;empty&gt;</source>
        <translation>&lt;tyhjä&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <location filename="../src/gui/util/qundostack.cpp" line="816"/>
        <source>Undo</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/gui/util/qundostack.cpp" line="843"/>
        <source>Redo</source>
        <translation>Tee uudestaan</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2586"/>
        <source>LRM Left-to-right mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2587"/>
        <source>RLM Right-to-left mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2588"/>
        <source>ZWJ Zero width joiner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2589"/>
        <source>ZWNJ Zero width non-joiner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2590"/>
        <source>ZWSP Zero width space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2591"/>
        <source>LRE Start of left-to-right embedding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2592"/>
        <source>RLE Start of right-to-left embedding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2593"/>
        <source>LRO Start of left-to-right override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2594"/>
        <source>RLO Start of right-to-left override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2595"/>
        <source>PDF Pop directional formatting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qtextedit.cpp" line="2601"/>
        <source>Insert Unicode control character</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <location filename="../src/gui/kernel/qwhatsthis.cpp" line="486"/>
        <source>What&apos;s This?</source>
        <translation>Mikä tämä on?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../src/gui/kernel/qwidget.cpp" line="4131"/>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="554"/>
        <source>Go Back</source>
        <translation>Siirry takaisin</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="555"/>
        <source>Continue</source>
        <translation>Jatka</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="565"/>
        <source>Commit</source>
        <translation>Toteuta</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="557"/>
        <source>Done</source>
        <translation>Valmis</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="558"/>
        <source>Quit</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="559"/>
        <source>Help</source>
        <translation>Ohje</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="561"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Takaisin</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="566"/>
        <source>&amp;Finish</source>
        <translation>&amp;Viimeistele</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="567"/>
        <source>Cancel</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="568"/>
        <source>&amp;Help</source>
        <translation>O&amp;hje</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="1061"/>
        <source>&amp;Restore</source>
        <translation>&amp;Palauta</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="1062"/>
        <source>&amp;Move</source>
        <translation>&amp;Siirrä</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="1063"/>
        <source>&amp;Size</source>
        <translation>&amp;Koko</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="1065"/>
        <source>Mi&amp;nimize</source>
        <translation>P&amp;ienennä</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="1067"/>
        <source>Ma&amp;ximize</source>
        <translation>S&amp;uurenna</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="1070"/>
        <source>&amp;Close</source>
        <translation>&amp;Sulje</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="1075"/>
        <source>Stay on &amp;Top</source>
        <translation>Pidä &amp;päällimmäisenä</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="2137"/>
        <source>Sh&amp;ade</source>
        <translation>&amp;Varjosta</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="1919"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="85"/>
        <source>Minimize</source>
        <translation>Pienennä</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="87"/>
        <source>Restore Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="83"/>
        <source>Close</source>
        <translation>Sulje</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="2133"/>
        <source>&amp;Unshade</source>
        <translation>&amp;Palauta varjostettu</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <location filename="../src/xml/qxml.cpp" line="31"/>
        <source>no error occurred</source>
        <translation>virhettä ei tapahtunut</translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="32"/>
        <source>error triggered by consumer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="33"/>
        <source>unexpected end of file</source>
        <translation>tiedosto päättyi odottamatta</translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="34"/>
        <source>more than one document type definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="35"/>
        <source>error occurred while parsing element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="36"/>
        <source>tag mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="37"/>
        <source>error occurred while parsing content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="38"/>
        <source>unexpected character</source>
        <translation>odottamaton merkki</translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="39"/>
        <source>invalid name for processing instruction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="40"/>
        <source>version expected while reading the XML declaration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="41"/>
        <source>wrong value for standalone declaration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="42"/>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="43"/>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="44"/>
        <source>error occurred while parsing document type definition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="45"/>
        <source>letter is expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="46"/>
        <source>error occurred while parsing comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="47"/>
        <source>error occurred while parsing reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="48"/>
        <source>internal general entity reference not allowed in DTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="49"/>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="50"/>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="51"/>
        <source>unparsed entity reference in wrong context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="52"/>
        <source>recursive entities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxml.cpp" line="55"/>
        <source>error in the text declaration of an external entity</source>
        <translation>virhe ulkoisen entiteetin tekstimäärittelyssä</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1687"/>
        <source>Extra content at end of document.</source>
        <translation>Tiedoston lopussa on ylimääräistä tietoa</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="613"/>
        <source>Invalid entity value.</source>
        <translation>Virheellinen entiteetin arvo.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="720"/>
        <source>Invalid XML character.</source>
        <translation>Virheellinen XML-merkki.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="978"/>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>Sekvenssi &quot;]]&gt;&quot; ei ole sallittu sisällössä.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1286"/>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>Nimiavaruuden etuliitettä &quot;%1&quot; ei ole määritelty</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1324"/>
        <source>Attribute redefined.</source>
        <translation>Attribuutti määriteltiin uudestaan.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1436"/>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>Odottamaton merkki &quot;%1&quot; julkisessa tunnisteessa.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1464"/>
        <source>Invalid XML version string.</source>
        <translation>Virheellinen XML-version merkkijono.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1466"/>
        <source>Unsupported XML version.</source>
        <translation>XMl-versio ei ole tuettu.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1489"/>
        <source>%1 is an invalid encoding name.</source>
        <translation>%1 on virheellinen merkistökoodauksen nimi.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1496"/>
        <source>Encoding %1 is unsupported</source>
        <translation>Merkistökoodaus %1 ei ole tuettu</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1498"/>
        <source>Invalid XML encoding name.</source>
        <translation>Virheellinen XML-koodauksen nimi.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1514"/>
        <source>Standalone accepts only yes or no.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1516"/>
        <source>Invalid attribute in XML declaration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1532"/>
        <source>Premature end of document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1534"/>
        <source>Invalid document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1574"/>
        <source>Expected </source>
        <translation>Odotettiin</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1585"/>
        <source>, but got &apos;</source>
        <translation>, mutta saatiin &quot;</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1589"/>
        <source>Unexpected &apos;</source>
        <translation>Odottamaton &quot;</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1719"/>
        <source>Expected character data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="718"/>
        <source>Recursive entity detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1223"/>
        <source>Start tag expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1434"/>
        <source>XML declaration not at start of document.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1403"/>
        <source>NDATA in parameter entity declaration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1437"/>
        <source>%1 is an invalid processing instruction name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1448"/>
        <source>Invalid processing instruction name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1661"/>
        <source>Illegal namespace declaration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1676"/>
        <source>Invalid XML name.</source>
        <translation>Virheellinen XML-nimi.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1699"/>
        <source>Opening and ending tag mismatch.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1708"/>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1774"/>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>Entiteettiä &quot;%1&quot; ei ole määritelty.</translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1760"/>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1788"/>
        <source>Invalid character reference.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1011"/>
        <source>Encountered incorrectly encoded content.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream.cpp" line="1487"/>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/xml/qxmlstream_p.h" line="1558"/>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
