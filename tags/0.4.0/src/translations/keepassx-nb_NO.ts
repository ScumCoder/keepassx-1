<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="nb_NO">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>Informasjon om hvordan man oversetter KeePassX finnes under:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>Team</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation>Utvikler, prosjektadministrator</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation>Webdesigner</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation>Utvikler</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation>Takk til</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation>Paycher for bedre MacOS X støtte</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation>Programikon</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation>Diverse forbedringer</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Fil &apos;%1&apos; ble ikke funnet.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>Sikre deg at programmet er riktig installert.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation>Nåværende oversettelse</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation>Forfatter</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="50"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="74"/>
        <source>AppName</source>
        <translation>AppName</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="92"/>
        <source>AppFunc</source>
        <translation>AppFunc</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>http://keepassx.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="163"/>
        <source>Credits</source>
        <translation>Credits</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="188"/>
        <source>Translation</source>
        <translation>Oversettelse</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="213"/>
        <source>License</source>
        <translation>Lisens</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX er distribuert under betingelsene i lisensen: General Public License (GPL) version 2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>Legg til bokmerke</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>Tittel:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>Fil:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>Browse...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>Editer bokmerke</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass database (*.kdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>All filer (*)</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - Auto-Inntasting</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>Klikk på et element for å taste det inn automatisk.</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation>Tittel</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="90"/>
        <source>Auto-Type</source>
        <translation>Auto-Inntasting</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 Bit   (default)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Velg antall krypteringssykler.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="70"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; er ikke et gyldig tall.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>Antall krypteringssykler må være større enn 0.</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation>I dag</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation>1 uke</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation>2 uker</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation>3 uker</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation>1 måned</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation>3 måneder</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation>6 måneder</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation>1 år</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation>Kalender...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 bit</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation>Editer element</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Passordene er ikke like.  Vennligst kontroller dem.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="256"/>
        <source>[Untitled Entry]</source>
        <translation>[ikke navngitt]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="294"/>
        <source>Add Attachment...</source>
        <translation>Legg til vedlegg...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>Could not open file.</source>
        <translation>Kan ikke åpne fil.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="321"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>Det valgte element har ikke vedlegg eller er tomt.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="324"/>
        <source>Save Attachment...</source>
        <translation>Lagre vedlegg...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error while writing the file.</source>
        <translation>Feil under skriving av fil.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="351"/>
        <source>Delete Attachment?</source>
        <translation>Slett vedlegg?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="352"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Du er i ferd med å slette vedlegget for dette elementet. Er du sikker?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>No, Cancel</source>
        <translation>Nei, avbryt</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation>Nytt element</translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="121"/>
        <source>Password Generator</source>
        <translation>Passordgenerator</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="278"/>
        <source>%1 Bits</source>
        <translation>%1 bits</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Erstatt...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Slett</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>Legg til eget ikon</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>Velg</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Add Icons...</source>
        <translation>Legg til ikoner...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="94"/>
        <source>Images (%1)</source>
        <translation>Bilder (%1)</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: Fil kan ikke lastes.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="68"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>Feil under lesing av ikon(er):</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>An error occured while loading the icon.</source>
        <translation>Feil under lesing av ikon.</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="350"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="355"/>
        <source>Select a directory...</source>
        <translation>Velg en katalog...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="362"/>
        <source>Select an executable...</source>
        <translation>Velg en eksekverbar fil...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="424"/>
        <source>System Language</source>
        <translation>Systemspråk</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>Tilfeldig nummer-generator</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>Samler entropi...
Beveg musen eller trykk noen taster på tastaturet til nok entropi er samlet.</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Funnet nytt frø for tilfeldig nummer!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>Entropisamling</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation>Rik-tekst-editor</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation>Understreket</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation>Ventrejustert</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation>Sentrert</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation>Høyrejustert</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation>Justified</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation>Tekstfarge</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation>Tekststørrelse</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation>Maler</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>Tittel</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>Vedlegg</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>Opprettelsesdato</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>Sist aksessert</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>Sist endret</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>Utgåttdato</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>Tid til utgått</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>Aldri</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>Databaseinstillinger</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>Kryptering</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>Algortime:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>Krypteringssykler:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation>Beregn sykler for et 1-sekunds forsinkelse på denne datamaskin</translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation>Tittel</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation>Opprettelse</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation>Sist aksessert</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation>Sist endret</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation>Utgåttdato</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Editer element</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="52"/>
        <source>Ge&amp;n.</source>
        <translation>Ge&amp;n.</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="113"/>
        <source>Quality:</source>
        <translation>Kvalitet:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="126"/>
        <source>Attachment:</source>
        <translation>Vedlegg:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="139"/>
        <source>Title:</source>
        <translation>Tittel:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="152"/>
        <source>Username:</source>
        <translation>Brukernavn:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="165"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="350"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="178"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="191"/>
        <source>Group:</source>
        <translation>Gruppe:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="210"/>
        <source>Password Repet.:</source>
        <translation>Gjenta passord:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="223"/>
        <source>Password:</source>
        <translation>Passord:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="236"/>
        <source>Expires:</source>
        <translation>Går ut:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="383"/>
        <source>Never</source>
        <translation>Aldri</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="419"/>
        <source>%1 Bit</source>
        <translation>%1 bit</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="462"/>
        <source>Icon:</source>
        <translation>Ikon:</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Gruppeegenskaper</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>Ikon:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>Tittel:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>Utgåtte elementer</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>Dobbelklikk på et element for å gå til det.</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>Tittel</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>Utgått</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>Utgåtte elememter i databasen</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>KeePassX XML Fil</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML Filer (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>All filer (*)</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>Text Fil</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>All filer (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>Text Filer (*.txt)</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>Eksport feilet</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation>Export fil...</translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>No error occurred.</source>
        <translation>Ingen feil.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An error occurred while reading from the file.</source>
        <translation>Feil under lesing av fil.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>An error occurred while writing to the file.</source>
        <translation>Feil under skriving av fil.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>A fatal error occurred.</source>
        <translation>En alvrolig feil oppsto.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>An resource error occurred.</source>
        <translation>En ressursfeil oppsto.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>The file could not be opened.</source>
        <translation>Filen kunne ikke åpnes.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The operation was aborted.</source>
        <translation>Handlingen ble avbrutt.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>A timeout occurred.</source>
        <translation>En tidsavbrytelse oppsto.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>An unspecified error occurred.</source>
        <translation>En uspesifisert feil oppsto.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be removed.</source>
        <translation>Filen kunne ikke slettes.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be renamed.</source>
        <translation>Ikke mulig å gi filen nytt navn.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The position in the file could not be changed.</source>
        <translation>Posisjonen i filen kunne ikke endres.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="74"/>
        <source>The file could not be resized.</source>
        <translation>Filen kunne ikke endre størrelse.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="75"/>
        <source>The file could not be accessed.</source>
        <translation>Filen kunne ikke leses.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="76"/>
        <source>The file could not be copied.</source>
        <translation>Filen kunne ikke kopieres.</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Passordgenerator</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="303"/>
        <source>Options</source>
        <translation>Valg</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="56"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>Bruk følgende tegngruppe:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="59"/>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="89"/>
        <source>&amp;Lower Letters</source>
        <translation>Små bokstaver</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="95"/>
        <source>Alt+L</source>
        <translation type="obsolete">Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="118"/>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="106"/>
        <source>&amp;Numbers</source>
        <translation>Tall</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="131"/>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="123"/>
        <source>&amp;Upper Letters</source>
        <translation>Store bokstaver</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="141"/>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="140"/>
        <source>&amp;Special Characters</source>
        <translation>Spesielle tegn</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>Use &amp;only following characters:</source>
        <translation>Bruk kun følgende tegn:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="172"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="317"/>
        <source>Length:</source>
        <translation>Lengde:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="353"/>
        <source>Quality:</source>
        <translation>Kvalitet:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="380"/>
        <source>Enable entropy collection</source>
        <translation>Muliggjør entropiinnsamling</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="404"/>
        <source>Alt+M</source>
        <translation type="obsolete">Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="406"/>
        <source>Collect only once per session</source>
        <translation>Samle inn kun en gang pr sesjon</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="436"/>
        <source>New Password:</source>
        <translation>Nytt passord:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="459"/>
        <source>Generate</source>
        <translation>Generer</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Random</source>
        <translation>Tilfeldig</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>&amp;Underline</source>
        <translation>Understrek</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="116"/>
        <source>&amp;White Spaces</source>
        <translation>Skilletegn</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="133"/>
        <source>&amp;Minus</source>
        <translation>Minus</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="193"/>
        <source>Exclude look-alike characters</source>
        <translation>Fjern tegn som ligner</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="200"/>
        <source>Ensure that password contains characters from every group</source>
        <translation>Sikre at passordet inneholder tegn fra alle grupper</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="216"/>
        <source>Pronounceable</source>
        <translation>Mulig å uttale</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="240"/>
        <source>Lower Letters</source>
        <translation>Små bokstaver</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="247"/>
        <source>Upper Letters</source>
        <translation>Store bokstaver</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="254"/>
        <source>Numbers</source>
        <translation>Tall</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="261"/>
        <source>Special Characters</source>
        <translation>Spesielle tegn</translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML Filer (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>All filer (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>Import feilet</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Ugyldig XML data (se stdout for detaljer).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>Ugyldig XML fil.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>Dokumentet inneholder ingen data.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>KeePass XML Filer (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>All filer (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>Import feilet</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>XML-parsing feilet på linje %1 kolonne %2
%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>Parse-feil: Filen er ikke en gyldig KeePassX XML fil.</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>PwManager Filer (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation>All filer (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation>Import feilet</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation>Filen er tom.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation>Filen er ikke en gyldig PwManager fil.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation>Filversjon ikke støttet.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation>Hash-algortimen er ikke støttet.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>Krypteringsalgoritmen er ikke støttet.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation>Komprimerte filer er ikke støttet enda.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation>Feil passord.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation>Filen er skadet (hashtesten feilet).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Ugyldig XML data (se stdout for detaljer).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>Import fil...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>Import feilet</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="524"/>
        <source>Could not open file.</source>
        <translation>Kunne ikke åpne fil.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="540"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>Uventet filstørrelse (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="557"/>
        <source>Wrong Signature</source>
        <translation>Feil signatur</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="562"/>
        <source>Unsupported File Version.</source>
        <translation>Filversjon ikke støttet.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="571"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>Ukjent krypteringsalgoritmen.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="606"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>Dekrypterng feilet.
Nøkkelen er feil eller filen skadet.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="632"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>Hashtest feilet. Nøkkelen er feil eller filen skadet.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="711"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>En uspesifisert feil oppsto: Offset er utenfor grensen.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="717"/>
        <source>Invalid group tree.</source>
        <translation>Ugyldig gruppetre.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="933"/>
        <source>Key file is empty.</source>
        <translation>Nøkkelfil er tom.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1313"/>
        <source>The database must contain at least one group.</source>
        <translation>Databasen må inneholde minst en gruppe.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="598"/>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation>Ikke mulig å inisialisere twofish-algoritmen.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1333"/>
        <source>The database has been opened read-only.</source>
        <translation>Databsen er åpnet for bare-les.</translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1177"/>
        <source>Bytes</source>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1185"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1191"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1196"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="150"/>
        <source>Delete?</source>
        <translation>Slett?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>Minst en gruppe må finnes før et element opprettes.</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="477"/>
        <source>Title</source>
        <translation>Tittel</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="479"/>
        <source>Username</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="481"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="483"/>
        <source>Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="485"/>
        <source>Comments</source>
        <translation>Kommentarer</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="487"/>
        <source>Expires</source>
        <translation>Går ut</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="489"/>
        <source>Creation</source>
        <translation>Opprettelse</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="491"/>
        <source>Last Change</source>
        <translation>Siste endring</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="493"/>
        <source>Last Access</source>
        <translation>Sist aksessert</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="495"/>
        <source>Attachment</source>
        <translation>Vedlegg</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="497"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation>Er du sikker på at du vil slette dette elementet?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="149"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation>Er du sikker på at du vil slette disse %1 elementene?</translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="58"/>
        <source>Search Results</source>
        <translation>Søkeresultat</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="89"/>
        <source>Delete?</source>
        <translation>Slett?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="90"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation>Er du sikker at på at du vil slette denne gruppen, alle undergrupper og alle deres elementer?</translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="1420"/>
        <source>Ready</source>
        <translation>Klar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1323"/>
        <source>Locked</source>
        <translation>Låst</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1338"/>
        <source>Unlocked</source>
        <translation>Ulåst</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="370"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="375"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="378"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="379"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="499"/>
        <source>The database file does not exist.</source>
        <translation>Databasefilen finnes ikke.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1423"/>
        <source>Loading Database...</source>
        <translation>Laster databasen...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1426"/>
        <source>Loading Failed</source>
        <translation>Lasting feilet</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="477"/>
        <source>Unknown error while loading database.</source>
        <translation>Ukjent feil i forbindelse med lasting av databasen.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="479"/>
        <source>The following error occured while opening the database:</source>
        <translation>Følgende feil oppsto når databasen ble forsøkt åpnet:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="516"/>
        <source>Save modified file?</source>
        <translation>Lagre endret fil?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1111"/>
        <source>new</source>
        <translation>ny</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="603"/>
        <source>Open Database...</source>
        <translation>Åpne database...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass database (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>All Files (*)</source>
        <translation>All filer (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="729"/>
        <source>Expired</source>
        <translation>Utgått</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <source>1 Month</source>
        <translation>1 måned</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="745"/>
        <source>%1 Months</source>
        <translation>%1 måneder</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="751"/>
        <source>1 Year</source>
        <translation>1 år</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="753"/>
        <source>%1 Years</source>
        <translation>%1 år</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="759"/>
        <source>1 Day</source>
        <translation>1 dag</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="761"/>
        <source>%1 Days</source>
        <translation>%1 dager</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="765"/>
        <source>less than 1 day</source>
        <translation>mindre enn 1 dag</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="855"/>
        <source>Clone Entry</source>
        <translation>Klon element</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="857"/>
        <source>Delete Entry</source>
        <translation>Slett Element</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="870"/>
        <source>Clone Entries</source>
        <translation>Klon elementer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="872"/>
        <source>Delete Entries</source>
        <translation>Slett elementer</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="912"/>
        <source>File could not be saved.</source>
        <translation>Filen kunne ikke lages.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>Save Database...</source>
        <translation>Lagre database...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1324"/>
        <source>Un&amp;lock Workspace</source>
        <translation>Lås opp arbeidsflate</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1339"/>
        <source>&amp;Lock Workspace</source>
        <translation>Lås arbeidsflate</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1105"/>
        <source>Show &amp;Toolbar</source>
        <translation>Vis statslinje</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="373"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="407"/>
        <source>Database locked</source>
        <translation>Database låst</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="411"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation>Databasen du forsøker å åpne er låst.
Dette betyr at neon andre bruker den eller at KeePassX kræsjet forrige gang denne databsen ble åpnet.

Vil du åpne den allikevel?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation>Filen er endret.
Vil du lagre endringene?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation>Kan ikke slette låsefilen til databasen.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="414"/>
        <source>Open read-only</source>
        <translation>Åpne for bare-les</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1429"/>
        <source>Couldn&apos;t create lock file. Opening the database read-only.</source>
        <translation>Kanikke lage låsefil. Åpner databsen for bare-les.</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="140"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="147"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Fil &apos;%1&apos; ble ikke funnet.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="55"/>
        <source>Groups</source>
        <translation>Grupper</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;Help</source>
        <translation>Hjelp</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="136"/>
        <source>&amp;File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="140"/>
        <source>&amp;Export to...</source>
        <translation>Export til...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="145"/>
        <source>&amp;Import from...</source>
        <translation>Import fra...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="191"/>
        <source>&amp;View</source>
        <translation>View</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="230"/>
        <source>E&amp;xtras</source>
        <translation>Ekstra</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="262"/>
        <source>&amp;Open Database...</source>
        <translation>Åpne database...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="267"/>
        <source>&amp;Close Database</source>
        <translation>Lukk database</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="272"/>
        <source>&amp;Save Database</source>
        <translation>Lagre database</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="277"/>
        <source>Save Database &amp;As...</source>
        <translation>Lagre database som...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="282"/>
        <source>&amp;Database Settings...</source>
        <translation>Databaseinnstillinger...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="287"/>
        <source>Change &amp;Master Key...</source>
        <translation>Endre masternøkkel...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="292"/>
        <source>&amp;Lock Workspace</source>
        <translation>Lås arbeidsflate</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="474"/>
        <source>&amp;Settings...</source>
        <translation>Innstillinger...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="482"/>
        <source>&amp;About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="498"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>KeePassX Håndbok...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="506"/>
        <source>Hide</source>
        <translation>Skjul</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="566"/>
        <source>Recycle Bin...</source>
        <translation>Søppelkasse...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="150"/>
        <source>&amp;Bookmarks</source>
        <translation>Bokmerker</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="195"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation>Ikonstørrelse på verktøylinjen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="203"/>
        <source>&amp;Columns</source>
        <translation>Kolonner</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="257"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation>Ordne bokmerker...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="297"/>
        <source>&amp;Quit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="310"/>
        <source>&amp;Edit Group...</source>
        <translation>Editer gruppe...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="315"/>
        <source>&amp;Delete Group</source>
        <translation>Slett gruppe</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="320"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation>Kopier passord til utklippstavlen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="325"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation>Kopier brukernavn til utklippstavlen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="330"/>
        <source>&amp;Open URL</source>
        <translation>Åpne URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="335"/>
        <source>&amp;Save Attachment As...</source>
        <translation>Lagre vedlegg som...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="340"/>
        <source>Add &amp;New Entry...</source>
        <translation>Legg til nytt element...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="345"/>
        <source>&amp;View/Edit Entry...</source>
        <translation>Se på/editer element...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="350"/>
        <source>De&amp;lete Entry</source>
        <translation>Slett element</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="355"/>
        <source>&amp;Clone Entry</source>
        <translation>Klon element</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="360"/>
        <source>Search &amp;in Database...</source>
        <translation>Søk i database...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="365"/>
        <source>Search in this &amp;Group...</source>
        <translation>Søk i denne gruppen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="373"/>
        <source>Show &amp;Entry Details</source>
        <translation>Se på elementdetaljer</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="381"/>
        <source>Hide &amp;Usernames</source>
        <translation>Skjul brukernavn</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="389"/>
        <source>Hide &amp;Passwords</source>
        <translation>Skjul passord</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="397"/>
        <source>&amp;Title</source>
        <translation>Tittel</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="405"/>
        <source>User&amp;name</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="413"/>
        <source>&amp;URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="421"/>
        <source>&amp;Password</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="429"/>
        <source>&amp;Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="437"/>
        <source>E&amp;xpires</source>
        <translation>Går ut</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="445"/>
        <source>C&amp;reation</source>
        <translation>Opprettet</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="453"/>
        <source>&amp;Last Change</source>
        <translation>Siste endring</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="461"/>
        <source>Last &amp;Access</source>
        <translation>Sist aksessert</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="469"/>
        <source>A&amp;ttachment</source>
        <translation>Vedlegg</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="493"/>
        <source>Show &amp;Statusbar</source>
        <translation>Vis statslinje</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="511"/>
        <source>&amp;Perform AutoType</source>
        <translation>Utfør auto-inntasting</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="519"/>
        <source>&amp;16x16</source>
        <translation>16x16</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="527"/>
        <source>&amp;22x22</source>
        <translation>22x22</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="535"/>
        <source>2&amp;8x28</source>
        <translation>28x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="540"/>
        <source>&amp;New Database...</source>
        <translation>New database...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="545"/>
        <source>&amp;Password Generator...</source>
        <translation>Passordgenerator...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="556"/>
        <source>&amp;Group (search results only)</source>
        <translation>Gruppe (kun søkeresultat)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="561"/>
        <source>Show &amp;Expired Entries...</source>
        <translation>Vis utgåtte elementer...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="574"/>
        <source>&amp;Add Bookmark...</source>
        <translation>Legg til bokmerke...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="579"/>
        <source>Bookmark &amp;this Database...</source>
        <translation>Legg til bokmerke for denne databasen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="305"/>
        <source>&amp;Add New Subgroup...</source>
        <translation>Legg til ny undergruppe...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="584"/>
        <source>Copy URL to Clipboard</source>
        <translation>Kopier URL til utklippstavle</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="589"/>
        <source>Add New Group...</source>
        <translation>Legg til ny gruppe...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Entries</source>
        <translation>Elementer</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="240"/>
        <source>&amp;Groups</source>
        <translation>Grupper</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="594"/>
        <source>Sort groups</source>
        <translation>Sorter grupper</translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>Ordne bokmerker</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="32"/>
        <source>Enter Master Key</source>
        <translation>Skriv inn masternøkkel</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="35"/>
        <source>Set Master Key</source>
        <translation>Sett masternøkkel</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="38"/>
        <source>Change Master Key</source>
        <translation>Endre masternøkkel</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="42"/>
        <source>Database Key</source>
        <translation>Databasenøkkel</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="132"/>
        <source>Last File</source>
        <translation>Siste fil</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="178"/>
        <source>Select a Key File</source>
        <translation>Velg en nøkkelfil</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>All Files (*)</source>
        <translation>All filer (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>Key Files (*.key)</source>
        <translation>Nøkkelfiler (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="206"/>
        <source>Please enter a Password or select a key file.</source>
        <translation>Legg inn passord eller velg en nøkkelfil.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="211"/>
        <source>Please enter a Password.</source>
        <translation>Legg inn passord.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="216"/>
        <source>Please provide a key file.</source>
        <translation>Finn nøkkelfil.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="262"/>
        <source>%1:
No such file or directory.</source>
        <translation>%1:
Fil eller katalog ikke funnet.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="228"/>
        <source>The selected key file or directory is not readable.</source>
        <translation>Nøkkelfilen eller katalogen er ikke lesbar.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="247"/>
        <source>The given directory does not contain any key files.</source>
        <translation>Katalogen inneholder ingen nøkkelfiler.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="253"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation>Katalogen inneholder mer enn en nøkkelfil. Spesifiser filen.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="266"/>
        <source>%1:
File is not readable.</source>
        <translation>%1: Filen er ikke lesbar.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="340"/>
        <source>Create Key File...</source>
        <translation>Lag nøkkelfil...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>Siste fil</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Legg inn passord og/eller velg nøkkelfil.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Nøkkel</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>Passord:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="191"/>
        <source>&amp;Browse...</source>
        <translation>Browse...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="194"/>
        <source>Alt+B</source>
        <translation type="obsolete">Alt+B</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="163"/>
        <source>Key File:</source>
        <translation>Nøkkelfil:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="200"/>
        <source>Generate Key File...</source>
        <translation>Generer nøkkelfil...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="232"/>
        <source>Please repeat your password:</source>
        <translation>Gjenta passord:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="250"/>
        <source>Back</source>
        <translation>Tilbake</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="277"/>
        <source>Passwords are not equal.</source>
        <translation>Passordene er ikke like.</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>Søk</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Søk...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="65"/>
        <source>Include:</source>
        <translation>Inkluder:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="74"/>
        <source>Pass&amp;words</source>
        <translation>Passord</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="81"/>
        <source>A&amp;nhang</source>
        <translation>A&amp;nhang</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="88"/>
        <source>U&amp;RLs</source>
        <translation>URLer</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="95"/>
        <source>C&amp;omments</source>
        <translation>Kommentarer</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="102"/>
        <source>&amp;Usernames</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="109"/>
        <source>&amp;Titles</source>
        <translation>Tittler</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="124"/>
        <source>&amp;Case Sensitive</source>
        <translation>Case Sensitive</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="131"/>
        <source>Regular E&amp;xpression</source>
        <translation>Regulært uttrykk</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation type="obsolete">Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="138"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Inkluder undergrupper (rekursivt)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="150"/>
        <source>Search For:</source>
        <translation>Søk etter:</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Ikonvalg</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="122"/>
        <source>Show system tray icon</source>
        <translation>Show system tray icon</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="150"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>Minimize to tray instead of taskbar</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="180"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>Minimize to tray when clicking the main window&apos;s close button</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="191"/>
        <source>Remember last opened file</source>
        <translation>Husk sist åpnet fil</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="194"/>
        <source>Alt+Ö</source>
        <translation type="obsolete">Alt+Ö</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="219"/>
        <source>Remember last key type and location</source>
        <translation>Husk forrige nøkkeltype og plassering</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="249"/>
        <source>Start minimized</source>
        <translation>Start minimert</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="279"/>
        <source>Start locked</source>
        <translation>Start låst</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="295"/>
        <source>Save recent directories of file dialogs</source>
        <translation>Save recent directories of file dialogs</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="318"/>
        <source>Clear History Now</source>
        <translation>Slett historikk nå</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="340"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>Spør alltid før sletting av grupper eller elementer</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="474"/>
        <source>Banner Color</source>
        <translation>Bannerfarge</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="488"/>
        <source>Text Color:</source>
        <translation>tekstfarge:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="617"/>
        <source>Change...</source>
        <translation>Endre...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="542"/>
        <source>Color 2:</source>
        <translation>Farge 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="667"/>
        <source>C&amp;hange...</source>
        <translation>Endre...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="679"/>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="680"/>
        <source>Color 1:</source>
        <translation>Farge 1:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="708"/>
        <source>Alternating Row Colors</source>
        <translation>Alternerende radfarger</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="720"/>
        <source>Customize Entry Detail View...</source>
        <translation>Tilpass elementdetaljer-viewet...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="749"/>
        <source>Group tree at start-up:</source>
        <translation>Gruppetre ved oppstart:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="756"/>
        <source>Restore last state</source>
        <translation>Gjenopprett siste tilstand</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="763"/>
        <source>Expand all items</source>
        <translation>Ekspander alle elementer</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="770"/>
        <source>Do not expand any item</source>
        <translation>Ikke ekspander elementer</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="94"/>
        <source>Security</source>
        <translation>Sikkerhet</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="928"/>
        <source>Edit Entry Dialog</source>
        <translation>Editer Element dialog</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="943"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="956"/>
        <source>Clear clipboard after:</source>
        <translation>Slett utklippstavle etter:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="994"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>Lås arbeidsflaten når programmet minimeres</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1260"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>Du kan slå av funksjonalitet i KeePassX her for å få et rent brukergrensesnitt.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1270"/>
        <source>Bookmarks</source>
        <translation>Bokmerker</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1308"/>
        <source>Plug-Ins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1317"/>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1324"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Gnome Desktop-integrasjon (Gtk 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1331"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>KDE 4 Desktop-integrasjon</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1346"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>Du må starte programmet på nytt for å se endringene.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1366"/>
        <source>Configure...</source>
        <translation>Konfiguerer...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="99"/>
        <source>Advanced</source>
        <translation>Avansert</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1126"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>Auto-inntasting fintuning</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1135"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>Tid mellom aktivering av auto-inntasting-handling av brukeren og frste simulrte tasting.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1184"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1148"/>
        <source>Pre-Gap:</source>
        <translation>Pre-Gap:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1171"/>
        <source>Key Stroke Delay:</source>
        <translation>Tasteforsinkelse:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1181"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>Forsinkelse mellom to simulerte taster. Øk dene hvis auto-inntasting hopper over tegn.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1066"/>
        <source>Custom Browser Command</source>
        <translation>Egen nettleserkommando</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1078"/>
        <source>Browse</source>
        <translation>Browse</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1090"/>
        <source>Media Root:</source>
        <translation>Media Root:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1100"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>Katalogen hvor CDer og minnebrikker vanligvis er montert.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1107"/>
        <source>Browse...</source>
        <translation>Browse...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1116"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>Velg denne hvis du ønsker å bruke dine bokmerker og siste filer uavhengig av deres absolutte stier.  Dette er nyttig når man bruker KeePassX portabelt og dermed har forskjellige &apos;mount-points&apos; i filsystemet.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1119"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>Lagre relavtive stier (bokmerker og siste fil)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1199"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>Globalt auto-innstasting hurtigvalg:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="435"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation>Automaitsk lagre databasen ved avslutning eller låsing</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="919"/>
        <source>Show plain text passwords in:</source>
        <translation>Vis klarttekst passord i:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="935"/>
        <source>Database Key Dialog</source>
        <translation>Databasenøkkel-dialog</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1032"/>
        <source>seconds</source>
        <translation>sukunder</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1003"/>
        <source>Lock database after inactivity of</source>
        <translation>Lås databasen etter inaktivtet</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1228"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation>Bruk elementers tittel for å matche vinduet for global auto-inntasting</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="74"/>
        <source>General (1)</source>
        <translation>Generelt (1)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="79"/>
        <source>General (2)</source>
        <translation>Generelt (2)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="84"/>
        <source>Appearance</source>
        <translation>Utseende</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="89"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="372"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation>Lagre backups av endrede elementer i &apos;Backup&apos; gruppen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="400"/>
        <source>Delete backup entries older than:</source>
        <translation>Slett backupelementer eldre enn:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="426"/>
        <source>days</source>
        <translation>dager</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="442"/>
        <source>Automatically save database after every change</source>
        <translation>Automaitsk lagre databasen etter hver endring</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="845"/>
        <source>Language:</source>
        <translation>Språk:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="852"/>
        <source>Author:</source>
        <translation>Forfatter:</translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Legg inn passord</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Passord:</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation>Auto-inntasting: Velg målvindu</translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation>For å velge et målvindu, enten velg et åpent vindy fra drop-down-listen
eller skriv inn vindustittelen manuelt:</translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../lib/tools.cpp" line="352"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation>Fredrik Rødland</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation>translate [aaaatttt] rodland.no - http://rodland.no</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="351"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation>Norwegian bokmål (Norway)</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Arbeidsflatebn er låst.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>Lås opp</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>Lukk database</translation>
    </message>
</context>
</TS>
