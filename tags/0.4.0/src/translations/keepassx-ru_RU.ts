<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="ru_RU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Current Translation: Russian&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Автор:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Дмитрий Функ</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">dmitry.funk@gmail.com</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>Команда разработчиков</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation>Разработчик, руководитель проекта</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation>Web дизайнер</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation>Благодарность</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation>Исправления для улучшения поддержки MacOS X</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">www.outofhanwell.com</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation>Значок программы</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation>Различные исправления и улучшения</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>Убедитесь что программа установлена корректно.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно открыть файл &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation>Разработчик</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>Информация переводе KeePassX на другие языки находится по адресу:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation>Текущий язык</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="50"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="213"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="188"/>
        <source>Translation</source>
        <translation>Перевод</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Кроссплатформенный менеджер паролей&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="163"/>
        <source>Credits</source>
        <translation>Благодарности</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="74"/>
        <source>AppName</source>
        <translation>AppName</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="92"/>
        <source>AppFunc</source>
        <translation>AppFunc</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX распространяется под лицензией
General Public License (GPL) version 2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>Добавить закладку</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>Файл:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>Обзор...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>Редактировать закладку</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass база паролей (*.kdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - Автоввод</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>Нажатие на запись активирует функцию Auto-Type.</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation>Группа</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="90"/>
        <source>Auto-Type</source>
        <translation>Автоввод</translation>
    </message>
</context>
<context>
    <name>CAboutDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="obsolete">Убедитесь что программа установлена корректно.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Невозможно открыть файл &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Current Translation: Russian&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Автор:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">dmitry.funk@gmail.com</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Дмитрий Функ</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Team</source>
        <translation type="obsolete">Команда разработчиков</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Developer, Project Admin</source>
        <translation type="obsolete">Разработчик, руководитель проекта</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Web Designer</source>
        <translation type="obsolete">Web дизайнер</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Thanks To</source>
        <translation type="obsolete">Благодарность</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Patches for better MacOS X support</source>
        <translation type="obsolete">Исправления для улучшения поддержки MacOS X</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">www.outofhanwell.com</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Information on how to translate KeePassX can be found under:
http://keepassx.sourceforge.net/</source>
        <translation type="obsolete">Информацию по переводу KeePassX можно найти на:
http://keepassx.sourceforge.net/</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Main Application Icon</source>
        <translation type="obsolete">Значок программы</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 бит   (по умолчанию)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 бит</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Пожалуйста определите количество циклов шифрования.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="70"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; не корректное целое значение.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>Количество циклов шифрования должно быть больше нуля.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>Внимание</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Пароль и повтор пароля не эквивалентны.
Проверьте введённые данные.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="324"/>
        <source>Save Attachment...</source>
        <translation>Сохранить вложение...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Overwrite?</source>
        <translation type="obsolete">Перезаписать?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not remove old file.</source>
        <translation type="obsolete">Невозможно удалить старый файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not create new file.</source>
        <translation type="obsolete">Невозможно создать новый файл.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error while writing the file.</source>
        <translation>Ошибка записи в файл.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="351"/>
        <source>Delete Attachment?</source>
        <translation>Удалить вложение?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="352"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Вложение будет удалено.
Продолжить?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>No, Cancel</source>
        <translation>Нет, Отмена</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation>Изменить запись</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>Could not open file.</source>
        <translation>Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 бит</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="294"/>
        <source>Add Attachment...</source>
        <translation>Добавить вложение...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="321"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>Выбранная запись не содержит вложения или содержит вложение нулевой длины.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation>Сегодня</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation>1 неделя</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation>2 недели</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation>3 недели</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation>1 месяц</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation>3 месяца</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation>6 месяцев</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation>1 год</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation>Календарь...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="256"/>
        <source>[Untitled Entry]</source>
        <translation>[безымянная запись]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation>Новая запись</translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation type="obsolete">Предупреждение</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation type="obsolete">Необходимо ввести более одного символа</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open &apos;/dev/random&apos; or &apos;/dev/urandom&apos;.</source>
        <translation type="obsolete">Невозможно открыть &apos;/dev/random&apos; или &apos;/dev/urandom&apos;.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="121"/>
        <source>Password Generator</source>
        <translation>Генератор паролей</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1 Bit</source>
        <translation type="obsolete">%1 бит</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="278"/>
        <source>%1 Bits</source>
        <translation>%1 битов</translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a Password.</source>
        <translation type="obsolete">Введите пароль.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please choose a key file.</source>
        <translation type="obsolete">Выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">Введите пароль или выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Database Key</source>
        <translation type="obsolete">Ключ базы паролей</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select a Key File</source>
        <translation type="obsolete">Выбор файл-ключа</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>*.key</source>
        <translation type="obsolete">*.key</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Warning</source>
        <translation type="obsolete">Внимание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">Пароль и повтор пароля не эквивалентны.
Проверьте введённые данные.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">Введите пароль или выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with the name &apos;pwsafe.key&apos; already exisits in the given directory.
Do you want to replace it?</source>
        <translation type="obsolete">Файл с именем &apos;pwsafe.key&apos; уже существует в данной директории.
Заменить его?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The exisiting file is not writable.</source>
        <translation type="obsolete">Ошибка записи в существующий файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with the this name already exisits.
Do you want to replace it?</source>
        <translation type="obsolete">Файл с таким именем уже существует.
Заменить его?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Не удалось прочитать выбранный файл-ключ или директорию.
Проверьте права доступа.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>CSearchDlg</name>
    <message>
        <location filename="" line="0"/>
        <source>Notice</source>
        <translation type="obsolete">Предупреждение</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a search string.</source>
        <translation type="obsolete">Введите строку для поиска.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search</source>
        <translation type="obsolete">Поиск</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Add Icons...</source>
        <translation>Добавить значок...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="94"/>
        <source>Images (%1)</source>
        <translation>Изображение (%1)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: Файл не может быть загружен.
</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Заменить...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">Ошибка при загрузке значков:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>An error occured while loading the icon.</source>
        <translation>Ошибка при загрузке значка.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>Добавить свою иконку</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>Выбрать</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: Не удалось загрузить файл.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="68"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>Ошибка загрузки иконок:</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="350"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="355"/>
        <source>Select a directory...</source>
        <translation>Выберите директорию...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="362"/>
        <source>Select an executable...</source>
        <translation>Выбрать выполняемый файл...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="424"/>
        <source>System Language</source>
        <translation>Язык системы</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>Календарь</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>Сбор энтропии</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>Генерация случайных чисел</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>Сбор энтропии...
Пожалуйста, двигайте мышку или нажимайте на клавиши до тех пор, пока необходимое количество энтропии будет собрано.</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Необходимое количество энтропии для генерации случайных чисел собрано!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>Группа</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>Имя вложения</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>Дата создания</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>Дата последнего доступа</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>Дата последнего изменения</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>Срок годности</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>Время до конца срока годности</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation>Редактор текста</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation>Жирный</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation>Подчёркивание</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation>Выровненный влево</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation>Центрированный</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation>Выровненный вправо</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation>Выровненный</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation>Цвет текста</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation>13</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation>Шаблон</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>Никогда</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>Настройки базы паролей</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>Шифрование</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>Алгоритм:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>Циклов шифрования:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation>Вычисление количества циклов, которые могут быть обработаны за одну секунду на этом компьютере</translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation>Группа</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation>Создание</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation>Последний доступ</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation>Дата последнего изменения</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation>Срок годности</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Изменить запись</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="152"/>
        <source>Username:</source>
        <translation>Имя:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="210"/>
        <source>Password Repet.:</source>
        <translation>Повтор пароля:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="139"/>
        <source>Title:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="178"/>
        <source>URL:</source>
        <translation>Ссылка:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="223"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="113"/>
        <source>Quality:</source>
        <translation>Качество:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="165"/>
        <source>Comment:</source>
        <translation>Комментарий:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="236"/>
        <source>Expires:</source>
        <translation>Срок годности:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="191"/>
        <source>Group:</source>
        <translation>Группа:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="350"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="462"/>
        <source>Icon:</source>
        <translation>Значок:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="52"/>
        <source>Ge&amp;n.</source>
        <translation>&amp;Генерация.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="383"/>
        <source>Never</source>
        <translation>Никогда</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="126"/>
        <source>Attachment:</source>
        <translation>Вложение:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="419"/>
        <source>%1 Bit</source>
        <translation>%1 бит</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Параметры группы</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>Название:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>Значок:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>Записи с истекшим сроком годности</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>Двойное нажатие эту запись.</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>Группа</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>Срок годности истек</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>Записи в базе данных с истекшим сроком годности</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML-файлы (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>KeePassX XML-файл</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Невозможно открыть файл  (FileError=%1)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>Текстовые файлы (*.txt)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>Текстовый файл</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>Экспорт завершился неудачно</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation>Файл для экспорта...</translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>No error occurred.</source>
        <translation>Ошибок не возникло.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An error occurred while reading from the file.</source>
        <translation>Ошибка чтения из файла.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>An error occurred while writing to the file.</source>
        <translation>Ошибка записи в файл.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>A fatal error occurred.</source>
        <translation>Произошла фатальная ошибка.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>An resource error occurred.</source>
        <translation>Произошла ошибка ресурсов.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>The file could not be opened.</source>
        <translation>Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The operation was aborted.</source>
        <translation>Операция была прервана.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>A timeout occurred.</source>
        <translation>Время ожидания истекло.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>An unspecified error occurred.</source>
        <translation>Произошла неизвестная ошибка.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be removed.</source>
        <translation>Невозможно удалить файл.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be renamed.</source>
        <translation>Невозможно переименовать файл.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The position in the file could not be changed.</source>
        <translation>Позиция в файле не может быть изменена.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="74"/>
        <source>The file could not be resized.</source>
        <translation>Невозможно изменить размер файла.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="75"/>
        <source>The file could not be accessed.</source>
        <translation>Нету доступа к файлу.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="76"/>
        <source>The file could not be copied.</source>
        <translation>Невозможно скопировать файл.</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="141"/>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="118"/>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="404"/>
        <source>Alt+M</source>
        <translation type="obsolete">Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="95"/>
        <source>Alt+L</source>
        <translation type="obsolete">Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Генератор паролей</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Accep&amp;t</source>
        <translation type="obsolete">&amp;Принять</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="459"/>
        <source>Generate</source>
        <translation>Генерировать</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="436"/>
        <source>New Password:</source>
        <translation>Новый пароль:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="353"/>
        <source>Quality:</source>
        <translation>Качество:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="303"/>
        <source>Options</source>
        <translation>Опции</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="123"/>
        <source>&amp;Upper Letters</source>
        <translation>&amp;Прописные буквы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="89"/>
        <source>&amp;Lower Letters</source>
        <translation>&amp;Строчные буквы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="106"/>
        <source>&amp;Numbers</source>
        <translation>&amp;Цифры</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="140"/>
        <source>&amp;Special Characters</source>
        <translation>&amp;Специальные символы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation type="obsolete">Знак минуса</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation type="obsolete">По&amp;дчёркивание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>h&amp;igher ANSI-Characters</source>
        <translation type="obsolete">ANSI-символы &amp;второй половины таблицы</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>Use &amp;only following characters:</source>
        <translation>&amp;Только следующие символы:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="172"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="317"/>
        <source>Length:</source>
        <translation>Длинна:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Use &quot;/dev/rando&amp;m&quot;</source>
        <translation type="obsolete">Использовать  &quot;/dev/rando&amp;m&quot;</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="56"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>Только следующие &amp;группы символов:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="59"/>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation type="obsolete">&amp;Пробелы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="131"/>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="380"/>
        <source>Enable entropy collection</source>
        <translation>Включить механизм сбора энтропии</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="406"/>
        <source>Collect only once per session</source>
        <translation>Сбор только один раз за сессию</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Random</source>
        <translation>Случайные числа</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>&amp;Underline</source>
        <translation>По&amp;дчёркивание</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="116"/>
        <source>&amp;White Spaces</source>
        <translation>&amp;Пробелы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="133"/>
        <source>&amp;Minus</source>
        <translation>&amp;Знак минуса</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="193"/>
        <source>Exclude look-alike characters</source>
        <translation>Исключить похожие символы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="200"/>
        <source>Ensure that password contains characters from every group</source>
        <translation>Пороли должны включать в себя символы из всех групп</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="216"/>
        <source>Pronounceable</source>
        <translation>Удобно произносимые пароли</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="240"/>
        <source>Lower Letters</source>
        <translation>Строчные буквы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="247"/>
        <source>Upper Letters</source>
        <translation>Прописные буквы</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="254"/>
        <source>Numbers</source>
        <translation>Цифры</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="261"/>
        <source>Special Characters</source>
        <translation>Специальные символы</translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML-файлы (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>Импорт завершился неудачно</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Ошибка в формате данных XML (см. stdout).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>Неверный файл XML.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>Документ не содержит данных.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>KeePassX XML-файл (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>Импорт завершился неудачно</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>Ошибка в формате данных XML. Строчка %1, столбец %2:%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>Ошибка в формате данных: Неправильный KeePassX XML файл.</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>Файл PwManager (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation>Импорт завершился неудачно</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation>Файл пуст.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation>Неправильный PwManager файл.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation>Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation>Неподдерживаемы хэш алгоритм.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation>Архивированные файлы, к сожалению, пока не поддерживаются.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation>Неверный пароль.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation>Файл поврежден (неверных хэш-код).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Ошибка в формате данных XML (см. stdout).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>Файл для импорта...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>Импорт завершился неудачно</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="524"/>
        <source>Could not open file.</source>
        <translation>Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="540"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>Неожиданный размер файла (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="557"/>
        <source>Wrong Signature</source>
        <translation>Неверная сигнатура</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="562"/>
        <source>Unsupported File Version.</source>
        <translation>Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="571"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="606"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>Расшифровка прервана.
Ключ неверен или файл повреждён.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="632"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>Неверных хэш-код.Ключ неверен или файл повреждён.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="717"/>
        <source>Invalid group tree.</source>
        <translation>Ошибка в дереве групп.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="933"/>
        <source>Key file is empty.</source>
        <translation>Файл пустой.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1313"/>
        <source>The database must contain at least one group.</source>
        <translation>База паролей должна содержать как минимум одну группу.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1325"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Невозможно открыть файл для записи.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="711"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>Неожиданная ошибка: индекс вышел за пределы допустимого.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="598"/>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation>Ошибка инициализации алгоритма twofish.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1333"/>
        <source>The database has been opened read-only.</source>
        <translation>База паролей открыта в режиме &quot;только чтение&quot;.</translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1177"/>
        <source>Bytes</source>
        <translation>Байт</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1185"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1191"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1196"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="477"/>
        <source>Title</source>
        <translation>Название</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="479"/>
        <source>Username</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="481"/>
        <source>URL</source>
        <translation>Ссылка</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="483"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="485"/>
        <source>Comments</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="487"/>
        <source>Expires</source>
        <translation>Окончание</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="489"/>
        <source>Creation</source>
        <translation>Создание</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="491"/>
        <source>Last Change</source>
        <translation>Последнее изменение</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="493"/>
        <source>Last Access</source>
        <translation>Последний доступ</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="495"/>
        <source>Attachment</source>
        <translation>Вложение</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1 items</source>
        <translation type="obsolete">%1 пунктов</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="150"/>
        <source>Delete?</source>
        <translation>Стереть?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="497"/>
        <source>Group</source>
        <translation>Группа</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>Чтобы можно было дабавить запись должна существовать как минимум одна группа.</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation>Запись будет удалена. Продолжить?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="149"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation>%1 записей будет удалено. Продолжить?</translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="58"/>
        <source>Search Results</source>
        <translation>Результаты поиска</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Groups</source>
        <translation type="obsolete">Группы</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="89"/>
        <source>Delete?</source>
        <translation>Стереть?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="90"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation>Группа со всеми подгруппами и со всеми содержащимися в них записями будет удалена. Продолжить?</translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="370"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="378"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="379"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="516"/>
        <source>Save modified file?</source>
        <translation>Сохранить изменения файла?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation type="obsolete">Текущий файл был изменён. Хотите
сохранить изменения?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX - %1</source>
        <translation type="obsolete">KeePassX - %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;B&gt;Group: &lt;/B&gt;%1  &lt;B&gt;Title: &lt;/B&gt;%2  &lt;B&gt;Username: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Password: &lt;/B&gt;%5  &lt;B&gt;Creation: &lt;/B&gt;%6  &lt;B&gt;Last Change: &lt;/B&gt;%7  &lt;B&gt;LastAccess: &lt;/B&gt;%8  &lt;B&gt;Expires: &lt;/B&gt;%9</source>
        <translation type="obsolete">&lt;B&gt;Группа: &lt;/B&gt;%1  &lt;B&gt;Название: &lt;/B&gt;%2  &lt;B&gt;Имя: &lt;/B&gt;%3  &lt;B&gt;Ссылка: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Пароль: &lt;/B&gt;%5  &lt;B&gt;Создано: &lt;/B&gt;%6  &lt;B&gt;Изменено: &lt;/B&gt;%7  &lt;B&gt;Доступ: &lt;/B&gt;%8  &lt;B&gt;Окончание: &lt;/B&gt;%9</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="855"/>
        <source>Clone Entry</source>
        <translation>Дублировать запись</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="857"/>
        <source>Delete Entry</source>
        <translation>Удалить запись</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="870"/>
        <source>Clone Entries</source>
        <translation>Дублировать записи</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="872"/>
        <source>Delete Entries</source>
        <translation>Удалить записи</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">Невозможно сохранить файл.
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Database As...</source>
        <translation type="obsolete">Сохранить базу паролей как...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1420"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>[new]</source>
        <translation type="obsolete">[новый]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="603"/>
        <source>Open Database...</source>
        <translation>Открыть базу паролей...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1423"/>
        <source>Loading Database...</source>
        <translation>Загрузка базы паролей...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1426"/>
        <source>Loading Failed</source>
        <translation>Ошибка загрузки</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Export To...</source>
        <translation type="obsolete">Экспорт в...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in Import_PwManager::importFile()()</source>
        <translation type="obsolete">Неизвестная ошибка в Import_PwManager::importFile()()</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in Import_KWalletXml::importFile()</source>
        <translation type="obsolete">Неизвестная ошибка в Import_KWalletXml::importFile()</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in PwDatabase::openDatabase()</source>
        <translation type="obsolete">Неизвестная ошибка в PwDatabase::openDatabase()</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="375"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">Отобразить панель инструментов</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="477"/>
        <source>Unknown error while loading database.</source>
        <translation>Неизвестная ошибка при загруски базы данных.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass база паролей (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>Save Database...</source>
        <translation>Сохранить базу паролей...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <source>1 Month</source>
        <translation>1 месяц</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="745"/>
        <source>%1 Months</source>
        <translation>%1 месяца</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="751"/>
        <source>1 Year</source>
        <translation>1 год</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="753"/>
        <source>%1 Years</source>
        <translation>%1 лет</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="759"/>
        <source>1 Day</source>
        <translation>1 день</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="761"/>
        <source>%1 Days</source>
        <translation>%1 дней</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="765"/>
        <source>less than 1 day</source>
        <translation>меньше, чем один день</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1323"/>
        <source>Locked</source>
        <translation>заблокировано</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1338"/>
        <source>Unlocked</source>
        <translation>разблокированно</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="499"/>
        <source>The database file does not exist.</source>
        <translation>Файл базы паролей не найден.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1111"/>
        <source>new</source>
        <translation>новый</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="729"/>
        <source>Expired</source>
        <translation>просрочен</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1324"/>
        <source>Un&amp;lock Workspace</source>
        <translation>&amp;Разблокировать главное окно</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1339"/>
        <source>&amp;Lock Workspace</source>
        <translation>&amp;Заблокировать главное окно</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="479"/>
        <source>The following error occured while opening the database:</source>
        <translation>Ошибка при открытии быза паролей:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="912"/>
        <source>File could not be saved.</source>
        <translation>Не удалось сохранить файл.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1105"/>
        <source>Show &amp;Toolbar</source>
        <translation>&amp;Отобразить панель инструментов</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="373"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="407"/>
        <source>Database locked</source>
        <translation>База паролей заблокирована</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="411"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation>База паролей, которую Вы сейчас пытаетесь открыть, заблокирована.
Это происходит если кто-то в данных момент уже открыл базу или KeePassX не был нормально завершен, когда эта база была открыта.

Продолжить открытие?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation>База паролей была изменена.
Сохранить изменения?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation>Не удалось удалить файл блокирования базы паролей.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="414"/>
        <source>Open read-only</source>
        <translation>Открыть только для чтения</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1429"/>
        <source>Couldn&apos;t create lock file. Opening the database read-only.</source>
        <translation>Не удалось создать файл блокирования базы паролей. База паролей будет открыта только для чтения.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="610"/>
        <source>Information</source>
        <translation type="obsolete">Информация</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="622"/>
        <source>WARNING! Synchronization has not yet been well tested.
WARNING! It can cause database corruption.

So please BACKUP your databases first!

The synchronization proceeds as following:
1 You choose a database to synchronize with the current database
2 The current and the chosen database will be recursively processed
  - all missing groups and items in one database will be copied
    to the other and vice versa
  - all existing groups matched by title will be synchronized
    (current database has a priority)
  - all existing items matched by title will be synchronized
    (according to last modification time)
  - all groups and items whose title ends with &quot;</source>
        <translation type="obsolete">ВНИМАНИЕ!  Синхронизация еще не протестирована в достаточной степени.
ВНИМАНИЕ!  Синхронизация может повредить одну или обе базы паролей.

Пожалуйста, сделайте РЕЗЕРВНЫЕ КОПИИ баз паролей.

Принцип работы синхронизации:
1 Вы выбираете другую базу паролей для синхронизации с окрытой базой
2 Обе базы паролей обрабатываются рекурсивно
  - все существующие только в одной базе группы и записи переписываются
    в другую
  - все существующие в обеих базах группы и записи синхронизируются
  - все группы и записи имя которых заканчивается на &quot;delete.it&quot; стираются
    из обеех баз паролей</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="629"/>
        <source>Please, open a database first.</source>
        <translation type="obsolete">Для начала откройте пожалуйста базу паролей.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="633"/>
        <source>Open Database to synchronize with...</source>
        <translation type="obsolete">Выбор базы паролей для синхронизации...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="637"/>
        <source>Can&apos;t synchronize with the own file.</source>
        <translation type="obsolete">База паролей уже открыта. Выберете другую базу паролей нельзя синхронизации.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>Error saving current file</source>
        <translation type="obsolete">Ошибка сохранения файла</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="651"/>
        <source>The current file was modified and must be saved before synchronization can proceed.
Do you want to save the changes?</source>
        <translation type="obsolete">Базу паролей необходимо сохранить перед началом синхронизации.
Продолжить?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="695"/>
        <source>Synchronization complete</source>
        <translation type="obsolete">Синхронизация завершена</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="696"/>
        <source>Synchronization successfully finished.

</source>
        <translation type="obsolete">Синхронизация завершена.

</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="697"/>
        <source>Groups processed: </source>
        <translation type="obsolete">Групп обработано: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="699"/>
        <source>Groups created: </source>
        <translation type="obsolete">Групп создано: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="701"/>
        <source>Groups synchronized: </source>
        <translation type="obsolete">Групп синхронизировано: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="703"/>
        <source>Groups deleted: </source>
        <translation type="obsolete">Групп стёрто: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="705"/>
        <source>Entries processed: </source>
        <translation type="obsolete">Записей обработано: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="707"/>
        <source>Entries created: </source>
        <translation type="obsolete">Записей создано: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="709"/>
        <source>Entries synchronized: </source>
        <translation type="obsolete">Записей синхронизировано: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="711"/>
        <source>Entries deleted: </source>
        <translation type="obsolete">Записей стёрто: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="718"/>
        <source>Unknown error while saving database: </source>
        <translation type="obsolete">Неизвестная ошибка при сохранении базы паролей: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="721"/>
        <source>The following error occurred while saving the database:</source>
        <translation type="obsolete">Ошибка при сохранении базы паролей:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="722"/>
        <source>Error:</source>
        <translation type="obsolete">Ошибка:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="726"/>
        <source>Close failed</source>
        <translation type="obsolete">Ошибка закрытия файла</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="627"/>
        <source>WARNING! Synchronization has not yet been well tested.
WARNING! It can cause database corruption.

So please BACKUP your databases first!

The synchronization proceeds as following:
1 You choose a database to synchronize with the current database
2 The current and the chosen database will be recursively processed
  - all missing groups and items in one database will be copied
    to the other and vice versa
  - all existing groups matched by title will be synchronized
    (current database has a priority)
  - all existing items matched by title will be synchronized
    (according to last modification time)
  - all groups and items whose title ends with &quot;.deleteit&quot; will be deleted
    in both databases

Please consider:
- No conflicts resolving is possible
- No stored icons synchronization is implemented yet</source>
        <translation type="obsolete">ВНИМАНИЕ!  Синхронизация еще не протестирована в достаточной степени.
ВНИМАНИЕ!  Синхронизация может повредить одну или обе базы паролей.

Пожалуйста, сделайте РЕЗЕРВНЫЕ КОПИИ баз паролей.

Принцип работы синхронизации:
1 Вы выбираете другую базу паролей для синхронизации с окрытой базой
2 Обе базы паролей обрабатываются рекурсивно
  - все существующие только в одной базе группы и записи
    переписываются в другую
  - все существующие в обеих базах группы и записи синхронизируются
  - все группы и записи имя которых заканчивается на &quot;delete.it&quot; стираются
    из обеех баз паролей

Обращаем Ваше внимание на то что:
- Распознование конфликтов не поддерживается
- Синхронизация сохраненных в базе иконок пока не реализована</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="140"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="147"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Columns</source>
        <translation type="obsolete">Столбцы</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>PwManager File (*.pwm)</source>
        <translation type="obsolete">Файл PwManager (*.pwm)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KWallet XML-File (*.xml)</source>
        <translation type="obsolete">XML-файл KWallet (*.xml)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="589"/>
        <source>Add New Group...</source>
        <translation>Добавить новую группу...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Edit Group...</source>
        <translation type="obsolete">Изменить группу...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete Group</source>
        <translation type="obsolete">Удалить группу</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">Скопировать &amp;пароль в буфер обмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">Скопировать имя в буфер обмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Open URL</source>
        <translation type="obsolete">Открыть &amp;ссылку</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Attachment As...</source>
        <translation type="obsolete">Сохранить вложение как...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add New Entry...</source>
        <translation type="obsolete">Добавить новую запись...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">Просмотр/правка записи...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete Entry</source>
        <translation type="obsolete">Удалить запись</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clone Entry</source>
        <translation type="obsolete">Дублировать запись</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search In Database...</source>
        <translation type="obsolete">Поиск в базе паролей...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search in this group...</source>
        <translation type="obsolete">Поиск в текущей группе...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">Отобразить панель инструментов</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Entry Details</source>
        <translation type="obsolete">Отобразить данные записи</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hide Usernames</source>
        <translation type="obsolete">Скрыть имена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hide Passwords</source>
        <translation type="obsolete">Скрыть пароли</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Title</source>
        <translation type="obsolete">Название</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Username</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>URL</source>
        <translation type="obsolete">Ссылка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password</source>
        <translation type="obsolete">Пароль</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Comment</source>
        <translation type="obsolete">Комментарий</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Expires</source>
        <translation type="obsolete">Окончание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Creation</source>
        <translation type="obsolete">Создание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last Change</source>
        <translation type="obsolete">Последнее изменение</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last Access</source>
        <translation type="obsolete">Последний доступ</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Attachment</source>
        <translation type="obsolete">Вложение</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Statusbar</source>
        <translation type="obsolete">Отобразить панель статуса</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Plain Text (*.txt)</source>
        <translation type="obsolete">Plain Text (*.txt)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="506"/>
        <source>Hide</source>
        <translation>Скрыть</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Perform AutoType</source>
        <translation type="obsolete">Применить автоввод</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Type Here</source>
        <translation type="obsolete">Ввести сюда</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">Размер значков панели инструментов</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>16x16</source>
        <translation type="obsolete">16x16</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>22x22</source>
        <translation type="obsolete">22x22</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>28x28</source>
        <translation type="obsolete">28x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="191"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="136"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="145"/>
        <source>&amp;Import from...</source>
        <translation>&amp;Импортировать из...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="140"/>
        <source>&amp;Export to...</source>
        <translation>&amp;Экспортировать в...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Edit</source>
        <translation type="obsolete">&amp;Правка</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="230"/>
        <source>E&amp;xtras</source>
        <translation>&amp;Дополнительно</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;Help</source>
        <translation>&amp;Справка</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="540"/>
        <source>&amp;New Database...</source>
        <translation>Со&amp;здать базу паролей...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="262"/>
        <source>&amp;Open Database...</source>
        <translation>&amp;Открыть базу паролей...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="267"/>
        <source>&amp;Close Database</source>
        <translation>&amp;Закрыть базу паролей</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="272"/>
        <source>&amp;Save Database</source>
        <translation>&amp;Сохранить базу паролей</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="277"/>
        <source>Save Database &amp;As...</source>
        <translation>Сохранить базу паролей &amp;как...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="282"/>
        <source>&amp;Database Settings...</source>
        <translation>Н&amp;астройки базы паролей...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="287"/>
        <source>Change &amp;Master Key...</source>
        <translation>&amp;Изменить основной пароль...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>E&amp;xit</source>
        <translation type="obsolete">В&amp;ыход</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="474"/>
        <source>&amp;Settings...</source>
        <translation>&amp;Настройка...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="482"/>
        <source>&amp;About...</source>
        <translation>&amp;О программе...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="498"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>&amp;Руководство &quot;KeePassX&quot;...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="566"/>
        <source>Recycle Bin...</source>
        <translation>Мусорная корзина...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="55"/>
        <source>Groups</source>
        <translation>Группы</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="292"/>
        <source>&amp;Lock Workspace</source>
        <translation>&amp;Заблокировать главное окно</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="150"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Закладки</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="195"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation>Раз&amp;мер значков панели инструментов</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="203"/>
        <source>&amp;Columns</source>
        <translation>&amp;Столбцы</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="257"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation>&amp;Настроить закладки...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="297"/>
        <source>&amp;Quit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="310"/>
        <source>&amp;Edit Group...</source>
        <translation>Изменить &amp;группу...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="315"/>
        <source>&amp;Delete Group</source>
        <translation>У&amp;далить группу</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="320"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation>Скопировать &amp;пароль в буфер обмена</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="325"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation>Скопировать &amp;имя в буфер обмена</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="330"/>
        <source>&amp;Open URL</source>
        <translation>Открыть &amp;ссылку</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="335"/>
        <source>&amp;Save Attachment As...</source>
        <translation>&amp;Сохранить вложение как...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="340"/>
        <source>Add &amp;New Entry...</source>
        <translation>Добавить новую &amp;запись...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="345"/>
        <source>&amp;View/Edit Entry...</source>
        <translation>&amp;Просмотр/правка записи...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="350"/>
        <source>De&amp;lete Entry</source>
        <translation>&amp;Удалить запись</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="355"/>
        <source>&amp;Clone Entry</source>
        <translation>&amp;Дублировать запись</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="360"/>
        <source>Search &amp;in Database...</source>
        <translation>По&amp;иск в базе паролей...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="365"/>
        <source>Search in this &amp;Group...</source>
        <translation>Поиск в текущей г&amp;руппе...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="373"/>
        <source>Show &amp;Entry Details</source>
        <translation>Отобразить данные &amp;записи</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="381"/>
        <source>Hide &amp;Usernames</source>
        <translation>Скрыть &amp;имена</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="389"/>
        <source>Hide &amp;Passwords</source>
        <translation>Скрыть &amp;пароли</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="397"/>
        <source>&amp;Title</source>
        <translation>&amp;Название</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="405"/>
        <source>User&amp;name</source>
        <translation>Имя &amp;пользователя</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="413"/>
        <source>&amp;URL</source>
        <translation>&amp;Ссылка</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="421"/>
        <source>&amp;Password</source>
        <translation>&amp;Пароль</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="429"/>
        <source>&amp;Comment</source>
        <translation>&amp;Комментарий</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="437"/>
        <source>E&amp;xpires</source>
        <translation>&amp;Окончание срока годности</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="445"/>
        <source>C&amp;reation</source>
        <translation>&amp;Создание</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="453"/>
        <source>&amp;Last Change</source>
        <translation>Последнее &amp;изменение</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="461"/>
        <source>Last &amp;Access</source>
        <translation>Последний &amp;доступ</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="469"/>
        <source>A&amp;ttachment</source>
        <translation>&amp;Вложение</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="493"/>
        <source>Show &amp;Statusbar</source>
        <translation>Отобразить &amp;панель статуса</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="511"/>
        <source>&amp;Perform AutoType</source>
        <translation>Применить &amp;автоввод</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="519"/>
        <source>&amp;16x16</source>
        <translation>&amp;16x16</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="527"/>
        <source>&amp;22x22</source>
        <translation>&amp;22x22</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="535"/>
        <source>2&amp;8x28</source>
        <translation>2&amp;8x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="545"/>
        <source>&amp;Password Generator...</source>
        <translation>&amp;Генератор паролей...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="556"/>
        <source>&amp;Group (search results only)</source>
        <translation>&amp;Группа (поиск в результатах предыдущего поиска)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="561"/>
        <source>Show &amp;Expired Entries...</source>
        <translation>Показать записи с &amp;истекшим сроком годности...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="574"/>
        <source>&amp;Add Bookmark...</source>
        <translation>&amp;Добавить закладку...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="579"/>
        <source>Bookmark &amp;this Database...</source>
        <translation>&amp;Добавить эту базу паролей в закладки...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="305"/>
        <source>&amp;Add New Subgroup...</source>
        <translation>&amp;Добавить новую подгруппу...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="584"/>
        <source>Copy URL to Clipboard</source>
        <translation>Скопировать ссылку в буфер обмена</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Entries</source>
        <translation>&amp;Записи</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="240"/>
        <source>&amp;Groups</source>
        <translation>&amp;Группы</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="594"/>
        <source>Sort groups</source>
        <translation>Сортировать группы</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="600"/>
        <source>S&amp;ynchronize Database...</source>
        <translation type="obsolete">&amp;Синхронизировать базу паролей...</translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>Настроить закладки</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="32"/>
        <source>Enter Master Key</source>
        <translation>Введите главный пароль</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="35"/>
        <source>Set Master Key</source>
        <translation>Определение главного пароля</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="38"/>
        <source>Change Master Key</source>
        <translation>Изменение основного пароля</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="42"/>
        <source>Database Key</source>
        <translation>Ключ базы паролей</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="132"/>
        <source>Last File</source>
        <translation>Последний файл</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="178"/>
        <source>Select a Key File</source>
        <translation>Выбор файл-ключа</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>All Files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>Key Files (*.key)</source>
        <translation>Key-файлы (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="206"/>
        <source>Please enter a Password or select a key file.</source>
        <translation>Введите пароль или выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="211"/>
        <source>Please enter a Password.</source>
        <translation>Введите пароль.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="216"/>
        <source>Please provide a key file.</source>
        <translation>Выберете файл-ключ.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="262"/>
        <source>%1:
No such file or directory.</source>
        <translation>%1:
Файл или директория не найдены.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="228"/>
        <source>The selected key file or directory is not readable.</source>
        <translation>Выбранный файл или директория не доступны.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="247"/>
        <source>The given directory does not contain any key files.</source>
        <translation>Выбранная директория не содержит файлов-ключей.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="253"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation>Выбранная директория содержит более одного файла-ключа.
Пожалуйста, укажите конкретный файл-ключ.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="266"/>
        <source>%1:
File is not readable.</source>
        <translation>%1:
Файл не доступен.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="340"/>
        <source>Create Key File...</source>
        <translation>Создать файл-ключ...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="" line="0"/>
        <source>TextLabel</source>
        <translation type="obsolete">ТекстЛабел</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Введите пароль и/или выберите файл-ключ.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Ключ</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file or directory:</source>
        <translation type="obsolete">Папка с файл-ключом:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="191"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Обзор...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="194"/>
        <source>Alt+B</source>
        <translation type="obsolete">Alt+B</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">Использовать пароль И файл-ключ</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Exit</source>
        <translation type="obsolete">Выход</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password Repet.:</source>
        <translation type="obsolete">Повтор пароля:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>Последний файл</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="163"/>
        <source>Key File:</source>
        <translation>Файл-ключ:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="200"/>
        <source>Generate Key File...</source>
        <translation>Создать файл-ключ...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="232"/>
        <source>Please repeat your password:</source>
        <translation>Введите повторно пароль:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="250"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="277"/>
        <source>Passwords are not equal.</source>
        <translation>Пароли не одинаковы.</translation>
    </message>
</context>
<context>
    <name>PwDatabase</name>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Error</source>
        <translation type="obsolete">Неизвестная ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong Signature</source>
        <translation type="obsolete">Неверная сигнатура</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open key file.</source>
        <translation type="obsolete">Невозможно открыть файл-ключ.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file could not be written.</source>
        <translation type="obsolete">Файл-ключ не записываем.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Невозможно открыть файл для записи.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Расшифровка прервана.
Ключ неверен или файл повреждён.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="0"/>
        <source>Warning</source>
        <translation type="obsolete">Внимание</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Файл &apos;%1&apos; не найден.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File not found.</source>
        <translation type="obsolete">Файл не найден.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported file version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported hash algorithm.</source>
        <translation type="obsolete">Неподдерживаемы хэш алгоритм.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported encryption algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong password.</source>
        <translation type="obsolete">Неверный пароль.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File is empty.</source>
        <translation type="obsolete">Файл пуст.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid XML file.</source>
        <translation type="obsolete">Неверный файл XML.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Document does not contain data.</source>
        <translation type="obsolete">Документ не содержит данных.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Warning:</source>
        <translation type="obsolete">Внимание:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid RGB color value.
</source>
        <translation type="obsolete">Неверное значение цвета RGB.
</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Never</source>
        <translation type="obsolete">Никогда</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="81"/>
        <source>A&amp;nhang</source>
        <translation type="unfinished">&amp;Вложение</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Поиск...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="150"/>
        <source>Search For:</source>
        <translation>Поиск:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="131"/>
        <source>Regular E&amp;xpression</source>
        <translation>Регулярное &amp;выражение</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation type="obsolete">Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="124"/>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;С учётом регистра</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="65"/>
        <source>Include:</source>
        <translation>Включая:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="109"/>
        <source>&amp;Titles</source>
        <translation>&amp;Название</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="102"/>
        <source>&amp;Usernames</source>
        <translation>&amp;Имя</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="95"/>
        <source>C&amp;omments</source>
        <translation>Ко&amp;мментарий</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="88"/>
        <source>U&amp;RLs</source>
        <translation>&amp;Ссылки</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="74"/>
        <source>Pass&amp;words</source>
        <translation>&amp;Пароли</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search</source>
        <translation type="obsolete">Поиск</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clo&amp;se</source>
        <translation type="obsolete">&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="138"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Включая подгруппы (рекурсивно)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="90"/>
        <source>Attachment</source>
        <translation type="obsolete">Вложение</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Выбор значка</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">Добавить свой значок...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Pick</source>
        <translation type="obsolete">Выбрать</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Отмена</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="956"/>
        <source>Clear clipboard after:</source>
        <translation>Буфер очищать через:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Seconds</source>
        <translation type="obsolete">секунд</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Sh&amp;ow passwords in plain text by default</source>
        <translation type="obsolete">&amp;Отображать пароли в текстовом виде по умолчанию</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="943"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation type="obsolete">&amp;Внешний вид</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="474"/>
        <source>Banner Color</source>
        <translation>Цвет банера</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="488"/>
        <source>Text Color:</source>
        <translation>Цвет текста:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="617"/>
        <source>Change...</source>
        <translation>Изменить...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="542"/>
        <source>Color 2:</source>
        <translation>Цвет 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="667"/>
        <source>C&amp;hange...</source>
        <translation>&amp;Изменить</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="679"/>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="680"/>
        <source>Color 1:</source>
        <translation>Цвет 1:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Expand group tree when opening a database</source>
        <translation type="obsolete">Раскрывать дерево групп при открытии базы паролей</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Other</source>
        <translation type="obsolete">&amp;Другой</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Browser Command:</source>
        <translation type="obsolete">Команда браузера:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Securi&amp;ty</source>
        <translation type="obsolete">Безопасность</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="708"/>
        <source>Alternating Row Colors</source>
        <translation>Переменные цвета столбцов</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1107"/>
        <source>Browse...</source>
        <translation>Обзор...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="219"/>
        <source>Remember last key type and location</source>
        <translation>Запоминать последний тип и положение ключа</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Mounting Root:</source>
        <translation type="obsolete">Корень монтирования:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="191"/>
        <source>Remember last opened file</source>
        <translation>Запоминать последний открытый файл</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="122"/>
        <source>Show system tray icon</source>
        <translation>Показывать иконку в системном треи </translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="180"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>Сворачиваться в системный трей при закрытии главного окна  </translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="194"/>
        <source>Alt+Ö</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="295"/>
        <source>Save recent directories of file dialogs</source>
        <translation>Сохранять последнии выбранные директории диалогов выбора файла</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="749"/>
        <source>Group tree at start-up:</source>
        <translation>Группировка дерева при запуске программы:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="756"/>
        <source>Restore last state</source>
        <translation>Восстанавливать последнее состояние</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="763"/>
        <source>Expand all items</source>
        <translation>Раскрыть все дерево</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="770"/>
        <source>Do not expand any item</source>
        <translation>Не раскрывать дерево</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="94"/>
        <source>Security</source>
        <translation>Безопасность</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="928"/>
        <source>Edit Entry Dialog</source>
        <translation>Диалоге редактирования записей</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1308"/>
        <source>Plug-Ins</source>
        <translation>Дополнения (Plug-Ins)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1317"/>
        <source>None</source>
        <translation>ничего</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1324"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Gnome интеграция (Gtk 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1331"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>KDE 4 интеграция</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1346"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>Изменения вступят в силу после перезагруски программы.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1366"/>
        <source>Configure...</source>
        <translation>Конфигурация...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="99"/>
        <source>Advanced</source>
        <translation>Дополнительно</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="318"/>
        <source>Clear History Now</source>
        <translation>Очистить историю прямо сейчас</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="340"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>Переспрашивать перед удалением записей и групп</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="720"/>
        <source>Customize Entry Detail View...</source>
        <translation>Настройта окна детальной информации записи...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1260"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>Для упрощения интерфейса пользователя Вы можете здесь отключить некоторые функции KeePassX.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1270"/>
        <source>Bookmarks</source>
        <translation>Закладки</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1126"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>Настройка функции автоввода</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1135"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>Задержка неред началом симуляции нажатия клавишь.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1184"/>
        <source>ms</source>
        <translation>мс</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1148"/>
        <source>Pre-Gap:</source>
        <translation>Pre-Gap:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1171"/>
        <source>Key Stroke Delay:</source>
        <translation>Время нажатия клавиши:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1181"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>Задержка между нажатиями клавишь.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1100"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>Директория монтировки устройсв (CD, USB накопителей).</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1090"/>
        <source>Media Root:</source>
        <translation>Медиа Root:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1116"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>Не использовать абсолютные пути для закладок и последнего открытого файла. Это наиболее удобно при использовании переносной версии KeePassX.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1119"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>Сохранять относительные пути (для закладок и последнего открытого файла)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="150"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>Минимизировать в системный трей</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="249"/>
        <source>Start minimized</source>
        <translation>Минимизировать при загрузке</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="279"/>
        <source>Start locked</source>
        <translation>Блокировать главное окно при запуске</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="994"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>Блокировать главное окно при минимизации программы</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1199"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>Глобальная горячая клавиша для автоввода:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1066"/>
        <source>Custom Browser Command</source>
        <translation>Команда браузера</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1078"/>
        <source>Browse</source>
        <translation>Обзор</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="435"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation>Автоматически сохранять базу паролей при закрытии программы или блокировки главного окна</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="919"/>
        <source>Show plain text passwords in:</source>
        <translation>Отображать пароли в:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="935"/>
        <source>Database Key Dialog</source>
        <translation>Диалоге пароля базы</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1032"/>
        <source>seconds</source>
        <translation>секунд</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1003"/>
        <source>Lock database after inactivity of</source>
        <translation>Блокировать базу данных в случае неиспользования компьютера в течении</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1228"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation>Использовать названия записей при поиске окна для автоввода</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="74"/>
        <source>General (1)</source>
        <translation>Основные (1)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="79"/>
        <source>General (2)</source>
        <translation>Основные (2)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="84"/>
        <source>Appearance</source>
        <translation>Представление</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="89"/>
        <source>Language</source>
        <translation>Язык (Language)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="372"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation>При изменении записей сохранять копию в группу &apos;Backup&apos;</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="400"/>
        <source>Delete backup entries older than:</source>
        <translation>Стирать копии записей через:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="426"/>
        <source>days</source>
        <translation>дней</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="442"/>
        <source>Automatically save database after every change</source>
        <translation>Автоматически сохранять базу паролей при каждом изменении</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="837"/>
        <source>System Language</source>
        <translation type="obsolete">Язык системы</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="842"/>
        <source>English</source>
        <translation type="obsolete">Английский</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="845"/>
        <source>Language:</source>
        <translation>Язык (Language):</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="852"/>
        <source>Author:</source>
        <translation>Автор:</translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Введите пароль:</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
</context>
<context>
    <name>StandardDatabase</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Невозможно открыть файл.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong Signature</source>
        <translation type="obsolete">Неверная сигнатура</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Неподдерживаемая версия файла.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Неизвестный алгоритм шифрования.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Расшифровка прервана.
Ключ неверен или файл повреждён.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Невозможно открыть файл для записи.</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation>Автоввод: выбирете окно для ввода</translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation>Выберете окно для автоввода из списка или введите имя окна вручную:</translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../lib/tools.cpp" line="352"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation>Дмитрий Функ, Борис Бродский</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation>dmitry.funk@gmail.com, Brodsky_Boris@yahoo.com</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="351"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation>Русский (Россия)</translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>Title</source>
        <translation type="obsolete">Название</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Username</source>
        <translation type="obsolete">Имя</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Главное окно заблокировано.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>Разблокировать</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>Закрыть базу паролей</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <location filename="" line="0"/>
        <source>Database Settings</source>
        <translation type="obsolete">Н&amp;астройки базы паролей...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Encryption</source>
        <translation type="obsolete">Шифрование</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Algorithm:</source>
        <translation type="obsolete">Алгоритм:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ctrl+K</source>
        <translation type="obsolete">Ctrl+K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Отмена</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Ctrl+C</source>
        <translation type="obsolete">Ctrl+C</translation>
    </message>
</context>
</TS>
