<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="ja_JP">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;現在の翻訳: 日本語&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;作者:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation>Nardog</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation>http://nardog.takoweb.com</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source><byte value="x9"/>Information on how to translate KeePassX can be found under:
<byte value="x9"/>http://keepassx.sourceforge.net/</source>
        <translation type="obsolete"><byte value="x9"/>KeePassX を翻訳する方法の情報は下でご覧になれます:
<byte value="x9"/>http://keepassx.sourceforge.net/</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>チーム</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Tarek Saidi</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="46"/>
        <source>Developer, Project Admin</source>
        <translation>開発者、プロジェクト管理者</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>tariq@users.berlios.de</source>
        <translation type="obsolete">tariq@users.berlios.de</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Eugen Gorschenin</source>
        <translation type="obsolete">Eugen Gorschenin</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Web Designer</source>
        <translation>Web デザイナ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>geugen@users.berlios.de</source>
        <translation type="obsolete">geugen@users.berlios.de</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Thanks To</source>
        <translation>謝辞</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Patches for better MacOS X support</source>
        <translation>よりよい MacOS X サポートのパッチ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>www.outofhanwell.com</source>
        <translation type="obsolete">www.outofhanwell.com</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>James Nicholls</source>
        <translation type="obsolete">James Nicholls</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Main Application Icon</source>
        <translation>メイン アプリケーションのアイコン</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Constantin Makshin</source>
        <translation type="obsolete">Constantin Makshin</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Various fixes and improvements</source>
        <translation>さまざまな修正と向上</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>dinosaur-rus@users.sourceforge.net</source>
        <translation type="obsolete">dinosaur-rus@users.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="65"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="66"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>ファイル &apos;%1&apos; が見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>プログラムが正しくインストールされていることを確実にしてください。</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">ファイル &apos;%1&apos; を開けませんでした</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">以下のエラーが発生しました:
%1</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>http://keepassx.sf.net</source>
        <translation type="obsolete">http://keepassx.sf.net</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>KeePassX を翻訳する方法についての情報は次の下に見つかります:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Developer</source>
        <translation>開発者</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="unfinished">なし</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="60"/>
        <source>About</source>
        <translation>バージョン情報</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="199"/>
        <source>License</source>
        <translation>ライセンス</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="182"/>
        <source>Translation</source>
        <translation>翻訳</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - クロス プラットフォーム パスワード マネージャ&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="165"/>
        <source>Credits</source>
        <translation>クレジット</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="137"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>http://keepassx.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="144"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2 or later.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX は General Public License (GPL) version 2 以降の
条件の下で配布されています。</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="84"/>
        <source>AppName</source>
        <translation>AppName</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="102"/>
        <source>AppFunc</source>
        <translation>AppFunc</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Copyright (C) 2005 - 2007 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2007 KeePassX Team 
KeePassX は General Public License (GPL) 
version 2 の条件の下に配布されています。</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="151"/>
        <source>Copyright (C) 2005 - 2008 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="unfinished">Copyright (C) 2005 - 2007 KeePassX Team 
KeePassX は General Public License (GPL) 
version 2 の条件の下に配布されています。 {2005 ?} {2008 ?} {2.?}</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>ブックマークの追加</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>タイトル:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>ファイル:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>参照...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>ブックマークの編集</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass データベース (*.kdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="68"/>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation>1 つより多くの &apos;自動入力:&apos; キー シーケンスが見つかりました。
許可されているのは 1 つのエントリあたり 1 つのみです。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Error</source>
        <translation type="obsolete">エラー</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="95"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation>文字 %1 に近い [自動入力] シーケンスでの構文エラーです
<byte value="x9"/>閉じ &apos;}&apos; のない &apos;{&apos; が見つかりました</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Auto-Type string contains illegal characters</source>
        <translation type="obsolete">自動入力の文字列は不法な文字を含みます</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="133"/>
        <source>Auto-Type string contains invalid characters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - 自動入力</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>自動入力するにはエントリ上でクリックします。</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="89"/>
        <source>Auto-Type</source>
        <translation>自動入力</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 ビット   (既定)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 ビット</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="65"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>暗号化の周囲数を決定してください。</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="71"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; は有効な整数値ではありません。</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="75"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>暗号化の周囲数は 0 より多い必要があります。</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="176"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="176"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>パスワードとパスワードの反復が同じではありません。
ご入力をチェックしてください。</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="330"/>
        <source>Save Attachment...</source>
        <translation>添付の保存...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>Yes</source>
        <translation>はい</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="349"/>
        <source>Error while writing the file.</source>
        <translation>ファイルの書き込み中のエラーです。</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>Delete Attachment?</source>
        <translation>添付を削除しますか?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>このエントリの添付を削除しようとしています。
よろしいですか?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="358"/>
        <source>No, Cancel</source>
        <translation>いいえ、キャンセル</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="337"/>
        <source>Could not open file.</source>
        <translation>ファイルを開けませんでした。</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="100"/>
        <source>%1 Bit</source>
        <translation>%1 ビット</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="300"/>
        <source>Add Attachment...</source>
        <translation>添付の追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Test 2</source>
        <translation type="obsolete">テスト 2</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="327"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>選択されたエントリは添付がないか空です。</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="60"/>
        <source>Today</source>
        <translation>今日</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="62"/>
        <source>1 Week</source>
        <translation>1 週間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>2 Weeks</source>
        <translation>2 週間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="64"/>
        <source>3 Weeks</source>
        <translation>3 週間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>1 Month</source>
        <translation>1 ヶ月間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Months</source>
        <translation>3 ヶ月間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="68"/>
        <source>6 Months</source>
        <translation>6 ヶ月間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>1 Year</source>
        <translation>1 年間</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="72"/>
        <source>Calendar...</source>
        <translation>カレンダー...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="136"/>
        <source>Edit Entry</source>
        <translation>エントリの編集</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="255"/>
        <source>[Untitled Entry]</source>
        <translation>[無題のエントリ]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="136"/>
        <source>New Entry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation>通知</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation>少なくとも 1 文字入力する必要があります</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="76"/>
        <source>Password Generator</source>
        <translation>パスワード ジェネレータ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Accept</source>
        <translation type="obsolete">承認</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="252"/>
        <source>%1 Bits</source>
        <translation>%1 ビット</translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Error</source>
        <translation type="obsolete">エラー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Please enter a Password.</source>
        <translation type="obsolete">パスワードを入力してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Please choose a key file.</source>
        <translation type="obsolete">キー ファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">パスワードを入力するかキー ファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Database Key</source>
        <translation type="obsolete">データベース キー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Select a Key File</source>
        <translation type="obsolete">キー ファイルの選択</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The selected key file or directory does not exist.</source>
        <translation type="obsolete">選択されたキー ファイルまたはディレクトリが存在しません。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="obsolete">ディレクトリがキー ファイルを含みません。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The given directory contains more then one key file.
Please specify the key file directly.</source>
        <translation type="obsolete">ディレクトリは 1 つより多くのキー ファイルを含みます。
直接キー ファイルを指定してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The key file found in the given directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">ディレクトリに見つかったキー ファイルは読み込み可能ではありません。
権限をチェックしてください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key file could not be found.</source>
        <translation type="obsolete">キー ファイルが見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key file is not readable.
Please check your permissions.</source>
        <translation type="obsolete">キー ファイルは読み込み可能ではありません。
権限をチェックしてください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Warning</source>
        <translation type="obsolete">警告</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">パスワードとパスワードの反復が同じではありません。
ご入力をチェックしてください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">パスワードを入力するかキー ファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">選択されたキー ファイルまたはディレクトリは読み込み可能ではありません。
権限をチェックしてください。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>All Files (*)</source>
        <translation type="obsolete">すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key Files (*.key)</source>
        <translation type="obsolete">キー ファイル (*.key)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>File exists.</source>
        <translation type="obsolete">ファイルは存在します。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>A file with the selected name already exists, should this file be used as key file or do you want to overwrite it with a newly generated one?</source>
        <translation type="obsolete">選択された名前のファイルはすでに存在します、このファイルをキー ファイルとして使用するか新しく生成されたもので上書きしますか?</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Use</source>
        <translation type="obsolete">使用</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Overwrite</source>
        <translation type="obsolete">上書き</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key file could not be created.
%1</source>
        <translation type="obsolete">キー ファイルは作成できませんでした。
%1</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Last File</source>
        <translation type="obsolete">最後のファイル</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>削除</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="92"/>
        <source>Add Icons...</source>
        <translation>アイコンの追加...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Images (%1)</source>
        <translation>イメージ (%1)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: ファイルは読み込めませんでした。
</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="97"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>置換...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">アイコンの読み込み中にエラーが発生しました:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="97"/>
        <source>An error occured while loading the icon.</source>
        <translation>アイコンの読み込み中にエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>カスタム アイコンの追加</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>抽出</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: ファイルは読み込めませんでした。</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="67"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>アイコンの読み込み中にエラーが発生しました:</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="293"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="298"/>
        <source>Select a directory...</source>
        <translation>ディレクトリの選択...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Error: %1</source>
        <translation type="obsolete">エラー: %1</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="305"/>
        <source>Select an executable...</source>
        <translation>実行ファイルの選択...</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>カレンダー</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>エントロピー コレクション</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>ランダム数字ジェネレータ</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>エントロピーを収集しています...
ランダム数字ジェネレータの再シードに十分なエントロピーが収集されるまでマウスを動かすか何かキーを押してください。</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;ランダム プールが正常に再シードされました!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>コメント</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>添付名</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>作成日</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>最終アクセス日</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>最終変更日</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>満了日</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>満了まで</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>ダイアログ</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="26"/>
        <source>Rich Text Editor</source>
        <translation>リッチ テキスト エディタ</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="40"/>
        <source>Bold</source>
        <translation>太字</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="127"/>
        <source>B</source>
        <translation>太</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="59"/>
        <source>Italic</source>
        <translation>斜体</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="62"/>
        <source>I</source>
        <translation>斜</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="72"/>
        <source>Underlined</source>
        <translation>下線</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="75"/>
        <source>U</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="85"/>
        <source>Left-Aligned</source>
        <translation>左揃え</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="88"/>
        <source>L</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="98"/>
        <source>Centered</source>
        <translation>中央揃え</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="140"/>
        <source>C</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="111"/>
        <source>Right-Aligned</source>
        <translation>右揃え</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="114"/>
        <source>R</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="124"/>
        <source>Justified</source>
        <translation>両端揃え</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="137"/>
        <source>Text Color</source>
        <translation>テキストの色</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="147"/>
        <source>Font Size</source>
        <translation>フォント サイズ</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="154"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="159"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="164"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="169"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="174"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="179"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="184"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="189"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="194"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="199"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="204"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="209"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="214"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="219"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="224"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="229"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="234"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="239"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>Templates</source>
        <translation>テンプレート</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="250"/>
        <source>T</source>
        <translation>テ</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="282"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Restore Default</source>
        <translation type="obsolete">既定の復元</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Save</source>
        <translation type="obsolete">保存</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>しない</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>データベースの設定</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>暗号化</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>アルゴリズム:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>暗号化の周囲:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="250"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="251"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="252"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="253"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="254"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="255"/>
        <source>Creation</source>
        <translation>作成</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="256"/>
        <source>Last Access</source>
        <translation>最終アクセス</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="257"/>
        <source>Last Modification</source>
        <translation>最終変更</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Expiration</source>
        <translation>満了</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Comment</source>
        <translation>コメント</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>エントリの編集</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="146"/>
        <source>Username:</source>
        <translation>ユーザー名:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="211"/>
        <source>Password Repet.:</source>
        <translation>パスワードの反復:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="133"/>
        <source>Title:</source>
        <translation>タイトル:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="179"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="224"/>
        <source>Password:</source>
        <translation>パスワード:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="107"/>
        <source>Quality:</source>
        <translation>品質:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="159"/>
        <source>Comment:</source>
        <translation>コメント:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="237"/>
        <source>Expires:</source>
        <translation>満了:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="192"/>
        <source>Group:</source>
        <translation>グループ:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="166"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="438"/>
        <source>Icon:</source>
        <translation>アイコン:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="58"/>
        <source>Ge&amp;n.</source>
        <translation>生成(&amp;N)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="356"/>
        <source>Never</source>
        <translation>しない</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="120"/>
        <source>Attachment:</source>
        <translation>添付:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&gt;</source>
        <translation type="obsolete">&gt;</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="395"/>
        <source>%1 Bit</source>
        <translation>%1 ビット</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>グループのプロパティ</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="44"/>
        <source>Title:</source>
        <translation>タイトル:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="37"/>
        <source>Icon:</source>
        <translation>アイコン:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">キャンセル(&amp;C)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>O&amp;K</source>
        <translation type="obsolete">OK(&amp;K)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="70"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>Expried Entries of the Database</source>
        <translation type="obsolete">データベースの満了済みエントリ</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>満了済みエントリ</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>ジャンプするエントリ上でダブル クリックします。</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>満了</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>データベースの満了済みエントリ</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML ファイル (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>KeePassX XML ファイル</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>テキスト ファイル (*.txt)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>テキスト ファイル</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Import File...</source>
        <translation>ファイルのインポート...</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>エクスポートが失敗しました</translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="51"/>
        <source>No error occurred.</source>
        <translation>エラーが発生しませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="52"/>
        <source>An error occurred while reading from the file.</source>
        <translation>ファイルからの読み込み中にエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="53"/>
        <source>An error occurred while writing to the file.</source>
        <translation>ファイルへの書き込み中にエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="54"/>
        <source>A fatal error occurred.</source>
        <translation>致命的なエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="55"/>
        <source>An resource error occurred.</source>
        <translation>リソース エラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="56"/>
        <source>The file could not be opened.</source>
        <translation>ファイルは開けませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="57"/>
        <source>The operation was aborted.</source>
        <translation>操作は中止されました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="58"/>
        <source>A timeout occurred.</source>
        <translation>タイムアウトが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="59"/>
        <source>An unspecified error occurred.</source>
        <translation>予期しないエラーが発生しました。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="60"/>
        <source>The file could not be removed.</source>
        <translation>ファイルは削除できませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="61"/>
        <source>The file could not be renamed.</source>
        <translation>ファイルは名前を変更できませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>The position in the file could not be changed.</source>
        <translation>ファイルの位置は変更できませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>The file could not be resized.</source>
        <translation>ファイルはサイズを変更できませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>The file could not be accessed.</source>
        <translation>ファイルはアクセスできませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>The file could not be copied.</source>
        <translation>ファイルはコピーできませんでした。</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="135"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="112"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="306"/>
        <source>Alt+M</source>
        <translation>Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="89"/>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>パスワード ジェネレータ</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="372"/>
        <source>Generate</source>
        <translation>生成</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="362"/>
        <source>New Password:</source>
        <translation>新しいパスワード:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="276"/>
        <source>Quality:</source>
        <translation>品質:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="38"/>
        <source>Options</source>
        <translation>オプション</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="132"/>
        <source>&amp;Upper Letters</source>
        <translation>大文字(&amp;U)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="86"/>
        <source>&amp;Lower Letters</source>
        <translation>小文字(&amp;L)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="109"/>
        <source>&amp;Numbers</source>
        <translation>数字(&amp;N)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="152"/>
        <source>&amp;Special Characters</source>
        <translation>特殊文字(&amp;S)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation>マイナス</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation>下線(&amp;N)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="179"/>
        <source>Use &amp;only following characters:</source>
        <translation>以下の文字のみ使用する(&amp;O):</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="182"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="240"/>
        <source>Length:</source>
        <translation>長さ:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="47"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>以下の文字グループを使用する(&amp;W):</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation>空白(&amp;S)</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="125"/>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="303"/>
        <source>Enable entropy collection</source>
        <translation>エントロピーの収集を有効にする</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="332"/>
        <source>Collect only once per session</source>
        <translation>収集は 1 セッションあたり 1 回のみ</translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML ファイル (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>インポートが失敗しました</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>不正な XML データです (詳細は stdout をご覧ください)。</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>不正な XML データです。</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>ドキュメントがデータを含みません。</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>KeePass XML ファイル (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>インポートが失敗しました</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>行 %1 列 %2 での XML 構文解析エラー:
%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>構文解析エラー: ファイルは有効な KeePassX XML ファイルではありません。</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="27"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>PwManager ファイル (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="112"/>
        <source>Import Failed</source>
        <translation>インポートが失敗しました</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="38"/>
        <source>File is empty.</source>
        <translation>ファイルは空です。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="46"/>
        <source>File is no valid PwManager file.</source>
        <translation>ファイルは有効な PwManager ファイルではありません。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="49"/>
        <source>Unsupported file version.</source>
        <translation>未サポートのファイル バージョンです。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="55"/>
        <source>Unsupported hash algorithm.</source>
        <translation>未サポートのハッシュ アルゴリズムです。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="58"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>未サポートの暗号化アルゴリズムです。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="65"/>
        <source>Compressed files are not supported yet.</source>
        <translation>圧縮ファイルはまだサポートされていません。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="90"/>
        <source>Wrong password.</source>
        <translation>間違ったパスワードです。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="105"/>
        <source>File is damaged (hash test failed).</source>
        <translation>ファイルは損害を受けています (ハッシュ テストが失敗しました)。</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="112"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>不正な XML データです (詳細は stdout をご覧ください)。</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>ファイルのインポート...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>インポートが失敗しました</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="434"/>
        <source>Could not open file.</source>
        <translation>ファイルを開けませんでした。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="445"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>予期しないファイル サイズです (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="462"/>
        <source>Wrong Signature</source>
        <translation>間違った署名</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="467"/>
        <source>Unsupported File Version.</source>
        <translation>未サポートのファイル バージョンです。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="476"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>不明な暗号化アルゴリズムです。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="505"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>複合化が失敗しました。
キーが間違っているかファイルが損害を受けています。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="524"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>ハッシュ テストが失敗しました。
キーが間違っているかファイルが損害を受けています。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [G1]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[G1]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [G2]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[G２]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [E1]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[E1]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [E2]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[E2]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unexpected error: Offset is out of range. [E3]</source>
        <translation type="obsolete">予期しないエラー: オフセットは範囲外です。[E3]</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="609"/>
        <source>Invalid group tree.</source>
        <translation>不正なグループ ツリーです。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="794"/>
        <source>Key file is empty.</source>
        <translation>キー ファイルは空です。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1133"/>
        <source>The database must contain at least one group.</source>
        <translation>データベースは少なくとも 1 つのグループを含む必要があります。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1147"/>
        <source>Could not open file for writing.</source>
        <translation>書き込み用のファイルを開けませんでした。</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="603"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>予期しないエラー: オフセットは範囲外です。</translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="992"/>
        <source>Bytes</source>
        <translation>バイト</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1000"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1006"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1011"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="409"/>
        <source>Title</source>
        <translation>タイトル</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="411"/>
        <source>Username</source>
        <translation>ユーザー名</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="413"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="415"/>
        <source>Password</source>
        <translation>パスワード</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="417"/>
        <source>Comments</source>
        <translation>コメント</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="419"/>
        <source>Expires</source>
        <translation>満了</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="421"/>
        <source>Creation</source>
        <translation>作成</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="423"/>
        <source>Last Change</source>
        <translation>最終変更</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="425"/>
        <source>Last Access</source>
        <translation>最終アクセス</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="427"/>
        <source>Attachment</source>
        <translation>添付</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Are you sure you want delete this entry?</source>
        <translation type="obsolete">このエントリを削除してもよろしいですか?</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Are you sure you want delete these %1 entries?</source>
        <translation type="obsolete">これら %1 個のエントリを削除してもよろしいですか?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Delete?</source>
        <translation>削除しますか?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="429"/>
        <source>Group</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>少なくとも 1 のグループはエントリの追加前に存在する必要があります。</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="225"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="144"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="146"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="54"/>
        <source>Search Results</source>
        <translation>検索結果</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Groups</source>
        <translation type="obsolete">グループ</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="85"/>
        <source>Delete?</source>
        <translation>削除しますか?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="86"/>
        <source>Are you sure you want to delete this group, all it&apos;s child groups and all their entries?</source>
        <translation>このグループ、すべての子グループ、およびそれらのエントリをすべて削除してもよろしいですか?</translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="346"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="348"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="351"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="353"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="354"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="347"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="453"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>The following error occured while opening the database:
%1</source>
        <translation type="obsolete">データベースを開いている間に以下のエラーが発生しました:
%1</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="470"/>
        <source>Save modified file?</source>
        <translation>変更されたファイルを保存しますか?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation>現在のファイルは変更されました。変更を
保存しますか?</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Yes</source>
        <translation type="obsolete">はい</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>No</source>
        <translation type="obsolete">いいえ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="788"/>
        <source>Clone Entry</source>
        <translation>エントリを閉じる</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="790"/>
        <source>Delete Entry</source>
        <translation>エントリの削除</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="802"/>
        <source>Clone Entries</source>
        <translation>エントリのクローン</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="804"/>
        <source>Delete Entries</source>
        <translation>エントリの削除</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">ファイルは保存できませんでした。
%1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="445"/>
        <source>Ready</source>
        <translation>レディ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>[new]</source>
        <translation type="obsolete">[新規]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="544"/>
        <source>Open Database...</source>
        <translation>データベースを開く...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="416"/>
        <source>Loading Database...</source>
        <translation>データベースを読み込んでいます...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="430"/>
        <source>Loading Failed</source>
        <translation>読み込みが失敗しました</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">ツール バーの表示</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>%1 - KeePassX</source>
        <translation type="obsolete">%1 - KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="432"/>
        <source>Unknown error while loading database.</source>
        <translation>データベースの読み込み中の不明なエラーです。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass データベース (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>All Files (*)</source>
        <translation>すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="830"/>
        <source>Save Database...</source>
        <translation>データベースの保存...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>KeePassX - [unsaved]</source>
        <translation type="obsolete">KeePassX - [未保存]</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>New Database</source>
        <translation type="obsolete">新しいデータベース</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>expired</source>
        <translation type="obsolete">満了済み</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="683"/>
        <source>1 Month</source>
        <translation>1 ヶ月間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="685"/>
        <source>%1 Months</source>
        <translation>%1 ヶ月間</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>, </source>
        <translation type="obsolete">、</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="691"/>
        <source>1 Year</source>
        <translation>1 年間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="693"/>
        <source>%1 Years</source>
        <translation>%1 年間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="699"/>
        <source>1 Day</source>
        <translation>1 日間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="701"/>
        <source>%1 Days</source>
        <translation>%1 日間</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="705"/>
        <source>less than 1 day</source>
        <translation>1 日未満</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Set Master Key</source>
        <translation type="obsolete">マスター キーの設定</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>* - KeePassX</source>
        <translation type="obsolete">* - KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1222"/>
        <source>Locked</source>
        <translation>ロック済み</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1237"/>
        <source>Unlocked</source>
        <translation>未ロック</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="349"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="350"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="453"/>
        <source>The database file does not exist.</source>
        <translation>データベース ファイルが存在しません。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="434"/>
        <source>The following error occured while opening the database:</source>
        <translation>データベースを開いている間に以下のエラーが発生ました:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="883"/>
        <source>new</source>
        <translation>新規</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="669"/>
        <source>Expired</source>
        <translation>満了済み</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="833"/>
        <source>File could not be saved.</source>
        <translation>ファイルは保存できませんでした。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1223"/>
        <source>Un&amp;lock Workspace</source>
        <translation>ワークスペースのロック解除(&amp;L)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1238"/>
        <source>&amp;Lock Workspace</source>
        <translation>ワークスペースのロック(&amp;L)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="295"/>
        <source>Show &amp;Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="345"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="136"/>
        <source>Error</source>
        <translation>エラー</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="138"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>ファイル &apos;%1&apos; は見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="138"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Columns</source>
        <translation type="obsolete">列</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Add New Group...</source>
        <translation type="obsolete">新しいグループの追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Edit Group...</source>
        <translation type="obsolete">グループの編集...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Delete Group</source>
        <translation type="obsolete">グループの削除</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">クリップボードへパスワードをコピー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">クリップボードへユーザー名をコピー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Open URL</source>
        <translation type="obsolete">URL を開く</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Save Attachment As...</source>
        <translation type="obsolete">名前を付けて添付を保存...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Add New Entry...</source>
        <translation type="obsolete">新しいエントリの追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">エントリの表示/編集...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Delete Entry</source>
        <translation type="obsolete">エントリの削除</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Clone Entry</source>
        <translation type="obsolete">エントリのクローン</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search In Database...</source>
        <translation type="obsolete">データベースから検索...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search in this group...</source>
        <translation type="obsolete">このグループから検索...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Entry Details</source>
        <translation type="obsolete">エントリの詳細の表示</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Hide Usernames</source>
        <translation type="obsolete">ユーザー名を隠す</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Hide Passwords</source>
        <translation type="obsolete">パスワードを隠す</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Title</source>
        <translation type="obsolete">タイトル</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Username</source>
        <translation type="obsolete">ユーザー名</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>URL</source>
        <translation type="obsolete">URL</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Password</source>
        <translation type="obsolete">パスワード</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Comment</source>
        <translation type="obsolete">コメント</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Expires</source>
        <translation type="obsolete">満了</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Creation</source>
        <translation type="obsolete">作成</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Last Change</source>
        <translation type="obsolete">最終変更</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Last Access</source>
        <translation type="obsolete">最終アクセス</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Attachment</source>
        <translation type="obsolete">添付</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Statusbar</source>
        <translation type="obsolete">ステータス バーの表示</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="481"/>
        <source>Hide</source>
        <translation>非表示</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Perform AutoType</source>
        <translation type="obsolete">自動入力を行う</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">ツール バー アイコンのサイズ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>16x16</source>
        <translation type="obsolete">16x16</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>22x22</source>
        <translation type="obsolete">22x22</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>28x28</source>
        <translation type="obsolete">28x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="186"/>
        <source>&amp;View</source>
        <translation>表示(&amp;V)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;File</source>
        <translation>ファイル(&amp;F)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="137"/>
        <source>&amp;Import from...</source>
        <translation>インポート(&amp;I)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="132"/>
        <source>&amp;Export to...</source>
        <translation>エクスポート(&amp;E)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="164"/>
        <source>&amp;Edit</source>
        <translation>編集(&amp;E)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="225"/>
        <source>E&amp;xtras</source>
        <translation>追加(&amp;X)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="120"/>
        <source>&amp;Help</source>
        <translation>ヘルプ(&amp;H)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="246"/>
        <source>&amp;Open Database...</source>
        <translation>データベースを開く(&amp;O)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="251"/>
        <source>&amp;Close Database</source>
        <translation>データベースを閉じる(&amp;C)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="256"/>
        <source>&amp;Save Database</source>
        <translation>データベースの上書き保存(&amp;S)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="261"/>
        <source>Save Database &amp;As...</source>
        <translation>名前を付けてデータベースを保存(&amp;A)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="266"/>
        <source>&amp;Database Settings...</source>
        <translation>データベースの設定(&amp;D)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="271"/>
        <source>Change &amp;Master Key...</source>
        <translation>マスター キーの変更(&amp;M)...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>E&amp;xit</source>
        <translation type="obsolete">終了(&amp;X)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="455"/>
        <source>&amp;Settings...</source>
        <translation>設定(&amp;S)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="460"/>
        <source>&amp;About...</source>
        <translation>バージョン情報(&amp;A)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="473"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>KeePassX ハンドブック(&amp;K)...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="515"/>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation>スタンダード KeePass シングル ユーザー データベース (*.kdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="520"/>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation>アドバンスド KeePass データベース (*.kdb)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>New Database...</source>
        <translation type="obsolete">新しいデータベース...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Password Generator...</source>
        <translation type="obsolete">パスワード ジェネレータ...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Group (search results only)</source>
        <translation type="obsolete">グループ (検索結果のみ)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Expired Entries...</source>
        <translation type="obsolete">満了済みエントリの表示...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show Expired Entries</source>
        <translation type="obsolete">満了済みエントリの表示</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="551"/>
        <source>Recycle Bin...</source>
        <translation>ごみ箱...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Lock Workspace</source>
        <translation type="obsolete">ワークスペースのロック</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="47"/>
        <source>Groups</source>
        <translation>グループ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Bookmarks</source>
        <translation type="obsolete">ブックマーク</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Manage Bookmarks...</source>
        <translation type="obsolete">ブックマークの管理...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="276"/>
        <source>&amp;Lock Workspace</source>
        <translation>ワークスペースのロック(&amp;L)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Q&amp;uit</source>
        <translation type="obsolete">終了(&amp;U)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search in Database...</source>
        <translation type="obsolete">データベースから検索...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search in this Group...</source>
        <translation type="obsolete">このグループから検索...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Add Bookmark...</source>
        <translation type="obsolete">ブックマークの追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Bookmark this Database...</source>
        <translation type="obsolete">このデータベースをブックマーク...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="142"/>
        <source>&amp;Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="190"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="198"/>
        <source>&amp;Columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="241"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="281"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Add New Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="291"/>
        <source>&amp;Edit Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="296"/>
        <source>&amp;Delete Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="301"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="306"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="311"/>
        <source>&amp;Open URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="316"/>
        <source>&amp;Save Attachment As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="321"/>
        <source>Add &amp;New Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="326"/>
        <source>&amp;View/Edit Entry...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="331"/>
        <source>De&amp;lete Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="336"/>
        <source>&amp;Clone Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="341"/>
        <source>Search &amp;in Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="346"/>
        <source>Search in this &amp;Group...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="354"/>
        <source>Show &amp;Entry Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="362"/>
        <source>Hide &amp;Usernames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="370"/>
        <source>Hide &amp;Passwords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="378"/>
        <source>&amp;Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="386"/>
        <source>User&amp;name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="394"/>
        <source>&amp;URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="402"/>
        <source>&amp;Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="410"/>
        <source>&amp;Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="418"/>
        <source>E&amp;xpires</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="426"/>
        <source>C&amp;reation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="434"/>
        <source>&amp;Last Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="442"/>
        <source>Last &amp;Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="450"/>
        <source>A&amp;ttachment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="468"/>
        <source>Show &amp;Statusbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="486"/>
        <source>&amp;Perform AutoType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="494"/>
        <source>&amp;16x16</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="502"/>
        <source>&amp;22x22</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="510"/>
        <source>2&amp;8x28</source>
        <translation type="unfinished">28x28 {2&amp;8x?}</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="525"/>
        <source>&amp;New Database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="530"/>
        <source>&amp;Password Generator...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="541"/>
        <source>&amp;Group (search results only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="546"/>
        <source>Show &amp;Expired Entries...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="559"/>
        <source>&amp;Add Bookmark...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="564"/>
        <source>Bookmark &amp;this Database...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>ブックマークの管理</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="31"/>
        <source>Enter Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="34"/>
        <source>Set Master Key</source>
        <translation type="unfinished">マスター キーの設定</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="37"/>
        <source>Change Master Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="41"/>
        <source>Database Key</source>
        <translation type="unfinished">データベース キー</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="125"/>
        <source>Last File</source>
        <translation type="unfinished">最後のファイル</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="171"/>
        <source>Select a Key File</source>
        <translation type="unfinished">キー ファイルの選択</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="335"/>
        <source>All Files (*)</source>
        <translation type="unfinished">すべてのファイル (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="335"/>
        <source>Key Files (*.key)</source>
        <translation type="unfinished">キー ファイル (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="199"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="unfinished">パスワードを入力するかキー ファイルを選択してください。</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="204"/>
        <source>Please enter a Password.</source>
        <translation type="unfinished">パスワードを入力してください。</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="209"/>
        <source>Please provide a key file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="255"/>
        <source>%1:
No such file or directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="221"/>
        <source>The selected key file or directory is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="240"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="unfinished">ディレクトリがキー ファイルを含みません。</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="246"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="259"/>
        <source>%1:
File is not readable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="333"/>
        <source>Create Key File...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="" line="279"/>
        <source>TextLabel</source>
        <translation type="obsolete">TextLabel</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>パスワードを入力するかキー ファイルを選択します。</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>キー</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="138"/>
        <source>Password:</source>
        <translation>パスワード:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key file or directory:</source>
        <translation type="obsolete">キー ファイルまたはディレクトリ:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="186"/>
        <source>&amp;Browse...</source>
        <translation>参照(&amp;B)...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="189"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">パスワードとキー ファイルを使用する</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Exit</source>
        <translation type="obsolete">終了</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Password Repet.:</source>
        <translation type="obsolete">パスワードの反復:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>最後のファイル</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="158"/>
        <source>Key File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="198"/>
        <source>Generate Key File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="222"/>
        <source>Please repeat your password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="234"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="261"/>
        <source>Passwords are not equal.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="279"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">ファイル &apos;%1&apos; が見つかりませんでした。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Error</source>
        <translation type="obsolete">エラー</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Never</source>
        <translation type="obsolete">しない</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Initialization failed.</source>
        <translation type="obsolete">初期化が失敗しました。</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="120"/>
        <source>Could not locate library file.</source>
        <translation type="unfinished">ライブラリ ファイルを検索できませんでした。</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>検索</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="90"/>
        <source>A&amp;nhang</source>
        <translation>アンハング(&amp;N)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>検索...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="180"/>
        <source>Search For:</source>
        <translation>検索する文字列:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="158"/>
        <source>Regular E&amp;xpression</source>
        <translation>正規表現(&amp;X)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation>Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="148"/>
        <source>&amp;Case Sensitive</source>
        <translation>大文字と小文字を区別する(&amp;C)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="68"/>
        <source>Include:</source>
        <translation>範囲:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="130"/>
        <source>&amp;Titles</source>
        <translation>タイトル(&amp;T)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="120"/>
        <source>&amp;Usernames</source>
        <translation>ユーザー名(&amp;U)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="110"/>
        <source>C&amp;omments</source>
        <translation>コメント(&amp;O)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="100"/>
        <source>U&amp;RLs</source>
        <translation>URL(&amp;R)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="80"/>
        <source>Pass&amp;words</source>
        <translation>パスワード(&amp;W)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Search</source>
        <translation type="obsolete">検索</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Clo&amp;se</source>
        <translation type="obsolete">閉じる(&amp;S)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="168"/>
        <source>Include Subgroups (recursive)</source>
        <translation>サブグループを含める (再帰的)</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>アイコンの選択</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">カスタム アイコンの追加...</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Pick</source>
        <translation type="obsolete">抽出</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Cancel</source>
        <translation type="obsolete">キャンセル</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="132"/>
        <source>Alt+Ö</source>
        <translation>Shift+Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="728"/>
        <source>Clear clipboard after:</source>
        <translation>クリップボードをクリアする:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Seconds</source>
        <translation type="obsolete">秒後</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="700"/>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation>外観(&amp;R)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="318"/>
        <source>Banner Color</source>
        <translation>バナーの色</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="335"/>
        <source>Text Color:</source>
        <translation>テキストの色:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="470"/>
        <source>Change...</source>
        <translation>変更...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="392"/>
        <source>Color 2:</source>
        <translation>色 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="526"/>
        <source>C&amp;hange...</source>
        <translation>変更(&amp;H)...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="529"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="542"/>
        <source>Color 1:</source>
        <translation>色 1:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Browser Command:</source>
        <translation type="obsolete">ブラウザ コマンド:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="570"/>
        <source>Alternating Row Colors</source>
        <translation>交互の列の色</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1026"/>
        <source>Browse...</source>
        <translation>参照...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="160"/>
        <source>Remember last key type and location</source>
        <translation>最後のキーの種類と場所を記憶する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="129"/>
        <source>Remember last opened file</source>
        <translation>最後に開かれたファイルを記憶する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="51"/>
        <source>The integration plugins provide features like usage of the native file dialogs and message boxes of the particular desktop environments.</source>
        <translation>統合プラグインは特定のデスクトップ環境のネイティブのファイル ダイアログとメッセージボックスの使用のような機能を供給します。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="54"/>
        <source>General</source>
        <translation>全般</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="60"/>
        <source>Show system tray icon</source>
        <translation>システム トレイ アイコンを表示する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="118"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>メイン ウィンドウの閉じるボタンのクリック時にトレイへ最小化する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="236"/>
        <source>Save recent directories of file dialogs</source>
        <translation>ファイル ダイアログの最近のディレクトリを保存する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="614"/>
        <source>Group tree at start-up:</source>
        <translation>起動時のグループ ツリー:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="621"/>
        <source>Restore last state</source>
        <translation>最後の状態を復元する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="628"/>
        <source>Expand all items</source>
        <translation>すべてのアイテムを展開する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="635"/>
        <source>Do not expand any item</source>
        <translation>すべてのアイテムを展開しない</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="673"/>
        <source>Security</source>
        <translation>セキュリティ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Show passwords in plain text in:</source>
        <translation type="obsolete">プレーン テキストでパスワードを表示する:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="697"/>
        <source>Edit Entry Dialog</source>
        <translation>[エントリの編集] ダイアログ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Key Dialogs</source>
        <translation type="obsolete">[キー] ダイアログ</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="864"/>
        <source>Desktop Integration</source>
        <translation>デスクトップ統合</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="895"/>
        <source>Plug-Ins</source>
        <translation>プラグイン</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="904"/>
        <source>None</source>
        <translation>なし</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="911"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Gnome デスクトップ統合 (Gtk 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="918"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>KDE 4 デスクトップ統合</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="933"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>変更を影響させる前にプログラムを再起動する必要があります。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="953"/>
        <source>Configure...</source>
        <translation>構成...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="976"/>
        <source>Advanced</source>
        <translation>詳細設定</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="259"/>
        <source>Clear History Now</source>
        <translation>今すぐ履歴をクリア</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="288"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>常にエントリまたはグループの削除前に質問する</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Unified Title and Toolbar</source>
        <translation type="obsolete">統一タイトルとツール バー</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="582"/>
        <source>Customize Entry Detail View...</source>
        <translation>エントリの詳細表示のカスタマイズ...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="827"/>
        <source>Features</source>
        <translation>機能</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="833"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>ユーザー インターフェイスをスリムに維持する際は必要に応じてここで KeePassX のいくつかの機能を無効にできます。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="843"/>
        <source>Bookmarks</source>
        <translation>ブックマーク</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1045"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>自動入力の微調整</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1057"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>ユーザーによる自動入力のアクティブ化と最初のシミュレート済みキー ストロークの間の時間です。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1106"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1070"/>
        <source>Pre-Gap:</source>
        <translation>プリギャップ:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1093"/>
        <source>Key Stroke Delay:</source>
        <translation>キー ストロークの遅延:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1103"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>2 つのシミュレート済みキー ストロークの間の遅延です。自動入力がランダムに文字をスキップする場合はこれを上げます。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1019"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>CD やメモリ スティックのようなストレージ デバイスが通常マウントされるディレクトリです。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1009"/>
        <source>Media Root:</source>
        <translation>メディア ルート:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>System Default</source>
        <translation type="obsolete">システム既定</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1035"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>ブックマークとそれらの絶対パスから独立した最後に開かれたファイルを使用したい場合はこれを有効にします。これは特にポータブルに KeePassX を使用しそのためファイル システムのマウント ポイントの変更があるときに有用です。</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1038"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>相対パス (ブックマークと最後のファイル) を保存する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="88"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>タスク バーの代わりにトレイへ最小化する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="190"/>
        <source>Start minimized</source>
        <translation>最小化済みで起動する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="220"/>
        <source>Start locked</source>
        <translation>ロック済みで起動する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="766"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>メイン ウィンドウの最小化時にワークスペースをロックする</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="985"/>
        <source>Custom Browser Command</source>
        <translation>カスタム ブラウザ コマンド</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="997"/>
        <source>Browse</source>
        <translation>参照</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1121"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>グローバル自動入力ショートカット:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Use entry titles to match the window for Global Auto-Type</source>
        <translation type="obsolete">グローバグ自動入力のウィンドウへの一致にエントリのタイトルを使用する</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="281"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="688"/>
        <source>Show plain text passwords in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="707"/>
        <source>Database Key Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="804"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="775"/>
        <source>Lock database after inactivity of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1150"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="66"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="68"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="70"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="72"/>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="74"/>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>パスワードを入力します</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>パスワード:</translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <location filename="" line="279"/>
        <source>Recycle Bin</source>
        <translation type="obsolete">ごみ箱</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Double click on an entry to restore it.</source>
        <translation type="obsolete">復元するエントリ上でダブル クリックします。</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Group</source>
        <translation type="obsolete">グループ</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Title</source>
        <translation type="obsolete">タイトル</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Username</source>
        <translation type="obsolete">ユーザー名</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Expired</source>
        <translation type="obsolete">満了</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Empty Recycle Bin</source>
        <translation type="obsolete">ごみ箱を空にする</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Close</source>
        <translation type="obsolete">閉じる</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>フォーム</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;ワークスペースはロックされています。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>ロック解除</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>データベースを閉じる</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <location filename="" line="279"/>
        <source>Database Settings</source>
        <translation type="obsolete">データベースの設定</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Encryption</source>
        <translation type="obsolete">暗号化</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Algorithm:</source>
        <translation type="obsolete">アルゴリズム:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Encryption Rounds:</source>
        <translation type="obsolete">暗号化の周囲:</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>O&amp;K</source>
        <translation type="obsolete">OK(&amp;K)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Ctrl+K</source>
        <translation type="obsolete">Ctrl+K</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">キャンセル(&amp;C)</translation>
    </message>
    <message>
        <location filename="" line="279"/>
        <source>Ctrl+C</source>
        <translation type="obsolete">Ctrl+C</translation>
    </message>
</context>
</TS>
