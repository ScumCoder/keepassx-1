<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="it_IT">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Diego Pierotto</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">ita.translations@tiscali.it</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>Informazioni su come tradurre KeePassX si possono trovare qui:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>Gruppo</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation>Sviluppatore, Amministratore progetto</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation>Disegnatore Web</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation>Sviluppatore</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation>Ringraziamenti</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation>Patches per migliorie a MacOS X</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation>Icona applicazione principale</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation>Alcune correzioni e migliorie</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Impossibile trovare il file &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>Assicurati che il programma sia installato correttamente.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation>Traduzione attuale</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation>Autore</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="50"/>
        <source>About</source>
        <translation>Informazioni su</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="74"/>
        <source>AppName</source>
        <translation>AppName</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="92"/>
        <source>AppFunc</source>
        <translation>AppFunc</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>http://keepassx.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="163"/>
        <source>Credits</source>
        <translation>Crediti</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="188"/>
        <source>Translation</source>
        <translation>Traduzione</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="213"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="151"/>
        <source>Copyright (C) 2005 - 2008 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2008 KeePassX Team
KeePassX è distribuito sotto i termini della 
General Public License (GPL) versione 2.</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2008 KeePassX Team
KeePassX è distribuito sotto i termini della 
General Public License (GPL) versione 2. {2005 ?} {2009 ?} {2.?}</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX è distribuito sotto i termini della 
General Public License (GPL) versione 2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>Aggiungi ai Segnalibri</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>Titolo:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>File:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>Sfoglia...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>Modifica i Segnalibri</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>Database KeePass (*.kdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>Tutti i files (*)</translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="68"/>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation type="obsolete">Trovate più di una sequenza chiave di &apos;Auto Digitazione:&apos;.
E&apos; permessa solo una per voce.</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="95"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="obsolete">Errore sintassi nella sequenza Auto Digitazione vicina al carattere %1
<byte value="x9"/>Trovati &apos;{&apos; senza chiusura &apos;}&apos;</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="133"/>
        <source>Auto-Type string contains invalid characters</source>
        <translation type="obsolete">La stringa di Auto Digitazione contiene caratteri non validi</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - Auto Digitazione</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>Clicca su una voce per Auto digitarlo.</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation>Gruppo</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation>Nome utente</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="90"/>
        <source>Auto-Type</source>
        <translation>Auto Digitazione</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 Bit   (predefinito)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Warning</source>
        <translation>Attenzione</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Determina il numero di passaggi di cifratura.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="70"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; non è un valore integer valido.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>Il numero di passaggi di cifratura deve essere maggiore di 0.</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation>Oggi</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation>1 Settimana</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation>2 Settimane</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation>3 Settimane</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation>1 Mese</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation>3 Mesi</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation>6 Mesi</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation>1 Anno</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation>Calendario...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation>Modifica voce</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>Attenzione</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>La password e la conferma password non sono uguali.
Verifica i valori inseriti.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="256"/>
        <source>[Untitled Entry]</source>
        <translation>[Voce senza titolo]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="294"/>
        <source>Add Attachment...</source>
        <translation>Aggiungi allegato...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>Could not open file.</source>
        <translation>Impossibile aprire il file.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="321"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>La voce scelta non ha allegati oppure è vuota.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="324"/>
        <source>Save Attachment...</source>
        <translation>Salva allegato...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error while writing the file.</source>
        <translation>Errore durante la scrittura del file.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="351"/>
        <source>Delete Attachment?</source>
        <translation>Eliminare allegato?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="352"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Stai per eliminare l&apos;allegato di questa voce.
Sei sicuro?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>No, Cancel</source>
        <translation>No, Annulla</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation>Nuova voce</translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="121"/>
        <source>Password Generator</source>
        <translation>Generatore di password</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation type="obsolete">Avviso</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation type="obsolete">Devi inserire almeno un carattere</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="278"/>
        <source>%1 Bits</source>
        <translation>%1 Bits</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Sostituisci...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>Aggiungi icona personalizzata</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>Seleziona</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Add Icons...</source>
        <translation>Aggiungi icone...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="94"/>
        <source>Images (%1)</source>
        <translation>Immagini (%1)</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: Impossibile caricare il file.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="68"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>Errore durante il caricamento dell&apos;icona(e):</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>An error occured while loading the icon.</source>
        <translation>Errore durante il caricamento dell&apos;icona.</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="350"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="355"/>
        <source>Select a directory...</source>
        <translation>Seleziona una directory...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="362"/>
        <source>Select an executable...</source>
        <translation>Seleziona un eseguibile...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="424"/>
        <source>System Language</source>
        <translation>Lingua di sistema</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>Calendario</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>Generatore numero casuale</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>Impostazione entropia...
Sposta il mouse e/o premi alcuni tasti finché ci sia sufficiente entropia affinché il generatore imposti un numero casuale.</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Insieme casuale generato correttamente!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>Accumulazione entropia</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Finestra di dialogo</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation>Editor Rich Text</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation>Grassetto</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation>Corsivo</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation>Sottolineato</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation>Allineato a sinistra</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation>Centrato</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation>Allineato a destra</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation>Giustificato</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation>Colore testo</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation>Dimensione carattere</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation>Modelli</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>Gruppo</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>Nome utente</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>Commento</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>Nome allegato</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>Data creazione</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>Data ultimo accesso</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>Data ultima modifica</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>Data scadenza</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>Tempo fino alla scadenza</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>Mai</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>Impostazioni Database</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>Cifratura</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>Algoritmo:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>Passaggi di cifratura:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation>Calcola passaggi con ritardo di un secondo su questo computer</translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation>Gruppo</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation>Nome utente</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation>Creazione</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation>Ultimo accesso</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation>Ultima modifica</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation>Scadenza</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation>Commento</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Modifica voce</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="52"/>
        <source>Ge&amp;n.</source>
        <translation>Ge&amp;n.</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="113"/>
        <source>Quality:</source>
        <translation>Qualità:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="126"/>
        <source>Attachment:</source>
        <translation>Allegato:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="139"/>
        <source>Title:</source>
        <translation>Titolo:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="152"/>
        <source>Username:</source>
        <translation>Nome utente:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="165"/>
        <source>Comment:</source>
        <translation>Commento:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="350"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="178"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="191"/>
        <source>Group:</source>
        <translation>Gruppo:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="210"/>
        <source>Password Repet.:</source>
        <translation>Ripetizione password:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="223"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="236"/>
        <source>Expires:</source>
        <translation>Scade:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="383"/>
        <source>Never</source>
        <translation>Mai</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="419"/>
        <source>%1 Bit</source>
        <translation>%1 Bit</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="462"/>
        <source>Icon:</source>
        <translation>Icona:</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Proprietà gruppo</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>Icona:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>Titolo:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>Voce scaduta</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>Doppio click sulla voce per aprirla.</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>Gruppo</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>Nome utente</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>Scaduta</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>Voci scadute nel Database</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>File XML KeePassX</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>Files XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Tutti i files (*)</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>File di testo</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>Tutti i files (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>File di testo (*.txt)</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="obsolete">Importa file...</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>Esportazione fallita</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation>Esporta file...</translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>No error occurred.</source>
        <translation>Non si è verificato alcun errore.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An error occurred while reading from the file.</source>
        <translation>Si è verificato un errore durante la lettura del file.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>An error occurred while writing to the file.</source>
        <translation>Si è verificato un errore durante la scrittura del file.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>A fatal error occurred.</source>
        <translation>Si è verificato un errore fatale.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>An resource error occurred.</source>
        <translation>Si è verificato un errore di risorsa.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>The file could not be opened.</source>
        <translation>Impossibile aprire il file.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The operation was aborted.</source>
        <translation>L&apos;operazione è stata terminata.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>A timeout occurred.</source>
        <translation>Si è verificata una scadenza.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>An unspecified error occurred.</source>
        <translation>Si è verificato un errore non specificato.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be removed.</source>
        <translation>Impossibile rimuovere il file.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be renamed.</source>
        <translation>Impossibile rinominare il file.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The position in the file could not be changed.</source>
        <translation>La posizione nel file non può essere modificata.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="74"/>
        <source>The file could not be resized.</source>
        <translation>Impossibile ridimensionare il file.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="75"/>
        <source>The file could not be accessed.</source>
        <translation>Impossibile accedere al file.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="76"/>
        <source>The file could not be copied.</source>
        <translation>Impossibile copiare il file.</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Generatore di password</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="303"/>
        <source>Options</source>
        <translation>Opzioni</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="56"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>Utilizza i se&amp;guenti gruppi di carattere:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="59"/>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+G</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="89"/>
        <source>&amp;Lower Letters</source>
        <translation>Lettere M&amp;inuscole</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="95"/>
        <source>Alt+L</source>
        <translation type="obsolete">Alt+I</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation type="obsolete">So&amp;ttolineato</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="118"/>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="106"/>
        <source>&amp;Numbers</source>
        <translation>&amp;Numeri</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation type="obsolete">&amp;Spazi vuoti</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="131"/>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="123"/>
        <source>&amp;Upper Letters</source>
        <translation>Lettere M&amp;aiuscole</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="141"/>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation type="obsolete">Meno</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="140"/>
        <source>&amp;Special Characters</source>
        <translation>Caratteri &amp;Speciali</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>Use &amp;only following characters:</source>
        <translation>Utilizza s&amp;olo i seguenti caratteri:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="172"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="317"/>
        <source>Length:</source>
        <translation>Lunghezza:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="353"/>
        <source>Quality:</source>
        <translation>Qualità:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="380"/>
        <source>Enable entropy collection</source>
        <translation>Abilita accumulazione entropia</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="404"/>
        <source>Alt+M</source>
        <translation type="obsolete">Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="406"/>
        <source>Collect only once per session</source>
        <translation>Imposta solo una volta per sessione</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="436"/>
        <source>New Password:</source>
        <translation>Nuova password:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="459"/>
        <source>Generate</source>
        <translation>Genera</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Random</source>
        <translation>Casuale</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>&amp;Underline</source>
        <translation>&amp;Sottolinea</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="116"/>
        <source>&amp;White Spaces</source>
        <translation>S&amp;pazi bianchi</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="133"/>
        <source>&amp;Minus</source>
        <translation>&amp;Meno</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="193"/>
        <source>Exclude look-alike characters</source>
        <translation>Escludi caratteri identici</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="200"/>
        <source>Ensure that password contains characters from every group</source>
        <translation>Assicurati che la password contenga caratteri di ogni gruppo</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="216"/>
        <source>Pronounceable</source>
        <translation>Pronunciabile</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="240"/>
        <source>Lower Letters</source>
        <translation>Lettere minuscole</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="247"/>
        <source>Upper Letters</source>
        <translation>Lettere maiuscole</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="254"/>
        <source>Numbers</source>
        <translation>Numeri</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="261"/>
        <source>Special Characters</source>
        <translation>Caratteri speciali</translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>Files XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Tutti i files (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>Importazione fallita</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Dati XML non validi (vedi stdout per dettagli).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>File XML non valido.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>Il documento non contiene dati.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>File XML KeePass (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>Tutti i files (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>Importazione fallita</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>Errore analisi XML nella riga %1 colonna %2:
%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>Errore analisi: Il file non è un valido file XML di KeePassX.</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>Files PwManager (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation>Tutti i files (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation>Importazione fallita</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation>Il fIle è vuoto.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation>Il file non è un valido file PwManager.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation>Versione file non supportata.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation>Algoritmo di hash non supportato.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>Algoritmo di cifratura non supportato.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation>I file compressi non sono ancora supportati.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation>Password errata.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation>Il file è danneggiato (prova di hash fallita).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Dati XML non validi (vedi stdout per dettagli).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>Importa file...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>Importazione fallita</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="524"/>
        <source>Could not open file.</source>
        <translation>Impossibile aprire il file.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="540"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>Dimensione file inattesa (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="557"/>
        <source>Wrong Signature</source>
        <translation>Firma errata</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="562"/>
        <source>Unsupported File Version.</source>
        <translation>Versione file non supportata.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="571"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>Algoritmo di cifratura sconosciuto.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="606"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>Decifratura fallita.
La chiave è errata oppure il file è danneggiato.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="632"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>Prova di hash fallita.
La chiave è errata oppure il file è danneggiato.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="711"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>Errore inatteso: La posizione è fuori campo.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="717"/>
        <source>Invalid group tree.</source>
        <translation>Albero del gruppo non valido.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="933"/>
        <source>Key file is empty.</source>
        <translation>Il fIle chiave è vuoto.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1313"/>
        <source>The database must contain at least one group.</source>
        <translation>Il database deve contenere almeno un gruppo.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1325"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Impossibile aprire il file per la scrittura.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="598"/>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation>Impossibile inizializzare l&apos;algoritmo twofish.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1333"/>
        <source>The database has been opened read-only.</source>
        <translation>Il database è stato aperto in sola lettura.</translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1177"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1185"/>
        <source>KiB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1191"/>
        <source>MiB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1196"/>
        <source>GiB</source>
        <translation>GB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="150"/>
        <source>Delete?</source>
        <translation>Eliminare?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="262"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="262"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>Deve esistere almeno un gruppo prima di aggiungere una voce.</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="262"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="481"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="483"/>
        <source>Username</source>
        <translation>Nome utente</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="485"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="487"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="489"/>
        <source>Comments</source>
        <translation>Commenti</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="491"/>
        <source>Expires</source>
        <translation>Scade</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="493"/>
        <source>Creation</source>
        <translation>Creazione</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="495"/>
        <source>Last Change</source>
        <translation>Ultima modifica</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="497"/>
        <source>Last Access</source>
        <translation>Ultimo accesso</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="499"/>
        <source>Attachment</source>
        <translation>Allegato</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="501"/>
        <source>Group</source>
        <translation>Gruppo</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation>Sei sicuro di voler eliminare questa voce?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="149"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation>Sei sicuro di voler eliminare queste %1 voci?</translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="58"/>
        <source>Search Results</source>
        <translation>Risultati della ricerca</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="89"/>
        <source>Delete?</source>
        <translation>Eliminare?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="86"/>
        <source>Are you sure you want to delete this group, all it&apos;s child groups and all their entries?</source>
        <translation type="obsolete">Sei sicuro di voler eliminare questo gruppo, tutti i suoi sottogruppi e le loro voci?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="90"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation>Sei sicuro di voler eliminare questo gruppo, tutti i suoi sottogruppi e le loro voci?</translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="1421"/>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1324"/>
        <source>Locked</source>
        <translation>Bloccato</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1339"/>
        <source>Unlocked</source>
        <translation>Sbloccato</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+E</source>
        <translation>Ctrl+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="370"/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="375"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="378"/>
        <source>Shift+Ctrl+S</source>
        <translation>Shift+Ctrl+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="379"/>
        <source>Shift+Ctrl+F</source>
        <translation>Shift+Ctrl+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="499"/>
        <source>The database file does not exist.</source>
        <translation>Il file di database non esiste.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1424"/>
        <source>Loading Database...</source>
        <translation>Caricamento Database...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1427"/>
        <source>Loading Failed</source>
        <translation>Caricamento fallito</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="477"/>
        <source>Unknown error while loading database.</source>
        <translation>Errore sconosciuto durante il caricamento del database.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="479"/>
        <source>The following error occured while opening the database:</source>
        <translation>Si è verificato il seguente errore durante l&apos;apertura del database:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="516"/>
        <source>Save modified file?</source>
        <translation>Salvare il file modificato?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation type="obsolete">Il file attuale è stato modificato. Vuoi
salvare le modifiche?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1112"/>
        <source>new</source>
        <translation>nuovo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="603"/>
        <source>Open Database...</source>
        <translation>Apri Database...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="909"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>Database KeePass (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="909"/>
        <source>All Files (*)</source>
        <translation>Tutti i files (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="730"/>
        <source>Expired</source>
        <translation>Scaduta</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="744"/>
        <source>1 Month</source>
        <translation>1 Mese</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="746"/>
        <source>%1 Months</source>
        <translation>%1 Mesi</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="752"/>
        <source>1 Year</source>
        <translation>1 Anno</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="754"/>
        <source>%1 Years</source>
        <translation>%1 Anni</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="760"/>
        <source>1 Day</source>
        <translation>1 Giorno</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="762"/>
        <source>%1 Days</source>
        <translation>%1 Giorni</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="766"/>
        <source>less than 1 day</source>
        <translation>meno di 1 giorno</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="856"/>
        <source>Clone Entry</source>
        <translation>Duplica voce</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="858"/>
        <source>Delete Entry</source>
        <translation>Elimina voce</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="871"/>
        <source>Clone Entries</source>
        <translation>Duplica voci</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="873"/>
        <source>Delete Entries</source>
        <translation>Elimina voci</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="913"/>
        <source>File could not be saved.</source>
        <translation>Impossibile salvare il file.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="909"/>
        <source>Save Database...</source>
        <translation>Salva Database...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1325"/>
        <source>Un&amp;lock Workspace</source>
        <translation>Sb&amp;locca l&apos;area di lavoro</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1340"/>
        <source>&amp;Lock Workspace</source>
        <translation>&amp;Blocca l&apos;area di lavoro</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1106"/>
        <source>Show &amp;Toolbar</source>
        <translation>Mostra barra degli &amp;strumenti</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="373"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="407"/>
        <source>Database locked</source>
        <translation>Database bloccato</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="411"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation>Il database che stai tentando di aprire è bloccato.
Questo significa che qualcun altro ha aperto il file o che KeePassX si è bloccato l&apos;ultima volta che si è aperto il database.

Vuoi comunque aprirlo?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation>Il file attuale è stato modificato. Vuoi salvare le modifiche?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation>Impossibile rimuovere il file di blocco del database.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="414"/>
        <source>Open read-only</source>
        <translation>Apri in sola lettura</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1430"/>
        <source>Couldn&apos;t create lock file. Opening the database read-only.</source>
        <translation>Impossibile creare il file di blocco. Apertura del database in sola lettura.</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="140"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="147"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Impossibile trovare il file &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="55"/>
        <source>Groups</source>
        <translation>Gruppi</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;Help</source>
        <translation>&amp;Guida</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="136"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="140"/>
        <source>&amp;Export to...</source>
        <translation>Esp&amp;orta in...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="145"/>
        <source>&amp;Import from...</source>
        <translation>Im&amp;porta da...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Edit</source>
        <translation type="obsolete">&amp;Modifica</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="191"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="230"/>
        <source>E&amp;xtras</source>
        <translation>&amp;Utilità</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="262"/>
        <source>&amp;Open Database...</source>
        <translation>&amp;Apri Database...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="267"/>
        <source>&amp;Close Database</source>
        <translation>&amp;Chiudi Database</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="272"/>
        <source>&amp;Save Database</source>
        <translation>&amp;Salva Database</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="277"/>
        <source>Save Database &amp;As...</source>
        <translation>Save Database &amp;come...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="282"/>
        <source>&amp;Database Settings...</source>
        <translation>Impostazioni &amp;Database...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="287"/>
        <source>Change &amp;Master Key...</source>
        <translation>Cambia la chiave &amp;principale...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="292"/>
        <source>&amp;Lock Workspace</source>
        <translation>&amp;Blocca l&apos;area di lavoro</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="474"/>
        <source>&amp;Settings...</source>
        <translation>&amp;Impostazioni...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="482"/>
        <source>&amp;About...</source>
        <translation>&amp;Informazioni su...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="498"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>Manuale &amp;KeePassX...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="506"/>
        <source>Hide</source>
        <translation>Nascondi</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="534"/>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation type="obsolete">Database KeePass standard a singolo utente (*.kdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="539"/>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation type="obsolete">Database KeePassX avanzato (*.kxdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="566"/>
        <source>Recycle Bin...</source>
        <translation>Cestino...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="150"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Segnalibri</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="195"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation>Dimensione &amp;icona barra degli strumenti</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="203"/>
        <source>&amp;Columns</source>
        <translation>&amp;Colonne</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="257"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation>Gestisci i Se&amp;gnalibri...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="297"/>
        <source>&amp;Quit</source>
        <translation>E&amp;sci</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Add New Group...</source>
        <translation type="obsolete">&amp;Aggiungi nuovo gruppo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="310"/>
        <source>&amp;Edit Group...</source>
        <translation>Modi&amp;fica gruppo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="315"/>
        <source>&amp;Delete Group</source>
        <translation>Elimina &amp;gruppo</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="320"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation>Copia la password negli &amp;Appunti</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="325"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation>Copia il nome &amp;utente negli Appunti</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="330"/>
        <source>&amp;Open URL</source>
        <translation>&amp;Apri URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="335"/>
        <source>&amp;Save Attachment As...</source>
        <translation>&amp;Salva allegato come...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="340"/>
        <source>Add &amp;New Entry...</source>
        <translation>Aggiungi &amp;nuova voce...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="345"/>
        <source>&amp;View/Edit Entry...</source>
        <translation>&amp;Visualizza/Modifica voce...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="350"/>
        <source>De&amp;lete Entry</source>
        <translation>E&amp;limina voce</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="355"/>
        <source>&amp;Clone Entry</source>
        <translation>&amp;Duplica voce</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="360"/>
        <source>Search &amp;in Database...</source>
        <translation>Cerca &amp;nel Database...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="365"/>
        <source>Search in this &amp;Group...</source>
        <translation>Cerca in questo &amp;gruppo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="373"/>
        <source>Show &amp;Entry Details</source>
        <translation>Mostra d&amp;ettagli voce</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="381"/>
        <source>Hide &amp;Usernames</source>
        <translation>Nascondi nomi &amp;utente</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="389"/>
        <source>Hide &amp;Passwords</source>
        <translation>Nascondi &amp;password</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="397"/>
        <source>&amp;Title</source>
        <translation>&amp;Titolo</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="405"/>
        <source>User&amp;name</source>
        <translation>&amp;Nome utente</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="413"/>
        <source>&amp;URL</source>
        <translation>&amp;URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="421"/>
        <source>&amp;Password</source>
        <translation>&amp;Password</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="429"/>
        <source>&amp;Comment</source>
        <translation>&amp;Commento</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="437"/>
        <source>E&amp;xpires</source>
        <translation>&amp;Scade</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="445"/>
        <source>C&amp;reation</source>
        <translation>C&amp;reazione</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="453"/>
        <source>&amp;Last Change</source>
        <translation>U&amp;ltima modifica</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="461"/>
        <source>Last &amp;Access</source>
        <translation>Ultimo &amp;accesso</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="469"/>
        <source>A&amp;ttachment</source>
        <translation>Allega&amp;to</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="493"/>
        <source>Show &amp;Statusbar</source>
        <translation>Mostra la barra di &amp;stato</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="511"/>
        <source>&amp;Perform AutoType</source>
        <translation>Esegui Auto &amp;Digitazione</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="519"/>
        <source>&amp;16x16</source>
        <translation>&amp;16x16</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="527"/>
        <source>&amp;22x22</source>
        <translation>&amp;22x22</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="535"/>
        <source>2&amp;8x28</source>
        <translation>2&amp;8x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="540"/>
        <source>&amp;New Database...</source>
        <translation>&amp;Nuovo Database...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="545"/>
        <source>&amp;Password Generator...</source>
        <translation>Generatore di &amp;password...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="556"/>
        <source>&amp;Group (search results only)</source>
        <translation>&amp;Gruppo (ricerca solo di risultati)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="561"/>
        <source>Show &amp;Expired Entries...</source>
        <translation>Mostra voci scadut&amp;e...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="574"/>
        <source>&amp;Add Bookmark...</source>
        <translation>&amp;Aggiungi ai Segnalibri...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="579"/>
        <source>Bookmark &amp;this Database...</source>
        <translation>Imposta il Database come &amp;Segnalibro...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="305"/>
        <source>&amp;Add New Subgroup...</source>
        <translation>&amp;Aggiungi nuovo sottogruppo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="584"/>
        <source>Copy URL to Clipboard</source>
        <translation>Copia URL negli Appunti</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="589"/>
        <source>Add New Group...</source>
        <translation>Aggiungi nuovo gruppo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Entries</source>
        <translation>&amp;Voci</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="240"/>
        <source>&amp;Groups</source>
        <translation>&amp;Gruppi</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="594"/>
        <source>Sort groups</source>
        <translation>Ordina gruppi</translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>Gestisci Segnalibri</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="32"/>
        <source>Enter Master Key</source>
        <translation>Inserisci la chiave principale</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="35"/>
        <source>Set Master Key</source>
        <translation>Imposta la chiave principale</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="38"/>
        <source>Change Master Key</source>
        <translation>Cambia chiave principale</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="42"/>
        <source>Database Key</source>
        <translation>Chiave Database</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="132"/>
        <source>Last File</source>
        <translation>Ultimo file</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="178"/>
        <source>Select a Key File</source>
        <translation>Seleziona un file chiave</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>All Files (*)</source>
        <translation>Tutti i files (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>Key Files (*.key)</source>
        <translation>File chiave (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="206"/>
        <source>Please enter a Password or select a key file.</source>
        <translation>Inserisci una password o seleziona un file chiave.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="211"/>
        <source>Please enter a Password.</source>
        <translation>Insersci una password.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="216"/>
        <source>Please provide a key file.</source>
        <translation>Fornisci un file chiave.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="262"/>
        <source>%1:
No such file or directory.</source>
        <translation>%1:
Nessun file o directory.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="228"/>
        <source>The selected key file or directory is not readable.</source>
        <translation>Il file chiave o la directory selezionata non è leggibile.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="247"/>
        <source>The given directory does not contain any key files.</source>
        <translation>La directory selezionata non contiene alcun file chiave.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="253"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation>La directory selezionata contiene più di un file chiave.
Specifica il file chiave direttamente.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="266"/>
        <source>%1:
File is not readable.</source>
        <translation>%1:
Il file non è leggibile.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="340"/>
        <source>Create Key File...</source>
        <translation>Crea un file chiave...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>Ultimo file</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Inserisci la password e/o seleziona il file chiave.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Chiave</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="191"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Sfoglia...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="194"/>
        <source>Alt+B</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="163"/>
        <source>Key File:</source>
        <translation>File chiave:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="200"/>
        <source>Generate Key File...</source>
        <translation>Genera file chiave...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="232"/>
        <source>Please repeat your password:</source>
        <translation>Ripeti la password:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="250"/>
        <source>Back</source>
        <translation>Indietro</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="277"/>
        <source>Passwords are not equal.</source>
        <translation>Le password non corrispondono.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="120"/>
        <source>Could not locate library file.</source>
        <translation type="obsolete">Impossibile trovare il file di libreria.</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Cerca...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="65"/>
        <source>Include:</source>
        <translation>Includi:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="74"/>
        <source>Pass&amp;words</source>
        <translation>Pass&amp;words</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="81"/>
        <source>A&amp;nhang</source>
        <translation>A&amp;nhang</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="88"/>
        <source>U&amp;RLs</source>
        <translation>U&amp;RL</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="95"/>
        <source>C&amp;omments</source>
        <translation>C&amp;ommenti</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="102"/>
        <source>&amp;Usernames</source>
        <translation>Nomi &amp;Utente</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="109"/>
        <source>&amp;Titles</source>
        <translation>&amp;Titoli</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="124"/>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;Case Sensitive</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="131"/>
        <source>Regular E&amp;xpression</source>
        <translation>&amp;Espressione regolare</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation type="obsolete">Alt+E</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="138"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Includi sottogruppi (ricorsivo)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="150"/>
        <source>Search For:</source>
        <translation>Cerca per:</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Selezione icona</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="51"/>
        <source>The integration plugins provide features like usage of the native file dialogs and message boxes of the particular desktop environments.</source>
        <translation type="obsolete">L&apos;integrazione dei plugins fornisce caratteristiche come l&apos;uso di file di finestre di dialogo nativi e caselle messaggi degli ambienti desktop particolari.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="54"/>
        <source>General</source>
        <translation type="obsolete">Generale</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="122"/>
        <source>Show system tray icon</source>
        <translation>Mostra icona nella barra di sistema</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="150"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>Riduci a icona nella barra di sistema anzichè nella barra delle applicazioni</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="180"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>Riduci alla barra di sistema al click del pulsante chiudi della finestra principale</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="191"/>
        <source>Remember last opened file</source>
        <translation>Ricorda l&apos;ultimo file aperto</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="194"/>
        <source>Alt+Ö</source>
        <translation type="obsolete">Alt+Ö</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="219"/>
        <source>Remember last key type and location</source>
        <translation>Ricorda l&apos;ultima chiave digitata e la posizione</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="249"/>
        <source>Start minimized</source>
        <translation>Avvia ridotto a icona</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="279"/>
        <source>Start locked</source>
        <translation>Avvia bloccato</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="295"/>
        <source>Save recent directories of file dialogs</source>
        <translation>Salva directory recenti dei file di dialoghi</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="318"/>
        <source>Clear History Now</source>
        <translation>Pulisci ora la cronologia </translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="340"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>Chiedi sempre prima di eliminare voci o gruppi</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation type="obsolete">Asp&amp;etto</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="474"/>
        <source>Banner Color</source>
        <translation>Colore banner</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="488"/>
        <source>Text Color:</source>
        <translation>Colore testo:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="617"/>
        <source>Change...</source>
        <translation>Cambia...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="542"/>
        <source>Color 2:</source>
        <translation>Colore 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="667"/>
        <source>C&amp;hange...</source>
        <translation>Ca&amp;mbia...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="679"/>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="680"/>
        <source>Color 1:</source>
        <translation>Colore 1:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="708"/>
        <source>Alternating Row Colors</source>
        <translation>Colore alternativo delle righe</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="720"/>
        <source>Customize Entry Detail View...</source>
        <translation>Personalizza la voce Visualizza dettagli...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="749"/>
        <source>Group tree at start-up:</source>
        <translation>Albero del gruppo all&apos;avvio:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="756"/>
        <source>Restore last state</source>
        <translation>Ripristina ultimo stato</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="763"/>
        <source>Expand all items</source>
        <translation>Espandi tutti i valori</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="770"/>
        <source>Do not expand any item</source>
        <translation>Non espandere alcun valore</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="94"/>
        <source>Security</source>
        <translation>Sicurezza</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="928"/>
        <source>Edit Entry Dialog</source>
        <translation>Modifica finestra di dialogo della voce</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="943"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="956"/>
        <source>Clear clipboard after:</source>
        <translation>Pulisci gli Appunti dopo:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="994"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>Blocca l&apos;area di lavoro quando riduci a icona la finestra principale</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="827"/>
        <source>Features</source>
        <translation type="obsolete">Caratteristiche</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1260"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>Qui puoi disabilitare alcune caratteristiche di KeePassX a seconda delle tue necessità in modo da mantenere snella l&apos;interfaccia utente.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1270"/>
        <source>Bookmarks</source>
        <translation>Segnalibri</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="864"/>
        <source>Desktop Integration</source>
        <translation type="obsolete">Integrazione con il Desktop</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1308"/>
        <source>Plug-Ins</source>
        <translation>Plug-Ins</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1317"/>
        <source>None</source>
        <translation>Nessuno</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1324"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Integrazione con il Desktop di Gnome (Gtk 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1331"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>Integrazione con il Desktop di KDE 4</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1346"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>Devi riavviare il programma per rendere effettivi i cambiamenti.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1366"/>
        <source>Configure...</source>
        <translation>Configura...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="99"/>
        <source>Advanced</source>
        <translation>Avanzate</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1126"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>Regola in modo fine l&apos;Auto Digitazione</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1135"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>Tempo tra l&apos;attivazione di un&apos;azione da parte dell&apos;utente di Auto Digitazione e la pressione della prima chiave simulata.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1184"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1148"/>
        <source>Pre-Gap:</source>
        <translation>Differenza precedente:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1171"/>
        <source>Key Stroke Delay:</source>
        <translation>Ritardo pressione tasti:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1181"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>Ritardo tra due pressioni dei tasti simulati. Aumentalo se l&apos;Auto Digitazione salta casualmente i caratteri.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1066"/>
        <source>Custom Browser Command</source>
        <translation>Personalizza il comando Sfoglia</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1078"/>
        <source>Browse</source>
        <translation>Sfoglia</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1090"/>
        <source>Media Root:</source>
        <translation>Radice dei media:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1100"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>La directory dove le periferiche di salvataggio come CD e chiavette USB vengono montate di solito.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1107"/>
        <source>Browse...</source>
        <translation>Sfoglia...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1116"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>Abilita questo se vuoi usare i segnalibri e l&apos;ultimo file aperto indipendentemente dai loro percorsi assoluti. Questo è utile specialmente quando si usa KeePassX in modo portatile e perciò i punti di mount cambiano nel file system.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1119"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>Salva i percorsi relativi (segnalibri e ultimo file)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1199"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>Collegamento Auto Digitazione globale:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="435"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation>Salva automaticamente il database all&apos;uscita e blocca l&apos;area di lavoro</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="919"/>
        <source>Show plain text passwords in:</source>
        <translation>Mostra password in chiaro in:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="935"/>
        <source>Database Key Dialog</source>
        <translation>Finestra chiave database</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1032"/>
        <source>seconds</source>
        <translation>secondi</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1003"/>
        <source>Lock database after inactivity of</source>
        <translation>Blocca il database dopo inattività di</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1228"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation>Utilizza i titoli della voce per far corrispondere la finestra di Auto Digitazione Globale</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="74"/>
        <source>General (1)</source>
        <translation>Generale (1)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="79"/>
        <source>General (2)</source>
        <translation>Generale (2)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="84"/>
        <source>Appearance</source>
        <translation>Aspetto</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="89"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="372"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation>Salva backup delle voci modificate nel gruppo &apos;Backup&apos;</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="400"/>
        <source>Delete backup entries older than:</source>
        <translation>Elimina le voci di backup più vecchie di:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="426"/>
        <source>days</source>
        <translation>giorni</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="442"/>
        <source>Automatically save database after every change</source>
        <translation>Salva automaticamente il database dopo ogni modifica</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="845"/>
        <source>Language:</source>
        <translation>Lingua:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="852"/>
        <source>Author:</source>
        <translation>Autore:</translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Inserisci la Password</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation>Auto Digitazione: seleziona la finestra da usare</translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation>Per specificare la finestra da usare, o selezioni una finestra attualmente aperta
dal menu a tendina, oppure inserisci il titolo della finestra manualmente:</translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../lib/tools.cpp" line="352"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation>Diego Pierotto</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation>ita.translations@tiscali.it</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="351"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation>Italiano (Italia)</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>Casella</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;L&apos;area di lavoro è bloccata.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>Sblocca</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>Chiudi Database</translation>
    </message>
</context>
</TS>
