<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="es_ES">
<defaultcodec></defaultcodec>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>La información sobre cómo traducir KeePassX se puede encontrar en:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>Equipo</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation>Desarrollador, Administrador del Proyecto</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation>Diseñador Web</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation>Desarrollador</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation>Agradecimientos</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation>Parches para un soporte en MacOS X mejorado</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation>Icono de la Aplicación Principal</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation>Varias correcciones y improvisaciones</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>El archivo &apos;%1&apos; no puede ser encontrado.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>Asegúrate de que el programa esté correctamente instalado.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation>Traducción Actual</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation>Spanish - Spain - Castellano - España</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation>Cross - http://genlinux.wordpress.com</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="50"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="74"/>
        <source>AppName</source>
        <translation>Nombre de Aplicación</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="92"/>
        <source>AppFunc</source>
        <translation>Función de Aplicación</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>keepassx@gmail.com</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="163"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="188"/>
        <source>Translation</source>
        <translation>Traducción</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="213"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyrigh (C) 2005 - 2009 KeePassX Team
KeePassX está distribuido con la licencia de
General Public License (GPL) version 2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>Añadir marcador</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>Fichero:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>Navega...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>Editar marcador</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>Bases de datos KeePass (*.kbd)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>Todos los ficheros (*)</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - Auto-Escribir</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>Haz click en una entrada para auto-escribirla.</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="90"/>
        <source>Auto-Type</source>
        <translation>Auto-Escribir</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael): 256 Bit (por defecto)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish: 256 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Warning</source>
        <translation>Avíso</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Por favor, determina el número de rondas de encriptación.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="70"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; no es un valor íntegro</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>El número de rondas de encriptación tiene que ser mayor de 0.</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation>Hoy</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation>1 Semana</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation>2 Semanas</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation>3 Semanas</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation>1 Mes</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation>3 Meses</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation>6 Meses</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation>1 Año</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation>Calendario...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation>Editar entrada</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>La contraseña y la contraseña repetida no son iguales
Por favor, comprueba que las hayas escrito correctamente</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="256"/>
        <source>[Untitled Entry]</source>
        <translation>[Entrada sin título]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="294"/>
        <source>Add Attachment...</source>
        <translation>Añadir Adjunto...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>Could not open file.</source>
        <translation>No se ha podido abrir el fichero.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="321"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>La entrada seleccionada no tiene adjuntos o está vacia.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="324"/>
        <source>Save Attachment...</source>
        <translation>Guardar adjunto....</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error while writing the file.</source>
        <translation>Error mientras se escribía en el fichero.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="351"/>
        <source>Delete Attachment?</source>
        <translation>Borrar adjunto?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="352"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Estás a punto de borrar el adjunto de esta entrada.
¿Estás seguro?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>No, Cancel</source>
        <translation>No, cancelar.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation>Nueva Entrada</translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="121"/>
        <source>Password Generator</source>
        <translation>Generador de Contraseñas</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="278"/>
        <source>%1 Bits</source>
        <translation>%1 Bits</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>Reemplazar...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>Añadir un icono personalizado</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>Seleccionar</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Add Icons...</source>
        <translation>Añadir iconos...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="94"/>
        <source>Images (%1)</source>
        <translation>Imágenes (%1)</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: El fichero no puede ser cargado.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="68"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>Ha ocurrido un error mientras se cargaban los iconos:</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>An error occured while loading the icon.</source>
        <translation>Ha ocurrido un error mientras se cargaba el icono.</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="350"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="355"/>
        <source>Select a directory...</source>
        <translation>Selecciona un directorio...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="362"/>
        <source>Select an executable...</source>
        <translation>Selecciona un ejecutable...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="424"/>
        <source>System Language</source>
        <translation>Idioma del sistema</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>Calendario</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>Generador de Números Aleatorios</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>Recolectando entropía...
Por favor, mueve el ratón y/o presiona teclas hasta que haya suficiente entropía para recoger de el generador de números aleatorios.</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Se terminó de generar!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>Colección de Entropía</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation>Editor de Texto Rico</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation>Cursiva</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation>Subrayado</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation>Aliniado a la Izquierda</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation>Centrado</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation>Aliniado a la Derecha</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation>Justificado</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation>Texto de Color</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation>Tamaño de la fuente</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation>Plantillas</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>Nombre del adjunto</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>Fecha de creación</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>Fecha del Último Acceso</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>Fecha de última modificación</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>Fecha de expiración</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>Tiempo hasta que expire</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>Parámetro de la base de datos</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>Encriptacion</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>Algoritmo:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>Rondas de Encriptación:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation>Calcular rondas para 1 segundo de parón en este equipo.</translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation>Nombre de Usuario</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation>Creación</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation>Último Acceso</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation>Última modificación</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation>Expiración</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Editar Entrada</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="52"/>
        <source>Ge&amp;n.</source>
        <translation>Ge&amp;n</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="113"/>
        <source>Quality:</source>
        <translation>Calidad:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="126"/>
        <source>Attachment:</source>
        <translation>Adjunto:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="139"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="152"/>
        <source>Username:</source>
        <translation>Nombre de Usuario:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="165"/>
        <source>Comment:</source>
        <translation>Comentario:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="350"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="178"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="191"/>
        <source>Group:</source>
        <translation>Grupo:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="210"/>
        <source>Password Repet.:</source>
        <translation>Repetir Contraseña:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="223"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="236"/>
        <source>Expires:</source>
        <translation>Expira:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="383"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="419"/>
        <source>%1 Bit</source>
        <translation>%1 Bit</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="462"/>
        <source>Icon:</source>
        <translation>Icóno:</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Propiedades del Grupo</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>Icóno.</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>Título:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>Entradas Expiradas</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>Doble click en una entrada para saltar a ella.</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>Expirado</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>Entradas Expiradas en la Base de Datos</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>Fichero KeePassX XML</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>Ficheros XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>Fichero de Texto</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>Todos los Ficheros (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>Ficheros de Texto (*.txt)</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>Exportar fallidos</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation>Exportar fichero...</translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>No error occurred.</source>
        <translation>No ha ocurrido ningún error.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An error occurred while reading from the file.</source>
        <translation>Ha ocurrido un error mientras se leía desde el fichero.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>An error occurred while writing to the file.</source>
        <translation>Ha ocurrido un error mientras se escribía en el fichero.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>A fatal error occurred.</source>
        <translation>Ha ocurrido un error fatal.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>An resource error occurred.</source>
        <translation>Ha ocurrido un error desde los recursos.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>The file could not be opened.</source>
        <translation>El fichero no ha podido ser abierto.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The operation was aborted.</source>
        <translation>La operación ha sido abortada.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>A timeout occurred.</source>
        <translation>Se ha alcanzado el tiempo máximo de espera.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>An unspecified error occurred.</source>
        <translation>Ha ocurrido un error sin especificar.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be removed.</source>
        <translation>El fichero no ha podido ser borrado.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be renamed.</source>
        <translation>El fichero no ha podido ser renombrado.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The position in the file could not be changed.</source>
        <translation>La posición en el fichero no ha podido ser cambiada.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="74"/>
        <source>The file could not be resized.</source>
        <translation>No se ha podido modificar el tamaño del fichero.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="75"/>
        <source>The file could not be accessed.</source>
        <translation>No se ha podido acceder al fichero.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="76"/>
        <source>The file could not be copied.</source>
        <translation>El fichero no ha podido ser copiado.</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Generador de Contraseñas</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="324"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="56"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation>Usar los siguientes grupos de carácteres:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="59"/>
        <source>Alt+W</source>
        <translation>ALT+W</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="92"/>
        <source>&amp;Lower Letters</source>
        <translation>&amp;Letras minúscula</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="95"/>
        <source>Alt+L</source>
        <translation>ALT+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="118"/>
        <source>Alt+N</source>
        <translation>ALT+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="115"/>
        <source>&amp;Numbers</source>
        <translation>&amp;Números</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="131"/>
        <source>Alt+S</source>
        <translation>ALT+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="138"/>
        <source>&amp;Upper Letters</source>
        <translation>&amp;Letras mayúsculas</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="141"/>
        <source>Alt+U</source>
        <translation>ALT+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="158"/>
        <source>&amp;Special Characters</source>
        <translation>&amp;Carácteres Especiales</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="169"/>
        <source>Use &amp;only following characters:</source>
        <translation>Usar &amp;sólo los siguientes carácteres:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="172"/>
        <source>Alt+O</source>
        <translation>ALT+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="338"/>
        <source>Length:</source>
        <translation>Longitud:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="374"/>
        <source>Quality:</source>
        <translation>Calidad:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="401"/>
        <source>Enable entropy collection</source>
        <translation>Activar recolección entrópica</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="404"/>
        <source>Alt+M</source>
        <translation>ALT+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="430"/>
        <source>Collect only once per session</source>
        <translation>Recolectar una vez por sesión</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="460"/>
        <source>New Password:</source>
        <translation>Nueva Contraseña:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="483"/>
        <source>Generate</source>
        <translation>Generar</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Random</source>
        <translation>Al azar</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="105"/>
        <source>&amp;Underline</source>
        <translation>&amp;Subrayado</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="128"/>
        <source>&amp;White Spaces</source>
        <translation>&amp;Espacios en Blanco</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>&amp;Minus</source>
        <translation>&amp;Menos</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="214"/>
        <source>Exclude look-alike characters</source>
        <translation>Excluir carácteres que se parezcan</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="221"/>
        <source>Ensure that password contains characters from every group</source>
        <translation>Asegurarse que la contraseña contenga carácteres de todos los grupos</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="237"/>
        <source>Pronounceable</source>
        <translation>Pronunciable</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="261"/>
        <source>Lower Letters</source>
        <translation>Letras minúsculas</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="268"/>
        <source>Upper Letters</source>
        <translation>Letras mayúsculas</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="275"/>
        <source>Numbers</source>
        <translation>Números</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="282"/>
        <source>Special Characters</source>
        <translation>Carácteres Especiales</translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>Ficheros XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>Todos los ficheros (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>Fallo al Importar</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Datos XML Inválidos (Véase el stdout para detalles).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>Archivo XML Inválido.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>El documento no contiene datos.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>Archivos de KeePass XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>Todos los ficheros (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>Fallo al importar</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>Error en el interprete de XML en la línea %1 columna %2: 
%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>Error en al interpretar: El fichero no es un fichero KeePassX XML válido.</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>Archivos de PwManager (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation>Fallo al importar</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation>El archivo está vacio.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation>El archivo no es un archivo de PwManager válido.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation>La versión del archivo no tiene soporte.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation>Algoritmo de hash sin soporte.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>Algoritmo de encriptación sin soporte.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation>La compresión de archivos no está soportada todavía.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation>Contraseña Incorrecta.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation>El archivo está dañado (fallo al testear hash).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>Datos XML Inválidos (Véase stdout para detalles).</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>Importar un archivo...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>Fallo al importar</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="524"/>
        <source>Could not open file.</source>
        <translation>No se ha podido abrir el archivo.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="540"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>Tamaño de archivo inesperado (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="557"/>
        <source>Wrong Signature</source>
        <translation>Fallo en la firma</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="562"/>
        <source>Unsupported File Version.</source>
        <translation>Versión del archivo sin soporte.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="571"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>Algoritmo de encriptación desconocido.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="606"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>Desencriptación fallida.
La clave es erronea ó el archivo está dañado.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="632"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>Fallo en la prueba de hash.
La clave es erronea ó el fichero está dañado.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="711"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>Error inesperado: El OffSet está fuera de rango.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="717"/>
        <source>Invalid group tree.</source>
        <translation>Arbol de grupo inválido.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="933"/>
        <source>Key file is empty.</source>
        <translation>El archivo de claves está vacio.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1313"/>
        <source>The database must contain at least one group.</source>
        <translation>La base de datos debe contener al menos un grupo.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="598"/>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation>Imposible iniciar el algoritmo de twofish.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1333"/>
        <source>The database has been opened read-only.</source>
        <translation>La base de datos ha sido abierta en modo sólo lectura.</translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1177"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1185"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1191"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1196"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="150"/>
        <source>Delete?</source>
        <translation>Borrar?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>Al menos un grupo debe existir antes de añadir una nueva entrada.</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="474"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="476"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="478"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="480"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="482"/>
        <source>Comments</source>
        <translation>Comentarios</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="484"/>
        <source>Expires</source>
        <translation>Expira</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="486"/>
        <source>Creation</source>
        <translation>Creación</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="488"/>
        <source>Last Change</source>
        <translation>Última modificación</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="490"/>
        <source>Last Access</source>
        <translation>Último Acceso</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="492"/>
        <source>Attachment</source>
        <translation>Adjunto</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="494"/>
        <source>Group</source>
        <translation>Grupo</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation>¿Estás seguro de que deseas borrar esta entrada?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="149"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation>¿Estás seguro de que quieres borrar estas %1 entradas?</translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="58"/>
        <source>Search Results</source>
        <translation>Resultados de búsqueda</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="89"/>
        <source>Delete?</source>
        <translation>¿Borrar?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="90"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation>¿Estás seguro de que quieres borrar este grupo y todos sus sub-grupos y todas las entradas?</translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="1418"/>
        <source>Ready</source>
        <translation>Lísto</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1321"/>
        <source>Locked</source>
        <translation>Bloqueado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1336"/>
        <source>Unlocked</source>
        <translation>Desbloqueado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+O</source>
        <translation>CTRL+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+S</source>
        <translation>CTRL+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+L</source>
        <translation>CTRL+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+Q</source>
        <translation>CTRL+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+G</source>
        <translation>CTRL+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+C</source>
        <translation>CTRL+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+B</source>
        <translation>CTRL+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+U</source>
        <translation>CTRL+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+Y</source>
        <translation>CTRL+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Ctrl+E</source>
        <translation>CTRL+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+D</source>
        <translation>CTRL+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Ctrl+K</source>
        <translation>CTRL+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="370"/>
        <source>Ctrl+F</source>
        <translation>CTRL+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="374"/>
        <source>Ctrl+V</source>
        <translation>CTRL+V</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+W</source>
        <translation>CTRL+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="377"/>
        <source>Shift+Ctrl+S</source>
        <translation>SHIFT+CTRL+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="378"/>
        <source>Shift+Ctrl+F</source>
        <translation>SHIFT+CTRL+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="534"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="498"/>
        <source>The database file does not exist.</source>
        <translation>El archivo de la base de datos no existe.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1421"/>
        <source>Loading Database...</source>
        <translation>Cargando la base de datos...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1424"/>
        <source>Loading Failed</source>
        <translation>Fallo al cargar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Unknown error while loading database.</source>
        <translation>Error desconocido mientras se cargaba la base de datos</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="478"/>
        <source>The following error occured while opening the database:</source>
        <translation>El siguiente error ha ocurrido mientras se abría la base de datos:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="515"/>
        <source>Save modified file?</source>
        <translation>¿Guardar el archivo modificado?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1109"/>
        <source>new</source>
        <translation>Nuevo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="600"/>
        <source>Open Database...</source>
        <translation>Abrir base de datos...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="907"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>Bases de Datos de KeePass (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="907"/>
        <source>All Files (*)</source>
        <translation>Todos los archivos (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="728"/>
        <source>Expired</source>
        <translation>Expirado</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="742"/>
        <source>1 Month</source>
        <translation>1 Mes</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="744"/>
        <source>%1 Months</source>
        <translation>%1 Meses</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="750"/>
        <source>1 Year</source>
        <translation>1 Año</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="752"/>
        <source>%1 Years</source>
        <translation>%1 Años</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="758"/>
        <source>1 Day</source>
        <translation>1 Día</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="760"/>
        <source>%1 Days</source>
        <translation>%1 Días</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="764"/>
        <source>less than 1 day</source>
        <translation>Menos de un día</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="854"/>
        <source>Clone Entry</source>
        <translation>Entrada Duplicada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="856"/>
        <source>Delete Entry</source>
        <translation>Borrar Entrada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="869"/>
        <source>Clone Entries</source>
        <translation>Entradas duplicadas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="871"/>
        <source>Delete Entries</source>
        <translation>Borrar Entradas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="911"/>
        <source>File could not be saved.</source>
        <translation>El archivo no ha podido ser guardado.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="907"/>
        <source>Save Database...</source>
        <translation>Guardar la base de datos...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1322"/>
        <source>Un&amp;lock Workspace</source>
        <translation>Desbloquear el área de trabajo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1337"/>
        <source>&amp;Lock Workspace</source>
        <translation>&amp;Bloquear el área de trabajo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="305"/>
        <source>Show &amp;Toolbar</source>
        <translation>Mostrar &amp;Barra de herramientas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="355"/>
        <source>Ctrl+N</source>
        <translation>CTRL+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Ctrl+P</source>
        <translation>CTRL+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Ctrl+X</source>
        <translation>CTRL+X</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+I</source>
        <translation>CTRL+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="406"/>
        <source>Database locked</source>
        <translation>Base de datos bloqueada</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="410"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation>La base de datos que estás intentando abrir está bloqueada.
Esto significa que alguien ha abierto este archivo ó que KeePassX falló la última vez que abrió la base de datos.
¿Quieres abrirlo aún así?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="516"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation>El actual archivo ha sido modificado.
¿Quieres guardar los cambios?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="534"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation>No se ha podido eliminar el bloqueo del archivo.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="413"/>
        <source>Open read-only</source>
        <translation>Abrir en modo Sólo Lectura</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1427"/>
        <source>Couldn&apos;t create lock file. Opening the database read-only.</source>
        <translation>No se ha podido crear el archivo de bloqueo. Abriendo la base de datos en modo sólo lectura.</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="140"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="147"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>El archivo &apos;%1&apos; no ha podido ser encontrado.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="55"/>
        <source>Groups</source>
        <translation>Grupos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="136"/>
        <source>&amp;File</source>
        <translation>&amp;Fichero</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="140"/>
        <source>&amp;Export to...</source>
        <translation>&amp;Exportar a...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="145"/>
        <source>&amp;Import from...</source>
        <translation>&amp;Importar desde...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="191"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="230"/>
        <source>E&amp;xtras</source>
        <translation>E&amp;xtras</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="262"/>
        <source>&amp;Open Database...</source>
        <translation>&amp;Abrir Base de Datos..</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="267"/>
        <source>&amp;Close Database</source>
        <translation>&amp;Cerrar Base de Datos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="272"/>
        <source>&amp;Save Database</source>
        <translation>&amp;Guardar Base de Datos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="277"/>
        <source>Save Database &amp;As...</source>
        <translation>Guardar Base de Datos c&amp;omo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="282"/>
        <source>&amp;Database Settings...</source>
        <translation>A&amp;justes de la Base de Datos...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="287"/>
        <source>Change &amp;Master Key...</source>
        <translation>Cambiar la contraseña &amp;Maestra...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="292"/>
        <source>&amp;Lock Workspace</source>
        <translation>&amp;Bloquear el área de trabajo</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="474"/>
        <source>&amp;Settings...</source>
        <translation>Aj&amp;ustes...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="482"/>
        <source>&amp;About...</source>
        <translation>Ac&amp;erca de...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="498"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>&amp;Libro de mano de KeePassX</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="506"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="566"/>
        <source>Recycle Bin...</source>
        <translation>Papelera...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="150"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Marcadores</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="195"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation>Tamaño de &amp;iconos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="203"/>
        <source>&amp;Columns</source>
        <translation>Co&amp;lumnas</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="257"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation>Maneja&amp;r Marcadores</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="297"/>
        <source>&amp;Quit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="310"/>
        <source>&amp;Edit Group...</source>
        <translation>&amp;Editar Grupo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="315"/>
        <source>&amp;Delete Group</source>
        <translation>&amp;Borrar Grupo</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="320"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation>Copiar contraseña al por&amp;tapapeles</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="325"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation>Copiar el &amp;Nombre de usuario al portapapeles</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="330"/>
        <source>&amp;Open URL</source>
        <translation>&amp;Abrir URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="335"/>
        <source>&amp;Save Attachment As...</source>
        <translation>&amp;Guardar Adjunto como...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="340"/>
        <source>Add &amp;New Entry...</source>
        <translation>Añadir &amp;Nueva Entrada</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="345"/>
        <source>&amp;View/Edit Entry...</source>
        <translation>&amp;Ver/Editar entrada...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="350"/>
        <source>De&amp;lete Entry</source>
        <translation>Bo&amp;rrar Entrada</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="355"/>
        <source>&amp;Clone Entry</source>
        <translation>Clo&amp;nar Entrada</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="360"/>
        <source>Search &amp;in Database...</source>
        <translation>Buscar &amp;en la Base de Datos...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="365"/>
        <source>Search in this &amp;Group...</source>
        <translation>Buscar en este &amp;Grupo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="373"/>
        <source>Show &amp;Entry Details</source>
        <translation>Mostrar Detalles de la &amp;Entrada</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="381"/>
        <source>Hide &amp;Usernames</source>
        <translation>Ocultar &amp;Nombres de Usuario</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="389"/>
        <source>Hide &amp;Passwords</source>
        <translation>Ocultar &amp;Contraseñas</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="397"/>
        <source>&amp;Title</source>
        <translation>&amp;Título</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="405"/>
        <source>User&amp;name</source>
        <translation>Nombre de &amp;Usuario</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="413"/>
        <source>&amp;URL</source>
        <translation>&amp;URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="421"/>
        <source>&amp;Password</source>
        <translation>&amp;Contraseña</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="429"/>
        <source>&amp;Comment</source>
        <translation>&amp;Comentario</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="437"/>
        <source>E&amp;xpires</source>
        <translation>&amp;Expira</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="445"/>
        <source>C&amp;reation</source>
        <translation>&amp;Creación</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="453"/>
        <source>&amp;Last Change</source>
        <translation>&amp;Último Cambio</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="461"/>
        <source>Last &amp;Access</source>
        <translation>Último &amp;Acceso</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="469"/>
        <source>A&amp;ttachment</source>
        <translation>A&amp;djunto</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="493"/>
        <source>Show &amp;Statusbar</source>
        <translation>Mostrar &amp;Barra de Estado</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="511"/>
        <source>&amp;Perform AutoType</source>
        <translation>&amp;Realizar Auto-Escritura</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="519"/>
        <source>&amp;16x16</source>
        <translation>&amp;16x16</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="527"/>
        <source>&amp;22x22</source>
        <translation>&amp;22x22</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="535"/>
        <source>2&amp;8x28</source>
        <translation>2&amp;8x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="540"/>
        <source>&amp;New Database...</source>
        <translation>&amp;Nueva Base de Datos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="545"/>
        <source>&amp;Password Generator...</source>
        <translation>&amp;Generador de Contraseñas...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="556"/>
        <source>&amp;Group (search results only)</source>
        <translation>&amp;Grupo (Resultados de la búsqueda sólamente)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="561"/>
        <source>Show &amp;Expired Entries...</source>
        <translation>Mostrar &amp;Entradas expiradas...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="574"/>
        <source>&amp;Add Bookmark...</source>
        <translation>&amp;Añadir Marcador...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="579"/>
        <source>Bookmark &amp;this Database...</source>
        <translation>Marcar &amp;ésta Base de Datos...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="305"/>
        <source>&amp;Add New Subgroup...</source>
        <translation>&amp;Añadir Nuevo Subgrupo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="584"/>
        <source>Copy URL to Clipboard</source>
        <translation>Copiar URL desde el portapapeles</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="589"/>
        <source>Add New Group...</source>
        <translation>Añadir un nuevo grupo...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Entries</source>
        <translation>&amp;Entradas</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="240"/>
        <source>&amp;Groups</source>
        <translation>&amp;Grupos</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="594"/>
        <source>Sort groups</source>
        <translation>Acortar grupos</translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>Gestionar Marcadores</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="32"/>
        <source>Enter Master Key</source>
        <translation>Escribe la Contraseña Maestra</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="35"/>
        <source>Set Master Key</source>
        <translation>Ajustar la contraseña Maestra</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="38"/>
        <source>Change Master Key</source>
        <translation>Cambiar la Contraseña Maestra</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="42"/>
        <source>Database Key</source>
        <translation>Contraseña de la Base de Datos</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="132"/>
        <source>Last File</source>
        <translation>Último archivo</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="178"/>
        <source>Select a Key File</source>
        <translation>Seleccionar un archivo de llave</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>All Files (*)</source>
        <translation>Todos los Archivos (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>Key Files (*.key)</source>
        <translation>Archivos Llave (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="206"/>
        <source>Please enter a Password or select a key file.</source>
        <translation>Por favor introduce una contraseña ó selecciona un archivo de llave.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="211"/>
        <source>Please enter a Password.</source>
        <translation>Por favor, escribe una contraseña.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="216"/>
        <source>Please provide a key file.</source>
        <translation>Por favor, selecciona una llave de archivo.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="262"/>
        <source>%1:
No such file or directory.</source>
        <translation>%1:
No existe el fichero ó el directorio.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="228"/>
        <source>The selected key file or directory is not readable.</source>
        <translation>El archivo llave seleccionado ó el directorio no son legibles.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="247"/>
        <source>The given directory does not contain any key files.</source>
        <translation>El directorio seleccionado no contiene ningún archivo llave.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="253"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation>El directorio seleccionado contiene más de un archivo llave.
Por favor, especifica cuál es el archivo directamente.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="266"/>
        <source>%1:
File is not readable.</source>
        <translation>%1:
El archivo no es legible.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="340"/>
        <source>Create Key File...</source>
        <translation>Crear un archivo llave...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>Último Archivo</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Escribe una contraseña y/o selecciona un archivo llave.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Clave</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="191"/>
        <source>&amp;Browse...</source>
        <translation>&amp;Navegar...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="194"/>
        <source>Alt+B</source>
        <translation>ALT+B</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="163"/>
        <source>Key File:</source>
        <translation>Archivo Llave:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="203"/>
        <source>Generate Key File...</source>
        <translation>Generar Archivo Llave...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="235"/>
        <source>Please repeat your password:</source>
        <translation>Por favor, repite tu contraseña:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="253"/>
        <source>Back</source>
        <translation>Atrás</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="280"/>
        <source>Passwords are not equal.</source>
        <translation>Las contraseñas no son iguales.</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>Buscar...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="68"/>
        <source>Include:</source>
        <translation>Incluir:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="80"/>
        <source>Pass&amp;words</source>
        <translation>Contras&amp;eñas</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation>ALT+W</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="90"/>
        <source>A&amp;nhang</source>
        <translation>Col&amp;gado</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation>ALT+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="100"/>
        <source>U&amp;RLs</source>
        <translation>U&amp;RLs</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation>ALT+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="110"/>
        <source>C&amp;omments</source>
        <translation>C&amp;omentarios</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation>ALT+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="120"/>
        <source>&amp;Usernames</source>
        <translation>&amp;Nombres de Usuario</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation>ALT+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="130"/>
        <source>&amp;Titles</source>
        <translation>&amp;Títulos</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation>ALT+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="148"/>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;Sensible a las mayúsculas y minúsculas</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation>ALT+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="158"/>
        <source>Regular E&amp;xpression</source>
        <translation>E&amp;xpresión Regular</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation>ALT+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="168"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Incluir sub-grupos (recursivo)</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="180"/>
        <source>Search For:</source>
        <translation>Buscar para:</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Selección de icono</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="122"/>
        <source>Show system tray icon</source>
        <translation>Mostrar icono de sistema</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="150"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>Minimizar al icono de sistema en vez de a la barra de tareas</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="180"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>Minimizar al icono de sistema cuando se hace click en el botón de cerrar de la ventana principal</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="191"/>
        <source>Remember last opened file</source>
        <translation>Recordar último archivo abierto</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="194"/>
        <source>Alt+Ö</source>
        <translation>ALT+Ö</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="222"/>
        <source>Remember last key type and location</source>
        <translation>Recordar último típo de clave y la localización</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="252"/>
        <source>Start minimized</source>
        <translation>Iniciar minimizado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="282"/>
        <source>Start locked</source>
        <translation>Iniciar bloqueado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="298"/>
        <source>Save recent directories of file dialogs</source>
        <translation>Guardar los directorios recientes de los diálogos de archivos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="321"/>
        <source>Clear History Now</source>
        <translation>Borrar Historial Ahora</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="343"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>Siempre preguntar antes de borrar entradas ó grupos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="477"/>
        <source>Banner Color</source>
        <translation>Color del banner</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="491"/>
        <source>Text Color:</source>
        <translation>Color del Texto:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="623"/>
        <source>Change...</source>
        <translation>Cambiar...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="548"/>
        <source>Color 2:</source>
        <translation>Color 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="676"/>
        <source>C&amp;hange...</source>
        <translation>C&amp;ambiar...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="679"/>
        <source>Alt+H</source>
        <translation>ALT+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="692"/>
        <source>Color 1:</source>
        <translation>Color 1:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="720"/>
        <source>Alternating Row Colors</source>
        <translation>Alternar la tabla de Colores</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="732"/>
        <source>Customize Entry Detail View...</source>
        <translation>Personalizar Visualización de detalles de la entrada</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="761"/>
        <source>Group tree at start-up:</source>
        <translation>Árbol de grupos al inicio:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="768"/>
        <source>Restore last state</source>
        <translation>Restaurar último estado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="775"/>
        <source>Expand all items</source>
        <translation>Expandir todos los objetos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="782"/>
        <source>Do not expand any item</source>
        <translation>No expandir ningún objeto</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="94"/>
        <source>Security</source>
        <translation>Seguridad</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="940"/>
        <source>Edit Entry Dialog</source>
        <translation>Diálogo de Edición de Entrada</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="943"/>
        <source>Alt+O</source>
        <translation>ALT+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="971"/>
        <source>Clear clipboard after:</source>
        <translation>Limpiar el portapapeles despues:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1009"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>Bloquear el área de trabajo cuando se minimize la ventana principal</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1275"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>Tú puedes desactivar varias funcionalidades de KeePassX aquí de acuerdo con tus necesidades para mantener tu apariencia mínima.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1285"/>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1323"/>
        <source>Plug-Ins</source>
        <translation>Añadidos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1332"/>
        <source>None</source>
        <translation>Ninguno</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1339"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Integración del Escritorio GNOME (GTK 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1346"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>Integración del Escritorio KDE 4</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1361"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>Necesitas reiniciar el programa para que los cambios surtan efecto.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1381"/>
        <source>Configure...</source>
        <translation>Configurar...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="99"/>
        <source>Advanced</source>
        <translation>Avanzado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1141"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>Mejoras Auto-Escritura</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1150"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>Tiempo desde la activación de Auto-Escritura por el usuario hasta la primera tecla simulatada.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1199"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1163"/>
        <source>Pre-Gap:</source>
        <translation>Pre-Vacio:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1186"/>
        <source>Key Stroke Delay:</source>
        <translation>Lapso Primera Pulsación:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1196"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>Lapso entre dos presiones simuladas. Aumentar en caso de que Auto-Escritura esté aleatoriamente saltándose carácteres.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1081"/>
        <source>Custom Browser Command</source>
        <translation>Comando de Navegador Personalizado</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1093"/>
        <source>Browse</source>
        <translation>Navegar</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1105"/>
        <source>Media Root:</source>
        <translation>Directorio Maestro:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1115"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>El directorio en el cuál los medios como los CD&apos;s y los lapiceros USB son montados.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1122"/>
        <source>Browse...</source>
        <translation>Navegar...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1131"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>Activa esta opción si quieres usar tus marcadones y el último archivo abierto independientemente de sus rutas completas. Esto es útil cuando estás usando KeePassX Portable y hay cambios en los puntos de montaje en el sistema de archivos.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1134"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>Guardar rutas relativas (Marcadores y último archivo)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1214"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>Acceso rápido Global a Auto-Escritura:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="438"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation>Automáticamente guardar la base de datos al salir y al bloquear el área de trabajo</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="931"/>
        <source>Show plain text passwords in:</source>
        <translation>Mostrar contraseñas en texto plano en:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="950"/>
        <source>Database Key Dialog</source>
        <translation>Diálogo de la clave de la Base de Datos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1047"/>
        <source>seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1018"/>
        <source>Lock database after inactivity of</source>
        <translation>Bloquear la base de datos en un periodo de inactividad de</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1243"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation>Usar títulos de entradas para que coincidan con la ventana para Auto-Escritura</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="74"/>
        <source>General (1)</source>
        <translation>General (1)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="79"/>
        <source>General (2)</source>
        <translation>General (2)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="84"/>
        <source>Appearance</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="89"/>
        <source>Language</source>
        <translation>Lenguaje</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="375"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation>Guardar copias de seguridad de las entradas modificadas en el grupo &apos;Copias de Seguridad&apos;</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="403"/>
        <source>Delete backup entries older than:</source>
        <translation>Borrar entradas de copias de seguridad más antigüas de:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="429"/>
        <source>days</source>
        <translation>Días</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="445"/>
        <source>Automatically save database after every change</source>
        <translation>Guardar automáticamente la base de datos despues de cualquier cambio</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="857"/>
        <source>Language:</source>
        <translation>Lenguaje:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="864"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation>CTRL</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation>SHIFT</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation>ALT</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation>ALTGR</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Introduce tu contraseña</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Contraseña:</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation>Auto-Escritura: Selecciona la Ventaa</translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation>Para especificar la ventana a elegir, selecciona una ya abierta expandiendo la lista ó introduce el título de la venta manualmente:</translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../lib/tools.cpp" line="352"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation>Cross</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation>http://genlinux.wordpress.com</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="351"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation>Spanish (Spain)</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>Desbloquear</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>Cerrar Base de Datos</translation>
    </message>
</context>
</TS>
