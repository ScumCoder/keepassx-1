<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="de_DE">
<context>
    <name>@default</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden. (FileError=%1)</translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Aktuelle Übersetzung: Deutsch&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Autor:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="44"/>
        <source>Team</source>
        <translation>Team</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="48"/>
        <source>Developer, Project Admin</source>
        <translation>Entwickler und Projektadministrator</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="52"/>
        <source>Web Designer</source>
        <translation>Web Designer</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="56"/>
        <source>Thanks To</source>
        <translation>Dank an</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="58"/>
        <source>Patches for better MacOS X support</source>
        <translation>Patches für bessere MacOS X Unterstützung</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="60"/>
        <source>Main Application Icon</source>
        <translation>Hauptanwendungssymbol</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="62"/>
        <source>Various fixes and improvements</source>
        <translation>vielzählige Verbesserungen und Erweiterungen</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="67"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="68"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation>Stellen Sie sicher, dass das Programm korrekt installiert wurde.</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">Folgender Fehler ist aufgetreten:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="42"/>
        <source>Information on how to translate KeePassX can be found under:</source>
        <translation>Informationen wie KeePassX übersetzt werden kann finden sich unter:</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="54"/>
        <source>Developer</source>
        <translation>Entwickler</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>Current Translation</source>
        <translation>aktuelle Übersetzung</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="32"/>
        <source>None</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation>nichts</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="34"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
</context>
<context>
    <name>AboutDlg</name>
    <message>
        <location filename="../forms/AboutDlg.ui" line="50"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Thanks To</source>
        <translation type="obsolete">Dank An</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="213"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="188"/>
        <source>Translation</source>
        <translation>Übersetzung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;/head&gt;&lt;body style=&quot; white-space: pre-wrap; font-family:Sans Serif; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-weight:600;&quot;&gt;KeePassX&lt;/span&gt; - Cross Platform Password Manager&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2006 Tarek Saidi 
KeePassX steht unter der
General Public License (GPL) Version 2.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>tarek.saidi@arcor.de</source>
        <translation type="obsolete">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://keepass.berlios.de/</source>
        <translation type="obsolete">http://keepass.berlios.de/</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="163"/>
        <source>Credits</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="127"/>
        <source>http://keepassx.sourceforge.net</source>
        <translation>http://keepassx.sourceforge.net</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="134"/>
        <source>keepassx@gmail.com</source>
        <translation>keepassx@gmail.com</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="74"/>
        <source>AppName</source>
        <translation>Anwendungsname</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="92"/>
        <source>AppFunc</source>
        <translation>Anwendungsfunktion</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="151"/>
        <source>Copyright (C) 2005 - 2008 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2008 KeePassX Team(sp)(new line)KeePassX is distributed under the terms of the(sp)(new line)General Public License (GPL) version 2.</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team 
KeePassX is distributed under the terms of the 
General Public License (GPL) version 2.</source>
        <translation type="obsolete">Copyright (C) 2005 - 2008 KeePassX Team(sp)(new line)KeePassX is distributed under the terms of the(sp)(new line)General Public License (GPL) version 2. {2005 ?} {2009 ?} {2.?}</translation>
    </message>
    <message>
        <location filename="../forms/AboutDlg.ui" line="141"/>
        <source>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</source>
        <translation>Copyright (C) 2005 - 2009 KeePassX Team
KeePassX is distributed under the terms of the
General Public License (GPL) version 2.</translation>
    </message>
</context>
<context>
    <name>AddBookmarkDlg</name>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="32"/>
        <source>Add Bookmark</source>
        <translation>Lesezeichen hinzufügen</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="56"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="66"/>
        <source>File:</source>
        <translation>Datei:</translation>
    </message>
    <message>
        <location filename="../forms/AddBookmarkDlg.ui" line="76"/>
        <source>Browse...</source>
        <translation>durchsuchen...</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="44"/>
        <source>Edit Bookmark</source>
        <translation>Lesezeichen bearbeiten</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass Datenbanken (*.kdb)</translation>
    </message>
    <message>
        <location filename="../dialogs/AddBookmarkDlg.cpp" line="57"/>
        <source>All Files (*)</source>
        <translation>alle Dateien (*)</translation>
    </message>
</context>
<context>
    <name>AutoType</name>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="68"/>
        <source>More than one &apos;Auto-Type:&apos; key sequence found.
Allowed is only one per entry.</source>
        <translation type="obsolete">Es wurde mehr als eine &apos;Auto-Type:&apos;-Zeichenkette gefunden.
Erlaubt ist nur eine pro Eintrag.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="obsolete">Syntaxfehler in Auto-Type-Zeichenkette bei Zeichen %1
Öffnende Klammer &apos;{&apos; ohne Gegenstück gefunden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="95"/>
        <source>Syntax Error in Auto-Type sequence near character %1
<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>Found &apos;{&apos; without closing &apos;}&apos;</source>
        <translation type="obsolete">Syntax Fehler in Auto-Type Sequenz nahe des Zeichens %1<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/>gefunden &apos;{&apos; ohne &apos;}&apos;<byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/><byte value="x9"/></translation>
    </message>
    <message>
        <location filename="../lib/AutoType_X11.cpp" line="133"/>
        <source>Auto-Type string contains invalid characters</source>
        <translation type="obsolete">Auto-Type Zeichenkette enthält ungültige Zeichen</translation>
    </message>
</context>
<context>
    <name>AutoTypeDlg</name>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="13"/>
        <source>KeePassX - Auto-Type</source>
        <translation>KeePassX - Auto-Type</translation>
    </message>
    <message>
        <location filename="../forms/AutoTypeDlg.ui" line="35"/>
        <source>Click on an entry to auto-type it.</source>
        <translation>Klicke auf einen Eintrag, um diesen mit Auto-Type auszuführen.</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="35"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="../dialogs/AutoTypeDlg.cpp" line="90"/>
        <source>Auto-Type</source>
        <translation>Auto-Type</translation>
    </message>
</context>
<context>
    <name>CAboutDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX %1</source>
        <translation type="obsolete">KeePassX %1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Make sure that the program is installed correctly.</source>
        <translation type="obsolete">Stellen Sie sicher, dass das Programm korrekt installiert wurde.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file &apos;%1&apos;</source>
        <translation type="obsolete">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The following error occured:
%1</source>
        <translation type="obsolete">Folgender Fehler ist aufgetreten:
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://keepass.berlios.de/index.php</source>
        <translation type="obsolete">http://keepass.berlios.de/index.php</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Current Translation: None&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>Please replace &apos;None&apos; with the language of your translation</comment>
        <translation type="obsolete">&lt;b&gt;Aktuelle Übersetzung: Deutsch&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;b&gt;Author:&lt;/b&gt; %1&lt;br&gt;</source>
        <translation type="obsolete">&lt;b&gt;Autor:&lt;/b&gt; %1&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSALTION_AUTHOR</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="obsolete">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/translation-howto.html</source>
        <translation type="obsolete">Informationen wie Sie eine Übersetztung für KeePassX  erstellen können finden Sie unter:
http://keepass.berlios.de/translation-howto.html</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Matthias Miller</source>
        <translation type="obsolete">Matthias Miller</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>http://www.outofhanwell.com/&lt;br&gt;Mac OS X Support</source>
        <translation type="obsolete">http://www.outofhanwell.com/&lt;br&gt;Mac OS X Unterstützung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="obsolete">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Information on how to translate KeePassX can be found under:
http://keepass.berlios.de/</source>
        <translation type="obsolete">Informationen über das Erstellen eine Übersetztung für KeePassX sind hier zu finden:
http://keepass.berlios.de</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Team</source>
        <translation type="obsolete">Team</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Developer, Project Admin</source>
        <translation type="obsolete">Entwickler und Projektadministrator</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Thanks To</source>
        <translation type="obsolete">Dank An</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Patches for better MacOS X support</source>
        <translation type="obsolete">Patches für bessere MacOS X Unterstützung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Main Application Icon</source>
        <translation type="obsolete">Anwendungssymbol</translation>
    </message>
</context>
<context>
    <name>CDbSettingsDlg</name>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="35"/>
        <source>AES(Rijndael):  256 Bit   (default)</source>
        <translation>AES(Rijndael):  256 Bit   (Standard)</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="36"/>
        <source>Twofish:  256 Bit</source>
        <translation>Twofish:  256 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="64"/>
        <source>Please determine the number of encryption rounds.</source>
        <translation>Bitte geben Sie die Zahl der Verschlüsselungsrunden an.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="70"/>
        <source>&apos;%1&apos; is not valid integer value.</source>
        <translation>&apos;%1&apos; ist kein gültiger Ganzzahlwert.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="74"/>
        <source>The number of encryption rounds have to be greater than 0.</source>
        <translation>Die Anzahl an Verschlüsselungsrunden muss mindestens Eins betragen.</translation>
    </message>
    <message>
        <location filename="../dialogs/DatabaseSettingsDlg.cpp" line="34"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>CEditEntryDlg</name>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Warning</source>
        <translation>Warnung</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="177"/>
        <source>Password and password repetition are not equal.
Please check your input.</source>
        <translation>Passwort und Passwortwiederholung stimmen nicht überein.
Bitte prüfen Sie Ihre Eingabe.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="324"/>
        <source>Save Attachment...</source>
        <translation>Anhang speichern...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Overwrite?</source>
        <translation type="obsolete">Überschreiben?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with this name already exists.
Do you want to replace it?</source>
        <translation type="obsolete">Eine Datei mit diesem Namen existiert bereits.
Möchten Sie diese ersetzen.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not remove old file.</source>
        <translation type="obsolete">Alte Datei konnte nicht entfernt werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not create new file.</source>
        <translation type="obsolete">Neue Datei konnte nicht angelegt werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="343"/>
        <source>Error while writing the file.</source>
        <translation>Beim Schreiben der Datei ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="351"/>
        <source>Delete Attachment?</source>
        <translation>Anhang löschen?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="352"/>
        <source>You are about to delete the attachment of this entry.
Are you sure?</source>
        <translation>Sie sind dabei den Dateianhang dieses Eintrages zu löschen.
Sind Sie sicher?</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="353"/>
        <source>No, Cancel</source>
        <translation>Nein, Abbrechen</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>Edit Entry</source>
        <translation>Eintrag bearbeiten</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="331"/>
        <source>Could not open file.</source>
        <translation>Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="105"/>
        <source>%1 Bit</source>
        <translation>%1 Bit</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="294"/>
        <source>Add Attachment...</source>
        <translation>Anhang hinzufügen...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="321"/>
        <source>The chosen entry has no attachment or it is empty.</source>
        <translation>Der ausgewählte Eintrag hat keinen Anhang oder ist leer.</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="63"/>
        <source>Today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="65"/>
        <source>1 Week</source>
        <translation>1 Woche</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="66"/>
        <source>2 Weeks</source>
        <translation>2 Wochen</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="67"/>
        <source>3 Weeks</source>
        <translation>3 Wochen</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="69"/>
        <source>1 Month</source>
        <translation>1  Monat</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="70"/>
        <source>3 Months</source>
        <translation>3 Monate</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="71"/>
        <source>6 Months</source>
        <translation>6 Monate</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="73"/>
        <source>1 Year</source>
        <translation>1 Jahr</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="75"/>
        <source>Calendar...</source>
        <translation>Kalender...</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="256"/>
        <source>[Untitled Entry]</source>
        <translation>[unbeschrifteter Eintrag]</translation>
    </message>
    <message>
        <location filename="../dialogs/EditEntryDlg.cpp" line="147"/>
        <source>New Entry</source>
        <translation>neuer Eintag</translation>
    </message>
</context>
<context>
    <name>CGenPwDialog</name>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="180"/>
        <source>Notice</source>
        <translation type="obsolete">Hinweis</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>You need to enter at least one character</source>
        <translation type="obsolete">Sie müssen mindestens ein Zeichen angeben.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="178"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open &apos;/dev/random&apos; or &apos;/dev/urandom&apos;.</source>
        <translation type="obsolete">&apos;/dev/random&apos; oder &apos;/dev/urandom&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="121"/>
        <source>Password Generator</source>
        <translation>Passwortgenerator</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordGenDlg.cpp" line="278"/>
        <source>%1 Bits</source>
        <translation>%1 Bits</translation>
    </message>
</context>
<context>
    <name>CPasswordDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a Password.</source>
        <translation type="obsolete">Bitte geben Sie ein Passwort ein.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please choose a key file.</source>
        <translation type="obsolete">Bitte wählen Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a Password or select a key file.</source>
        <translation type="obsolete">Bitte geben Sie ein Passwort ein oder wählen
Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Database Key</source>
        <translation type="obsolete">Datenbankschlüssel</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Select a Key File</source>
        <translation type="obsolete">Schlüsseldatei wählen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>*.key</source>
        <translation type="obsolete">*.key</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unexpected Error: File does not exist.</source>
        <translation type="obsolete">Unerwarteter Fehler: Datei existiert nicht.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The selected key file or directory does not exist.</source>
        <translation type="obsolete">Die gewählte Schlüsseldatei bzw. das gewählte Verzeichnis existiert nicht.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The given directory does not contain any key files.</source>
        <translation type="obsolete">Das angegebene Verzeichnis enthält keine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The given directory contains more then one key file.
Please specify the key file directly.</source>
        <translation type="obsolete">Das angegebene Verzeichnis enthält mehrere Schlüsseldateien.
Bitte geben Sie die gewünschte Schlüsseldatei direkt an.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The key file found in the given directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Die im angegebenen Verzeichnis gefundene Schlüsseldatei ist nicht lesbar.
Bitter prüfen Sie Ihre Zugriffsrechte.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file could not be found.</source>
        <translation type="obsolete">Schlüsseldatei konnte nicht gefunden werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Die angegebene Schlüsseldatei ist nicht lesbar.
Bitter prüfen Sie Ihre Zugriffsrechte.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password an password repetition are not equal.
Please check your input.</source>
        <translation type="obsolete">Passwort und Passwortwiederholung stimmen nicht überein.
Bitte prüfen Sie Ihre Eingabe.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a password or select a key file.</source>
        <translation type="obsolete">Bitte geben Sie ein Passwort ein oder wählen
Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with the name &apos;pwsafe.key&apos; already exisits in the given directory.
Do you want to replace it?</source>
        <translation type="obsolete">Im angegebenen Verzeichnis existiert bereits eine Datei mit dem Namen &apos;pwsafe.key&apos;.
Möchten Sie diese ersetzen? </translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The exisiting file is not writable.</source>
        <translation type="obsolete">Die exisitierende Datei ist nicht überschreibbar.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>A file with the this name already exisits.
Do you want to replace it?</source>
        <translation type="obsolete">Eine Datei mit diesem Namen existiert bereits.
Möchten Sie diese ersetzen.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The selected key file or directory is not readable.
Please check your permissions.</source>
        <translation type="obsolete">Das angegebene Verzeichnis oder die angegebene Schlüsseldatei ist nicht lesbar.
Bitter prüfen Sie Ihre Zugriffsrechte.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>CSearchDlg</name>
    <message>
        <location filename="" line="0"/>
        <source>Notice</source>
        <translation type="obsolete">Hinweis</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Please enter a search string.</source>
        <translation type="obsolete">Bitte geben Sie einen Suchbegriff ein.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search</source>
        <translation type="obsolete">Suchen</translation>
    </message>
</context>
<context>
    <name>CSelectIconDlg</name>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="30"/>
        <source>Delete</source>
        <translation>löschen</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="93"/>
        <source>Add Icons...</source>
        <translation>Symbol hinzufügen...</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="94"/>
        <source>Images (%1)</source>
        <translation>Symbole (%1)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1: File could not be loaded.
</source>
        <translation type="obsolete">%1: Datei konnte nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="29"/>
        <source>Replace...</source>
        <translation>ersetzen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>An error occured while loading the icon(s):
%1</source>
        <translation type="obsolete">Beim Laden der Symbole traten Fehler auf:
%1</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="98"/>
        <source>An error occured while loading the icon.</source>
        <translation>Beim Laden des Symbols ist ein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="31"/>
        <source>Add Custom Icon</source>
        <translation>Benutzersymbol hinzufügen</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="32"/>
        <source>Pick</source>
        <translation>wählen</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="62"/>
        <source>%1: File could not be loaded.</source>
        <translation>%1: Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/SelectIconDlg.cpp" line="68"/>
        <source>An error occured while loading the icon(s):</source>
        <translation>Ein Fehler ist während es öffnens der Symbole aufgetreten:</translation>
    </message>
</context>
<context>
    <name>CSettingsDlg</name>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="350"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="355"/>
        <source>Select a directory...</source>
        <translation>Verzeichnis wählen...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="362"/>
        <source>Select an executable...</source>
        <translation>ausführbare Datei auswählen...</translation>
    </message>
    <message>
        <location filename="../dialogs/SettingsDlg.cpp" line="424"/>
        <source>System Language</source>
        <translation type="unfinished">Systemsprache</translation>
    </message>
</context>
<context>
    <name>CalendarDialog</name>
    <message>
        <location filename="../forms/CalendarDlg.ui" line="13"/>
        <source>Calendar</source>
        <translation>Kalender</translation>
    </message>
</context>
<context>
    <name>CollectEntropyDlg</name>
    <message>
        <location filename="../dialogs/CollectEntropyDlg.cpp" line="30"/>
        <source>Entropy Collection</source>
        <translation>Entropiesammlung</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="25"/>
        <source>Random Number Generator</source>
        <translation>Zufallszahlengenerator</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="56"/>
        <source>Collecting entropy...
Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.</source>
        <translation>sammle Entropie...
Bitte bewegen Sie die Maus und/oder drücken Sie einige Tasten bis genügend Entropie gesammelt wurde, damit der Zufallszahlengenerator gefüllt werden kann.</translation>
    </message>
    <message>
        <location filename="../forms/CollectEntropyDlg.ui" line="172"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Random pool successfully reseeded!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;(new line)p, li { white-space: pre-wrap; }(new line)&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;&quot;&gt;(new line)&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600; color:#006400;&quot;&gt;Zufallsdatenfeld erfolgreich gefüllt!&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CustomizeDetailViewDialog</name>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="38"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="39"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="40"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="41"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="42"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="43"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="44"/>
        <source>Attachment Name</source>
        <translation>Anhangsname</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="45"/>
        <source>Creation Date</source>
        <translation>Erstellungsdatum</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="46"/>
        <source>Last Access Date</source>
        <translation>Datum des letzten Zugriffs</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="47"/>
        <source>Last Modification Date</source>
        <translation>Datum der letzten Änderung</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="48"/>
        <source>Expiration Date</source>
        <translation>Ablaufdatum</translation>
    </message>
    <message>
        <location filename="../dialogs/CustomizeDetailViewDlg.cpp" line="49"/>
        <source>Time till Expiration</source>
        <translation>Zeit bis zum Ablauf</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="13"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="34"/>
        <source>Rich Text Editor</source>
        <translation>Rich Text Editor</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="48"/>
        <source>Bold</source>
        <translation>Fett</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="135"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="67"/>
        <source>Italic</source>
        <translation>kursiv</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="70"/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="80"/>
        <source>Underlined</source>
        <translation>unterstrichen</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="83"/>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="93"/>
        <source>Left-Aligned</source>
        <translation>Ausrichtung links</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="96"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="106"/>
        <source>Centered</source>
        <translation>Zentriert</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="148"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="119"/>
        <source>Right-Aligned</source>
        <translation>Ausrichtung rechts</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="122"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="132"/>
        <source>Justified</source>
        <translation>Beurteilt</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="145"/>
        <source>Text Color</source>
        <translation>Textfarbe</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="155"/>
        <source>Font Size</source>
        <translation>Schriftgröße</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="162"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="167"/>
        <source>7</source>
        <translation>7</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="172"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="177"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="182"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="187"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="192"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="197"/>
        <source>14</source>
        <translation>14</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="202"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="207"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="212"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="217"/>
        <source>22</source>
        <translation>22</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="222"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="227"/>
        <source>26</source>
        <translation>26</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="232"/>
        <source>28</source>
        <translation>28</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="237"/>
        <source>36</source>
        <translation>36</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="242"/>
        <source>42</source>
        <translation>42</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="247"/>
        <source>78</source>
        <translation>78</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="255"/>
        <source>Templates</source>
        <translation>Vorlagen</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="258"/>
        <source>T</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../forms/CustomizeDetailViewDlg.ui" line="298"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <location filename="../Database.cpp" line="96"/>
        <source>Never</source>
        <translation>Nie</translation>
    </message>
</context>
<context>
    <name>DatabaseSettingsDlg</name>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="25"/>
        <source>Database Settings</source>
        <translation>Datenbankeinstellungen</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="47"/>
        <source>Encryption</source>
        <translation>Verschlüsselung</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="56"/>
        <source>Algorithm:</source>
        <translation>Algorithmus:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="66"/>
        <source>Encryption Rounds:</source>
        <translation>Verschlüsselungsrunden:</translation>
    </message>
    <message>
        <location filename="../forms/DatabaseSettingsDlg.ui" line="84"/>
        <source>Calculate rounds for a 1-second delay on this computer</source>
        <translation>Berechne Runden für eine Sekunde Verzögerung auf diesem Computer</translation>
    </message>
</context>
<context>
    <name>DetailViewTemplate</name>
    <message>
        <location filename="../KpxConfig.cpp" line="258"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="259"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="260"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="261"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="262"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="263"/>
        <source>Creation</source>
        <translation>Erstellung</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="264"/>
        <source>Last Access</source>
        <translation>letzter Zugriff</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="265"/>
        <source>Last Modification</source>
        <translation>letzte Änderung</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="266"/>
        <source>Expiration</source>
        <translation>Ablauf</translation>
    </message>
    <message>
        <location filename="../KpxConfig.cpp" line="267"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
</context>
<context>
    <name>EditEntryDialog</name>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="14"/>
        <source>Edit Entry</source>
        <translation>Eintrag bearbeiten</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="152"/>
        <source>Username:</source>
        <translation>Benutzername:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="210"/>
        <source>Password Repet.:</source>
        <translation>Passwort Wdhlg.:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="139"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="178"/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="223"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="113"/>
        <source>Quality:</source>
        <translation>Qualität:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="165"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="236"/>
        <source>Expires:</source>
        <translation>Läuft ab:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="191"/>
        <source>Group:</source>
        <translation>Gruppe:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+A</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="350"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="462"/>
        <source>Icon:</source>
        <translation>Symbol:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>% Bit</source>
        <translation type="obsolete">% Bit</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="52"/>
        <source>Ge&amp;n.</source>
        <translation>Ge&amp;n.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+K</source>
        <translation type="obsolete">Alt+K</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="383"/>
        <source>Never</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="126"/>
        <source>Attachment:</source>
        <translation>Anhang:</translation>
    </message>
    <message>
        <location filename="../forms/EditEntryDlg.ui" line="419"/>
        <source>%1 Bit</source>
        <translation>%1 Bit</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="20"/>
        <source>Group Properties</source>
        <translation>Gruppen-Eigenschaften</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="41"/>
        <source>Title:</source>
        <translation>Titel:</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="34"/>
        <source>Icon:</source>
        <translation>Symbol:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="../forms/EditGroupDlg.ui" line="67"/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
</context>
<context>
    <name>ExpiredEntriesDialog</name>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="13"/>
        <source>Expired Entries</source>
        <translation>abgelaufene Einträge</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="38"/>
        <source>Double click on an entry to jump to it.</source>
        <translation>Doppelklick auf einen Eintrag, um zu diesem zu springen.</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="61"/>
        <source>Group</source>
        <translation>Guppe</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="66"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="71"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../forms/ExpiredEntriesDlg.ui" line="76"/>
        <source>Expired</source>
        <translation>Abgelaufen</translation>
    </message>
    <message>
        <location filename="../dialogs/ExpiredEntriesDlg.cpp" line="50"/>
        <source>Expired Entries in the Database</source>
        <translation>abgelaufene Einträge in der Datenbank</translation>
    </message>
</context>
<context>
    <name>Export_KeePassX_Xml</name>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML Dateien (*.xml)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_KeePassX_Xml.h" line="32"/>
        <source>KeePassX XML File</source>
        <translation>KeePassX XML Datei</translation>
    </message>
</context>
<context>
    <name>Export_Txt</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file (FileError=%1)</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden. (FileError=%1)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>All Files (*)</source>
        <translation>alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.cpp" line="38"/>
        <source>Text Files (*.txt)</source>
        <translation>Textdateien (*.txt)</translation>
    </message>
    <message>
        <location filename="../export/Export_Txt.h" line="31"/>
        <source>Text File</source>
        <translation>Textdatei</translation>
    </message>
</context>
<context>
    <name>ExporterBase</name>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Import File...</source>
        <translation type="obsolete">importiere Datei...</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="30"/>
        <source>Export Failed</source>
        <translation>Export felgeschlagen</translation>
    </message>
    <message>
        <location filename="../export/Export.cpp" line="26"/>
        <source>Export File...</source>
        <translation>exportiere Datei...</translation>
    </message>
</context>
<context>
    <name>FileErrors</name>
    <message>
        <location filename="../lib/tools.cpp" line="62"/>
        <source>No error occurred.</source>
        <translation>Kein Fehler aufgetreten.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="63"/>
        <source>An error occurred while reading from the file.</source>
        <translation>Es ist ein Fehler beim lesen der Datei aufgetreten.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="64"/>
        <source>An error occurred while writing to the file.</source>
        <translation>Es ist ein Fehler beim schreiben der Datei aufgetreten.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="65"/>
        <source>A fatal error occurred.</source>
        <translation>Ein fataler Fehler hat sich ereignet.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="66"/>
        <source>An resource error occurred.</source>
        <translation>Ein Resourcenfehler hat siche ereignet.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="67"/>
        <source>The file could not be opened.</source>
        <translation>Die Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="68"/>
        <source>The operation was aborted.</source>
        <translation>Die Operation wurde abgebrochen.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="69"/>
        <source>A timeout occurred.</source>
        <translation>Ein Zeitlauffehler hat sich ereignet.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="70"/>
        <source>An unspecified error occurred.</source>
        <translation>Ein unspezifizierter Fehler hat sich ereignet.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="71"/>
        <source>The file could not be removed.</source>
        <translation>Die Datei konnte nicht gelöscht werden.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="72"/>
        <source>The file could not be renamed.</source>
        <translation>Die Datei konnte nicht umbenannt werden.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="73"/>
        <source>The position in the file could not be changed.</source>
        <translation>Die Position in der Datei konnte nicht geändert werden.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="74"/>
        <source>The file could not be resized.</source>
        <translation>Die Größe der Datei konnte nicht geändert werden.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="75"/>
        <source>The file could not be accessed.</source>
        <translation>Auf die Datei konnte nicht zugegriffen werden.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="76"/>
        <source>The file could not be copied.</source>
        <translation>Die Datei konnte nicht kopiert werden.</translation>
    </message>
</context>
<context>
    <name>GenPwDlg</name>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="141"/>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="118"/>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="404"/>
        <source>Alt+M</source>
        <translation type="obsolete">Alt+M</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="95"/>
        <source>Alt+L</source>
        <translation type="obsolete">Alt+L</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="13"/>
        <source>Password Generator</source>
        <translation>Passwortgenerator</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Accep&amp;t</source>
        <translation type="obsolete">Annehmen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbre&amp;chen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="459"/>
        <source>Generate</source>
        <translation>Generieren</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="436"/>
        <source>New Password:</source>
        <translation>Neues Passwort:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="353"/>
        <source>Quality:</source>
        <translation>Qualität:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="303"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="123"/>
        <source>&amp;Upper Letters</source>
        <translation type="unfinished">&amp;Großbuchstaben:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="89"/>
        <source>&amp;Lower Letters</source>
        <translation type="unfinished">&amp;Kleinbuchstaben:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="106"/>
        <source>&amp;Numbers</source>
        <translation type="unfinished">&amp;Zahlen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="140"/>
        <source>&amp;Special Characters</source>
        <translation type="unfinished">&amp;Sonderzeichen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="145"/>
        <source>Minus</source>
        <translation type="obsolete">Minus</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>U&amp;nderline</source>
        <translation type="obsolete">Unterstrich</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>h&amp;igher ANSI-Characters</source>
        <translation type="obsolete">höhere ANSI-Zeichen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="151"/>
        <source>Use &amp;only following characters:</source>
        <translation type="unfinished">Nur folgende &amp;Zeichen benutzen:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="172"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="317"/>
        <source>Length:</source>
        <translation>Länge:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Use &quot;/dev/rando&amp;m&quot;</source>
        <translation type="obsolete">&apos;/dev/random&apos; benutzen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="56"/>
        <source>Use follo&amp;wing character groups:</source>
        <translation type="unfinished">Folgende Zei&amp;chengruppen nutzen:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="59"/>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="122"/>
        <source>White &amp;Spaces</source>
        <translation type="obsolete">Leerzeichen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="131"/>
        <source>Alt+S</source>
        <translation type="obsolete">Alt+S</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="380"/>
        <source>Enable entropy collection</source>
        <translation>aktiviere Entropie Sammlung</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="406"/>
        <source>Collect only once per session</source>
        <translation>Sammle nur einmal pro Sitzung</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="50"/>
        <source>Random</source>
        <translation>Zufall</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="99"/>
        <source>&amp;Underline</source>
        <translation>Unterstrich</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="116"/>
        <source>&amp;White Spaces</source>
        <translation>Leerzeichen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="133"/>
        <source>&amp;Minus</source>
        <translation>Minus</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="193"/>
        <source>Exclude look-alike characters</source>
        <translation>ähnlich aussehende Zeichen ausschliessen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="200"/>
        <source>Ensure that password contains characters from every group</source>
        <translation>Sicherstellen, dass das Passwort Zeichen ein jeder Gruppe enthält</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="216"/>
        <source>Pronounceable</source>
        <translation>ausprechbar</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="240"/>
        <source>Lower Letters</source>
        <translation>Kleinbuchstaben</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="247"/>
        <source>Upper Letters</source>
        <translation>Großbuchstaben</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="254"/>
        <source>Numbers</source>
        <translation>Nummern</translation>
    </message>
    <message>
        <location filename="../forms/PasswordGenDlg.ui" line="261"/>
        <source>Special Characters</source>
        <translation>Sonderzeichen</translation>
    </message>
</context>
<context>
    <name>Import_KWalletXml</name>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>XML Files (*.xml)</source>
        <translation>XML Dateien (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="25"/>
        <source>All Files (*)</source>
        <translation>alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Import Failed</source>
        <translation>importieren fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="38"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>ungültige XML-Daten (siehe stdout für Fehlerbeschreibung).</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="57"/>
        <source>Invalid XML file.</source>
        <translation>ungültige XML-Datei.</translation>
    </message>
    <message>
        <location filename="../import/Import_KWalletXml.cpp" line="45"/>
        <source>Document does not contain data.</source>
        <translation>Dokument enthält keine Daten.</translation>
    </message>
</context>
<context>
    <name>Import_KeePassX_Xml</name>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>KeePass XML Files (*.xml)</source>
        <translation>KeePass XML Datei (*.xml)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="27"/>
        <source>All Files (*)</source>
        <translation>alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Import Failed</source>
        <translation>importieren fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="34"/>
        <source>XML parsing error on line %1 column %2:
%3</source>
        <translation>XML Phrasenfehler in Zeile %1 Spalte %2:
%3</translation>
    </message>
    <message>
        <location filename="../import/Import_KeePassX_Xml.cpp" line="53"/>
        <source>Parsing error: File is no valid KeePassX XML file.</source>
        <translation>Phrasenfehler: Datei ist keine gültige PeePassX XML Datei.</translation>
    </message>
</context>
<context>
    <name>Import_PwManager</name>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>PwManager Files (*.pwm)</source>
        <translation>PwManager Dateien (*.pwm)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="29"/>
        <source>All Files (*)</source>
        <translation>alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Import Failed</source>
        <translation>importieren fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="40"/>
        <source>File is empty.</source>
        <translation>Datei ist leer.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="48"/>
        <source>File is no valid PwManager file.</source>
        <translation>Datei ist keine gültige PwManager-Datei.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="51"/>
        <source>Unsupported file version.</source>
        <translation>Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="57"/>
        <source>Unsupported hash algorithm.</source>
        <translation>Nicht unterstützter Hash-Algorithmus.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="60"/>
        <source>Unsupported encryption algorithm.</source>
        <translation>Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="67"/>
        <source>Compressed files are not supported yet.</source>
        <translation>Komprimierte Dateien werden noch nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="91"/>
        <source>Wrong password.</source>
        <translation>falsches Passwort</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="103"/>
        <source>File is damaged (hash test failed).</source>
        <translation>Datei ist beschädigt (Hash-Test fehlgeschlagen).</translation>
    </message>
    <message>
        <location filename="../import/Import_PwManager.cpp" line="109"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation>ungültige XML-Daten (siehe stdout für Fehlerbeschreibung)</translation>
    </message>
</context>
<context>
    <name>ImporterBase</name>
    <message>
        <location filename="../import/Import.cpp" line="26"/>
        <source>Import File...</source>
        <translation>importiere Datei...</translation>
    </message>
    <message>
        <location filename="../import/Import.cpp" line="30"/>
        <source>Import Failed</source>
        <translation>importieren fehlgeschlagen</translation>
    </message>
</context>
<context>
    <name>Kdb3Database</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="524"/>
        <source>Could not open file.</source>
        <translation>Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="540"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation>unerwartete Dateigrößen (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="557"/>
        <source>Wrong Signature</source>
        <translation>falsche Signatur</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="562"/>
        <source>Unsupported File Version.</source>
        <translation>Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="571"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation>Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="606"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation>Entschlüsselung fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="632"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation>Hash-Test fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="717"/>
        <source>Invalid group tree.</source>
        <translation>Gruppenbaum ungültig.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="933"/>
        <source>Key file is empty.</source>
        <translation>Schlüsseldatei ist leer.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1313"/>
        <source>The database must contain at least one group.</source>
        <translation>Die Datenbank muss mindestens eine Gruppe enthalten.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1295"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Datei konnte nicht zum Schreiben geöffnent werden.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="711"/>
        <source>Unexpected error: Offset is out of range.</source>
        <translation>unerwarteter Fehler: Offset ist auserhalb der Reichweite.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="598"/>
        <source>Unable to initalize the twofish algorithm.</source>
        <translation type="unfinished">Der Twofish-Algorithmus ist nicht initialisierbar.</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1333"/>
        <source>The database has been opened read-only.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Kdb3Database::EntryHandle</name>
    <message>
        <location filename="../Kdb3Database.cpp" line="1177"/>
        <source>Bytes</source>
        <translation type="unfinished">Bytes</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1185"/>
        <source>KiB</source>
        <translation type="unfinished">KiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1191"/>
        <source>MiB</source>
        <translation type="unfinished">MiB</translation>
    </message>
    <message>
        <location filename="../Kdb3Database.cpp" line="1196"/>
        <source>GiB</source>
        <translation type="unfinished">GiB</translation>
    </message>
</context>
<context>
    <name>KeepassEntryView</name>
    <message>
        <location filename="../lib/EntryView.cpp" line="477"/>
        <source>Title</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="479"/>
        <source>Username</source>
        <translation>Benutzername</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="481"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="483"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="485"/>
        <source>Comments</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="487"/>
        <source>Expires</source>
        <translation>Läuft ab</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="489"/>
        <source>Creation</source>
        <translation>Erstellung</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="491"/>
        <source>Last Change</source>
        <translation>Letzte Änderung</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="493"/>
        <source>Last Access</source>
        <translation>Letzter Zugriff</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="495"/>
        <source>Attachment</source>
        <translation>Anhang</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>%1 items</source>
        <translation type="obsolete">%1 Elemente</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="150"/>
        <source>Delete?</source>
        <translation>löschen?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="497"/>
        <source>Group</source>
        <translation>Gruppe</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>At least one group must exist before adding an entry.</source>
        <translation>mindestens eine Gruppe muss existieren, bevor ein Eintrag hinzugefügt werden kann.</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="258"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="147"/>
        <source>Are you sure you want to delete this entry?</source>
        <translation>Sind Sie sicher, dass Sie diesen Eintrag löschen wollen?</translation>
    </message>
    <message>
        <location filename="../lib/EntryView.cpp" line="149"/>
        <source>Are you sure you want to delete these %1 entries?</source>
        <translation>Sind Sie sicher, dass Sie diese Einträge %1 löschen wollen?</translation>
    </message>
</context>
<context>
    <name>KeepassGroupView</name>
    <message>
        <location filename="../lib/GroupView.cpp" line="58"/>
        <source>Search Results</source>
        <translation>Suchergebnisse</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Groups</source>
        <translation type="obsolete">Gruppen</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="89"/>
        <source>Delete?</source>
        <translation>löschen?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="86"/>
        <source>Are you sure you want to delete this group, all it&apos;s child groups and all their entries?</source>
        <translation type="obsolete">Sind Sie sicher, dass Sie diese Gruppe und alle Untergruppen swie die Einträge löschen wollen?</translation>
    </message>
    <message>
        <location filename="../lib/GroupView.cpp" line="90"/>
        <source>Are you sure you want to delete this group, all its child groups and all their entries?</source>
        <translation>Sind Sie sicher, dass Sie diese Gruppe, die enthaltenden Untergruppen und all deren Einträge löschen wollen?</translation>
    </message>
</context>
<context>
    <name>KeepassMainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="357"/>
        <source>Ctrl+O</source>
        <translation>Strg+O</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="359"/>
        <source>Ctrl+S</source>
        <translation>Strg+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Ctrl+G</source>
        <translation>Strg+G</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>Ctrl+C</source>
        <translation>Strg+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="364"/>
        <source>Ctrl+B</source>
        <translation>Strg+B</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="365"/>
        <source>Ctrl+U</source>
        <translation>Strg+U</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="367"/>
        <source>Ctrl+Y</source>
        <translation>Strg+Y</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="368"/>
        <source>Ctrl+E</source>
        <translation>Strg+E</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="369"/>
        <source>Ctrl+D</source>
        <translation>Strg+D</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="370"/>
        <source>Ctrl+K</source>
        <translation>Strg+K</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="371"/>
        <source>Ctrl+F</source>
        <translation>Strg+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="358"/>
        <source>Ctrl+W</source>
        <translation>Strg+W</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="378"/>
        <source>Shift+Ctrl+S</source>
        <translation>Umschalt+Strg+S</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="379"/>
        <source>Shift+Ctrl+F</source>
        <translation>Umschalt+Strg+F</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>The following error occured while opening the database:
%1</source>
        <translation type="obsolete">Beim Öffnen der Datenbank ist folgender Fehler aufgetreten:
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="516"/>
        <source>Save modified file?</source>
        <translation>Geändete Datei speichern?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>The current file was modified. Do you want
to save the changes?</source>
        <translation type="obsolete">Die aktuelle Datei wurde verändert. Möchten Sie
die Änderungen speichern?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&lt;B&gt;Group: &lt;/B&gt;%1  &lt;B&gt;Title: &lt;/B&gt;%2  &lt;B&gt;Username: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Password: &lt;/B&gt;%5  &lt;B&gt;Creation: &lt;/B&gt;%6  &lt;B&gt;Last Change: &lt;/B&gt;%7  &lt;B&gt;LastAccess: &lt;/B&gt;%8  &lt;B&gt;Expires: &lt;/B&gt;%9</source>
        <translation type="obsolete">&lt;B&gt;Gruppe: &lt;/B&gt;%1  &lt;B&gt;Titel: &lt;/B&gt;%2  &lt;B&gt;Benutzername: &lt;/B&gt;%3  &lt;B&gt;URL: &lt;/B&gt;&lt;a href=%4&gt;%4&lt;/a&gt;  &lt;B&gt;Passwort: &lt;/B&gt;%5  &lt;B&gt;Erstellung: &lt;/B&gt;%6  &lt;B&gt;Letzte Änderung: &lt;/B&gt;%7  &lt;B&gt;Letzter Zugriff: &lt;/B&gt;%8  &lt;B&gt;Läuft ab: &lt;/B&gt;%9</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="855"/>
        <source>Clone Entry</source>
        <translation>Eintrag duplizieren</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="857"/>
        <source>Delete Entry</source>
        <translation>Eintrag löschen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="870"/>
        <source>Clone Entries</source>
        <translation>Einträge duplizieren</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="872"/>
        <source>Delete Entries</source>
        <translation>Einträge löschen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File could not be saved.
%1</source>
        <translation type="obsolete">Datei konnte nicht gespeichert werden.
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Database As...</source>
        <translation type="obsolete">Datenbank speichern unter...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1420"/>
        <source>Ready</source>
        <translation>Bereit</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>[new]</source>
        <translation type="obsolete">[neu]</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="603"/>
        <source>Open Database...</source>
        <translation>Datenbank öffnen...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1423"/>
        <source>Loading Database...</source>
        <translation>Lade Datenbank...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1426"/>
        <source>Loading Failed</source>
        <translation>Laden fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not create key file. The following error occured:
%1</source>
        <translation type="obsolete">Datei konnte nicht angelegt werden. Der folgende Fehler trat auf:
%1</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Export To...</source>
        <translation type="obsolete">Exportieren nach...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX [new]</source>
        <translation type="obsolete">KeePassX [neu]</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in Import_PwManager::importFile()()</source>
        <translation type="obsolete">Unbekannter Fehler in Import_PwManager::importFile()()</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in Import_KWalletXml::importFile()</source>
        <translation type="obsolete">Unbekannter Fehler in Import_KWalletXml::importFile()</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown error in PwDatabase::openDatabase()</source>
        <translation type="obsolete">Unbekannter Fehler in PwDatabase::openDatabase()</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="375"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX</source>
        <translation type="obsolete">KeePassX</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="477"/>
        <source>Unknown error while loading database.</source>
        <translation>Unbekannter Fehler während des öffnens der Datenbank.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>KeePass Databases (*.kdb)</source>
        <translation>KeePass Datenbank (*.kdb)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>All Files (*)</source>
        <translation>alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>Save Database...</source>
        <translation>speichere Datenbank...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <source>1 Month</source>
        <translation>1 Monat</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="745"/>
        <source>%1 Months</source>
        <translation>%1 Monate</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="751"/>
        <source>1 Year</source>
        <translation>1 Jahr</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="753"/>
        <source>%1 Years</source>
        <translation>%1 Jahre</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="759"/>
        <source>1 Day</source>
        <translation>1 Tag</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="761"/>
        <source>%1 Days</source>
        <translation>%1 Tage</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="765"/>
        <source>less than 1 day</source>
        <translation>weniger als einen Tag</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1323"/>
        <source>Locked</source>
        <translation>gesperrt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1338"/>
        <source>Unlocked</source>
        <translation>entsperrt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>Ctrl+L</source>
        <translation>Strg+L</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>Ctrl+Q</source>
        <translation>Strg+Q</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="499"/>
        <source>The database file does not exist.</source>
        <translation>Die Datenbankdatei existiert nicht.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1111"/>
        <source>new</source>
        <translation>neu</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="729"/>
        <source>Expired</source>
        <translation>abgelaufen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1324"/>
        <source>Un&amp;lock Workspace</source>
        <translation>entsperre Arbeitsbereich</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1339"/>
        <source>&amp;Lock Workspace</source>
        <translation>sperre Arbeitsbereich</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="479"/>
        <source>The following error occured while opening the database:</source>
        <translation>Der folgende Fehler hat sich beim öffnen der Datenbank ereignet:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="912"/>
        <source>File could not be saved.</source>
        <translation>Datei konnte nicht gespeichert werden.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1105"/>
        <source>Show &amp;Toolbar</source>
        <translation>Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="356"/>
        <source>Ctrl+N</source>
        <translation>Strg+N</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="372"/>
        <source>Ctrl+P</source>
        <translation>Strg+P</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="373"/>
        <source>Ctrl+X</source>
        <translation>Strg+X</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="366"/>
        <source>Ctrl+I</source>
        <translation>Strg+I</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="407"/>
        <source>Database locked</source>
        <translation>Datenbank gesperrt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="411"/>
        <source>The database you are trying to open is locked.
This means that either someone else has opened the file or KeePassX crashed last time it opened the database.

Do you want to open it anyway?</source>
        <translation>Die Datenbank, die Sie versuchen zu öffnen ist gesperrt.
Dies bedeutet, das jemand anders die Datei geöffnet hat oder KeePassX das letzte mal abgestürtzt ist, als es die Datenbank geöffnet war.
Soll die Datenbank auf jeden Fall geöffnet werden?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="445"/>
        <source>Couldn&apos;t create database lock file.</source>
        <translation type="obsolete">Konnte Datenbaksperrdatei nicht erstellen.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="517"/>
        <source>The current file was modified.
Do you want to save the changes?</source>
        <translation>Die aktuelle Datei wurde geändert. Möchten Sie die Änderungen speichern?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="535"/>
        <source>Couldn&apos;t remove database lock file.</source>
        <translation>Konnte Datenbaksperrdatei nicht entfernen.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="414"/>
        <source>Open read-only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1429"/>
        <source>Couldn&apos;t create lock file. Opening the database read-only.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../lib/tools.cpp" line="140"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="147"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation>Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="146"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/MainWindow.ui" line="17"/>
        <source>KeePassX</source>
        <translation>KeePassX</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File</source>
        <translation type="obsolete">Datei</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Import from...</source>
        <translation type="obsolete">Importieren aus...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>View</source>
        <translation type="obsolete">Ansicht</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Columns</source>
        <translation type="obsolete">Spalten</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Extras</source>
        <translation type="obsolete">Extras</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Help</source>
        <translation type="obsolete">Hilfe</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>New Database...</source>
        <translation type="obsolete">Neue Datenbank...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Open Database...</source>
        <translation type="obsolete">Datenbank öffnen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Close Database</source>
        <translation type="obsolete">Datenbank schließen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Database</source>
        <translation type="obsolete">Datenbank speichern</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Database As...</source>
        <translation type="obsolete">Datenbank speichern unter...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Database Settings...</source>
        <translation type="obsolete">Datenbankeinstellungen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Change Master Key...</source>
        <translation type="obsolete">Hauptschlüssel ändern...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Exit</source>
        <translation type="obsolete">Beenden</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>PwManager File (*.pwm)</source>
        <translation type="obsolete">PwManager-Datei (*.pwm)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KWallet XML-File (*.xml)</source>
        <translation type="obsolete">KWallet XML-Datei (*.xml)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="589"/>
        <source>Add New Group...</source>
        <translation>neue Gruppe hinzufügen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Edit Group...</source>
        <translation type="obsolete">Gruppe bearbeiten...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete Group</source>
        <translation type="obsolete">Gruppe löschen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copy Password to Clipboard</source>
        <translation type="obsolete">Passwort in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Copy Username to Clipboard</source>
        <translation type="obsolete">Benutzername in Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Open URL</source>
        <translation type="obsolete">URL öffnen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Save Attachment As...</source>
        <translation type="obsolete">Anhang speichern unter...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add New Entry...</source>
        <translation type="obsolete">Neuen Eintrag hinzufügen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>View/Edit Entry...</source>
        <translation type="obsolete">Eintrag anzeigen/bearbeiten...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Delete Entry</source>
        <translation type="obsolete">Eintrag löschen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clone Entry</source>
        <translation type="obsolete">Eintrag duplizieren</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search In Database...</source>
        <translation type="obsolete">In Datenbank suchen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search in this group...</source>
        <translation type="obsolete">In dieser Gruppe suchen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Toolbar</source>
        <translation type="obsolete">Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Entry Details</source>
        <translation type="obsolete">Eintragsdetails anzeigen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hide Usernames</source>
        <translation type="obsolete">Benutzernamen verbergen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hide Passwords</source>
        <translation type="obsolete">Passwörter verbergen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Title</source>
        <translation type="obsolete">Titel</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Username</source>
        <translation type="obsolete">Benutzername</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>URL</source>
        <translation type="obsolete">URL</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password</source>
        <translation type="obsolete">Passwort</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Comment</source>
        <translation type="obsolete">Kommentar</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Expires</source>
        <translation type="obsolete">Läuft ab</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Creation</source>
        <translation type="obsolete">Erstellung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last Change</source>
        <translation type="obsolete">Letzte Änderung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Last Access</source>
        <translation type="obsolete">Letzter Zugriff</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Attachment</source>
        <translation type="obsolete">Anhang</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Settings...</source>
        <translation type="obsolete">Einstellungen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>About...</source>
        <translation type="obsolete">Über...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Edit</source>
        <translation type="obsolete">Bearbeiten</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Show Statusbar</source>
        <translation type="obsolete">Statusleiste anzeigen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Export to...</source>
        <translation type="obsolete">Exportieren nach...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>KeePassX Handbook...</source>
        <translation type="obsolete">KeePassX Handbuch...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Plain Text (*.txt)</source>
        <translation type="obsolete">Klartext (*.txt)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="506"/>
        <source>Hide</source>
        <translation>verbergen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Perform AutoType</source>
        <translation type="obsolete">AutoType ausführen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Type Here</source>
        <translation type="obsolete">Type Here</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Toolbar Icon Size</source>
        <translation type="obsolete">Symbolleistengröße</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="191"/>
        <source>&amp;View</source>
        <translation type="unfinished">&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="136"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="145"/>
        <source>&amp;Import from...</source>
        <translation>&amp;importieren aus...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="140"/>
        <source>&amp;Export to...</source>
        <translation>&amp;exportieren nach...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Edit</source>
        <translation type="obsolete">&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="230"/>
        <source>E&amp;xtras</source>
        <translation>E&amp;xtras</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="128"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="540"/>
        <source>&amp;New Database...</source>
        <translation>&amp;neue Datenbank...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="262"/>
        <source>&amp;Open Database...</source>
        <translation>Datenbank &amp;öffnen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="267"/>
        <source>&amp;Close Database</source>
        <translation>Datenbank s&amp;chließen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="272"/>
        <source>&amp;Save Database</source>
        <translation>Datenbank &amp;speichern</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="277"/>
        <source>Save Database &amp;As...</source>
        <translation>D&amp;atenbank speichern unter...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="282"/>
        <source>&amp;Database Settings...</source>
        <translation>&amp;Datenbankeinstellungen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="287"/>
        <source>Change &amp;Master Key...</source>
        <translation>Hauptschlüssel &amp;ändern...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>E&amp;xit</source>
        <translation type="obsolete">Beend&amp;en</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="474"/>
        <source>&amp;Settings...</source>
        <translation>Ein&amp;stellungen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="482"/>
        <source>&amp;About...</source>
        <translation>&amp;Über...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="498"/>
        <source>&amp;KeePassX Handbook...</source>
        <translation>&amp;KeePassX Handbuch...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="534"/>
        <source>Standard KeePass Single User Database (*.kdb)</source>
        <translation type="obsolete">Standart KeePass Einzelbenutzer Datenbank (*.kdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="539"/>
        <source>Advanced KeePassX Database (*.kxdb)</source>
        <translation type="obsolete">fortgeschrittene KeePassX Datenbank (*.kxdb)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="566"/>
        <source>Recycle Bin...</source>
        <translation>Mülleimer...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="55"/>
        <source>Groups</source>
        <translation>Gruppen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="292"/>
        <source>&amp;Lock Workspace</source>
        <translation>sperre Arbeitsbereich</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="150"/>
        <source>&amp;Bookmarks</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="195"/>
        <source>Toolbar &amp;Icon Size</source>
        <translation>Werkezugleisten Symbol Größe</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="203"/>
        <source>&amp;Columns</source>
        <translation>Spalten</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="257"/>
        <source>&amp;Manage Bookmarks...</source>
        <translation>Lesezeichen bearbeiten...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="297"/>
        <source>&amp;Quit</source>
        <translation>schliessen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="286"/>
        <source>&amp;Add New Group...</source>
        <translation type="obsolete">Gruppe neu hinzufügen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="310"/>
        <source>&amp;Edit Group...</source>
        <translation type="unfinished">Gruppe b&amp;earbeiten...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="315"/>
        <source>&amp;Delete Group</source>
        <translation type="unfinished">Gruppe &amp;löschen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="320"/>
        <source>Copy Password &amp;to Clipboard</source>
        <translation type="unfinished">Pass&amp;wort in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="325"/>
        <source>Copy &amp;Username to Clipboard</source>
        <translation type="unfinished">Be&amp;nutzername in die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="330"/>
        <source>&amp;Open URL</source>
        <translation>Url öffnen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="335"/>
        <source>&amp;Save Attachment As...</source>
        <translation>Anhang speichern als...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="340"/>
        <source>Add &amp;New Entry...</source>
        <translation>Eintrag &amp;neu hinzufügen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="345"/>
        <source>&amp;View/Edit Entry...</source>
        <translation>Eintrag zeigen/&amp;bearbeiten...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="350"/>
        <source>De&amp;lete Entry</source>
        <translation>Eintrag &amp;löschen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="355"/>
        <source>&amp;Clone Entry</source>
        <translation>Eintrag duplizieren</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="360"/>
        <source>Search &amp;in Database...</source>
        <translation>Suche &amp;in Datenbank...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="365"/>
        <source>Search in this &amp;Group...</source>
        <translation>Suche in dieser &amp;Gruppe...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="373"/>
        <source>Show &amp;Entry Details</source>
        <translation>&amp;Eintragsdetails zeigen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="381"/>
        <source>Hide &amp;Usernames</source>
        <translation>Ben&amp;utzername verstecken</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="389"/>
        <source>Hide &amp;Passwords</source>
        <translation>&amp;Passwort verstecken</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="397"/>
        <source>&amp;Title</source>
        <translation>&amp;Titel</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="405"/>
        <source>User&amp;name</source>
        <translation type="unfinished">Benutzer&amp;name</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="413"/>
        <source>&amp;URL</source>
        <translation type="unfinished">&amp;URL</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="421"/>
        <source>&amp;Password</source>
        <translation type="unfinished">&amp;Passwort</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="429"/>
        <source>&amp;Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="437"/>
        <source>E&amp;xpires</source>
        <translation>Ablauf</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="445"/>
        <source>C&amp;reation</source>
        <translation>Erstellung</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="453"/>
        <source>&amp;Last Change</source>
        <translation>letzte Änderung</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="461"/>
        <source>Last &amp;Access</source>
        <translation>letzter Zugriff</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="469"/>
        <source>A&amp;ttachment</source>
        <translation>Anhang</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="493"/>
        <source>Show &amp;Statusbar</source>
        <translation>&amp;Statuszeile zeigen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="511"/>
        <source>&amp;Perform AutoType</source>
        <translation>Auto-Type ausführen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="519"/>
        <source>&amp;16x16</source>
        <translation>&amp;16x16</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="527"/>
        <source>&amp;22x22</source>
        <translation>&amp;22x22</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="535"/>
        <source>2&amp;8x28</source>
        <translation>2&amp;8x28</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="545"/>
        <source>&amp;Password Generator...</source>
        <translation>&amp;Passwortgenerator...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="556"/>
        <source>&amp;Group (search results only)</source>
        <translation>&amp;Gruppe (nur Suchergebnisse)</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="561"/>
        <source>Show &amp;Expired Entries...</source>
        <translation>Zeige abgelaufene &amp;Einträge...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="574"/>
        <source>&amp;Add Bookmark...</source>
        <translation>Lesezeichen hinzufügen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="579"/>
        <source>Bookmark &amp;this Database...</source>
        <translation>speichere diese Da&amp;tenbank als Lesezeichen...</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="305"/>
        <source>&amp;Add New Subgroup...</source>
        <translation>neue Untergruppe hinzufügen</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="584"/>
        <source>Copy URL to Clipboard</source>
        <translation>kopiere URL in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="172"/>
        <source>&amp;Entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="240"/>
        <source>&amp;Groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/MainWindow.ui" line="594"/>
        <source>Sort groups</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ManageBookmarksDlg</name>
    <message>
        <location filename="../forms/ManageBookmarksDlg.ui" line="19"/>
        <source>Manage Bookmarks</source>
        <translation>verwalte Lesezeichen</translation>
    </message>
</context>
<context>
    <name>PasswordDialog</name>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="32"/>
        <source>Enter Master Key</source>
        <translation>Hauptschlüssel eingeben</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="35"/>
        <source>Set Master Key</source>
        <translation>Hauptschlüssel festlegen</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="38"/>
        <source>Change Master Key</source>
        <translation>Hauptschlüssel ändern</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="42"/>
        <source>Database Key</source>
        <translation>Datenbankschlüssel</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="132"/>
        <source>Last File</source>
        <translation>letzte Datei</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="178"/>
        <source>Select a Key File</source>
        <translation>Schlüsseldatei wählen</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>All Files (*)</source>
        <translation>alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="342"/>
        <source>Key Files (*.key)</source>
        <translation>Schlüsseldateien (*.key)</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="206"/>
        <source>Please enter a Password or select a key file.</source>
        <translation>Bitte geben Sie ein Passwort ein oder wählen
Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="211"/>
        <source>Please enter a Password.</source>
        <translation>Bitte geben Sie ein Passwort ein.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="216"/>
        <source>Please provide a key file.</source>
        <translation>Bitte stellen Sie eine Schlüsseldatei zur Verfügung.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="262"/>
        <source>%1:
No such file or directory.</source>
        <translation>%1:
Datei oder Verzeichnis nicht gefunden.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="228"/>
        <source>The selected key file or directory is not readable.</source>
        <translation>Die ausgewählte Schlüsseldatei oder das Verzeichnis konnte nicht gelesen werden.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="247"/>
        <source>The given directory does not contain any key files.</source>
        <translation>Das angegebene Verzeichnis enthält keine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="253"/>
        <source>The given directory contains more then one key files.
Please specify the key file directly.</source>
        <translation>Das angegebene Verzeichnis enthält meher als eine Schlüsseldatei.
Bitte die Schlüsseldatei direkt auswählen.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="266"/>
        <source>%1:
File is not readable.</source>
        <translation>%1:
Datei ist nicht lesbar.</translation>
    </message>
    <message>
        <location filename="../dialogs/PasswordDlg.cpp" line="340"/>
        <source>Create Key File...</source>
        <translation>erzeuge Schlüsseldatei...</translation>
    </message>
</context>
<context>
    <name>PasswordDlg</name>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="100"/>
        <source>Enter a Password and/or choose a key file.</source>
        <translation>Geben Sie ein Passwort ein oder wählen Sie eine Schlüsseldatei.</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="113"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="137"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file or directory:</source>
        <translation type="obsolete">Schlüsseldatei oder Datenträger:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="191"/>
        <source>&amp;Browse...</source>
        <translation>durchsuchen...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="194"/>
        <source>Alt+B</source>
        <translation type="obsolete">Alt+B</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Use Password AND Key File</source>
        <translation type="obsolete">Passwort UND Schlüsseldatei verwenden</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Exit</source>
        <translation type="obsolete">Beenden</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Password Repet.:</source>
        <translation type="obsolete">Passwort Wdhlg.:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="73"/>
        <source>Last File</source>
        <translation>letzte Datei</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="163"/>
        <source>Key File:</source>
        <translation>Schlüsseldatei:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="200"/>
        <source>Generate Key File...</source>
        <translation>generiere Schlüsseldatei...</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="232"/>
        <source>Please repeat your password:</source>
        <translation>bitte geben Sie Ihr Passwort nochmal ein:</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="250"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="../forms/PasswordDlg.ui" line="277"/>
        <source>Passwords are not equal.</source>
        <translation>Passwörter sind nicht gleich.</translation>
    </message>
</context>
<context>
    <name>PwDatabase</name>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Error</source>
        <translation type="obsolete">Unbekannter Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Unerwartete Dateigrößen (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong Signature</source>
        <translation type="obsolete">Falsche Signatur</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>AES-Init Failed</source>
        <translation type="obsolete">AES Initialisierung fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Hash-Test fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open key file.</source>
        <translation type="obsolete">Schlüsseldatei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Key file could not be written.</source>
        <translation type="obsolete">Schlüsseldatei konnte nicht geschrieben werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Datei konnte nicht zum Schreiben geöffnent werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Entschlüsselung fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="" line="0"/>
        <source>Warning</source>
        <translation type="obsolete">Warnung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not save configuration file.
Make sure you have write access to &apos;~/.keepass&apos;.</source>
        <translation type="obsolete">Konfigurationsdatei konnte nicht gespeichert werden.
Stellen Sie sicher, dass Sie Schreibzugriff auf &apos;~/.keepass&apos; haben.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>OK</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File &apos;%1&apos; could not be found.</source>
        <translation type="obsolete">Datei &apos;%1&apos; konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File not found.</source>
        <translation type="obsolete">Datei nicht gefunden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File is no valid PwManager file.</source>
        <translation type="obsolete">Datei ist keine gültige PwManager-Datei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported file version.</source>
        <translation type="obsolete">Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported hash algorithm.</source>
        <translation type="obsolete">Nicht unterstützter Hash-Algorithmus.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported encryption algorithm.</source>
        <translation type="obsolete">Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Compressed files are not supported yet.</source>
        <translation type="obsolete">Komprimierte Dateien werden noch nicht unterstützt.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong password.</source>
        <translation type="obsolete">Falsches Passwort.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File is damaged (hash test failed).</source>
        <translation type="obsolete">Datei ist beschädigt (Hash-Test fehlgeschlagen).</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid XML data (see stdout for details).</source>
        <translation type="obsolete">Ungültige XML-Daten (siehe stdout für Fehlerbeschreibung).</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>File is empty.</source>
        <translation type="obsolete">Datei ist leer.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid XML file (see stdout for details).</source>
        <translation type="obsolete">Ungültige XML-Daten (siehe stdout für Fehlerbeschreibung).</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid XML file.</source>
        <translation type="obsolete">Ungültige XML-Datei.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Document does not contain data.</source>
        <translation type="obsolete">Dokument enthält keine Daten.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Warning:</source>
        <translation type="obsolete">Warnung:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Invalid RGB color value.
</source>
        <translation type="obsolete">Ungültiger RGB-Farbwert.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Never</source>
        <translation type="obsolete">Nie</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="120"/>
        <source>Could not locate library file.</source>
        <translation type="obsolete">Konnte Bibliotek nicht lokalisieren.</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../dialogs/SearchDlg.cpp" line="51"/>
        <source>Search</source>
        <translation>Suche</translation>
    </message>
</context>
<context>
    <name>Search_Dlg</name>
    <message>
        <location filename="../forms/SearchDlg.ui" line="133"/>
        <source>Alt+T</source>
        <translation type="obsolete">Alt+T</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="123"/>
        <source>Alt+U</source>
        <translation type="obsolete">Alt+U</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="81"/>
        <source>A&amp;nhang</source>
        <translation>A&amp;nhang</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="93"/>
        <source>Alt+N</source>
        <translation type="obsolete">Alt+N</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="83"/>
        <source>Alt+W</source>
        <translation type="obsolete">Alt+W</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="151"/>
        <source>Alt+C</source>
        <translation type="obsolete">Alt+C</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="19"/>
        <source>Search...</source>
        <translation>suche...</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="150"/>
        <source>Search For:</source>
        <translation>suchen nach:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="131"/>
        <source>Regular E&amp;xpression</source>
        <translation>regulärer Ausdruck</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="161"/>
        <source>Alt+X</source>
        <translation type="obsolete">Alt+X</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="124"/>
        <source>&amp;Case Sensitive</source>
        <translation>Groß- und Kleinschreibung beachten</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="65"/>
        <source>Include:</source>
        <translation>einbeziehen:</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="109"/>
        <source>&amp;Titles</source>
        <translation>Titel</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="102"/>
        <source>&amp;Usernames</source>
        <translation>Benutzernamen</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="95"/>
        <source>C&amp;omments</source>
        <translation>Kommentare</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="113"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="88"/>
        <source>U&amp;RLs</source>
        <translation>URLs</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="103"/>
        <source>Alt+R</source>
        <translation type="obsolete">Alt+R</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="74"/>
        <source>Pass&amp;words</source>
        <translation>Passwörter</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Search</source>
        <translation type="obsolete">Suchen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Clo&amp;se</source>
        <translation type="obsolete">Schließen</translation>
    </message>
    <message>
        <location filename="../forms/SearchDlg.ui" line="138"/>
        <source>Include Subgroups (recursive)</source>
        <translation>Untergruppen einbeziehen (rekursiv)</translation>
    </message>
</context>
<context>
    <name>SelectIconDlg</name>
    <message>
        <location filename="../forms/SelectIconDlg.ui" line="19"/>
        <source>Icon Selection</source>
        <translation>Symbolauswahl</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Add Custom Icon...</source>
        <translation type="obsolete">Eigenes Symbol hinzufügen...</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Pick</source>
        <translation type="obsolete">Wählen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message encoding="UTF-8">
        <location filename="../forms/SettingsDlg.ui" line="194"/>
        <source>Alt+Ö</source>
        <translation type="obsolete">Alt+Ö</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="13"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="956"/>
        <source>Clear clipboard after:</source>
        <translation>Zwischenablage löschen nach:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Seconds</source>
        <translation type="obsolete">Sekunden</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Sh&amp;ow passwords in plain text by default</source>
        <translation type="obsolete">Passwort standardmäßig im Klartext anzeigen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="943"/>
        <source>Alt+O</source>
        <translation type="obsolete">Alt+O</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="309"/>
        <source>Appea&amp;rance</source>
        <translation type="obsolete">Erscheinungsbild</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="474"/>
        <source>Banner Color</source>
        <translation>Bannerfarbe</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="488"/>
        <source>Text Color:</source>
        <translation>Textfarbe</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="617"/>
        <source>Change...</source>
        <translation>ändern...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="542"/>
        <source>Color 2:</source>
        <translation>Farbe 2:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="667"/>
        <source>C&amp;hange...</source>
        <translation>ändern...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="679"/>
        <source>Alt+H</source>
        <translation type="obsolete">Alt+H</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="680"/>
        <source>Color 1:</source>
        <translation>Farbe 1:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Expand group tree when opening a database</source>
        <translation type="obsolete">Gruppenbaum beim Öffnen aufklappen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Other</source>
        <translation type="obsolete">Sonstiges</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Remember last opend file</source>
        <translation type="obsolete">Zuletzt geöffnete Datei beim Starten öffnen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Browser Command:</source>
        <translation type="obsolete">Browser Befehl:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Securi&amp;ty</source>
        <translation type="obsolete">Sicherheit</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="708"/>
        <source>Alternating Row Colors</source>
        <translation>abwechselnde Zeilenfarben</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1107"/>
        <source>Browse...</source>
        <translation>durchsuchen...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="219"/>
        <source>Remember last key type and location</source>
        <translation>Art und Ort des Schlüssels der letzten Datenbank merken</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Mounting Root:</source>
        <translation type="obsolete">Datenträgerwurzelverzeichnis:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="191"/>
        <source>Remember last opened file</source>
        <translation>Zuletzt geöffnete Datei merken</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="54"/>
        <source>General</source>
        <translation type="obsolete">Allgemein</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="122"/>
        <source>Show system tray icon</source>
        <translation>zeige System-Tray Symbol</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="180"/>
        <source>Minimize to tray when clicking the main window&apos;s close button</source>
        <translation>minimiere zum tray, wenn das Hauptfenser geschlossen wird</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="295"/>
        <source>Save recent directories of file dialogs</source>
        <translation>speichere Verzeichnisse der letzten geöffneten Dateien</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="749"/>
        <source>Group tree at start-up:</source>
        <translation>gruppiere Baum beim Start:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="756"/>
        <source>Restore last state</source>
        <translation>letzte Einstellungen wiederherstellen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="763"/>
        <source>Expand all items</source>
        <translation>alle Unter-/Gruppen expandieren</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="770"/>
        <source>Do not expand any item</source>
        <translation>Unter-/Gruppen nicht expandieren</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="94"/>
        <source>Security</source>
        <translation>Sicherheit</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="928"/>
        <source>Edit Entry Dialog</source>
        <translation>Eintragbearbeitungs Dialog</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="864"/>
        <source>Desktop Integration</source>
        <translation type="obsolete">Benutzeroberflächenintegration</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1308"/>
        <source>Plug-Ins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1317"/>
        <source>None</source>
        <translation>nichts</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1324"/>
        <source>Gnome Desktop Integration (Gtk 2.x)</source>
        <translation>Gnome Oberflächenintegration (Gtk 2.x)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1331"/>
        <source>KDE 4 Desktop Integration</source>
        <translation>KDE 4 Oberflächenintegration</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1346"/>
        <source>You need to restart the program before the changes take effect.</source>
        <translation>Sie müssen das Program neu starten bevor die Änderungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1366"/>
        <source>Configure...</source>
        <translation>konfigurieren...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="99"/>
        <source>Advanced</source>
        <translation>Fortgeschritten</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="318"/>
        <source>Clear History Now</source>
        <translation>lösche Historie sofort</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="340"/>
        <source>Always ask before deleting entries or groups</source>
        <translation>Frage immer vor dem löschen von Einträgen und Gruppen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="720"/>
        <source>Customize Entry Detail View...</source>
        <translation>Anpassung der Eintragsdetailansicht</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="827"/>
        <source>Features</source>
        <translation type="obsolete">Eigenschaften</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1260"/>
        <source>You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.</source>
        <translation>Sie können einige Einstellung von KeePassX nach Ihren Wünschen abschalten, um das Erscheinungsbild gering zu halten.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1270"/>
        <source>Bookmarks</source>
        <translation>Lesezeichen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1126"/>
        <source>Auto-Type Fine Tuning</source>
        <translation>Auto-Type Feineinstellungen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1135"/>
        <source>Time between the activation of an auto-type action by the user and the first simulated key stroke.</source>
        <translation>Zeit zwischen der Aktivierung einer auto-type Aktion durch den Benutzer bis zum ersten simulierten Tastenanschlag.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1184"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1148"/>
        <source>Pre-Gap:</source>
        <translation type="unfinished">Pause vor erstem Tastenanschlag:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1171"/>
        <source>Key Stroke Delay:</source>
        <translation>Tasteneingabeverzögerung:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1181"/>
        <source>Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.</source>
        <translation>Verzögerung zwischen zwei simulierten Tasteneingaben. Erhöhen Sie dies, wenn Auto-Type unregelmäßig Zeichen auslässt.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1100"/>
        <source>The directory where storage devices like CDs and memory sticks are normally mounted.</source>
        <translation>Das Verzeichnis, indem normalerweise Speichermedien (Festplatten, CDs, DVDs, USB-Sticks) eingehängt werden.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1090"/>
        <source>Media Root:</source>
        <translation>Hauptmedienverzeichnis:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1116"/>
        <source>Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.</source>
        <translation>Aktivieren Sie dies, wenn Sie Ihre Lesezeichen und die zu letzt geöffnete Datei unabhänig von deren absoluten Pfaden verwenden wollen. Dies ist natürlich am sinnvollsten, wenn Sie KeePassX flexibel einsetzten und sich dadurch die Einhängepunkte im Dauteisystem ändern.</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1119"/>
        <source>Save relative paths (bookmarks and last file)</source>
        <translation>speichere relative Pfade (Lesezeichen und letzte Datei)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="150"/>
        <source>Minimize to tray instead of taskbar</source>
        <translation>minimiere nach tray anstatt zur Arbeitsleiste</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="249"/>
        <source>Start minimized</source>
        <translation>starte minimiert</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="279"/>
        <source>Start locked</source>
        <translation>starte gesperrt</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="994"/>
        <source>Lock workspace when minimizing the main window</source>
        <translation>sperre Arbeisbereich, wenn das Hauptfenster minimiert wird</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1199"/>
        <source>Global Auto-Type Shortcut:</source>
        <translation>globale Auto-Type Tastenzuordnung</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1066"/>
        <source>Custom Browser Command</source>
        <translation>anwenderspezifischer Browserbefehl</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1078"/>
        <source>Browse</source>
        <translation>durchsuchen...</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="435"/>
        <source>Automatically save database on exit and workspace locking</source>
        <translation>automaitsches speichern der Datenbank beim Beenden und beim Sperren des Arbeitsbereiches</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="919"/>
        <source>Show plain text passwords in:</source>
        <translation>zeige Klartextpasswörter in:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="935"/>
        <source>Database Key Dialog</source>
        <translation>Datenbank Schlüssel Dialog</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1032"/>
        <source>seconds</source>
        <translation>Sekunden</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1003"/>
        <source>Lock database after inactivity of</source>
        <translation>sperre Datenbank bei Inaktivität nach:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="1228"/>
        <source>Use entries&apos; title to match the window for Global Auto-Type</source>
        <translation>Vergleiche den Titel der Einträge mit Fenter für globale Auto-Type</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="74"/>
        <source>General (1)</source>
        <translation>Allgemein (1)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="79"/>
        <source>General (2)</source>
        <translation>Allgemein (2)</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="84"/>
        <source>Appearance</source>
        <translation>Aussehen</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="89"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="372"/>
        <source>Save backups of modified entries into the &apos;Backup&apos; group</source>
        <translation>speichere Sicherheitskopieen der bearbeiteten Einträge in der Gruppe &quot;Backup&quot;</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="400"/>
        <source>Delete backup entries older than:</source>
        <translation>lösche Sicherheiskopieen der Einträge, die älter sind als:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="426"/>
        <source>days</source>
        <translation>Tage</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="442"/>
        <source>Automatically save database after every change</source>
        <translation>speichere Datenbank automatisch nach jeder Änderung</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="837"/>
        <source>System Language</source>
        <translation type="obsolete">Systemsprache</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="842"/>
        <source>English</source>
        <translation type="obsolete">Englisch</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="845"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../forms/SettingsDlg.ui" line="852"/>
        <source>Author:</source>
        <translation>Author:</translation>
    </message>
</context>
<context>
    <name>ShortcutWidget</name>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="71"/>
        <source>Ctrl</source>
        <translation>Strg (Ctrl)</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="73"/>
        <source>Shift</source>
        <translation>Hochstellen</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="75"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="77"/>
        <source>AltGr</source>
        <translation>AltGr</translation>
    </message>
    <message>
        <location filename="../lib/ShortcutWidget.cpp" line="79"/>
        <source>Win</source>
        <translation>Win</translation>
    </message>
</context>
<context>
    <name>SimplePasswordDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>...</source>
        <translation type="obsolete">...</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="31"/>
        <source>Enter your Password</source>
        <translation>Ihr Passwort eingeben</translation>
    </message>
    <message>
        <location filename="../forms/SimplePasswordDlg.ui" line="51"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
<context>
    <name>StandardDatabase</name>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file.</source>
        <translation type="obsolete">Datei konnte nicht geöffnet werden.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unexpected file size (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</source>
        <translation type="obsolete">Unerwartete Dateigrößen (DB_TOTAL_SIZE &lt; DB_HEADER_SIZE)</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Wrong Signature</source>
        <translation type="obsolete">Falsche Signatur</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unsupported File Version.</source>
        <translation type="obsolete">Nicht unterstützte Dateiversion.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Unknown Encryption Algorithm.</source>
        <translation type="obsolete">Unbekannter bzw. nicht unterstüzter Verschlüsselungsalgorithmus.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Decryption failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Entschlüsselung fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Hash test failed.
The key is wrong or the file is damaged.</source>
        <translation type="obsolete">Hash-Test fehlgeschlagen.
Der Schlüssel ist falsch oder die Datei ist beschädigt.</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Could not open file for writing.</source>
        <translation type="obsolete">Datei konnte nicht zum Schreiben geöffnent werden.</translation>
    </message>
</context>
<context>
    <name>TargetWindowDlg</name>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="13"/>
        <source>Auto-Type: Select Target Window</source>
        <translation>Auto-Type: Zielfenster auswählen</translation>
    </message>
    <message>
        <location filename="../forms/TargetWindowDlg.ui" line="19"/>
        <source>To specify the target window, either select an existing currently-opened window
from the drop-down list, or enter the window title manually:</source>
        <translation>Um das Zielfenster auszuwählen, entweder ein bereits geöffnetes Fenster
aus der Auswähllistes auswählen, oder den Fenstertitel manuell eingeben:</translation>
    </message>
</context>
<context>
    <name>Translation</name>
    <message>
        <location filename="../lib/tools.cpp" line="352"/>
        <source>$TRANSLATION_AUTHOR</source>
        <translation type="unfinished">Tarek Saidi</translation>
    </message>
    <message>
        <location filename="../dialogs/AboutDlg.cpp" line="35"/>
        <source>$TRANSLATION_AUTHOR_EMAIL</source>
        <comment>Here you can enter your email or homepage if you want.</comment>
        <translation type="unfinished">tarek.saidi@arcor.de</translation>
    </message>
    <message>
        <location filename="../lib/tools.cpp" line="351"/>
        <source>$LANGUAGE_NAME</source>
        <comment>Insert your language name in the format: English (United States)</comment>
        <translation type="unfinished">Deutsch (Deutschland)</translation>
    </message>
</context>
<context>
    <name>TrashCanDialog</name>
    <message>
        <location filename="" line="0"/>
        <source>Title</source>
        <translation type="obsolete">Titel</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Username</source>
        <translation type="obsolete">Benutzername</translation>
    </message>
</context>
<context>
    <name>WorkspaceLockedWidget</name>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="13"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="47"/>
        <source>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;The workspace is locked.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Verdana&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Der Arbeitsbereich ist gesperrt.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="92"/>
        <source>Unlock</source>
        <translation>entsperren</translation>
    </message>
    <message>
        <location filename="../forms/WorkspaceLockedWidget.ui" line="99"/>
        <source>Close Database</source>
        <translation>Datenbank schließen</translation>
    </message>
</context>
<context>
    <name>dbsettingdlg_base</name>
    <message>
        <location filename="" line="0"/>
        <source>Database Settings</source>
        <translation type="obsolete">Datenbankeinstellungen</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Encryption</source>
        <translation type="obsolete">Verschlüsselung</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Algorithm:</source>
        <translation type="obsolete">Algorithmus:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>Encryption Rounds:</source>
        <translation type="obsolete">Verschlüsselungsrunden:</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>O&amp;K</source>
        <translation type="obsolete">O&amp;K</translation>
    </message>
    <message>
        <location filename="" line="0"/>
        <source>&amp;Cancel</source>
        <translation type="obsolete">Abbrechen</translation>
    </message>
</context>
</TS>
