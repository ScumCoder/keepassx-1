----------------------------
    0.4.0 (2009-03-25)
----------------------------
- Added pronounceable password generator
- Added action "Copy URL to Clipboard"
- Added "Tools" button to EditEntryDlg: Window List and Auto-Type sequence
- Improved Auto-Typing: ability to type all unicode characters
- Added option to save database after every change
- Associate KeePassX with *.kdb files on Linux and Mac OS
- Display warning when opening a database that is already opened
- Distinguish between adding groups and subgroups (Bug #2194057)
- Store list of preferred characters in password generator (Bug #2432748)
- Implemented backup feature
- Don't include entries from "Backup" group in search results
- Added menu action to sort groups (Bug #2268672)
- Clear Klipper history when clearing clipboard
- Use serif font for all password fields (Bug #2594868)
- Redesigned the Settings dialog and added ability to select language
- Added Finnish, Gallican, Italian, Norwegian and Turkish translations
- Cache and protect MasterKey - speeds up saving database
- Added 2 new password generator options
- Changed default config filename to "config.ini"
- XDG compliant config location on Linux
- Fixed: Crash on saving a database (Bug #1955677)
- Fixed: Unnamed Database saved as ".kdb" (Bugs #2109972, #2118340)
- Fixed: Date of Modification isn't updated (Bugs #2108658, #2121768)
- Fixed: Cannot open DB from KeePassX 0.2.2 (Bug #2535569)
- Fixed: Predefined expire times don't work (Bug #2109987)
- Fixed: Sorting isn't consistent (Bug #2108655)
- Fixed: KeepassX fails to lock itself after Ctrl-V (Bug #2106604)
- Fixed: Position of main window not properly restored (Bugs #2090649, #2371738, #2336064)
- Fixed: No password generated using list of very special characters (Bug #2230887)
- Fixed: Crash if minimize to systray with locked workbench on Mac OS (Bug #2482531)
- Fixed: Exports aren't sorted consistently (Bug #2108661)
- Fixed: Sudden exit on bookmarking a new (not saved) file (Bug #2599802)
- Fixed: -min parameter is sometimes ignored (Debian Bug #514414)
- Fixed: Cloned entries are not immediately sorted
- Fixed: Crash when moving entry to another group (Bug #2644545)
- Fixed: Different qm files in different paths, no overruling (Bug #2657158)
- Improved the initialization of the Windows RNG and fallback random number source (Bug #2091784)
- Improved Mac OS bundle information (Bugs #2096992, #1921260)
- Improve tab order in many dialogs (Bug #2130397)
- Added nostrip qmake option

----------------------------
    0.3.4 (2008-11-09)
----------------------------
- fixed crash when auto-typing special characters (Bug #2111588)
- only allow plain text in comment field

----------------------------
    0.3.3 (2008-08-11)
----------------------------
- fixed error when opening twofish encrypted databases (Bug #2025075)

----------------------------
    0.3.2 (2008-07-20)
----------------------------
- add default groups when creating a database
- improved the auto-type dialog (keyboard only usage, visual selection feedback)
- automatically try to unlock db when global auto-type key has been pressed
- fixed password encoding problem (Bug #1687864)
- fixed sorting of date columns (Bugs #1861726, #1922311)
- fixed problem when restoring window after auto-type (Bug #1978861)
- don't use entries in 'Backup' group for global auto-type (Bug #1915664)
- hide contents of search field while database is locked (Bug #1923554)
- fixed: custom entry icons are not saved (Bug #1995561)
- added new icons introduced in KeePass 1.11
- improved format of date strings (Bug #1932394)
- added missing menu accelerators (Bug #1955304)
- fixed: line breaks are ignored in Entry Detail View (Bug #1942362)
- master key transformations (rounds) are now computed in two threads (from KeePass 1.11)
- added ability to measure the number of rounds that can be calculated in 1 second
- increase and randomize the default key transformation number
- print help if parsing the arguments fails
- disable precompiled header on FreeBSD by default to fix compilation error (Bug #1943446)

---------------
    0.3.1
---------------
- made key/password dialog more user friendly
- added option for automatic database locking on inactivity
- native file dialogs are used again under MacOS X and Windows
- program accepts Qt command line switches like "-style" again
- parameter "-cfg" now accepts relative paths (Bug #1825446)
- fixed invalid entries in .desktop file (Bug #1906875)
- fixed potential compilation problem in AES implementation (Bug #1905810)
- fixed crash when re-arranging groups (Bug #1754998)
- fixed size problems of some dialogs
- files are not longer truncated when saving fails (Bug #1648616)
- improved seeding of the random number generator

---------------
    0.3.0a
---------------
- fixed bug which prevented MacOS X bundle from starting (Bug #1906517)
- fixed error message about missing license file when opening about dialog (Bug #1906696)

---------------
    0.3.0
---------------
- many bug fixes
- global Auto-Type
- fully customizable HTML based detail view
- better structured settings dialog
- secure random number generator based on 'Yarrow' including an optional entropy collector
- new dialog to view expiered entries
- calendar widget to edit expiration dates in a more confortable way
- restructured password generator
- much better appearance under MacOS X
- fine tuning options for auto-type
- system tray icon
- workspace locking

---------------
    0.2.2
---------------
-fixed crash when parsing config file under Win32
-fixed loss of entry icons when saving a database which was not created with KeePassX (no KPX_CUSTOM_ICONS metastream)
-introduces new metastream format for custom icons
-removed all old Qt3 support dependecies
-QtNetwork and QtSql are not longer requiered (when using Qt 4.1.3 or later)
-implemented correct UUID management for entries
-added delay of 0.3s before performing auto-type
-metastreams now get valid group IDs (KeePass/Win compatibility)
-fixed drawing errors when performing drag and drop operations in group view
-when there is no translation installed for the system's country preference but one for the same language the program will use it now
-when canceling the file dialog for the opening of an existing database a already openend database will not longer be closed
-same for the creation of a new database
-alpha blending for banner icons
-new standard banner icon
-MacOS packages: all Qt libraries are now included in the application bundle (extra installation is not longer necessary)

---------------
    0.2.1
---------------
-added AutoType feature (experimental!)
-added custom icons feature
-new command line option for manual language selection (-lang <LOCALE-CODE>)
-when saving an attachment the original filename is adopted by the file dialog
-fixed strange sorting behavior of entries (Bug #7083)
-sorting by dates now works as expected
-the 'Expires' column and the detailed entry view now also show the string 'never' 
 for entries which don't expire
-entry view now gets updated after changing the column setup
-added menu entry to change the size of the toolbar icons

---------------
    0.2.0
---------------
-ported whole application from Qt3 to Qt4
(better performance, less memory usage, ready for KDE4)
-improved Mac OS X support
-added Drag&Drop support
-multiple seclection mode for entries
-improved loading performance for large databases
-faster in-memory encryption
-search field in toolbar now works
-mainwindow size, splitter position and column sizes are restored at start-up
-added option for alternating row colors
-improved key/password dialog
-removed language dialog - program now uses system's default language
-loading translation files for Qt (e.g. file dialogs)
-added text export function
-added option "Never" for expire dates.
-fixed problem with hex. key files
-fixed problem with damaged file attachments after various entry operations
-fixed segmentation fault when using new icons with higher index
-fixed error when saving empty databases